import { AfterViewInit, Component, forwardRef, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { AppComponent } from '../../app.component';
import { ScrollPanel } from 'primeng/scrollpanel';
import { AuthService } from '../../core/services/auth/auth.service';
import { Role } from '../../core/enums/role.enum';

@Component({
    selector: 'admin-menu',
    templateUrl: './menu.component.html'
})
// @ts-ignore
export class MenuComponent implements OnInit, AfterViewInit {

    // @ts-ignore
    @Input() reset: boolean;

    model: any[];

    // @ts-ignore
    @ViewChild('scrollPanel') layoutMenuScrollerViewChild: ScrollPanel;

    // @ts-ignore
    constructor(@Inject(forwardRef(() => AppComponent)) public app: AppComponent, private authService: AuthService) {

    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.layoutMenuScrollerViewChild.moveBar();
        }, 100);
    }

    ngOnInit() {
        this.model = [
            {
                label: 'MENU.Dashboard',
                visible: this.authService.isUserInRole(Role.USER),
                icon: 'pi pi-home', routerLink: ['/']
            },
            {
                label: 'MENU.USERS.Title',
                icon: 'pi pi-users',
                visible: this.authService.isUserInRole(Role.ADMIN),
                routerLink: ['/users/crud'],
            },
            {
                label: 'MENU.ACTIVITIES.Title',
                icon: 'pi pi-book',
                routerLink: ['/activities'],
                visible: this.authService.isUserInRole(Role.USER),
                items: [
                    {
                        label: 'MENU.ACTIVITIES.Adoptions',
                        routerLink: ['/activities/adoptions'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.Castrations',
                        routerLink: ['/activities/castrations'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.VisitClinic',
                        routerLink: ['/activities/visit-clinic'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.BuySupplies',
                        routerLink: ['/activities/buy-supplies'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.PickupKeepers',
                        routerLink: ['/activities/pickup-keepers'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.Rescues',
                        routerLink: ['/activities/rescues'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.NewMembers',
                        routerLink: ['/activities/new-members'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.Meetings',
                        routerLink: ['/activities/meetings'],
                        visible: this.authService.isUserInRole(Role.USER)
                    },
                    {
                        label: 'MENU.ACTIVITIES.Others',
                        routerLink: ['/activities/others'],
                        visible: this.authService.isUserInRole(Role.USER)
                    }
                ]
            },
            {
                label: 'MENU.ASSOCIATION.PersonsCrud',
                icon: 'pi pi-id-card',
                routerLink: ['/association/persons'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.ASSOCIATION.ColoniesCrud',
                icon: 'pi pi-globe',
                routerLink: ['/association/colonies'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.ASSOCIATION.TrapsCrud',
                icon: 'pi pi-table',
                routerLink: ['/association/traps'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.ASSOCIATION.AnimalsCrud',
                icon: 'pi pi-github',
                routerLink: ['/association/animals'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.ASSOCIATION.PlacesCrud',
                icon: 'pi pi-shopping-cart',
                routerLink: ['/association/places'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.ASSOCIATION.Reports',
                icon: 'pi pi-file-o',
                routerLink: ['/association/reports'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.ACCOUNTING.Title',
                icon: 'pi pi-money-bill',
                routerLink: ['/accounting/account-notes'],
                visible: this.authService.isUserInRole(Role.USER)
            },
            {
                label: 'MENU.SETTINGS.Title',
                icon: 'pi pi-cog',
                visible: this.authService.isUserInRole(Role.ADMIN),
                items: [
                    {
                        label: 'MENU.SETTINGS.SystemCrud',
                        routerLink: ['/settings/system-crud'],
                        visible: this.authService.isUserInRole(Role.ADMIN)
                    },
                    {
                        label: 'MENU.SETTINGS.Tags',
                        routerLink: ['/settings/tags-crud'],
                        visible: this.authService.isUserInRole(Role.ADMIN)
                    }
                ]
            }
        ];
    }
}
