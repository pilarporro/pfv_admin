import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppComponent } from '../../app.component';
import { SelectItem } from 'primeng/api';
import { take, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { interval } from 'rxjs';
import { Unsubscribable } from '../../core/utils/unsubscribable';
import { User } from '../../core/model/persons/user.model';
import { AuthService } from '../../core/services/auth/auth.service';
import { UserService } from '../../core/services/persons/user.service';

@Component({
  selector: 'admin-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent extends Unsubscribable implements OnInit {

  username: string;
  realm: string;
  listOfZones: SelectItem[] = [];
  selectedZone: SelectItem = {label: '', value: ''};
  displayProfile: boolean;
  user: User;

  constructor(public app: AppComponent,
              private authService: AuthService,
              private userService: UserService,
              public router: Router,
              public translate: TranslateService) {
    super();
  }

  ngOnInit() {
    this.user = this.authService.getLoggedUser();
  }

  logout() {
    this.authService.signout();
    this.router.navigate(['auth', 'signin']);
  }

  changeLanguage(lang: string) {
    localStorage.setItem('lang', lang);
    this.translate.use(lang);
  }

  showUserInfo(): void {
    this.displayProfile = true;
  }

  openHelp(): void {

  }
}
