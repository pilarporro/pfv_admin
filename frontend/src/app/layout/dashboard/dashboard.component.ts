import { Component, OnInit } from '@angular/core';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { Unsubscribable } from '../../core/utils/unsubscribable';
import { DataLoaderResponse } from '../../core/model/data-loader/load-profiles-response.model';
import { User } from '../../core/model/persons/user.model';
import { AuthService } from '../../core/services/auth/auth.service';
import { DataLoaderService } from '../../core/services/data-loader/data-loader.service';
import { Utils } from '../../core/utils/utils';
import { GlobalMessageService } from '../../core/services/global-messages/global-message.service';
import { FileUpload } from 'primeng/fileupload';
import { HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse } from '@angular/common/http';
import { NotificationType } from '../../core/model/global-messages/notification-type.model';
import { FileSelectEvent } from '../../core/model/file/file-select-event.model';
import { Activity } from '../../core/model/actitivities/activity.model';
import { ActivityListComponent } from '../../activities/activity-list/activity-list.component';
import { ActivityType } from '../../core/enums/activity-type.enum';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends Unsubscribable implements OnInit {
  form: FormGroup;
  dataLoaderResponse: DataLoaderResponse;
  user: User;
  loading: boolean = false;

  uploadFile: any;

  dialogNewActivityIsShown: boolean = false;
  newActivity: Activity;

  constructor(public authService: AuthService, private messageService: GlobalMessageService, public dataLoaderService: DataLoaderService) {
    super();
    this.user = this.authService.getLoggedUser();

    this.createDataLoaderForm();
  }

  createDataLoaderForm() {
    this.form = new FormGroup({
      type: new FormControl(),
      value: new FormControl(),
      path: new FormControl()
    });
  }

  ngOnInit() {
    if (this.user.admin) {
      this.form.get('type').setValue('');
      this.form.get('value').setValue('');
      this.form.get('path').setValue('');
    }
  }

  loadData() {
    this.dataLoaderResponse = undefined;
    if (this.form.valid) {
      // console.log(this.form.getRawValue());
      this.dataLoaderService.load(this.form.getRawValue())
        .pipe(take(1))
        .subscribe(response => {
          this.dataLoaderResponse = response;
        });
    } else {
      Utils.updateValidity(this.form);
    }
  }

  selectFile(event: FileSelectEvent, fileUpload: FileUpload) {
    if (fileUpload.msgs.length > 0) {
      console.log(fileUpload.msgs);
    } else {
      this.uploadFile = null;
    }
  }

  handleUpload(event: { files: File[] }, fileUpload: FileUpload) {
    this.uploadFile = {
      file: event.files[0],
      progress: 0
    };
    this.dataLoaderService.uploadAccountNotes(this.uploadFile.file).pipe(
        takeUntil(this.ngUnsubscribe$),
        finalize(() => {
          fileUpload.clear();
        })
    ).subscribe((httpEvent: HttpEvent<any>) => {
      if (httpEvent.type === HttpEventType.UploadProgress) {
        this.handleProgress(httpEvent);
      } else if (httpEvent instanceof HttpResponse) {
        this.handleSuccess(httpEvent, fileUpload);
      }
    });
  }

  private handleProgress(event: HttpProgressEvent) {
    const percentDone = Math.round((event.loaded * 100) / event.total);
    this.uploadFile.progress = percentDone;
  }

  private handleSuccess(event: HttpResponse<DataLoaderResponse>, fileUpload: FileUpload) {
    this.dataLoaderResponse = event.body;
    this.uploadFile = undefined;
    this.messageService.showNotificationWithText(
        NotificationType.SUCCESS, '',
        `Total: ${event.body.total}`
        );
  }

  showNewCastration() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.CASTRATION, this.authService));
  }

  showNewVisitClinic() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.VISIT_CLINIC, this.authService));
  }

  showNewAdoption() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.ADOPTION, this.authService));
  }

  showNewOther() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.OTHER, this.authService));
  }

  showNewBuySupplies() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.BUY_SUPPLIES, this.authService));
  }

  showNewRescue() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.RESCUE, this.authService));
  }

  showNewMember() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.NEW_MEMBER, this.authService));
  }

  showNewPickUpKeeper() {
    this.showNewActivity(ActivityListComponent.newActivity(ActivityType.PICKUP_KEEPER, this.authService));
  }

  showNewActivity(activity: Activity) {
    this.newActivity = activity;
    this.dialogNewActivityIsShown = true;
  }

  newActivityCreated(itemCreated: Activity) {

  }
}
