import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { BreadcrumbService } from '../../core/services/common/breadcrumb.service';
import { Breadcrumb } from '../../core/model/base/breadcrumb.model';

@Component({
  selector: 'admin-breadcrumb',
  templateUrl: './breadcrumb.component.html'
})
export class BreadcrumbComponent {

  constructor(private breadcrumbService: BreadcrumbService) {
  }

  get items$(): Observable<Breadcrumb[]> {
    return this.breadcrumbService.breadCrumbs$;
  }

}
