import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingListComponent } from './core/components/setting-list/setting-list.component';
import { CardModule } from 'primeng/card';
import { DataViewModule } from 'primeng/dataview';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { RippleModule } from 'primeng/ripple';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { SidebarModule } from 'primeng/sidebar';
import { InputNumberModule } from 'primeng/inputnumber';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { InplaceModule } from 'primeng/inplace';

import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CarouselModule } from 'primeng/carousel';
import { ChartModule } from 'primeng/chart';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { GalleriaModule } from 'primeng/galleria';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { LightboxModule } from 'primeng/lightbox';
import { ListboxModule } from 'primeng/listbox';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { OrderListModule } from 'primeng/orderlist';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { PanelMenuModule } from 'primeng/panelmenu';
import { PasswordModule } from 'primeng/password';
import { PickListModule } from 'primeng/picklist';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SlideMenuModule } from 'primeng/slidemenu';
import { SliderModule } from 'primeng/slider';
import { SplitButtonModule } from 'primeng/splitbutton';
import { StepsModule } from 'primeng/steps';
import { TabMenuModule } from 'primeng/tabmenu';
import { TabViewModule } from 'primeng/tabview';
import { TerminalModule } from 'primeng/terminal';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { EnumPipe } from './core/pipes/enum.pipe';
import { DateAndTimeFormatPipe } from './core/pipes/date-and-time-format.pipe';
import { DateFormatPipe } from './core/pipes/date-format.pipe';
import { TimeFormatPipe } from './core/pipes/time-format.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageLoaderComponent } from './core/components/image-loader/image-loader.component';
import { LabelListDropdownComponent } from './core/components/forms/label-list-dropdown/label-list-dropdown.component';
import { ListDropdownComponent } from './core/components/forms/list-dropdown/list-dropdown.component';
import { MunicipalityAreaComponent } from './core/components/forms/municipality-area/municipality-area.component';
import { InputSpinnerComponent } from './core/components/forms/input-spinner/input-spinner.component';
import { EditorComponent } from './core/components/forms/editor/editor.component';
import { DeleteTooltipComponent } from './core/components/delete-tooltip/delete-tooltip.component';
import { GlobalMessageComponent } from './core/components/global-message/global-message.component';
import { CheckboxComponent } from './core/components/forms/checkbox/checkbox.component';
import { BlockUiOnLoadingRequestComponent } from './core/components/block-ui-on-loading-request/block-ui-on-loading-request.component';
import { TricheckboxComponent } from './core/components/forms/tricheckbox/tricheckbox.component';
import { ChipsComponent } from './core/components/forms/chips/chips.component';
import { TableComponent } from './core/components/table/table.component';
import { DateComponent } from './core/components/forms/date/date.component';
import { InputTextComponent } from './core/components/forms/input-text/input-text.component';
import { TextAreaComponent } from './core/components/forms/textarea/textarea.component';
import { ValidationMessageComponent } from './core/components/forms/validation-message/validation-message.component';
import { MessageComponent } from './core/components/message/message.component';
import { InfoPanelComponent } from './core/components/info-panel/info-panel.component';
import { BaseDateComponent } from './core/components/forms/base-date/base-date.component';
import { ConfirmDialogComponent } from './core/components/confirm-dialog/confirm-dialog.component';
import { FileUploadComponent } from './core/components/forms/file-upload/file-upload.component';
import { SelectButtonComponent } from './core/components/forms/select-button/select-button.component';
import { MunicipalityComponent } from './core/components/forms/municipality/municipality.component';
import { EnumDropdownComponent } from './core/components/forms/enum-dropdown/enum-dropdown.component';
import { LabelEnumDropdownComponent } from './core/components/forms/label-enum-dropdown/label-enum-dropdown.component';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { UserDetailDataComponent } from './users/crud/user-detail/user-detail-data/user-detail-data.component';
import { UserDetailComponent } from './users/crud/user-detail/user-detail.component';
import { SpinnerModule } from 'primeng/spinner';
import { PersonPickerComponent } from './core/components/forms/person-picker/person-picker.component';
import { PhotoEditorComponent } from './association/persons/person-detail/photo-editor/photo-editor.component';
import { AnimalPickerComponent } from './core/components/forms/animal-picker/animal-picker.component';
import { PersonDetailDataComponent } from './association/persons/person-detail/person-detail-data/person-detail-data.component';
import { PersonDetailComponent } from './association/persons/person-detail/person-detail.component';
import { AnimalDetailDataComponent } from './association/animals/animal-detail/animal-detail-data/animal-detail-data.component';
import { AnimalDetailComponent } from './association/animals/animal-detail/animal-detail.component';
import { AnimalDetailPhotosComponent } from './association/animals/animal-detail/animal-detail-photos/animal-detail-photos.component';
import { PersonDetailPhotosComponent } from './association/persons/person-detail/person-detail-photos/person-detail-photos.component';
import { PlacePickerComponent } from './core/components/forms/place-picker/place-picker.component';
import { PlaceDetailComponent } from './association/places/place-detail/place-detail.component';
import { PlaceDetailDataComponent } from './association/places/place-detail/place-detail-data/place-detail-data.component';
import { ActivityPickerComponent } from './core/components/forms/activity-picker/activity-picker.component';
import { ActivityDetailComponent } from './activities/activity-detail/activity-detail.component';
import { ActivityDetailDataComponent } from './activities/activity-detail/activity-detail-data/activity-detail-data.component';
import { ActivityDetailPhotosComponent } from './activities/activity-detail/activity-detail-photos/activity-detail-photos.component';
import { ColonyPickerComponent } from './core/components/forms/colony-picker/colony-picker.component';
import { ColonyListComponent } from './association/colonies/colony-list/colony-list.component';
import { ColonyDetailComponent } from './association/colonies/colony-detail/colony-detail.component';
import { ColonyDetailDataComponent } from './association/colonies/colony-detail/colony-detail-data/colony-detail-data.component';
import { ColonyDetailPhotosComponent } from './association/colonies/colony-detail/colony-detail-photos/colony-detail-photos.component';
import { AgmCoreModule } from '@agm/core';
import { CommonLocationDataComponent } from './core/components/common-location-data/common-location-data.component';
import { GooglePlaceDirective } from './core/components/google-places-autocomplete/google-places-autocomplete.directive';
import { BankMovementFormatPipe } from './core/pipes/bank-movement-format.pipe';
import { DateMemberFormatPipe } from './core/pipes/date-member-format.pipe';

export const ANGULAR_MODULES: any[] = [
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
];

const APP_COMPONENTS: any[] = [
    GooglePlaceDirective,
    CommonLocationDataComponent,
    ColonyDetailComponent,
    ColonyDetailDataComponent,
    ColonyDetailPhotosComponent,
    ColonyPickerComponent,
    ActivityDetailComponent,
    ActivityDetailDataComponent,
    ActivityDetailPhotosComponent,
    PersonDetailDataComponent,
    PersonDetailComponent,
    AnimalDetailDataComponent,
    AnimalDetailComponent,
    PlaceDetailComponent,
    PlaceDetailDataComponent,
    AnimalDetailPhotosComponent,
    PersonDetailPhotosComponent,
    PhotoEditorComponent,
    PersonPickerComponent,
    PlacePickerComponent,
    AnimalPickerComponent,
    ActivityPickerComponent,
    UserDetailDataComponent,
    UserDetailComponent,
    SettingListComponent,
    ChipsComponent,
    TricheckboxComponent,
    BlockUiOnLoadingRequestComponent,
    TableComponent,
    EnumDropdownComponent,
    DateComponent,
    InputTextComponent,
    TextAreaComponent,
    ValidationMessageComponent,
    MessageComponent,
    InfoPanelComponent,
    BaseDateComponent,
    ConfirmDialogComponent,
    GlobalMessageComponent,
    LabelEnumDropdownComponent,
    FileUploadComponent,
    SelectButtonComponent,
    MunicipalityComponent,
    CheckboxComponent,
    GlobalMessageComponent,
    DeleteTooltipComponent,
    EditorComponent,
    InputSpinnerComponent,
    MunicipalityAreaComponent,
    ListDropdownComponent,
    LabelListDropdownComponent,
    ImageLoaderComponent
];

const APP_PIPES: any[] = [
    EnumPipe,
    DateAndTimeFormatPipe,
    DateFormatPipe,
    BankMovementFormatPipe,
    DateMemberFormatPipe,
    TimeFormatPipe
];

export const PRIME_MODULES = [
    SpinnerModule,
    ImageCropperModule,
    AccordionModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CardModule,
    CarouselModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule,
    FullCalendarModule,
    GalleriaModule,
    InplaceModule,
    InputNumberModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OrderListModule,
    OrganizationChartModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    RippleModule,
    ScrollPanelModule,
    SelectButtonModule,
    SidebarModule,
    SlideMenuModule,
    SliderModule,
    SplitButtonModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    TriStateCheckboxModule,
    VirtualScrollerModule,
    BlockUIModule,
    ProgressSpinnerModule
];

@NgModule({
    imports: [
        ANGULAR_MODULES,
        PRIME_MODULES,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyANmKmOL9h_dOzqtk5saOr-isuqvD97y_U',
            libraries: ['places']
        })
    ],
    declarations: [
        APP_COMPONENTS,
        APP_PIPES
    ],
    exports: [
        ANGULAR_MODULES,
        PRIME_MODULES,
        APP_COMPONENTS,
        APP_PIPES,
        TranslateModule
    ]
})
// @ts-ignore
export class AppCommonModule {

}
