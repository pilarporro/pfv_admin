import { User } from '../persons/user.model';

export interface AuthResponse {
  token: string;
  type: string;
  user: User;
}
