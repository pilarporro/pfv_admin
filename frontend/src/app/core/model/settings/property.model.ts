import { AuditableEntity } from '../base/auditable-entity.model';

export interface Property extends AuditableEntity {
  property?: string;
  value?: string;
}
