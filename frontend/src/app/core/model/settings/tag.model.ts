import { AuditableEntity } from '../base/auditable-entity.model';
import { TagType } from '../../enums/tag-type.enum';

export interface Tag extends AuditableEntity {
  name?: string;
  type?: TagType;
  terms?: string;
  uses?: number;
}
