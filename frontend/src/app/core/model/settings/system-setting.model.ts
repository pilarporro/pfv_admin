import { AuditableEntity } from '../base/auditable-entity.model';
import { SystemSettingType } from '../../enums/system-setting-type.enum';

export interface SystemSetting extends AuditableEntity {
  property?: SystemSettingType;
  value?: string;
}
