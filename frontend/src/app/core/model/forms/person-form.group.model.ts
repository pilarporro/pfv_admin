import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { AuditableEntityWithPhotosFormGroup } from './auditable-entity-with-photos-form-group.model';
import { Person } from '../persons/person.model';
import { LocationFormGroup } from './location-form-group.model';

export class PersonFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor() {
        super({
            idNumber: new FormControl(),
            email: new FormControl(),
            name: new FormControl(undefined, Validators.required),
            lastName: new FormControl(),
            phone: new FormControl(),
            bankAccount: new FormControl(),
            notes: new FormControl(),
            location: new LocationFormGroup(),
            isMember: new FormControl(),
            nextPayment: new FormControl(),
            memberFrom: new FormControl(),
            memberTo: new FormControl(),
            paymentPeriodicity: new FormControl(),
            bankMatcher: new FormControl(),
            colonies: new FormArray([])
        });
    }

    patchEntity(entity: Person): void {
        if (Utils.isNotDefined(entity.colonies)) {
            entity.colonies = [];
        }

        this.patchValue(entity);

        this.patchAliasIdLabelArray(this.get('colonies') as FormArray, entity.colonies);
        super.patchEntity(entity);
    }

    parseToEntity(): Person {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const person: Person = rawValue;
        if (Utils.isDefined(person.memberFrom) && (person.memberFrom.setHours)) {
            person.memberFrom.setHours(12);
        }
        if (Utils.isDefined(person.memberTo) && (person.memberTo.setHours)) {
            person.memberTo.setHours(12);
        }
        console.log('person', person);
        if (Utils.isDefined(person.nextPayment) && (person.nextPayment.setHours)) {
            person.nextPayment.setHours(12);
        }

        return person;
    }
}
