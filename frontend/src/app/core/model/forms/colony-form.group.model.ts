import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { AuditableEntityWithPhotosFormGroup } from './auditable-entity-with-photos-form-group.model';
import { Person } from '../persons/person.model';
import { LocationFormGroup } from './location-form-group.model';
import { AliasIdLabel } from '../base/alias-id-label.model';
import { Colony } from '../animals/colony.model';
import { AliasLabelIdFormGroup } from './alias-label-id-form-group.model';

export class ColonyFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor() {
        super({
            name: new FormControl(undefined, Validators.required),
            notes: new FormControl(),
            totalAnimals: new FormControl(),
            totalNeuteredAnimals: new FormControl(),
            totalFemaleAnimals: new FormControl(),
            totalNeuteredFemaleAnimals: new FormControl(),
            location: new LocationFormGroup(),
            person: new AliasLabelIdFormGroup(true),
            initDate: new FormControl()
        });
    }

    patchEntity(entity: Colony): void {
        if (entity) {
            if (Utils.isDefined(entity.initDate) && (entity.initDate.setHours)) {
                entity.initDate.setHours(12);
            }

            if (Utils.isNotDefined(entity.person)) {
                entity.person = {};
            }

            this.patchValue(entity);
            super.patchEntity(entity);
        }
    }

    parseToEntity(): Colony {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const colony: Colony = rawValue;

        return colony;
    }
}
