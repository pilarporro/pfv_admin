import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';

import { AuditableFormGroup } from './auditable-form-group.model';
import { Photo } from '../base/photo.model';


export class PhotoFormGroup extends FormGroup {

  static createPhotoFromFile(file: Storage): Photo {
    return {
      folder: file.folder,
      diskName: file.diskName,
      diskNameThumbnail: file.diskNameThumbnail,
      temporary: file.temporary,
      url: file.url,
      urlThumbnail: file.urlThumbnail
    };
  }

  constructor(photo?: Photo) {
    super({
      ...{
        folder: new FormControl(),
        diskName: new FormControl(),
        diskNameThumbnail: new FormControl(),
        temporary: new FormControl(),
        url: new FormControl(),
        urlThumbnail: new FormControl()
      }
    });

    if (photo) {
      this.patchValue(photo);
    }
  }
}
