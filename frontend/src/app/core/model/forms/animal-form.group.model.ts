import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { AuditableEntityWithPhotosFormGroup } from './auditable-entity-with-photos-form-group.model';
import { LocationFormGroup } from './location-form-group.model';
import { Animal } from '../animals/animal.model';
import { AliasLabelIdFormGroup } from './alias-label-id-form-group.model';

export class AnimalFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor() {
        super({
            notes: new FormControl(),
            name: new FormControl(undefined, Validators.required),
            colonyId: new FormControl(),
            owner: new AliasLabelIdFormGroup(),
            adopter: new AliasLabelIdFormGroup(),
            keeper: new AliasLabelIdFormGroup(),
            tags: new FormControl(),
            animalStatus: new FormControl(),
            birthDate: new FormControl(),
            deathDate: new FormControl(),
            color: new FormControl(),
            gender: new FormControl(undefined, Validators.required),
            adoptionDate: new FormControl(),
            pickUpDate: new FormControl(),
            neutered: new FormControl(undefined, Validators.required),
            type: new FormControl(undefined, Validators.required)
        });
    }

    patchEntity(entity: Animal): void {
        if (Utils.isNotDefined(entity.owner)) {
            entity.owner = {};
        }
        if (Utils.isNotDefined(entity.keeper)) {
            entity.keeper = {};
        }
        if (Utils.isNotDefined(entity.adopter)) {
            entity.adopter = {};
        }
        this.patchValue(entity);
        super.patchEntity(entity);
    }

    parseToEntity(): Animal {
        const rawValue = this.getRawValue();

        Utils.checkEmptyValues(rawValue);
        const animal: Animal = rawValue;

        if (Utils.isDefined(animal.pickUpDate) && (animal.pickUpDate.setHours)) {
            animal.pickUpDate.setHours(12);
        }
        if (Utils.isDefined(animal.adoptionDate) && (animal.adoptionDate.setHours)) {
            animal.adoptionDate.setHours(12);
        }
        if (Utils.isDefined(animal.deathDate) && (animal.deathDate.setHours)) {
            animal.deathDate.setHours(12);
        }
        if (Utils.isDefined(animal.birthDate) && (animal.birthDate.setHours)) {
            animal.birthDate.setHours(12);
        }

        return animal;
    }
}
