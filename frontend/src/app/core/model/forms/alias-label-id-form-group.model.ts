import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AliasIdLabel } from '../base/alias-id-label.model';
import { Animal } from '../animals/animal.model';
import { Utils } from '../../utils/utils';

export class AliasLabelIdFormGroup extends FormGroup {

    constructor(mandatory: boolean = false) {
        if (!mandatory) {
            super({
                alias: new FormControl(),
                label: new FormControl(),
                id: new FormControl()
            });
        } else {
            super({
                alias: new FormControl(),
                label: new FormControl(),
                id: new FormControl(undefined, Validators.required)
            });
        }
    }

    static  fromEntity(aliasIdLabel: AliasIdLabel): AliasLabelIdFormGroup {
        const form: AliasLabelIdFormGroup = new AliasLabelIdFormGroup(true);
        form.patchValue(aliasIdLabel);

        return form;
    }

    patchEntity(aliasIdLabel: AliasIdLabel): void {
        this.patchValue({
            alias: aliasIdLabel.alias,
            id: aliasIdLabel.id,
            label: aliasIdLabel.label
        }, {emitEvent: false});
    }
}
