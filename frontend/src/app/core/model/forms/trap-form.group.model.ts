import { FormControl, Validators } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { AuditableEntityWithPhotosFormGroup } from './auditable-entity-with-photos-form-group.model';
import { Trap } from '../persons/trap.model';
import { AliasLabelIdFormGroup } from './alias-label-id-form-group.model';

export class TrapFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor() {
        super({
            name: new FormControl(undefined, Validators.required),
            notes: new FormControl(),
            usingFrom: new FormControl(),
            owner: new AliasLabelIdFormGroup(),
            person: new AliasLabelIdFormGroup()
        });
    }

    patchEntity(entity: Trap): void {
        if (Utils.isNotDefined(entity.person)) {
            entity.person = {};
        }

        if (Utils.isNotDefined(entity.owner)) {
            entity.owner = {};
        }

        this.patchValue(entity);
        super.patchEntity(entity);
    }

    parseToEntity(): Trap {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const trap: Trap = rawValue;
        if (Utils.isDefined(trap.usingFrom) && (trap.usingFrom.setHours)) {
            trap.usingFrom.setHours(12);
        }
        return trap;
    }
}
