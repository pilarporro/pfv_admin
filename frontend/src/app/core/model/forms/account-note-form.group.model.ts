import { FormArray, FormControl, Validators } from '@angular/forms';
import { AccountNote } from '../accounting/account-note.model';
import { AuditableEntityWithPhotosFormGroup } from './auditable-entity-with-photos-form-group.model';
import { AliasLabelIdFormGroup } from './alias-label-id-form-group.model';
import { Utils } from '../../utils/utils';

export class AccountNoteFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor() {
        super({
            type: new FormControl(undefined, Validators.required),
            subtype: new FormControl(undefined, Validators.required),
            date: new FormControl(undefined, Validators.required),
            notes: new FormControl(),
            total: new FormControl(undefined, Validators.required),
            bankBalance: new FormControl(),
            cashBalance: new FormControl(),
            grantBalance: new FormControl(),
            place: new AliasLabelIdFormGroup(false),
            person: new AliasLabelIdFormGroup(false),
            activities: new FormArray([])
        });
    }

    patchEntity(entity: AccountNote): void {
        if (Utils.isNotDefined(entity.activities)) {
            entity.activities = [];
        }

        if (Utils.isNotDefined(entity.place)) {
            entity.place = {};
        }

        if (Utils.isNotDefined(entity.person)) {
            entity.person = {};
        }

        this.patchValue(entity);
        this.patchAliasIdLabelArray(this.get('activities') as FormArray, entity.activities);
        super.patchEntity(entity);
    }

    parseToEntity(): AccountNote {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const accountNote: AccountNote = rawValue;

        if (Utils.isDefined(accountNote.date) && (accountNote.date.setHours)) {
            accountNote.date.setHours(12);
        }

        return super.parseToEntity(accountNote);
    }
}
