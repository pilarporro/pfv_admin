import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';

import { Utils } from '../../utils/utils';
import { AuditableFormGroup } from './auditable-form-group.model';
import { Photo } from '../base/photo.model';
import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';


export class AuditableEntityWithPhotosFormGroup extends AuditableFormGroup {
  photos: FormArray;

  constructor(controls: { [key: string]: AbstractControl; }) {
    super({
      ...controls,
      ...{
        photos: new FormArray([])
      }
    });

    this.photos = this.get('photos') as FormArray;
  }

  createPhotoFormGroup(): FormGroup {
    return new FormGroup({
      folder: new FormControl(),
      diskName: new FormControl(),
      temporary: new FormControl(),
      url: new FormControl()
    });
  }

  addPhotoFormGroup(photo: Photo): FormGroup {
    const photoFormGroup: FormGroup = this.createPhotoFormGroup();
    photoFormGroup.patchValue(photo);
    // this.photos.push(photoFormGroup);
    this.photos.controls = [...this.photos.controls, photoFormGroup];
    return photoFormGroup;
  }

  addPhotoFormGroupFromFile(file: Photo): FormGroup {
    const photo: Photo = {
      folder: file.folder,
      diskName: file.diskName,
      temporary: file.temporary,
      url: file.url
    };

    return this.addPhotoFormGroup(photo);
  }

  patchEntity(entity: AuditableEntityWithPhotos): void {
    if (Utils.isNotDefined(entity.photos)) {
      entity.photos = [];
    }
    while (this.photos.length > 0) {
      this.photos.removeAt(0);
    }
    entity.photos.forEach((photo, index) => {
      this.addPhotoFormGroup(photo);
    });
  }

  parseToEntity(entity?: AuditableEntityWithPhotos): AuditableEntityWithPhotos {
    return entity;
  }
}
