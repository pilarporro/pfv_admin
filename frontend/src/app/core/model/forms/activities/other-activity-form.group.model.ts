import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { Rescue } from '../../actitivities/rescue.model';
import { VisitClinic } from '../../actitivities/visit-clinic.model';
import { OtherActivity } from '../../actitivities/other.model';

export class OtherActivityFormGroup extends ActivityFormGroup {

    constructor() {
        super({
        });
    }

    patchEntity(entity: OtherActivity): void {
        super.patchEntity(entity);
    }

    parseToEntity(): OtherActivity {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const other: OtherActivity = rawValue;

        return super.parseToEntity(other);
    }
}
