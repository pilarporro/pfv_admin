import { FormArray, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { NewMember } from '../../actitivities/new-member.model';

export class NewMemberFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            newMember: new AliasLabelIdFormGroup(true)
        });
    }

    patchEntity(entity: NewMember): void {
        if (Utils.isNotDefined(entity.newMember)) {
            entity.newMember = {};
        }

        super.patchEntity(entity);
    }

    parseToEntity(): NewMember {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const newMember: NewMember = rawValue;

        return super.parseToEntity(newMember);
    }
}
