import { FormArray, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { Rescue } from '../../actitivities/rescue.model';

export class RescueFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            caller: new AliasLabelIdFormGroup(true),
            animals: new FormArray([], Validators.required)
        });
    }

    patchEntity(entity: Rescue): void {
        if (Utils.isNotDefined(entity.animals)) {
            entity.animals = [];
        }

        if (Utils.isNotDefined(entity.caller)) {
            entity.caller = {};
        }

        super.patchEntity(entity);

        this.patchAliasIdLabelArray(this.get('animals') as FormArray, entity.animals);
    }

    parseToEntity(): Rescue {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const rescue: Rescue = rawValue;

        return super.parseToEntity(rescue);
    }
}
