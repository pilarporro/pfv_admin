import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { BuySupplies } from '../../actitivities/buy-supplies.model';
import { Castration } from '../../actitivities/castration.model';

export class CastrationFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            animal: new AliasLabelIdFormGroup(true),
            colony: new AliasLabelIdFormGroup(),
            vetClinic: new AliasLabelIdFormGroup(true),
            chip: new FormControl(),
            paymentType: new FormControl(undefined, Validators.required),
            invoiceRef: new FormControl()
        });
    }

    patchEntity(entity: Castration): void {
        if (Utils.isNotDefined(entity.animal)) {
            entity.animal = {};
        }

        if (Utils.isNotDefined(entity.colony)) {
            entity.colony = {};
        }

        if (Utils.isNotDefined(entity.vetClinic)) {
            entity.vetClinic = {};
        }

        super.patchEntity(entity);
    }

    parseToEntity(): Castration {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const castration: Castration = rawValue;

        return super.parseToEntity(castration);
    }
}
