import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { Rescue } from '../../actitivities/rescue.model';
import { VisitClinic } from '../../actitivities/visit-clinic.model';
import { PaymentType } from '../../../enums/payment-type.enum';

export class VisitClinicFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            vetClinic: new AliasLabelIdFormGroup(true),
            animals: new FormArray([], Validators.required),
            chip: new FormControl(),
            invoiceRef: new FormControl(),
            paymentType: new FormControl(undefined, Validators.required),
            visitClinicType: new FormControl(undefined, Validators.required)
        });
    }

    patchEntity(entity: VisitClinic): void {
        if (Utils.isNotDefined(entity.animals)) {
            entity.animals = [];
        }

        if (Utils.isNotDefined(entity.vetClinic)) {
            entity.vetClinic = {};
        }

        super.patchEntity(entity);

        this.patchAliasIdLabelArray(this.get('animals') as FormArray, entity.animals);
    }

    parseToEntity(): VisitClinic {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const visitClinic: VisitClinic = rawValue;

        return super.parseToEntity(visitClinic);
    }
}
