import { FormArray, FormControl, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';

export class AdoptionFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            adopter: new AliasLabelIdFormGroup(true),
            chip: new FormControl(),
            animal: new AliasLabelIdFormGroup(true)
        });
    }

    patchEntity(entity: Adoption): void {
        if (Utils.isNotDefined(entity.animal)) {
            entity.animal = {};
        }

        if (Utils.isNotDefined(entity.adopter)) {
            entity.adopter = {};
        }

        super.patchEntity(entity);
    }

    parseToEntity(): Adoption {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const adoption: Adoption = rawValue;

        return super.parseToEntity(adoption);
    }
}
