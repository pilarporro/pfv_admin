import { FormArray, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { Rescue } from '../../actitivities/rescue.model';
import { PickUpKeeper } from '../../actitivities/pickup-keeper.model';
import { Meeting } from '../../actitivities/meeting.model';

export class MeetingFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            persons: new FormArray([], Validators.required)
        });
    }

    patchEntity(entity: Meeting): void {
        if (Utils.isNotDefined(entity.persons)) {
            entity.persons = [];
        }

        super.patchEntity(entity);

        this.patchAliasIdLabelArray(this.get('persons') as FormArray, entity.persons);
    }

    parseToEntity(): Meeting {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const meeting: Meeting = rawValue;

        return super.parseToEntity(meeting);
    }
}
