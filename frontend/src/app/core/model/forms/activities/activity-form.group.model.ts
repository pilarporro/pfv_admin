import { AbstractControl, FormArray, FormControl, Validators } from '@angular/forms';
import { AuditableEntityWithPhotosFormGroup } from '../auditable-entity-with-photos-form-group.model';
import { Activity } from '../../actitivities/activity.model';
import { Utils } from '../../../utils/utils';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';

export abstract class ActivityFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor(controls: { [key: string]: AbstractControl; }) {
        super({
            ...controls,
            ...{
                notes: new FormControl(),
                person: new AliasLabelIdFormGroup(true),
                tags: new FormControl(),
                type: new FormControl(),
                date: new FormControl(undefined, Validators.required)
            }
        });
    }

    patchEntity(entity: Activity): void {
        if (Utils.isNotDefined(entity.person)) {
            entity.person = {};
        }

        this.patchValue(entity);
        super.patchEntity(entity);
    }

    parseToEntity(activity?: Activity): Activity {
        if (activity) {
            if (Utils.isDefined(activity.date) && (activity.date.setHours)) {
                activity.date.setHours(12);
            }
        }

        return activity;
    }
}
