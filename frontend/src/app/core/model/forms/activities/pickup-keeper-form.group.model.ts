import { FormArray, Validators } from '@angular/forms';
import { Utils } from '../../../utils/utils';
import { ActivityFormGroup } from './activity-form.group.model';
import { Adoption } from '../../actitivities/adoption.model';
import { AliasLabelIdFormGroup } from '../alias-label-id-form-group.model';
import { AliasIdLabel } from '../../base/alias-id-label.model';
import { Rescue } from '../../actitivities/rescue.model';
import { PickUpKeeper } from '../../actitivities/pickup-keeper.model';

export class PickUpKeeperFormGroup extends ActivityFormGroup {

    constructor() {
        super({
            keeper: new AliasLabelIdFormGroup(true),
            animals: new FormArray([], Validators.required)
        });
    }

    patchEntity(entity: PickUpKeeper): void {
        if (Utils.isNotDefined(entity.animals)) {
            entity.animals = [];
        }

        if (Utils.isNotDefined(entity.keeper)) {
            entity.keeper = {};
        }

        super.patchEntity(entity);

        this.patchAliasIdLabelArray(this.get('animals') as FormArray, entity.animals);
    }

    parseToEntity(): PickUpKeeper {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const pickUpKeeper: PickUpKeeper = rawValue;

        return super.parseToEntity(pickUpKeeper);
    }
}
