import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { Entity } from '../base/entity.model';
import { AliasIdLabel } from '../base/alias-id-label.model';
import { AliasLabelIdFormGroup } from './alias-label-id-form-group.model';

export class AuditableFormGroup extends FormGroup {

    constructor(controls: { [key: string]: AbstractControl; }, updateOn: any = 'blur') {
        // console.log('Super AuditableFormGroup');
        // console.log(updateOn);
        super({
            ...controls,
            ...{
                id: new FormControl(),
                alias: new FormControl(),
                created: new FormControl(),
                createdBy: new FormControl(),
                lastModified: new FormControl(),
                lastModifiedBy: new FormControl(),
                active: new FormControl(),
                markToDelete: new FormControl(),
                notification: new FormControl()
            }
        });
    }

    patchEntity(entity: Entity): void {
        this.patchValue(entity);
    }

    parseToEntity(): Entity {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);

        return rawValue as Entity;
    }

    patchAliasIdLabelArray(array: FormArray, aliasIdLabels: AliasIdLabel[]) {
        while (array.length > 0) {
            array.removeAt(0);
        }
        aliasIdLabels.forEach((animal, index) => {
            array.push(AliasLabelIdFormGroup.fromEntity(animal));
        });
    }
}
