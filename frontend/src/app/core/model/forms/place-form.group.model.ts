import { FormControl, Validators } from '@angular/forms';
import { Utils } from '../../utils/utils';
import { AuditableEntityWithPhotosFormGroup } from './auditable-entity-with-photos-form-group.model';
import { LocationFormGroup } from './location-form-group.model';
import { Place } from '../places/place.model';

export class PlaceFormGroup extends AuditableEntityWithPhotosFormGroup {

    constructor() {
        super({
            email: new FormControl(),
            name: new FormControl(undefined, Validators.required),
            phone: new FormControl(),
            type: new FormControl(undefined, Validators.required),
            notes: new FormControl(),
            location: new LocationFormGroup()
        });
    }

    patchEntity(entity: Place): void {
        this.patchValue(entity);
        super.patchEntity(entity);
    }

    parseToEntity(): Place {
        const rawValue = this.getRawValue();
        Utils.checkEmptyValues(rawValue);
        const place: Place = rawValue;

        return place;
    }
}
