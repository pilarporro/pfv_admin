import { FormControl, FormGroup } from '@angular/forms';

export class LocationFormGroup extends FormGroup {

    constructor() {
        super({
            address: new FormControl(),
            postalCode: new FormControl(),
            street: new FormControl(),
            number: new FormControl(),
            areaId: new FormControl(),
            municipalityId: new FormControl(),
            country: new FormControl(),
            geoLocation: new FormControl(),
            placeId: new FormControl(),
            url: new FormControl()
        });
    }
}
