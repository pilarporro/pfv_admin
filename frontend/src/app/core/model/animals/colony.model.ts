import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { AliasIdLabel } from '../base/alias-id-label.model';
import { Location } from '../base/location.model';

export interface Colony extends AuditableEntityWithPhotos {
  name?: string;
  notes?: string;
  totalAnimals?: number;
  totalNeuteredAnimals?: number;
  totalFemaleAnimals?: number;
  totalNeuteredFemaleAnimals?: number;
  location?: Location;
  person?: AliasIdLabel;
  initDate?: Date;
}
