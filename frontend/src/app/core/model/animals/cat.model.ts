import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { AnimalStatus } from '../../enums/animal-status.enum';
import { Gender } from '../../enums/gender.enum';
import { AnimalType } from '../../enums/animal-type.enum';
import { Animal } from './animal.model';

export interface Cat extends Animal {

}

