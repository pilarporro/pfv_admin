import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { AnimalStatus } from '../../enums/animal-status.enum';
import { Gender } from '../../enums/gender.enum';
import { AnimalType } from '../../enums/animal-type.enum';
import { AliasIdLabel } from '../base/alias-id-label.model';
import { Color } from '../../enums/color.enum';

export interface Animal extends AuditableEntityWithPhotos {
  name?: string;
  notes?: string;
  colonyId?: string;
  owner?: AliasIdLabel;
  adopter?: AliasIdLabel;
  colonyAlias?: string;
  keeper?: AliasIdLabel;
  tags?: string[];
  animalStatus?: AnimalStatus;
  birthDate?: Date;
  deathDate?: Date;
  gender?: Gender;
  color?: Color;
  adoptionDate?: Date;
  pickUpDate?: Date;
  type?: AnimalType;
  neutered?: boolean;
}
