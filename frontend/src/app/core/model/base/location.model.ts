import { Country } from '../../enums/country.enum';

export interface Location {
  street?: string;
  number?: string;
  postalCode?: string;
  areaId?: string;
  areaName?: string;
  municipalityId?: string;
  country?: Country;
  geoLocation?: number[];
  placeId?: string;
  url?: string;
}
