import { Entity } from './entity.model';

export interface AliasIdLabel extends Entity {
  label?: string;
  id?: string;
  alias?: string;
}
