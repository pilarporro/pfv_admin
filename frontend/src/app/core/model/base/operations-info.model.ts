import { Operation } from './operation.model';

export interface OperationsInfo {
  delete?: Operation;
}
