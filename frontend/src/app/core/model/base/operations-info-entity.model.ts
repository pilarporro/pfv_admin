import { OperationsInfo } from './operations-info.model';
import { Entity } from './entity.model';

export interface OperationsInfoEntity extends Entity {
  operationsInfo?: OperationsInfo;
}
