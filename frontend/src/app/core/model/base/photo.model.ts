export interface Photo {
  temporary?: boolean;
  folder?: string;
  diskName?: string;
  diskNameThumbnail?: string;
  urlThumbnail?: string;
  url?: string;
}
