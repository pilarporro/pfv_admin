import { OperationsInfoEntity } from './operations-info-entity.model';

export interface AuditableEntity extends OperationsInfoEntity {
  active?: boolean;
  created?: Date;
  createdBy?: string;
  lastModified?: Date;
  lastModifiedBy?: string;
}
