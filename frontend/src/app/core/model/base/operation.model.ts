export interface Operation {
  allowed?: boolean;
  reasons?: string[];
}
