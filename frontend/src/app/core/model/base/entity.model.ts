export interface Entity {
  id?: string;
  alias?: string;
  markToDelete?: boolean;
  notification?: boolean,
}
