export class Breadcrumb {
  constructor(public displayName: string,
              public paramName: string,
              public paramValue: string,
              public terminal: boolean,
              public url: string,
              public active: boolean) {
  }
}
