import { AuditableEntity } from './auditable-entity.model';
import { Photo } from './photo.model';

export interface AuditableEntityWithPhotos extends AuditableEntity {
  photos?: Photo[];
  withPhotos?: boolean;
}
