import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { AnimalStatus } from '../../enums/animal-status.enum';
import { Gender } from '../../enums/gender.enum';
import { AnimalType } from '../../enums/animal-type.enum';
import { ActivityType } from '../../enums/activity-type.enum';
import { AliasIdLabel } from '../base/alias-id-label.model';
import { AccountType } from '../../enums/account-type.enum';
import { AccountSubtype } from '../../enums/account-subtype.enum';
import { VetClinic } from '../places/vet-clinit.model';
import { Person } from '../persons/person.model';

export interface AccountNote extends AuditableEntityWithPhotos {
  type?: AccountType;
  subtype?: AccountSubtype;
  date?: Date;
  notes?: string;
  total?: number;
  bankBalance?: number;
  cashBalance?: number;
  grantBalance?: number;
  place?: AliasIdLabel;
  person?: AliasIdLabel;
  activities?: AliasIdLabel[];
  manualNote?: boolean;
}
