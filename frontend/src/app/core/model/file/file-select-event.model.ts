export interface FileSelectEvent {
  originalEvent: Event;
  files: FileList;
}
