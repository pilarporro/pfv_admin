import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { AliasIdLabel } from '../base/alias-id-label.model';

export interface Trap extends AuditableEntityWithPhotos {
  name?: string;
  notes?: string;
  usingFrom?: any;
  person?: AliasIdLabel;
  owner?: AliasIdLabel;
}

