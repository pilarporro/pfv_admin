import { AuditableEntity } from '../base/auditable-entity.model';
import { Role } from '../../enums/role.enum';
import { AliasIdLabel } from '../base/alias-id-label.model';

export interface User extends AuditableEntity {
  username?: string;
  password?: string;
  name?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  roles?: Role[];
  admin?: boolean;
  guest?: boolean;
  user?: boolean;
  person?: AliasIdLabel;
}
