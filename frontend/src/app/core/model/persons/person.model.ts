import { AuditableEntity } from '../base/auditable-entity.model';
import { Role } from '../../enums/role.enum';
import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { Colony } from '../animals/colony.model';
import { PaymentPeriodicity } from '../../enums/payment-periodicity.enum';
import { Location } from '../base/location.model';
import { AliasIdLabel } from '../base/alias-id-label.model';

export interface Person extends AuditableEntityWithPhotos {
  email?: string;
  fullName?: string;
  name?: string;
  lastName?: string;
  phone?: string;
  bankAccount?: string;
  bankMatcher?: string;
  notes?: string;
  idNumber?: string;
  location?: Location;
  isMember?: boolean;
  memberFrom?: any;
  memberTo?: any;
  nextPayment?: any;
  paymentPeriodicity?: PaymentPeriodicity;
  colonies?: AliasIdLabel[];
}

