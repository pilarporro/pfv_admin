import { AuditableEntity } from '../base/auditable-entity.model';

export interface Area extends AuditableEntity {
  name?: string;
  municipalityId?: string;
}
