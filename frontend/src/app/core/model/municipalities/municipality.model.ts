import { AuditableEntity } from '../base/auditable-entity.model';

export interface Municipality extends AuditableEntity {
  name?: string;
  province?: string;
  city?: string;
  location?: number[];
}