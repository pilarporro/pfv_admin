import { FilterMetadata } from 'primeng/api';

export interface TableFilters {
  [key: string]: FilterMetadata;
}
