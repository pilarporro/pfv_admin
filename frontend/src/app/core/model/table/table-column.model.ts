import { TableColumType } from '../../enums/table/table-column-type.enum';
import { Observable } from 'rxjs';

export interface TableColumn {
  field?: string;
  header?: string;
  width?: string;
  type?: TableColumType;
  subtype?: string;
  enumClass?: string;
  class?: string;
  abbr?: boolean;
  sortable?: boolean;
  sortField?: string;
  filter?: boolean;
  filterMatchMode?: string;
  filterFields?: string[];
  hasHtmlContent?: boolean;
  hasFilterConverter?: boolean;
  onClick?: Function;
  rowToLabelConverter?(row: any): Observable<string>;
  filterConverter?(row: any): Observable<string>;
}
