import { FilterOperation } from '../../enums/table/filter-operation.enum';

export interface FilterParam {
  offsetTimezone: number;
  key: string;
  value: any;
  operation: FilterOperation;
}
