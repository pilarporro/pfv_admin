import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { PlaceType } from '../../enums/place-type.enum';
import { Location } from '../base/location.model';

export interface Place extends AuditableEntityWithPhotos {
  name?: string;
  notes?: string;
  email?: string;
  phone?: string;
  location?: Location;
  type?: PlaceType;
}