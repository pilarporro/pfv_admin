import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';

export interface NewMember extends Activity {
  newMember?: AliasIdLabel;
}
