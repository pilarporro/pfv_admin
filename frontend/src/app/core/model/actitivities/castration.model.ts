import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';
import { PaymentType } from '../../enums/payment-type.enum';

export interface Castration extends Activity {
  vetClinic?: AliasIdLabel;
  animal?: AliasIdLabel;
  paymentType?: PaymentType;
  invoiceRef?: string;
  colony?: AliasIdLabel;
}
