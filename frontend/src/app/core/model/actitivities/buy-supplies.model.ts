import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';
import { PaymentType } from '../../enums/payment-type.enum';

export interface BuySupplies extends Activity {
  place?: AliasIdLabel;
  paymentType?: PaymentType;
}
