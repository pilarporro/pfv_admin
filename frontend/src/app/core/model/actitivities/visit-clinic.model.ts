import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';
import { PaymentType } from '../../enums/payment-type.enum';
import { VisitClinicType } from '../../enums/visit-clinic-type.enum';

export interface VisitClinic extends Activity {
  vetClinic?: AliasIdLabel;
  animals?: AliasIdLabel[];
  paymentType?: PaymentType;
  invoiceRef?: string;
  visitClinicType?: VisitClinicType;
}
