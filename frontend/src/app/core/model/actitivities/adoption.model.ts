import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';

export interface Adoption extends Activity {
  adopter?: AliasIdLabel;
  animal?: AliasIdLabel;
}
