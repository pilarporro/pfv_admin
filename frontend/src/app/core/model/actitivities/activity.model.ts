import { AuditableEntityWithPhotos } from '../base/auditable-entity-with-photos.model';
import { AnimalStatus } from '../../enums/animal-status.enum';
import { Gender } from '../../enums/gender.enum';
import { AnimalType } from '../../enums/animal-type.enum';
import { ActivityType } from '../../enums/activity-type.enum';
import { AliasIdLabel } from '../base/alias-id-label.model';

export interface Activity extends AuditableEntityWithPhotos {
  date?: Date;
  notes?: string;
  person?: AliasIdLabel;
  tags?: string[];
  type?: ActivityType;
  chip?: string;
}
