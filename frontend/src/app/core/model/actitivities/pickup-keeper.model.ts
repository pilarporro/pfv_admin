import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';

export interface PickUpKeeper extends Activity {
  keeper?: AliasIdLabel;
  animals?: AliasIdLabel[];
}
