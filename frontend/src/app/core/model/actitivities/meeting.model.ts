import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';

export interface Meeting extends Activity {
  persons?: AliasIdLabel[];
}
