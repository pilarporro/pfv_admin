import { AliasIdLabel } from '../base/alias-id-label.model';
import { Activity } from './activity.model';

export interface Rescue extends Activity {
  caller?: AliasIdLabel;
  animals?: AliasIdLabel[];
}
