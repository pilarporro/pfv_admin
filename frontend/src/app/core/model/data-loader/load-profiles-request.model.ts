export interface DataLoaderRequest {
  type?: string;
  value?: string;
  path?: string;
}
