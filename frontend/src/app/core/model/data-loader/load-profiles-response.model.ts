import { DataLoaderErrorItemResponse } from './data-loader-error-item-response.model';

export interface DataLoaderResponse {
  total?: string;
  errors: DataLoaderErrorItemResponse[];
}
