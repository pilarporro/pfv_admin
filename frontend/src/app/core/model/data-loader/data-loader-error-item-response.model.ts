export interface DataLoaderErrorItemResponse {
  numLine?: number;
  message?: string;
}
