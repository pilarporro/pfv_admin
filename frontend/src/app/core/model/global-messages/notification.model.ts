import { NotificationType } from './notification-type.model';

export class Notification {
  type: NotificationType;
  title: string;
  message: string;
  duration: number;
  sticky: boolean;

  constructor(type: NotificationType, title: string, message: string, duration: number, sticky: boolean) {
    this.type = type;
    this.title = title;
    this.message = message;
    this.duration = duration;
    this.sticky = sticky;
  }
}
