import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { Unsubscribable } from '../../utils/unsubscribable';
import { take, takeUntil } from 'rxjs/operators';
import { GlobalMessageService } from '../../services/global-messages/global-message.service';
import { AuthService } from '../../services/auth/auth.service';
import { LogService } from '../../services/logs/log.service';
import { Notification } from '../../model/global-messages/notification.model';
import { NotificationType } from '../../model/global-messages/notification-type.model';

@Component({
    selector: 'admin-global-message',
    templateUrl: './global-message.component.html',
    styleUrls: ['./global-message.component.scss']
})
export class GlobalMessageComponent extends Unsubscribable implements OnInit {

    defaultDuration: number = 3000;

    constructor(
        private globalMessageService: GlobalMessageService,
        private authService: AuthService,
        private translateService: TranslateService,
        private logService: LogService,
        private messageService: MessageService) {
        super();
    }

    ngOnInit(): void {
        this.globalMessageService.getNotification()
            .pipe(takeUntil(this.ngUnsubscribe$)).subscribe(notification => {
            // console.log(notification);
            this.showNotification(notification);
        });

        this.globalMessageService.getGlobalError()
            .pipe(takeUntil(this.ngUnsubscribe$)).subscribe(anyError => {
            if (this.authService.isAuthenticated()) {
                this.logService.error(JSON.stringify(anyError));
            }
            this.showGlobalError(anyError);
        });
    }

    showNotification(notification: Notification): void {
        this.messageService.clear('pfv-common-global');

        const duration: number = notification.duration || this.defaultDuration;

        if (notification.type) {
            this.messageService.add({
                key: 'pfv-common-global',
                severity: notification.type,
                summary: notification.title,
                detail: notification.message,
                life: duration,
                sticky: notification.sticky
            });
        } else {
            console.warn('notification.type', notification.type, 'not found');
        }
    }

    showGlobalError(anyError: any): void {
        let showGlobalError: boolean = true;

        if (anyError instanceof HttpErrorResponse) {
            if (anyError.error && anyError.error.messages) {
                const messagesObj = anyError.error.messages;

                if ((messagesObj.globalErrors && messagesObj.globalErrors.length > 0)
                    || (messagesObj.fieldErrors && messagesObj.fieldErrors.length > 0)) {
                    // Show the globalErrors and fieldErrors in a list-structure
                    let messageDescriptions = '<ul>';
                    if (messagesObj.globalErrors && messagesObj.globalErrors.length > 0) {
                        messagesObj.globalErrors.forEach(errorObj => {
                            messageDescriptions += `<li>${errorObj.description}</li>`;
                        });
                    }

                    if (messagesObj.fieldErrors && messagesObj.fieldErrors.length > 0) {
                        messagesObj.fieldErrors.forEach(errorObj => {
                            messageDescriptions += `<li>${errorObj.field} - ${errorObj.description}</li>`;
                        });
                    }

                    messageDescriptions += '</ul>';

                    showGlobalError = false;

                    this.translateService.get('COMMON.ERRORS.GlobalAndFieldErrorMessageTitle')
                        .pipe(takeUntil(this.ngUnsubscribe$))
                        .subscribe(translation => {
                            this.messageService.add({
                                key: 'mqd-common-global',
                                severity: NotificationType.ERROR,
                                summary: translation,
                                detail: messageDescriptions
                            });
                        });
                }
            }
        } else if (typeof anyError === 'string') {
            showGlobalError = false;

            this.translateService.get('COMMON.ERRORS.GeneralErrorMessageTitle')
                .pipe(takeUntil(this.ngUnsubscribe$))
                .subscribe(translation => {
                    this.messageService.add({
                        key: 'pfv-common-global',
                        severity: NotificationType.ERROR,
                        summary: translation,
                        detail: anyError
                    });
                });
        }

        if (showGlobalError) {
            forkJoin(
                this.translateService.get('COMMON.ERRORS.GeneralErrorMessageTitle'),
                this.translateService.get('COMMON.ERRORS.GeneralErrorMessage')
            )
                .pipe(take(1))
                .subscribe(translations => {
                    const [titleTranslation, messageTranslation] = translations;
                    this.messageService.add({
                        key: 'pfv-common-global',
                        severity: NotificationType.ERROR,
                        summary: titleTranslation,
                        detail: messageTranslation
                    });
                });
        }
    }
}
