import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'admin-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  @Input()
  header: string;

  @Input()
  id: string = '';
}
