import { TranslateService } from '@ngx-translate/core';
import { Directive, Input } from '@angular/core';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Entity } from '../model/base/entity.model';
import { GenericCrudService } from '../services/base/generic-crud.service';
import { CommonDialogComponent } from './common-dialog.component';
import { Utils } from '../utils/utils';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class CommonDetailComponent<T extends Entity> extends CommonDialogComponent<T> {
    @Input()
    readonly: false;

    aliasInUse: boolean = false;
    hasSaved: boolean = false;
    closeAfterSave: boolean = false;

    constructor(protected genericCrudService: GenericCrudService<T>,
                protected translateService: TranslateService,
                protected modalTitleAddKey: string,
                protected modalTitleEditKey: string) {
        super(genericCrudService, translateService);
    }

    getHeader(): Observable<string> {
        if (!this.data) {
            return undefined;
        }
        if (this.data.id === undefined) {
            return this.translateService.get(this.modalTitleAddKey);
        } else {
            return this.translateService.get(this.modalTitleEditKey);
        }
    }

    parseToEntity(): T {
        return this.form.getRawValue();
    }

    patchEntity() {

    }

    formValid(): boolean {
        return this.form.valid;
    }

    save(): void {
        // console.log('save');
        // console.log(this.formValid());
        this.aliasInUse = false;
        if (this.formValid()) {
            this.genericCrudService.findByAlias(this.form.get('alias').value)
                .pipe(take(1))
                .subscribe(response => {
                    if (response && (response.id !== this.data.id)) {
                        this.aliasInUse = true;
                    } else {
                        this.saveEffective();
                    }
                });
        } else {
            Utils.updateValidity(this.form);
        }
    }

    saveEffective(): void {
        if (this.formValid()) {
            // console.log('SAVING');
            // console.log(this.data);
            this.hasSaved = true;
            if (this.data.id === undefined) {
                this.genericCrudService.add(this.parseToEntity())
                    .pipe(take(1))
                    .subscribe(response => {
                        this.afterSaving(response);
                    });
            } else {
                this.genericCrudService.update(this.parseToEntity())
                    .pipe(take(1))
                    .subscribe(response => {
                        this.afterSaving(response);
                    });
            }
        } else {
            Utils.updateValidity(this.form);
            // console.log(Utils.getAllErrors(this.form));
        }
    }

    afterSaving(response): void {
        this.data = response;
        this.patchEntity();
        if (this.closeAfterSave) {
            this.hideDialog();
            this.successDialog.emit(response);
        }
    }

    saveAndClose(): void {
        this.closeAfterSave = true;
        this.save();
    }

    close(): void {
        this.hideDialog();
        if (this.hasSaved) {
            this.successDialog.emit(this.data);
        } else {
            this.cancelDialog.emit();
        }
    }

}
