import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Unsubscribable } from '../utils/unsubscribable';
import { BaseHttpService } from '../services/base/base-http.service';
import { Utils } from '../utils/utils';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class CommonDialogComponent<DATA> extends Unsubscribable {
  @Input()
  display: boolean = false;

  @Input()
  data: DATA;

  form: FormGroup;

  @Output() displayChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output() successDialog: EventEmitter<any> = new EventEmitter();

  @Output() cancelDialog: EventEmitter<any> = new EventEmitter();

  constructor(protected genericCrudService: BaseHttpService, protected translateService: TranslateService) {
    super();
  }

  cancel(): void {
    this.hideDialog();
    this.cancelDialog.emit();
  }

  hideDialog(): void {
    this.display = false;
    this.displayChange.emit(this.display);
  }

  success(): void {
    this.successDialog.emit();
    this.hideDialog();
  }

  isMobile(): boolean {
    return Utils.isMobile();
  }
}
