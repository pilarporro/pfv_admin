import { Component, Input, OnInit } from '@angular/core';
import { LoadingHttp } from '../../services/base/loading-http';

@Component({
  selector: 'admin-block-ui-on-loading-request',
  templateUrl: './block-ui-on-loading-request.component.html'
})
export class BlockUiOnLoadingRequestComponent implements OnInit {
  isLoading: Boolean = false;

  @Input()
  service: LoadingHttp;

  ngOnInit() {
    this.service.getIsLoading$().subscribe(value => {
      this.isLoading = value;
    });
  }

}
