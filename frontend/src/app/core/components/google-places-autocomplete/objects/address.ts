import { AddressComponent } from './addressComponent';
import { Geometry } from './geometry';
import { Photo } from './photo';
import { OpeningHours } from './openingHours';
import { PlaceReview } from './placeReview';
export class Address {
    // tslint:disable-next-line:variable-name
    address_components: AddressComponent[];
    // tslint:disable-next-line:variable-name
    adr_address: string;
    // tslint:disable-next-line:variable-name
    formatted_address: string;
    // tslint:disable-next-line:variable-name
    formatted_phone_number: string;
    geometry: Geometry;
    // tslint:disable-next-line:variable-name
    html_attributions: string[];
    icon: string;
    id: string;
    // tslint:disable-next-line:variable-name
    international_phone_number: string;
    name: string;
    // tslint:disable-next-line:variable-name
    opening_hours: OpeningHours;
    // tslint:disable-next-line:variable-name
    permanently_closed: boolean;
    photos: Photo[];
    // tslint:disable-next-line:variable-name
    place_id: string;
    // tslint:disable-next-line:variable-name
    price_level: number;
    rating: number;
    reviews: PlaceReview[];
    types: string[];
    url: string;
    // tslint:disable-next-line:variable-name
    utc_offset: number;
    vicinity: string;
    website: string;
}
