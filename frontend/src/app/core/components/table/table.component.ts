import { ChangeDetectionStrategy, Component, ContentChild, ElementRef, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { Unsubscribable } from '../../utils/unsubscribable';
import { TableColumn } from '../../model/table/table-column.model';
import { TableColumType } from '../../enums/table/table-column-type.enum';
import { TableFilters } from '../../model/table/table-filters.model';
import { Table } from 'primeng/table';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Utils } from '../../utils/utils';

@Component({
  selector: 'admin-table',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent extends Unsubscribable {
  TableColumType: typeof TableColumType = TableColumType;

  @ViewChild('table') table: any;
  @ContentChild('extraButtons') extraButtons: TemplateRef<ElementRef>;
  @ContentChild('extraCaption') extraCaption: TemplateRef<ElementRef>;
  @ContentChild('filterBar') filterBar: TemplateRef<ElementRef>;

  @Input()
  columns: TableColumn[];

  @Input()
  rowsPerPageOptions: number[] = [15, 100, 500];

  @Input()
  withGlobalFilter: boolean = true;

  @Input()
  value: any[];

  @Input()
  paginator: boolean = true;

  @Input()
  sortField: string;

  @Input()
  sortOrder: number;

  @Input()
  filters: TableFilters = {};

  @Input()
  withButtons: boolean = true;

  @Input()
  withAddButton: boolean = true;

  @Input()
  withEditButton: boolean = true;

  @Input()
  withDeleteButton: boolean = true;

  @Input()
  withReloadButton: boolean = true;

  @Input()
  addDisabled: boolean = false;

  @Input()
  selectionMode: string;

  @Input()
  selected: any;

  @Input()
  lazy: boolean = false;

  @Input()
  totalRecords: number;

  @Input()
  dataKey: string;

  @Input()
  rows: number = 15;

  @Output()
  onAdd: EventEmitter<any> = new EventEmitter();

  @Output()
  onEdit: EventEmitter<any> = new EventEmitter();

  @Output()
  onDelete: EventEmitter<any> = new EventEmitter();

  @Output()
  onSelect: EventEmitter<any> = new EventEmitter();

  @Output()
  onReload: EventEmitter<any> = new EventEmitter();

  @Output()
  onLazyLoad: EventEmitter<any> = new EventEmitter();

  urlPhoto: string;

  constructor(private translateService: TranslateService) {
    super();
  }

  get columnCheckboxPresent() {
    return this.columns.find(column => column.type === TableColumType.CHECKBOX);
  }

  reset() {
    this.selected = undefined;
    this.table.reset();
  }

  showPhoto(event, url: string, overlaypanel: OverlayPanel) {
    this.urlPhoto = url;
    overlaypanel.toggle(event);
  }

  showTooltip(element: HTMLElement) {
    if (element.offsetWidth < element.scrollWidth) {
      element.setAttribute('title', element.textContent);
    } else {
      element.removeAttribute('title');
    }
  }

  showTooltipHeader(element: HTMLElement, column: TableColumn) {
    if (column.abbr && !element.getAttribute('title')) {
      this.translateService.get(column.header)
        .pipe(take(1))
        .subscribe(translation => {
          element.setAttribute('title', translation);
        });
    }
  }

  countVisibleColumns(): number {
    return this.columns.length;
  }

  get(row: any, path: string): any {
    return this.find(row, path.split('.'));
  }

  find(object: any, parts: string[]): any {
    if (Utils.isNotDefined(object)) {
      return '';
    }
    if (parts.length === 1) {
      return object[parts[0]];
    } else {
      return this.find(object[parts.splice(0, 1)[0]], parts);
    }
  }

  viewNewWindow(url) {
    if (url) {
      const windowOpenend: any = window.open(url, '_blank');
    }
  }

}
