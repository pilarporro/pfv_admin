import { Directive, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Message } from 'primeng/api';
import { Unsubscribable } from '../../utils/unsubscribable';
import { AuditableEntityWithPhotosFormGroup } from '../../model/forms/auditable-entity-with-photos-form-group.model';
import { Photo } from '../../model/base/photo.model';
import { GlobalMessageService } from '../../services/global-messages/global-message.service';
import { StorageService } from '../../services/common/storage.service';
import { PhotoFormGroup } from '../../model/forms/photo-form-group.model';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class CommonDetailPhotosComponent extends Unsubscribable implements OnInit {
    @Input()
    form: AuditableEntityWithPhotosFormGroup;

    @Input()
    readonly: boolean = false;

    displayPhotoEditor: boolean = false;
    photoToEdit: Photo;
    photoFormToEdit: FormGroup;
    loading: boolean = false;

    // maintainAspectRatio: boolean = false;
    imageExtensions: string[] = ['jpg', 'jpeg', 'gif', 'png'];

    validExtensions: string[] = [...this.imageExtensions, 'docx', 'pdf'];

    constructor(private messageService: GlobalMessageService, private storageService: StorageService) {
        super();
    }

    ngOnInit() {
        // console.log(this.form.photos);
    }

    addFile(file: Photo) {
        this.loading = false;
        this.form.addPhotoFormGroupFromFile(file);
    }

    getFileType(photo: PhotoFormGroup) {
        if (photo.get('diskName').value) {
            const extension: string = photo.get('diskName').value.split('.').pop().toLowerCase();
            if ((extension.length > 4) || (!this.validExtensions.includes(extension))) {
                return 'file';
            } else if (this.imageExtensions.includes(extension)) {
                return 'image';
            }

            return extension;
        } else {
            return 'file';
        }
    }

    delete(idx: number) {
        this.form.photos.removeAt(idx);
    }

    edit(photo: FormGroup) {
        this.photoFormToEdit = photo;
        this.photoToEdit = photo.getRawValue();
        // this.maintainAspectRatio = photo.get('forLanding').value === true;
        this.displayPhotoEditor = true;
    }

    addCroppedImage(file: Photo) {
        this.photoFormToEdit.patchValue(file);
    }

    clonePhoto(photo: FormGroup) {
        this.storageService.clonePhoto(photo.getRawValue())
            .pipe(take(1))
            .subscribe(cloned => {
                this.form.addPhotoFormGroupFromFile(cloned);
            });
    }

    selectFile() {
        this.loading = true;
    }

    showError(event: Message[]) {
        event.forEach(error => this.messageService.showError(error.detail));
    }

    viewFileNewWindow(photo: PhotoFormGroup) {
        const windowOpenend: any = window.open(`${photo.get('url').value}`, '_blank');
    }

    downloadFile(photo: PhotoFormGroup) {
        const url = `${photo.get('url').value}`;
        const anchor = document.createElement('a');
        anchor.download = photo.get('diskName').value;
        anchor.href = url;
        // anchor.target = '_slef';
        document.body.appendChild(anchor);
        anchor.click();
        setTimeout(() => {
            document.body.removeChild(anchor);
            window.URL.revokeObjectURL(url);
        }, 100);
    }
}
