import { Directive, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { take } from 'rxjs/operators';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { SettingListComponent } from './setting-list/setting-list.component';
import { Observable } from 'rxjs';
import { Entity } from '../model/base/entity.model';
import { Unsubscribable } from '../utils/unsubscribable';
import { TableColumn } from '../model/table/table-column.model';
import { TableFilters } from '../model/table/table-filters.model';
import { GenericCrudService } from '../services/base/generic-crud.service';
import { TranslationsUtilsService } from '../services/common/translations-utils.service';
import { AuthService } from '../services/auth/auth.service';
import { FilterParam } from '../model/table/filter-param.model';
import { Utils } from '../utils/utils';
import { PagedWrapper } from '../model/table/paged-wrapper.model';
import { AccountNote } from '../model/accounting/account-note.model';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class CommonListComponent<T extends Entity> extends Unsubscribable {
    @ViewChild(SettingListComponent) settingList: SettingListComponent;

    isAdmin: boolean = false;

    selectedColumns: TableColumn[];
    columns: TableColumn[];
    entities: T[] = [];
    totalRecords: number = 0;

    // for add purposes:
    newEntity: T;
    // what gets passed to detail:
    detailEntity: T;
    // for edit purposes:
    selectedEntity: T;

    // pop-up control add:
    displayAddDialog: boolean = false;
    displayEditDialog: boolean = false;

    error: any;

    lastEvent: LazyLoadEvent;

    filters: TableFilters = {};
    pageSize: number;

    @Input()
    sortField: string = 'id';

    @Input()
    sortOrder: number = 1;

    @Output()
    sortFieldChange: EventEmitter<string> = new EventEmitter<string>();

    @Output()
    sortOrderChange: EventEmitter<number> = new EventEmitter<number>();

    protected constructor(private genericCrudService: GenericCrudService<T>,
                          protected confirmationService: ConfirmationService,
                          protected translationsUtilsService: TranslationsUtilsService, protected authService?: AuthService) {
        super();
        if (this.authService) {
            this.isAdmin = this.authService.getLoggedUser().admin;
        }
        this.initColumns();
    }

    abstract initColumns();

    select(selected: any) {
        this.selectedEntity = selected;
    }

    paginate(event: LazyLoadEvent) {
        console.log('paginate', event);
        this.lastEvent = event;
        let filterParams: FilterParam[] = [];
        if (event.filters && Object.keys(event.filters).length > 0) {
            this.filters = event.filters;
            this.addDefaultFilters();
        }
        filterParams = Utils.convertTableFiltersToFilterParams(this.filters);
        this.fetchData(((event.first + event.rows) / event.rows) - 1, event.rows, event.sortField, event.sortOrder, filterParams);
    }

    addDefaultFilters() {

    }

    reload() {
        this.refreshData();
    }

    refreshData(): void {
        this.paginate(this.lastEvent);
    }

    abstract createNewEntity(): T;

    showAddDialog(): void {
        this.newEntity = this.createNewEntity();
        this.detailEntity = this.newEntity;
        this.displayAddDialog = true;
    }

    showEditDialog(selectedEntity): void {
        if (selectedEntity) {
            this.selectedEntity = selectedEntity;
        }

        this.detailEntity = this.selectedEntity;
        this.selectedEntity = undefined;
        this.displayEditDialog = true;
    }

    delete(entity: T, entityLabel: string): void {
        this.translationsUtilsService.getDeleteTranslationWithParams(entityLabel)
            .pipe(take(1))
            .subscribe(latestValues => {
                const [title, body] = latestValues;
                this.confirmationService.confirm({
                    message: body,
                    header: title,
                    rejectVisible: true,
                    accept: () => {
                        this.deleteEntity(entity);
                    }
                });
            });
    }

    deleteEntity(entity) {
        this.genericCrudService.delete(entity)
            .pipe(take(1))
            .subscribe(() => {
                    this.entities.splice(this.entities.indexOf(entity, 0), 1);
                    this.settingList.tableComponent.reset();
                    this.selectedEntity = undefined;
                },
                error => {
                    console.log('something went wrong:  $error');
                    this.error = error.error.messages.globalErrors.reasons;
                    // wordt nog aangepast in toekomst.
                });
    }

    protected fetchData(page: number, size: number, sortField?: string, sortOrder?: number, filterParams?: FilterParam[]) {
        this.sortField = sortField; // Necessary because of JAN-17
        this.sortOrder = sortOrder; // Necessary because of JAN-17
        this.sortFieldChange.emit(this.sortField);
        this.sortOrderChange.emit(this.sortOrder);
        this.fetchDataFunction(page, size, sortField, sortOrder, filterParams)
            .pipe(take(1))
            .subscribe(data => {
                this.entities = data.content;
                this.totalRecords = data.totalElements;
                this.afterFetch(page, size, sortField, sortOrder, filterParams);
            });
    }

    afterFetch(page: number, size: number, sortField?: string, sortOrder?: number,
               filterParams?: FilterParam[]) {

    }

    protected fetchDataFunction(page: number, size: number, sortField?: string, sortOrder?: number,
                                filterParams?: FilterParam[]): Observable<PagedWrapper<T>> {
        return this.genericCrudService.listWithPaging(page, size, sortField, sortOrder, filterParams);
    }

    get dialogIsShown() {
        return this.displayAddDialog || this.displayEditDialog;
    }

    set dialogIsShown(dialogShown: boolean) {
        this.displayAddDialog = this.displayEditDialog = dialogShown;
    }

    readNotification(row: T) {
        row.notification = !row.notification;
        this.genericCrudService.update(row)
            .pipe(take(1))
            .subscribe(id => {
                this.entities = [...this.entities];
            });
    }

}
