import { Component, ContentChild, ElementRef, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { TableColumType } from '../../enums/table/table-column-type.enum';
import { TableComponent } from '../table/table.component';
import { TableColumn } from '../../model/table/table-column.model';
import { TableFilters } from '../../model/table/table-filters.model';
import { UserService } from '../../services/persons/user.service';
import { Utils } from '../../utils/utils';
import { OverlayPanel } from 'primeng/overlaypanel';
import { AreaService } from '../../services/common/area.service';
import { KeyLabel } from '../../model/base/key-label.model';
import { MunicipalityService } from '../../services/common/municipality.service';
import { PersonService } from '../../services/persons/person.service';
import { PlaceService } from '../../services/places/place.service';
import { AnimalService } from '../../services/animals/animal.service';
import { AliasIdLabel } from '../../model/base/alias-id-label.model';

@Component({
    selector: 'admin-setting-list',
    templateUrl: './setting-list.component.html',
    styleUrls: ['./setting-list.component.scss']
})
export class SettingListComponent {
    TableColumType: typeof TableColumType = TableColumType;

    @ViewChild(TableComponent) tableComponent: TableComponent;
    @ContentChild('tableExtraButtons') tableExtraButtons: TemplateRef<ElementRef>;

    // tslint:disable-next-line:variable-name
    _columns: TableColumn[];

    @Input()
    set columns(columns: TableColumn[]) {
        this._columns = columns;
        this.dateFilterValues = {};

        columns.forEach(column => {
            if ((column.type === TableColumType.DATE_MEMBER)
                || (column.type === TableColumType.DATETIME) || (column.type === TableColumType.DATE)) {
                this.dateFilterValues[column.field] = [undefined, undefined];
            }
        });
        if (!this.selectedColumns) {
            this.selectedColumns = columns.slice(0, columns.length);
        }

        this.settingFilterValues();
    }

    get columns(): TableColumn[] {
        return this._columns;
    }

    @Input()
    selectedColumns: TableColumn[];

    @Input()
    rowsPerPageOptions: number[] = [15, 100, 500];

    @Input()
    value: any[];

    @Input()
    selectionMode: string = 'single';

    @Input()
    selected: any;

    @Input()
    totalRecords: number;

    @Input()
    withAddButton: boolean = true;

    @Input()
    withEditButton: boolean = true;

    @Input()
    withDeleteButton: boolean = true;

    @Input()
    withReloadButton: boolean = false;

    @Output()
    onSelect: EventEmitter<any> = new EventEmitter();

    @Output()
    onAdd: EventEmitter<any> = new EventEmitter();

    @Output()
    onEdit: EventEmitter<any> = new EventEmitter();

    @Output()
    onReload: EventEmitter<any> = new EventEmitter();

    @Output()
    onDelete: EventEmitter<any> = new EventEmitter();

    @Output()
    onLazyLoad: EventEmitter<any> = new EventEmitter();

    // tslint:disable-next-line:variable-name
    _filters: TableFilters;

    @Input()
    set filters(filters: TableFilters) {
        this._filters = filters;
        this.settingFilterValues();
    }

    get filters(): TableFilters {
        return this._filters;
    }

    filterValues: { [field: string]: any };
    suggestionValues: { [field: string]: any };

    @Input()
    sortField: string;

    @Input()
    sortOrder: number;

    dateFilterValues: { [field: string]: Date[] } = {};
    fieldSelected: string;
    showTimeSelected: boolean;
    newFiltering: Date;

    constructor(private userService: UserService,
                private animalService: AnimalService,
                private placeService: PlaceService,
                private personService: PersonService,
                private areaService: AreaService,
                private municipalityService: MunicipalityService) {

    }

    settingFilterValues() {
        if (this.columns && this.filters) {
            if (Utils.isNotDefined(this.filterValues)) {
                this.filterValues = {};
                this.columns.forEach(column => {
                    this.filterValues[column.field] = this.filters[column.field] ? this.filters[column.field].value : undefined;
                });
            }
            if (Utils.isNotDefined(this.suggestionValues)) {
                this.suggestionValues = {};
                this.columns.forEach(column => {
                    this.suggestionValues[column.field] = [];
                });
            }
        }
    }

    filterByUsername(field: string, value?: any) {
        if (Utils.isDefined(value)) {
            this.tableComponent.table.filter(value.id, field, 'equals');
        } else {
            this.tableComponent.table.filter(undefined, field, 'not_null');
        }
    }

    filterByPerson(field: string, value?: any) {
        if (Utils.isDefined(value)) {
            this.tableComponent.table.filter([value.id], field, 'in');
        } else {
            this.tableComponent.table.filter(undefined, field, 'not_null');
        }
    }

    filterByAnimal(field: string, value?: any) {
        if (Utils.isDefined(value)) {
            this.tableComponent.table.filter([value.id], field, 'in');
        } else {
            this.tableComponent.table.filter(undefined, field, 'not_null');
        }
    }

    filterByPlace(field: string, value?: any) {
        if (Utils.isDefined(value)) {
            this.tableComponent.table.filter([value.id], field, 'in');
        } else {
            this.tableComponent.table.filter(undefined, field, 'not_null');
        }
    }

    filterByMunicipality(field: string, value?: any) {
        if (Utils.isDefined(value)) {
            this.tableComponent.table.filter([value.id], field, 'in');
        } else {
            this.tableComponent.table.filter(undefined, field, 'not_null');
        }
    }

    findByMunicipalityStartsWith(suggestionKey: string, municipality: string) {
        return this.municipalityService.findByAliasStartsWith(municipality).pipe(take(1))
            .subscribe(aliasIdLabels => {
                this.suggestionValues[suggestionKey] = [];
                aliasIdLabels.forEach(aliasIdLabel => {
                    this.suggestionValues[suggestionKey].push(aliasIdLabel);
                });
            });
    }

    findPlacesByAliasStartsWith(suggestionKey: string, alias: string) {
        return this.placeService.findByAliasStartsWith(alias).pipe(take(1))
            .subscribe(aliasIdLabels => {
                this.suggestionValues[suggestionKey] = [];
                aliasIdLabels.forEach(aliasIdLabel => {
                    this.suggestionValues[suggestionKey].push(aliasIdLabel);
                });
            });
    }

    findAnimalsByAliasStartsWith(suggestionKey: string, alias: string) {
        return this.animalService.findByAliasStartsWith(alias).pipe(take(1))
            .subscribe(aliasIdLabels => {
                this.suggestionValues[suggestionKey] = [];
                aliasIdLabels.forEach(aliasIdLabel => {
                    this.suggestionValues[suggestionKey].push(aliasIdLabel);
                });
            });
    }


    findPersonsByAliasStartsWith(suggestionKey: string, alias: string) {
        return this.personService.findByAliasStartsWith(alias).pipe(take(1))
            .subscribe(aliasIdLabels => {
                this.suggestionValues[suggestionKey] = [];
                aliasIdLabels.forEach(aliasIdLabel => {
                    this.suggestionValues[suggestionKey].push(aliasIdLabel);
                });
            });
    }

    findByTypeAndUsernameStartsWith(suggestionKey: string, username: string) {
        const getUsernames: Array<Observable<any>> = new Array<Observable<any>>();
        getUsernames.push(this.userService.findByAliasStartsWith(username));

        forkJoin(getUsernames)
            .pipe(take(1))
            .subscribe((values: any[]) => {
                this.suggestionValues[suggestionKey] = [];
                values.forEach((listUsernamesAndIds: any[]) => {
                    listUsernamesAndIds.forEach(usernameAndId => {
                        this.suggestionValues[suggestionKey].push(usernameAndId);
                    });
                });
            });
    }

    select(selected: any) {
        this.selected = selected;
        this.onSelect.emit(selected);
    }

    showLocalDateTimeFilter(event, field: string, showTimeSelected: boolean, overlaypanel: OverlayPanel) {
        this.fieldSelected = field;
        this.showTimeSelected = showTimeSelected;
        overlaypanel.show(event);
    }

    prepareFilterLocalDateTime(overlaypanel: OverlayPanel) {
        overlaypanel.hide();
        // console.log(this.dateFilterValues[this.fieldSelected]);
        this.tableComponent.table.filter(this.dateFilterValues[this.fieldSelected], this.fieldSelected, 'between');
        this.fieldSelected = undefined;
    }

    changeInitDate(date: Date) {
        if ((this.showTimeSelected) && Utils.isDefined(this.dateFilterValues[this.fieldSelected][0])) {
            date.setHours(0, 0, 0);
            this.dateFilterValues[this.fieldSelected][0] = date;
        }
    }

    changeEndDate(date: Date) {
        if ((this.showTimeSelected) && Utils.isDefined(this.dateFilterValues[this.fieldSelected][1])) {
            date.setHours(23, 59, 59);
            this.dateFilterValues[this.fieldSelected][1] = date;
        }
    }

    getAllAreas(): Observable<KeyLabel[]> {
        return this.callFunctionToGetAliases(this.areaService.findByAliasStartsWith(''));
    }

    getAllPlaces(subtype: string): Observable<KeyLabel[]> {
        if (subtype) {
            return this.callFunctionToGetAliases(this.placeService.findByTypeAndAliasStartsWith(subtype, ''));
        } else {
            return this.callFunctionToGetAliases(this.placeService.findByAliasStartsWith(''));
        }
    }

    callFunctionToGetAliases(observable: Observable<AliasIdLabel[]>): Observable<KeyLabel[]> {
        return observable.pipe(
            take(1),
            map((aliasIdLabels) =>
                aliasIdLabels.map((aliasIdLabel => {
                    return {
                        key: aliasIdLabel.id,
                        label: aliasIdLabel.label
                    };
                }))
            )
        );
    }

    viewNewWindow(url) {
        const windowOpenend: any = window.open(url, '_blank');
    }

}
