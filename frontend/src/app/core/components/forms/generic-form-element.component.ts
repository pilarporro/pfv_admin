import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { Unsubscribable } from '../../utils/unsubscribable';
import { isFunction } from 'rxjs/internal/util/isFunction';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class GenericFormElement extends Unsubscribable {
  @Input()
  inputToFocus: boolean = false;

  @Input()
  withMessage: boolean = true;

  @Input()
  message: string;

  @Input()
  class: string;

  @Input()
  field: FormControl;

  @Input()
  readonly: boolean = false;

  @Input()
  uiSize: number = 10;

  @Input()
  uiLabel: number = 2;

  @Output()
  onChange: EventEmitter<any> = new EventEmitter();

  @Output()
  onSelect: EventEmitter<any> = new EventEmitter();

  get required() {
    return this.hasRequiredField(this.field);
  }

  focus() {

  }

  protected hasRequiredField = (abstractControl: AbstractControl): boolean => {
    if (abstractControl.validator && isFunction(abstractControl.validator)) {
      const validator = abstractControl.validator({} as AbstractControl);
      if (validator && validator.required) {
        return true;
      }
    }

    return false;
  };
}
