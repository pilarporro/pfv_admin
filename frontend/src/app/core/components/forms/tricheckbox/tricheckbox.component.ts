import { Component } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';

@Component({
  selector: 'admin-tricheckbox',
  templateUrl: './tricheckbox.component.html',
  styleUrls: ['./tricheckbox.component.scss']
})
export class TricheckboxComponent extends GenericFormElement {

}
