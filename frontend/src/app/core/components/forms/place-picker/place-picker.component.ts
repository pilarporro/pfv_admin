import { Component } from '@angular/core';
import { GenericPickerComponent } from '../generic-picker/generic-picker.component';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { PickerType } from '../../../enums/picker-type.enum';
import { Place } from '../../../model/places/place.model';
import { PlaceService } from '../../../services/places/place.service';
import { PlaceListComponent } from '../../../../association/places/place-list/place-list.component';

@Component({
  selector: 'admin-place-picker',
  templateUrl: './../generic-picker/generic-picker.component.html',
  styleUrls: ['./place-picker.component.scss']
})
export class PlacePickerComponent extends GenericPickerComponent<Place> {

  constructor(placeService: PlaceService, municipalityService: MunicipalityService) {
    super(placeService, municipalityService, PickerType.PLACE);
  }

  createNewItem(): Place {
    return PlaceListComponent.newPlace(this.municipalityService);
  }
}
