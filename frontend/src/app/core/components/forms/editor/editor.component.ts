import { Component, Input } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';

@Component({
  selector: 'admin-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent extends GenericFormElement {

}
