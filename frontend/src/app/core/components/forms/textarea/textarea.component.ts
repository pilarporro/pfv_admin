import { Component, Input } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';

@Component({
  selector: 'admin-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss']
})
export class TextAreaComponent extends GenericFormElement {
  @Input()
  autoResize: boolean = false;

  @Input()
  showLabel: boolean = true;

  @Input()
  rows: number = 1;

  @Input()
  maxLength: number = 4000;

  @Input()
  placeholder: string;

}
