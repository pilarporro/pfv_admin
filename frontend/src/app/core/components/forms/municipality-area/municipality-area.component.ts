import { Component, OnInit } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { AreaService } from '../../../services/common/area.service';
import { take, takeUntil } from 'rxjs/operators';
import { AliasIdLabel } from '../../../model/base/alias-id-label.model';

@Component({
  selector: 'admin-municipality-area',
  templateUrl: './municipality-area.component.html',
  styleUrls: ['./municipality-area.component.scss']
})
export class MunicipalityAreaComponent extends GenericFormElement implements OnInit {
  areas: AliasIdLabel[];
  area: AliasIdLabel;

  constructor(private areaService: AreaService) {
    super();
  }

  ngOnInit(): void {
    if (this.field.value) {
      this.searchArea(this.field.value);
    }
    this.field.valueChanges.pipe(takeUntil(this.ngUnsubscribe$)).subscribe(value => {
      this.searchArea(value);
    });
  }

  searchArea(value) {
    if (value) {
      this.areaService.getByIdMinimal(value)
          .pipe(take(1))
          .subscribe(area => {
            this.area = area;
          });
    } else {
      this.area = undefined;
    }
  }

  searchAreas(event) {
    this.areaService.findByAliasStartsWith(event.query)
      .pipe(take(1))
      .subscribe(areas => {
        this.areas = areas;
      });
  }

  clear() {
    this.field.setValue(undefined, {emitEvent: false});
  }

  areaSelected(value) {
    if (value) {
      this.field.setValue(value.id, {emitEvent: false});
      this.onSelect.emit(value);
    } else {
      this.clear();
    }
  }
}
