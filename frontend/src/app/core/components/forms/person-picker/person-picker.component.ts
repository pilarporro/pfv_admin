import { Component } from '@angular/core';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { PersonService } from '../../../services/persons/person.service';
import { Animal } from '../../../model/animals/animal.model';
import { Person } from '../../../model/persons/person.model';
import { GenericPickerComponent } from '../generic-picker/generic-picker.component';
import { PickerType } from '../../../enums/picker-type.enum';
import { AnimalListComponent } from '../../../../association/animals/animal-list/animal-list.component';
import { PersonListComponent } from '../../../../association/persons/person-list/person-list.component';

@Component({
  selector: 'admin-person-picker',
  templateUrl: './../generic-picker/generic-picker.component.html',
  styleUrls: ['./person-picker.component.scss']
})
export class PersonPickerComponent extends GenericPickerComponent<Person> {

  constructor(personService: PersonService, municipalityService: MunicipalityService) {
    super(personService, municipalityService, PickerType.PERSON);
  }

  createNewItem(): Person {
    return PersonListComponent.newPerson(this.municipalityService);
  }
}
