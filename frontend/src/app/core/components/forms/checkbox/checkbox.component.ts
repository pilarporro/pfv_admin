import { Component } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';

@Component({
  selector: 'admin-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent extends GenericFormElement {

}
