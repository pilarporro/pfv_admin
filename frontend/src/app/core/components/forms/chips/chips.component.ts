import { Component, Input } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';

@Component({
  selector: 'admin-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss']
})
export class ChipsComponent extends GenericFormElement {

}
