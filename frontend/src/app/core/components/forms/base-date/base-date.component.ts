import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'admin-base-date',
  templateUrl: './base-date.component.html',
  styleUrls: ['./base-date.component.scss'],
  // Onze ControlValueAccessor registreren
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BaseDateComponent),
      multi: true
    }
  ]
})
export class BaseDateComponent implements OnInit, ControlValueAccessor {
  _dateValue: Date;

  @Input()
  showTime: boolean = false;

  @Input()
  disabled: boolean = false;

  @Input()
  showSeconds: boolean = false;

  @Input()
  monthNavigator: boolean = true;

  @Input()
  yearNavigator: boolean = true;

  @Output()
  onChange: EventEmitter<any> = new EventEmitter();

  @Output()
  onSelect: EventEmitter<any> = new EventEmitter();

  es: any;

  // Lege methode voorzien om component 'dirty' of 'touched' te markeren (deze methode wordt later overschreven)
  private _onTouchedCallback: () => {};

  // Lege methode voorzien om changes door te geven die later overschreven wordt
  propagateChange = (_: any) => {
  };

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.es = {
      firstDayOfWeek: 1,
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
      dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril',
        'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
    };
  }

  get dateValue() {
    return this._dateValue;
  }

  set dateValue(val: Date) {
    this._dateValue = val;
    this.propagateChange(this._dateValue);
    this._onTouchedCallback();
  }

  // method that writes a new value from the form model into the view
  writeValue(value: any) {
    this._dateValue = value;
  }

  // method that registers a handler that should be called when something in the view has changed
  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  // this method registers a handler specifically for when a control receives a touch event
  registerOnTouched(fn) {
    this._onTouchedCallback = fn;
  }

  clickDatapickerDate(event: any): void {
    this.focus();
    this.onSelect.emit(event);
  }

  focus() {
    this.elementRef.nativeElement.querySelector('input').focus();
  }

}
