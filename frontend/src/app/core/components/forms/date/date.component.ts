import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { FormControl, FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { map, take } from 'rxjs/operators';
import { AutoComplete } from 'primeng/autocomplete';

@Component({
  selector: 'admin-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent extends GenericFormElement {
  form: FormGroup;

  @Input()
  showTime: boolean = false;

  @Output()
  onSelect: EventEmitter<Date> = new EventEmitter<Date>();

  constructor(private translateService: TranslateService) {
    super();

    this.es = {
      firstDayOfWeek: 1,
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
      dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril',
        'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
    };
  }

  es: any;

}
