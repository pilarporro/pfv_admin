import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { Observable } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';
import { Utils } from '../../../utils/utils';
import { KeyLabel } from '../../../model/base/key-label.model';

@Component({
  selector: 'admin-label-list-dropdown',
  templateUrl: './label-list-dropdown.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./label-list-dropdown.component.scss']
})
export class LabelListDropdownComponent extends GenericFormElement implements OnInit {

  @Input()
  listFunction: Observable<KeyLabel[]>;

  @Input()
  displayChooseMessage: boolean = true;

  @Input()
  multiselect: boolean = false;

  @Input()
  limitedAvailableValues: string[];

  label: string;

  ngOnInit(): void {
    if (this.readonly) {
        this.listFunction
          .pipe(take(1))
          .subscribe(keyLabels => {
            keyLabels.forEach(keyLabel => {
              if (keyLabel.key === this.field.value) {
                this.label = keyLabel.label;
              }
            });
          });
    }
  }
}
