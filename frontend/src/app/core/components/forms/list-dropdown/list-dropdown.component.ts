import { AfterViewInit, Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { AbstractDropdown } from '../abstract-dropdown/abstract-dropdown';
import { Observable } from 'rxjs';
import { KeyLabel } from '../../../model/base/key-label.model';
import { Dropdown } from 'primeng/dropdown';

@Component({
  selector: 'admin-list-dropdown',
  templateUrl: '../abstract-dropdown/abstract-dropdown.html',
  styleUrls: ['../abstract-dropdown/abstract-dropdown.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ListDropdownComponent),
      multi: true
    }
  ]
})
export class ListDropdownComponent extends AbstractDropdown implements OnInit, ControlValueAccessor {
  @Input()
  listFunction: Observable<KeyLabel[]>;

  constructor(translateService: TranslateService) {
    super(translateService);
  }

  ngOnInit() {
    this.listFunction
      .pipe(take(1))
      .subscribe(keyLabels => {
        keyLabels.sort((a, b) => a.label === b.label ? 0 : (a.label > b.label ? 1 : -1));

        keyLabels.forEach(keyLabel => {
          this.allValues.push({value: keyLabel.key, label: keyLabel.label});
          if (keyLabel.key === this._value) {
            if (this.dropdown && this.dropdown.updateSelectedOption) {
              this.dropdown.updateSelectedOption(keyLabel.key);
              this.resetForm();
            }
          }
        });

        this.limitAllValuesAndSort();
      });
  }

}
