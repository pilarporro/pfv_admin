import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { Utils } from '../../../utils/utils';
import { AbstractDropdown } from '../abstract-dropdown/abstract-dropdown';
import { forkJoin, Observable } from 'rxjs';
import { EnumTranslationService } from '../../../services/common/enum-translation.service';
import { KeyLabel } from '../../../model/base/key-label.model';

@Component({
    selector: 'admin-enum-dropdown',
    templateUrl: '../abstract-dropdown/abstract-dropdown.html',
    styleUrls: ['../abstract-dropdown/abstract-dropdown.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EnumDropdownComponent),
            multi: true
        }
    ]
})
export class EnumDropdownComponent extends AbstractDropdown implements OnInit, ControlValueAccessor {

    @Input()
    enumClass: string;

    constructor(translateService: TranslateService, private enumTranslationService: EnumTranslationService) {
        super(translateService);
    }

    ngOnInit() {
        super.ngOnInit();

        // Get the "enum type" based on the string name of the enum (this is some kind of "reflection")
        // The enums need to be put on the window-object manually in main.ts

        const enumType = window[this.enumClass];

        if (enumType === undefined) {
            throw new Error(`The enum with name ${this.enumClass} does not exist on the window object`);
        }

        this.getTranslations(enumType)
            .pipe(take(1))
            .subscribe(keyLabels => {
                keyLabels.sort((a, b) => a.label === b.label ? 0 : (a.label > b.label ? 1 : -1));

                keyLabels.forEach(keyLabel => {
                    this.allValues.push({value: keyLabel.key, label: keyLabel.label});
                    if (keyLabel.key === this._value) {
                        if (this.dropdown && this.dropdown.updateSelectedOption) {
                            this.dropdown.updateSelectedOption(keyLabel.key);
                            this.resetForm();
                        }
                    }
                });

                this.limitAllValuesAndSort();
            });
    }

    getTranslations(enumType): Observable<KeyLabel[]> {
        const observables: Array<Observable<KeyLabel>> = [];

        Object.keys(enumType).forEach(key => {
            if (!Utils.isNotDefined(enumType[key])) {
                observables.push(this.enumTranslationService.getTranslationObjectForEnumValue(this.enumClass, key));
            }
        });

        return forkJoin(observables);
    }
}
