import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'admin-validation-message',
  templateUrl: './validation-message.component.html',
  styleUrls: ['./validation-message.component.scss']
})
export class ValidationMessageComponent {
  errorMessages: any;

  @Input()
  control: FormControl;

  constructor(private translateService: TranslateService) {
    this.errorMessages = {
      default: 'COMMON.VALIDATION.Default',
      required: 'COMMON.VALIDATION.Required',
      minlength: 'COMMON.VALIDATION.MinLength',
      maxlength: 'COMMON.VALIDATION.MaxLength',
      pattern: 'COMMON.VALIDATION.Pattern',
      email: 'COMMON.VALIDATION.Email',
      max: 'COMMON.VALIDATION.Max',
      min: 'COMMON.VALIDATION.Min'
    };
  }

  get hasError() {
    return this.control && this.control.errors && (this.control.dirty || this.control.touched);
  }

  get errorMessage() {
    let message: string = '';
    if (this.control && this.control.errors) {
      const field = Object.keys(this.control.errors)[0];
      message = this.getMessage(field, this.control.errors[field]);
    }

    return message;
  }

  getMessage(type, params) {
    let key: string = this.errorMessages[type];
    let message: string = '';
    if (!key) {
      key = params.key || this.errorMessages['default'];
    }
    this.translateService.get(key, params).subscribe(res => message = res);

    return message;
  }
}
