import { Component, Input } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';

@Component({
  selector: 'admin-input-spinner',
  templateUrl: './input-spinner.component.html',
  styleUrls: ['./input-spinner.component.scss']
})
export class InputSpinnerComponent extends GenericFormElement {
  @Input()
  max: number = 0;
  @Input()
  min: number = 0;
}
