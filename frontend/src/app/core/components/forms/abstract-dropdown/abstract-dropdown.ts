import { Directive, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Unsubscribable } from '../../../utils/unsubscribable';
import { noop } from 'rxjs';
import { Dropdown } from 'primeng/dropdown';
import { MultiSelect } from 'primeng/multiselect';
import { SelectItem } from 'primeng/api';
import { take } from 'rxjs/operators';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractDropdown extends Unsubscribable implements OnInit, OnChanges {

  @Input()
  enumClass: string;

  @Input()
  displayChooseMessage: boolean = true;
  chooseMessage: string = '';
  @Input()
  selectedItemsLabel: string;

  @Input()
  public multiSelectMessage: string;

  @Input()
  field: FormControl;

  @Input()
  limitedAvailableValues: string[];

  @Input()
  editable: boolean = false;

  @Input()
  readonly: boolean = false;

  @Input()
  multiselect: boolean = false;

  @Input()
  maxSelectedLabels: number = 5;

  @Output()
  onChange: EventEmitter<any> = new EventEmitter();

  @ViewChild('basicDropdown', {static: true, read: Dropdown}) dropdown: Dropdown;
  @ViewChild('basicMultiselect', {static: true, read: MultiSelect}) multiselectComponent: MultiSelect;

  items: SelectItem[] = [];
  allValues: SelectItem[] = [];

  // tslint:disable-next-line:variable-name
  protected _value: any;

  // Blank method provided to mark "dirty" or "touched" component (this method will be overwritten later)
  private onTouchedCallback: () => void = noop;

  // Blank method provided to pass on changes that will be overwritten later
  propagateChange = (_: any) => {
  }

  protected constructor(private translateService: TranslateService) {
    super();
  }

  ngOnInit(): void {
    this.getLabelMessageTranslation();
  }

  protected getLabelMessageTranslation() {
    this.translateService.get('COMMON.DROPDOWNS.ChooseMessage').pipe(take(1))
      .subscribe(translation => {
        this.chooseMessage = translation;
      });
    if (!this.multiSelectMessage) {
      this.translateService.get('COMMON.DROPDOWNS.MultiSelectMessage').pipe(take(1))
        .subscribe(translation => {
          this.multiSelectMessage = translation;
        });
    }
    if (!this.selectedItemsLabel) {
      this.translateService.get('COMMON.DROPDOWNS.SelectedItemsLabel').pipe(take(1))
        .subscribe(translation => {
          this.selectedItemsLabel = translation;
        });
    }
  }

  changeValue(value) {
    if (this.field) {
      this.field.setValue(value);
    }
    this.onChange.emit(value);
  }

  selectedValueLabel(): string {
    if (this.multiselect) {
      return (this.value as any[]).map(value => this.getLabelFromValue(value)).join(', ');
      // this.value.split(',')
    } else {
      return this.getLabelFromValue(this.value);
    }
  }

  getLabelFromValue(value: any): string {
    const strValue: string = (typeof value === 'number') ? `${value}` : value;

    const selectedSelectItem: SelectItem = this.allValues.find(selectItem => {
      const strSelectValue: string = (typeof selectItem.value === 'number') ? `${selectItem.value}` : selectItem.value;

      return strSelectValue === strValue;
    });

    return selectedSelectItem ? selectedSelectItem.label : '';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.propertyIsEnumerable('limitedAvailableValues') && (!changes.propertyIsEnumerable('enumClass'))) {
      this.items = [];
      this.limitAllValuesAndSort();
    } else if (changes.propertyIsEnumerable('field') && this.field) {
      /*this.field.valueChanges
        .pipe(takeUntil(this.ngUnsubscribe$))
        .subscribe(value => {
          //this.value = value;

        });*/
    }
  }

  limitAllValuesAndSort() {
    this.allValues.sort((a, b) => a.label === b.label ? 0 : (a.label > b.label ? 1 : -1));

    if (this.limitedAvailableValues) {
      this.items = this.allValues.filter(selectItem => this.limitedAvailableValues.indexOf(selectItem.value) > 0);
    } else {
      this.items = this.allValues;
    }

    if (this.dropdown) {
      if (this.field) {
        this.value = this.field.value;
      } else {
        this.value = this._value;
      }
    } else if (this.multiselectComponent && this.field) {
      this.multiselectComponent.writeValue(this.field.value);
      this.multiselectComponent.updateLabel();
    }
  }

  focus() {
    if (this.dropdown) {
      this.dropdown.applyFocus();
    }
  }

  get value() {
    return this._value;
  }

  set value(value) {
    this._value = value;
    if (this.dropdown) {
      this.dropdown.writeValue(value);
      this.dropdown.updateSelectedOption(value);
    } else if (this.multiselectComponent) {
      this.multiselectComponent.writeValue(value);
      this.multiselectComponent.updateLabel();
    }
    this.propagateChange(this._value);
    // Mark the component as "touched/dirty" so we can use this to show validation errors
    this.onTouchedCallback();
  }

  hasSelectedValue() {
    if (this._value) {
      return true;
    }

    return false;
  }

  // method that writes a new value from the form model into the view
  writeValue(value: any) {
    this._value = value;
  }

  // method that registers a handler that should be called when something in the view has changed
  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  // this method registers a handler specifically for when a control receives a touch event
  registerOnTouched(fn) {
    this.onTouchedCallback = fn;
  }

  resetForm() {
    if (this.field) {
      this.field.parent.markAsUntouched();
      this.field.parent.markAsPristine();
    }
  }

  change(eventValue) {
    if (this.field) {
      this.field.setValue(eventValue);
      this.field.parent.markAsDirty();
      this.field.parent.markAsTouched();
    }
    this.onChange.emit(eventValue);
  }

}
