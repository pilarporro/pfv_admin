import { Component } from '@angular/core';
import { take } from 'rxjs/operators';
import { AnimalService } from '../../../services/animals/animal.service';
import { Utils } from '../../../utils/utils';
import { AliasLabelIdFormGroup } from '../../../model/forms/alias-label-id-form-group.model';
import { Animal } from '../../../model/animals/animal.model';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { AnimalListComponent } from '../../../../association/animals/animal-list/animal-list.component';
import { GenericPickerComponent } from '../generic-picker/generic-picker.component';
import { PickerType } from '../../../enums/picker-type.enum';

@Component({
    selector: 'admin-animal-picker',
    templateUrl: './../generic-picker/generic-picker.component.html',
    styleUrls: ['./animal-picker.component.scss']
})
export class AnimalPickerComponent extends GenericPickerComponent<Animal> {

    constructor(animalService: AnimalService, municipalityService: MunicipalityService) {
        super(animalService, municipalityService, PickerType.ANIMAL);
    }

    createNewItem(): Animal {
        return AnimalListComponent.newAnimal();;
    }
}
