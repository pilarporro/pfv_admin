import { Component, Input, OnInit } from '@angular/core';
import { GenericPickerComponent } from '../generic-picker/generic-picker.component';
import { Activity } from '../../../model/actitivities/activity.model';
import { ActivityService } from '../../../services/activities/activity.service';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { AuthService } from '../../../services/auth/auth.service';
import { PickerType } from '../../../enums/picker-type.enum';
import { ActivityListComponent } from '../../../../activities/activity-list/activity-list.component';
import { ActivityType } from '../../../enums/activity-type.enum';
import { Colony } from '../../../model/animals/colony.model';
import { ColonyService } from '../../../services/animals/colony.service';
import { ColonyListComponent } from '../../../../association/colonies/colony-list/colony-list.component';

@Component({
  selector: 'admin-colony-picker',
  templateUrl: './../generic-picker/generic-picker.component.html',
  styleUrls: ['./colony-picker.component.scss']
})
export class ColonyPickerComponent extends GenericPickerComponent<Colony> {

  constructor(colonyService: ColonyService, municipalityService: MunicipalityService, private authService: AuthService) {
    super(colonyService, municipalityService, PickerType.COLONY);
  }

  createNewItem(): Colony {
    return ColonyListComponent.newColony(this.municipalityService, this.idParent);
  }
}
