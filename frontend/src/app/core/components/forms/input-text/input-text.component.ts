import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { Utils } from '../../../utils/utils';

@Component({
    selector: 'admin-input-text',
    templateUrl: './input-text.component.html',
    styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent extends GenericFormElement {
    @ViewChild('inputElement') inputElement: ElementRef;
    @Input()
    isCurrency: boolean = false;
    @Input()
    isInteger: boolean = false;
    @Input()
    isDate: boolean = false;
    @Input()
    maxLength: number = 0;
    @Input()
    placeholder: string;
    @Input()
    isPassword: boolean = false;
    @Input()
    suffix: string;
    @Input()
    autocomplete: string = `new-${Utils.getRandomNum(1, 1000)}`;

    focus() {
        if (this.inputElement) {
            this.inputElement.nativeElement.focus();
        }
    }
}
