import { Component, Input, OnInit } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { Utils } from '../../../utils/utils';
import { EnumTranslationService } from '../../../services/common/enum-translation.service';
import { KeyLabel } from '../../../model/base/key-label.model';

@Component({
  selector: 'admin-select-button',
  templateUrl: './select-button.component.html',
  styleUrls: ['./select-button.component.scss']
})
export class SelectButtonComponent extends GenericFormElement implements OnInit {
  @Input()
  enumClass: string;

  @Input()
  options: SelectItem[];

  constructor(private translateService: TranslateService, private enumTranslationService: EnumTranslationService) {
    super();
  }

  ngOnInit() {
    if (!this.options) {
      this.initOptions();
    }
  }

  changeAnswer(value) {
    this.onChange.emit(value);
  }

  getReadonlyLabel(): string {
    if (this.options) {
      if (this.field.value !== undefined && this.field.value !== null) {
        return this.field.value === this.options[0].value ? this.options[0].label : this.options[1].label;
      }
    }

    return '';
  }

  initOptions() {
    if (this.enumClass) {
      const enumType = window[this.enumClass];

      if (enumType === undefined) {
        throw new Error(`The enum with name ${this.enumClass} does not exist on the window object`);
      }
      this.getTranslations(enumType)
        .pipe(take(1))
        .subscribe(keyLabels => {
          this.options = keyLabels.map(keyLabel => {
            return {label: keyLabel.label, value: keyLabel.key};
          });
        });
    }
  }

  getTranslations(enumType): Observable<KeyLabel[]> {
    const observables: Array<Observable<KeyLabel>> = [];

    Object.keys(enumType).forEach(key => {
      if (!Utils.isNotDefined(enumType[key])) {
        observables.push(this.enumTranslationService.getTranslationObjectForEnumValue(this.enumClass, key));
      }
    });

    return forkJoin(observables);
  }
}
