import { AfterViewInit, Directive, Input, OnInit, ViewChild } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { take } from 'rxjs/operators';
import { AliasIdLabel } from '../../../model/base/alias-id-label.model';
import { Utils } from '../../../utils/utils';
import { FormArray, FormControl } from '@angular/forms';
import { AliasLabelIdFormGroup } from '../../../model/forms/alias-label-id-form-group.model';
import { AutoComplete } from 'primeng/autocomplete';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { Entity } from '../../../model/base/entity.model';
import { GenericCrudService } from '../../../services/base/generic-crud.service';
import { PickerType } from '../../../enums/picker-type.enum';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class GenericPickerComponent<T extends Entity> extends GenericFormElement implements OnInit, AfterViewInit {
    PickerType: typeof PickerType = PickerType;

    @ViewChild(AutoComplete) autoComplete: AutoComplete;

    items: AliasIdLabel[] = [];

    @Input()
    multiple: boolean = false;

    @Input()
    fieldMultiple: FormArray;

    @Input()
    idParent: string;

    @Input()
    aliasLabelIdFormGroup: AliasLabelIdFormGroup;

    inputComplete: boolean = false;
    item: AliasIdLabel;
    itemsSelected: AliasIdLabel[];

    newItem: T;
    isNewItem: boolean;
    dialogNewItemIsShown: boolean = false;

    constructor(protected service: GenericCrudService<T>, protected municipalityService: MunicipalityService, public type: PickerType) {
        super();
    }

    showNewItem() {
        this.isNewItem = true;
        this.newItem = this.createNewItem();
        this.dialogNewItemIsShown = true;
    }

    abstract createNewItem(): T;

    newItemCreated(itemCreated: T) {
        if (this.isNewItem) {
            this.service.getByIdMinimal(itemCreated.id)
                .pipe(take(1))
                .subscribe(item => {
                    if (this.multiple) {
                        this.itemsSelected = [...this.itemsSelected, item];
                    } else {
                        this.item = item;
                    }
                    this.itemSelected(item);
                    if (this.multiple) {
                        this.autoComplete.writeValue(this.itemsSelected);
                    }
                });
        }
    }

    get required() {
        if (this.multiple) {
            return this.hasRequiredField(this.fieldMultiple);
        } else {
            return this.hasRequiredField(this.field);
        }
    }

    ngAfterViewInit(): void {
        /* Trick to make the value appears */
        this.autoComplete.hide();
        this.autoComplete.onOverlayAnimationDone = (event) => {
        };
    }

    ngOnInit(): void {
        if (!this.multiple) {
            this.field = this.aliasLabelIdFormGroup.get('id') as FormControl;
            if (this.field.value && Utils.isDefined(this.field.value)) {
                this.service.getByIdMinimal(this.field.value)
                    .pipe(take(1))
                    .subscribe(item => {
                        this.item = item;
                    });
            }
        } else {
            // //         console.log(this.fieldMultiple.value);
            this.itemsSelected = this.fieldMultiple.value;
        }
    }

    searchItems(event) {
        //         console.log('Search Items');
        this.service.findByAliasStartsWith(event.query)
            .pipe(take(1))
            .subscribe(items => {
                this.items = items;
            });
    }

    clear() {
        //         console.log('clear');

        if (this.multiple) {
            this.setMultipleValueToForm();
        } else {
            this.aliasLabelIdFormGroup.patchValue({
                alias: undefined,
                id: undefined
            }, {emitEvent: false});
        }
    }

    setMultipleValueToForm() {
        this.fieldMultiple.clear();
        this.itemsSelected.forEach(item => {
            const aliasLabelIdFormGroup = new AliasLabelIdFormGroup();
            aliasLabelIdFormGroup.patchEntity(item);
            this.fieldMultiple.push(aliasLabelIdFormGroup);
        });
    }

    itemSelected(item) {
        //         console.log('itemSelected');
        //         console.log(item);
        if (item) {
            if (this.multiple) {
                if (this.itemsSelected.includes(item)) {

                }
                this.setMultipleValueToForm();
            } else {
                this.aliasLabelIdFormGroup.patchEntity(item);

            }
            this.onSelect.emit(item);

        } else {
            this.clear();
        }
        //         console.log(this.aliasLabelIdFormGroup);

    }

    editItem(id) {
        this.isNewItem = false;
        this.service.getById(id).pipe(take(1))
            .subscribe(response => {
                this.newItem = response;
                this.dialogNewItemIsShown = true;
            });
    }
}
