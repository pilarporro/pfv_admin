import { Component } from '@angular/core';
import { GenericPickerComponent } from '../generic-picker/generic-picker.component';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { PickerType } from '../../../enums/picker-type.enum';
import { ActivityService } from '../../../services/activities/activity.service';
import { Activity } from '../../../model/actitivities/activity.model';
import { ActivityListComponent } from '../../../../activities/activity-list/activity-list.component';
import { ActivityType } from '../../../enums/activity-type.enum';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'admin-activity-picker',
  templateUrl: './../generic-picker/generic-picker.component.html',
  styleUrls: ['./activity-picker.component.scss']
})
export class ActivityPickerComponent extends GenericPickerComponent<Activity> {

  constructor(activityService: ActivityService, municipalityService: MunicipalityService, private authService: AuthService) {
    super(activityService, municipalityService, PickerType.ACTIVITY);
  }

  createNewItem(): Activity {
    return ActivityListComponent.newActivity(ActivityType.BUY_SUPPLIES, this.authService);
  }
}
