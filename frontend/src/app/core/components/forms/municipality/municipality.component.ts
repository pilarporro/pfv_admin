import { Component, OnInit, ViewChild } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { MunicipalityService } from '../../../services/common/municipality.service';
import { AutoComplete } from 'primeng/autocomplete';
import { take, takeUntil } from 'rxjs/operators';
import { AliasIdLabel } from '../../../model/base/alias-id-label.model';

@Component({
    selector: 'admin-municipality',
    templateUrl: './municipality.component.html',
    styleUrls: ['./municipality.component.scss']
})
export class MunicipalityComponent extends GenericFormElement implements OnInit {
    @ViewChild('autocompleteElement') autocompleteElement: AutoComplete;

    municipalities: AliasIdLabel[];
    inputComplete: boolean = false;
    municipality: AliasIdLabel;

    constructor(private municipalityService: MunicipalityService) {
        super();
    }

    ngOnInit(): void {
        if (this.field.value) {
            this.searchMunicipality(this.field.value);
        }
        this.field.valueChanges.pipe(takeUntil(this.ngUnsubscribe$)).subscribe(value => {
            this.searchMunicipality(value);
        });
    }

    searchMunicipality(value) {
        if (value) {
            this.municipalityService.getByIdMinimal(this.field.value)
                .pipe(take(1))
                .subscribe(municipality => {
                    this.municipality = municipality;
                });
        } else {
            this.municipality = undefined;
        }
    }

    searchMunicipalities(event) {
        this.municipalityService.findByAliasStartsWith(event.query)
            .pipe(take(1))
            .subscribe(municipalities => {
                this.municipalities = municipalities;
            });
    }

    clear() {
        this.field.setValue(undefined, {emitEvent: false});
    }

    focus() {
        if (this.autocompleteElement) {
            // console.log('focus mun');
            this.autocompleteElement.focusInput();
        }
    }

    municipalitySelected(value) {
        if (value) {
            this.field.setValue(value.id, {emitEvent: false});
            this.onSelect.emit(value);
        } else {
            this.clear();
        }
    }

    focusComponent() {
        if (this.inputComplete === true) {
            this.onSelect.emit(this.field.value);
            this.inputComplete = false;
        }
    }
}
