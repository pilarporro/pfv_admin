import { Component, Input, OnInit } from '@angular/core';
import { GenericFormElement } from '../generic-form-element.component';
import { forkJoin, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { EnumPipe } from '../../../pipes/enum.pipe';

@Component({
  selector: 'admin-label-enum-dropdown',
  templateUrl: './label-enum-dropdown.component.html',
  styleUrls: ['./label-enum-dropdown.component.scss']
})
export class LabelEnumDropdownComponent extends GenericFormElement implements OnInit {
  @Input()
  enumClass: string;

  @Input()
  displayChooseMessage: boolean = true;

  @Input()
  multiselect: boolean = false;

  @Input()
  limitedAvailableValues: string[];

  multipleValue: string;

  constructor(private enumPipe: EnumPipe) {
    super();
  }

  ngOnInit(): void {
    if (this.multiselect && this.readonly) {
      const values: string[] = this.field.value ? (this.field.value as string[]) : [];
      const observables: Array<Observable<string>> = [];

      values.forEach(value => {
        observables.push(this.enumPipe.transform(value, this.enumClass));
      });

      forkJoin(observables).pipe(take(1))
        .subscribe(responses => {
          this.multipleValue = responses.join(', ');
        });
    }
  }

}
