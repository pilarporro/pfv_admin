import { HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse } from '@angular/common/http';
import { Component, ContentChild, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { finalize, takeUntil } from 'rxjs/operators';
import { Unsubscribable } from '../../../utils/unsubscribable';
import { FileUploadMode } from '../../../model/file/file-upload-mode.enum';
import { StorageService } from '../../../services/common/storage.service';
import { FileSelectEvent } from '../../../model/file/file-select-event.model';
import { Message } from 'primeng/api';
import { FileUpload } from 'primeng/fileupload';

@Component({
  selector: 'admin-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent extends Unsubscribable {

  @Input()
  mode: FileUploadMode = FileUploadMode.Basic;

  @Input()
  accept: string = '*';

  @Input()
  disabled: boolean = false;

  @Input()
  auto: boolean;

  @Input()
  maxFileSize: number = 50000000;

  @Output()
  upload: EventEmitter<Storage> = new EventEmitter<Storage>();

  // tslint:disable-next-line:no-output-native
  @Output()
  select: EventEmitter<File> = new EventEmitter<File>();

  // tslint:disable-next-line:no-output-native
  @Output()
  error: EventEmitter<Message[]> = new EventEmitter<Message[]>();

  @ViewChild(FileUpload) fileUpload: FileUpload;

  @ContentChild('toolbar')
  toolbar: TemplateRef<any>;

  uploadFile: {
    file: File;
    progress: number;
  };

  fileUploadMode: typeof FileUploadMode = FileUploadMode;

  constructor(private storageService: StorageService) {
    super();
  }

  selectFile(event: FileSelectEvent) {
    // If there is an upload error, we'll display it outside the component, to make it look pretty
    // and readable
    if (this.fileUpload.msgs.length > 0) {
      this.error.emit(this.fileUpload.msgs);
    } else {
      this.uploadFile = null;
      this.select.emit(event.files[0]);
    }
  }

  handleUpload(event: { files: File[] }, fileUpload: FileUpload) {
    this.uploadFile = {
      file: event.files[0],
      progress: 0
    };
    this.storageService.upload(this.uploadFile.file).pipe(
      takeUntil(this.ngUnsubscribe$),
      finalize(() => {
        fileUpload.clear();
        this.uploadFile.progress = null;
      })
    ).subscribe((httpEvent: HttpEvent<any>) => {
      if (httpEvent.type === HttpEventType.UploadProgress) {
        this.handleProgress(httpEvent);
      } else if (httpEvent instanceof HttpResponse) {
        this.handleSuccess(httpEvent, fileUpload);
      }
    });
  }

  private handleProgress(event: HttpProgressEvent) {
    const percentDone = Math.round((event.loaded * 100) / event.total);

    this.uploadFile.progress = percentDone;
  }

  private handleSuccess(event: HttpResponse<Storage>, fileUpload: FileUpload) {
    this.upload.emit(event.body);
  }

}
