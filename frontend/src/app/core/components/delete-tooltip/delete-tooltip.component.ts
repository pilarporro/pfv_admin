import { Component, Input, OnChanges } from '@angular/core';
import { OperationsInfoEntity } from '../../model/base/operations-info-entity.model';

@Component({
  selector: 'admin-delete-tooltip',
  templateUrl: './delete-tooltip.component.html',
  styleUrls: ['./delete-tooltip.component.scss']
})
export class DeleteTooltipComponent implements OnChanges {

  @Input()
  entity: OperationsInfoEntity;
  @Input()
  possition?: string = 'top';
  @Input()
  overrideMessage?: string = undefined;
  @Input()
  ovverideDisableCondition?: boolean = undefined;

  disabled: boolean = true;

  displayMessage: string;

  constructor() {
  }

  ngOnChanges() {
    this.renderTooltip();
  }

  renderTooltip() {
    // do we have a defined entity?
    if (!this.entity) {
      this.disabled = true; // if its not defined hide tooltip
    } else {
      // its defined, does our entity have operationsInfo?
      if (this.entity.operationsInfo) {
        // it has operationsinfo
        if (this.entity.operationsInfo.delete.allowed) {
          // it can be deleted, show nothing
          this.disabled = true;
        } else {
          // it cant be deleted, lets show why
          this.displayMessage = this.generateOperationsInfoMessage();
          this.disabled = false;
        }
      } else {
        this.disabled = true;
      }
    }
  }

  generateOperationsInfoMessage(): string {
    let message = '';
    if (this.overrideMessage) {
      message = this.overrideMessage;
    } else {
      this.entity.operationsInfo.delete.reasons.forEach(reason => message += (`${reason} `));
    }

    return message;
  }
}
