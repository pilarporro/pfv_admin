import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { LocationFormGroup } from '../../model/forms/location-form-group.model';
import { MunicipalityService } from '../../services/common/municipality.service';
import { take } from 'rxjs/operators';
import PlaceResult = google.maps.places.PlaceResult;
import { Address } from '../google-places-autocomplete/objects/address';

@Component({
    selector: 'admin-common-location-data',
    templateUrl: './common-location-data.component.html',
    styleUrls: ['./common-location-data.component.scss']
})
export class CommonLocationDataComponent implements AfterViewInit {
    @ViewChild('googlePicker') googleText: any;

    @Input()
    form: LocationFormGroup;

    @Input()
    readonly: boolean = false;

    options = {
        types: [],
        componentRestrictions: {country: 'ES'}
    };


    constructor(private municipalityService: MunicipalityService) {
    }

    ngAfterViewInit(): void {
      /*
        this.municipalityService.autocomplete = new google.maps.places.Autocomplete(this.googleText.nativeElement, this.options);
        google.maps.event.addListener(this.municipalityService.autocomplete, 'place_changed', () => {
            const place = this.municipalityService.autocomplete.getPlace();
            this.onLocationSelected(place);
        });*/
    }

    onLocationSelected(address: Address) {
        if (address) {
            if (address.address_components) {
                address.address_components.forEach((component) => {
                    if (component.types.length > 0) {
                        switch (component.types[0]) {
                            case 'route':
                                this.form.get('street').setValue(component.long_name);
                                break;
                            case 'street_number':
                                this.form.get('number').setValue(component.long_name);
                                break;
                            case 'postal_code':
                                this.form.get('postalCode').setValue(component.long_name);
                                break;
                            case 'locality':
                                this.municipalityService.getByNameMinimal(component.long_name).pipe(take(1))
                                    .subscribe(response => {
                                        if (response) {
                                            this.form.get('municipalityId').setValue(response.id);
                                            if (this.municipalityService.defaultMunicipalityId !== response.id) {
                                                this.form.get('areaId').setValue(null);
                                            }
                                        } else {
                                            this.form.get('municipalityId').setValue(null);
                                            this.form.get('areaId').setValue(null);
                                        }
                                    });
                                break;
                        }
                    }
                });
            }

            if (address.geometry && address.geometry.location) {
                this.form.get('geoLocation').setValue([
                    address.geometry.location.lat(),
                    address.geometry.location.lng()
                ]);
            } else {
                this.form.get('geoLocation').setValue(undefined);
            }

            this.form.get('placeId').setValue(address.place_id);
            this.form.get('url').setValue(address.url);

        } else {
            this.form.patchValue({});
        }
    }

    viewNewWindow() {
        const windowOpenend: any = window.open(`${this.form.get('url').value}`, '_blank');
    }
}
