import { Component, Input } from '@angular/core';

@Component({
  selector: 'admin-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {
  @Input()
  display: boolean = true;

  @Input()
  message: string;

  @Input()
  severity: string = 'error';
}
