import { Component, Input } from '@angular/core';

@Component({
  selector: 'admin-image-loader',
  templateUrl: './image-loader.component.html',
  styleUrls: ['./image-loader.component.scss']
})
export class ImageLoaderComponent {
  @Input()
  src: string;

  @Input()
  title: string;

  loading: boolean = true;
  photoIsFound: boolean = true;

  constructor() {
  }

  endLoad() {
    this.loading = false;
  }

  errorLoad() {
    this.loading = false;
    this.photoIsFound = false;
  }

}
