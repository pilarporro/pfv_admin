export enum PaymentPeriodicity {
  MONTLHY = 'MONTLHY',
  BIANNUAL = 'BIANNUAL',
  THREEMONTHS = 'THREEMONTHS',
  ANNUAL = 'ANNUAL',
  OTHER = 'OTHER'
}
