export enum VisitClinicType {
  REVISION = 'REVISION',
  OTHER = 'OTHER',
  SURGERY = 'SURGERY',
  HANDING_OVER = 'HANDING_OVER'
}
