export enum PickerType {
  ANIMAL = 'ANIMAL',
  ACTIVITY = 'ACTIVITY',
  PERSON = 'PERSON',
  PLACE = 'PLACE',
  COLONY = 'COLONY'

}
