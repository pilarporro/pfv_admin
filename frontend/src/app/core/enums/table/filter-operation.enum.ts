export enum FilterOperation {
  Equals = 'EQUALS',
  NotEquals = 'NOT_EQUALS',
  Contains = 'CONTAINS',
  In = 'IN',
  StartsWith = 'STARTS_WITH',
  Between = 'BETWEEN',
  EndsWith = 'ENDS_WITH',
  GreaterThanOrEquals = 'GREATER_THAN_OR_EQUALS',
  LessThanOrEquals = 'LESS_THAN_OR_EQUALS',
  NotNull = 'NOT_NULL'
}
