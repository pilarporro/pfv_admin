export enum AccountType {
  BANK = 'BANK',
  CASH = 'CASH',
  PUBLIC_GRANT = 'PUBLIC_GRANT'
}
