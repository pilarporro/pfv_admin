import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Person } from '../../model/persons/person.model';
import { Place } from '../../model/places/place.model';
import { Observable } from 'rxjs';
import { AliasIdLabel } from '../../model/base/alias-id-label.model';
import { PlaceType } from '../../enums/place-type.enum';

@Injectable({
  providedIn: 'root'
})
export class PlaceService extends LoadingHttpCrudService<Place> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'places', environment);
  }

  findByTypeAndAliasStartsWith(type: string, alias: string): Observable<AliasIdLabel[]> {
    const params: HttpParams = new HttpParams()
        .set('type', `${type}`)
        .set('alias', `${alias}`);

    return this.http.get<AliasIdLabel[]>(`${this.endpoint}/by-type-and-alias-starts-with`, {params});
  }
}
