import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalMessageService } from './global-message.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class GlobalErrorInterceptor implements HttpInterceptor {

  constructor(private globalNotificationService: GlobalMessageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap(
      (event: HttpEvent<any>) => {
      },
      (error: any) => {
        if (error instanceof HttpErrorResponse) {
          if ((error.status !== 401)
            && (req.url.indexOf('admin.paysale.com') < 0)
            && (req.url.indexOf('offer.advendor.net') < 0)
            && (req.url.indexOf('tnccv.com') < 0)) {
            this.globalNotificationService.showError(error);
          }
        }
      }
    ));
  }
}
