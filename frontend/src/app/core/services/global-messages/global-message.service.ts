import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpErrorResponse } from '@angular/common/http';
import { forkJoin, Observable, Subject } from 'rxjs';
import { Notification } from '../../model/global-messages/notification.model';
import { NotificationType } from '../../model/global-messages/notification-type.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalMessageService {

  private defaultDuration: number = 3000;

  private notificationSubject: Subject<Notification> = new Subject<Notification>();

  private globalErrorSubject: Subject<String | HttpErrorResponse> = new Subject<String | HttpErrorResponse>();

  private lastHttpGlobalErrorReceived: HttpErrorResponse;
  private lastHttpGlobalErrorReceivedTimestamp: number;

  private lastStringGlobalErrorReceived: string;
  private lastStringGlobalErrorReceivedTimestamp: number;

  constructor(private translateService: TranslateService) {
  }

  getNotification(): Observable<Notification> {
    return this.notificationSubject.asObservable();
  }

  getGlobalError(): Observable<any> {
    return this.globalErrorSubject.asObservable();
  }

  showNotification(type: NotificationType, titleKey: string, messageKey: string, duration?: number, sticky?: boolean) {
    forkJoin(
      this.translateService.get(titleKey),
      this.translateService.get(messageKey)
    ).subscribe((values: [string, string]) => {
      const [title, message] = values;
      this.showNotificationWithText(type, title, message, duration, sticky);
    });
  }

  showNotificationWithText(type: NotificationType, title: string, message: string, duration?: number, sticky?: boolean) {
    const notification = new Notification(
      type,
      title,
      message,
      duration !== undefined ? duration : this.defaultDuration,
      sticky !== undefined ? sticky : false);
    this.notificationSubject.next(notification);
  }

  showSaveSuccessfulNotication() {
    this.showNotification(
      NotificationType.SUCCESS,
      'COMMON.NOTIFICATIONS.SavedSuccessfulTitle',
      'COMMON.NOTIFICATIONS.SavedSuccessfulMessage');
  }

  showError(error: any): void {
    if (error instanceof HttpErrorResponse) {
      if ((error as HttpErrorResponse).status === 403) {
        this.showNotification(NotificationType.ERROR, 'COMMON.ERRORS.AccessDeniedTitle',
          'COMMON.ERRORS.AccessDeniedMessage', undefined, true);
      } else {
        this.handleHttpErrorResponse(error);
      }
    } else if (typeof error === 'string') {
      this.handleStringError(error);
    } else {
      alert(`Unkown error: ${error}`);
    }
  }

  private handleHttpErrorResponse(error: HttpErrorResponse) {
    const currentErrorTimestamp = new Date().getTime();

    /**
     * Throw the error-event only in the following cases to avoid duplicate growls:
     * - if there was no previous error
     * - if the previous error occurred more than a second ago
     * - if the previous error had a different statuscode
     */
    if (!this.lastHttpGlobalErrorReceivedTimestamp || currentErrorTimestamp - this.lastHttpGlobalErrorReceivedTimestamp > 1000 || this.lastHttpGlobalErrorReceived.status !== error.status) {
      // Add error to subject, this will show our error growl pop-up
      this.globalErrorSubject.next(error);
    } else {
      console.log('Ignored the following error', error);
    }

    this.lastHttpGlobalErrorReceived = error;
    this.lastHttpGlobalErrorReceivedTimestamp = currentErrorTimestamp;

    if (error.status === 404) {
      // console.log('Not found ...');
    }
    if (error.status === 401 || error.status === 403) {
      // console.log('Auth failure ...');
    }
  }

  private handleStringError(error: string) {
    const currentErrorTimestamp = new Date().getTime();

    /**
     * Throw the error-event only in the following cases to avoid duplicate growls:
     * - if there was no previous error
     * - if the previous error occurred more than a second ago
     * - if the previous error had a different statuscode
     */
    if (!this.lastStringGlobalErrorReceivedTimestamp || currentErrorTimestamp - this.lastStringGlobalErrorReceivedTimestamp > 1000 || this.lastStringGlobalErrorReceived !== error) {
      // Add error to subject, this will show our error growl pop-up
      this.globalErrorSubject.next(error);
    } else {
      console.log('Ignored the following error', error);
    }

    this.lastStringGlobalErrorReceived = error;
    this.lastStringGlobalErrorReceivedTimestamp = currentErrorTimestamp;
  }

}
