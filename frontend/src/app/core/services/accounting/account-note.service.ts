import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Animal } from '../../model/animals/animal.model';
import { AccountNote } from '../../model/accounting/account-note.model';

@Injectable({
  providedIn: 'root'
})
export class AccountNoteService extends LoadingHttpCrudService<AccountNote> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'account-notes', environment);
  }
}
