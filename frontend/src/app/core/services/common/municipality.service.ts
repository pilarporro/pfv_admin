import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Observable } from 'rxjs';
import { Municipality } from '../../model/municipalities/municipality.model';
import { AliasIdLabel } from '../../model/base/alias-id-label.model';
import { take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MunicipalityService extends LoadingHttpCrudService<Municipality> {
    private municipalityId: string;
    autocomplete: google.maps.places.Autocomplete;

    constructor(http: HttpClient, @Inject('environment') environment) {
        super(http, 'municipalities', environment);
    }

    getByNameMinimal(name: string): Observable<AliasIdLabel> {
        return this.http.get<AliasIdLabel>(`${this.endpoint}/name-minimal/${name}`);
    }

    get defaultMunicipalityId(): string {
        return this.municipalityId;
    }

    set defaultMunicipalityId(municipalityId: string) {
        this.municipalityId = municipalityId;
    }

    loadDefaultMunicipality(): Observable<Municipality> {
        return this.http.get<Municipality>(`${this.endpoint}/default`);
    }

    findByAliasStartsWith(alias: string): Observable<AliasIdLabel[]> {
        const params: HttpParams = new HttpParams()
            .set('name', `${alias}`);

        return this.http.get<AliasIdLabel[]>(`${this.endpoint}`, {params});
    }

    findByArea(municipalityId: string, name: string): Observable<AliasIdLabel[]> {
        const params: HttpParams = new HttpParams()
            .set('name', `${name}`);

        return this.http.get<AliasIdLabel[]>(`${this.endpoint}/${municipalityId}/areas`, {params});
    }
}
