import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../base/base-http.service';
import { KeyLabel } from '../../model/base/key-label.model';

@Injectable({
    providedIn: 'root'
})
/**
 * This service can be used to fetch translations for enums from the backend.
 * The list of translations is cached in the service.
 */
export class EnumTranslationService extends BaseHttpService {

    /* Keep a "cache" per enumType (which is the key of this object) of the list of enum translations */
    private translationsReplaySubjectMap: { string?: ReplaySubject<KeyLabel[]> } = {};

    /* Keep a "cache" for the mapping between enum types <-> their corresponding URL-part */
    private enumToUrlMappingReplaySubject: ReplaySubject<{ string?: string }>;

    constructor(http: HttpClient, @Inject('environment') environment) {
        super(http, 'enums', environment);
    }

    getTranslationForEnumValue(enumClass: string, enumValue: string): Observable<string> {
        /**
         * Return a new Observable to either get the translation from the cache, or if it's not in the cache,
         * call the method getTranslationsForEnum which will do the GET-call and fill the cache
         */
        return new Observable(observer => {
            if (this.translationsReplaySubjectMap[enumClass] !== undefined) {
                this.translationsReplaySubjectMap[enumClass]
                    .subscribe(data => {
                        observer.next(this.getLabelFromKeyLabelList(data, enumValue));
                        observer.complete();
                    });
            } else {
                this.getTranslationListForEnumClass(enumClass)
                    .subscribe(data => {
                        observer.next(this.getLabelFromKeyLabelList(data, enumValue));
                        observer.complete();
                    });
            }
        });
    }

    getTranslationObjectForEnumValue(enumClass: string, enumValue: string): Observable<KeyLabel> {
        /**
         * Return a new Observable to either get the translation from the cache, or if it's not in the cache,
         * call the method getTranslationsForEnum which will do the GET-call and fill the cache
         */
        return new Observable(observer => {
            if (this.translationsReplaySubjectMap[enumClass] !== undefined) {
                this.translationsReplaySubjectMap[enumClass]
                    .subscribe(data => {
                        observer.next(this.getKeyLabelFromKeyLabelList(data, enumValue));
                        observer.complete();
                    });
            } else {
                this.getTranslationListForEnumClass(enumClass)
                    .subscribe(data => {
                        observer.next(this.getKeyLabelFromKeyLabelList(data, enumValue));
                        observer.complete();
                    });
            }
        });
    }

    private getTranslationListForEnumClass(enumClass: string): Observable<KeyLabel[]> {
        /* Provide a ReplaySubject for this enumType to be able to "cache" the results of the GET-call */
        if (this.translationsReplaySubjectMap[enumClass] === undefined) {
            this.translationsReplaySubjectMap[enumClass] = new ReplaySubject<KeyLabel[]>(1);
        }

        return new Observable(observer => {
            /* Fetch the URL-mapping from the cache, or if there is no cache yet, fetch the mapping from the backend */
            if (this.enumToUrlMappingReplaySubject) {
                this.enumToUrlMappingReplaySubject.subscribe(mappingData => {
                    /* Execute the backend call to fetch the translations for this specific enumType */
                    this.http.get<KeyLabel[]>(`${this.endpoint}/${enumClass}`)
                        .subscribe(data => {
                            /* Fill the "cache" */
                            this.translationsReplaySubjectMap[enumClass].next(data);

                            observer.next(data);
                            observer.complete();
                        });
                });
            } else {
                this.getEnumToUrlMapping()
                    .subscribe(mappingData => {
                        /* Execute the backend call to fetch the translations for this specific enumType */
                        this.http.get<KeyLabel[]>(`${this.endpoint}/${enumClass}`)
                            .subscribe(data => {
                                /* Fill the "cache" */
                                this.translationsReplaySubjectMap[enumClass].next(data);

                                observer.next(data);
                                observer.complete();
                            });
                    });
            }
        });
    }

    private getEnumToUrlMapping(): Observable<{ string?: string }> {
        this.enumToUrlMappingReplaySubject = new ReplaySubject<{ string?: string }>(1);

        /* Execute the backend call to fetch the mapping of enums <-> URL-parts */
        return this.http.get<{ string?: string }>(this.endpoint)
            .pipe(
                map(data => {
                    /* Fill the "cache" */
                    this.enumToUrlMappingReplaySubject.next(data);

                    return data;
                }));
    }

    getLabelFromKeyLabelList(keyLabelList: KeyLabel[], key: string): string {
        return this.getKeyLabelFromKeyLabelList(keyLabelList, key).label;
    }

    getKeyLabelFromKeyLabelList(keyLabelList: KeyLabel[], key: string): KeyLabel {
        if (keyLabelList) {
            for (const keyLabelPair of keyLabelList) {
                if (keyLabelPair.key === key) {
                    return keyLabelPair;
                }
            }
        }
    }

}
