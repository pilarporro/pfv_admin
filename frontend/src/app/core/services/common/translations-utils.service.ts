import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslationsUtilsService {

  constructor(private translateService: TranslateService) {

  }

  getDeleteTranslationWithParams(deleteName) {
    return forkJoin(
      this.translateService.get('COMMON.CONFIRMATIONS.DELETE.Title', {name: deleteName}),
      this.translateService.get('COMMON.CONFIRMATIONS.DELETE.Body', {name: deleteName})
    );
  }

// todo add returntyp
}
