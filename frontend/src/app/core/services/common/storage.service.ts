import { HttpClient, HttpEvent, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LoadingHttpService } from '../base/loading-http.service';
import { Photo } from '../../model/base/photo.model';

@Injectable({
  providedIn: 'root'
})
export class StorageService extends LoadingHttpService {

  constructor(private httpClient: HttpClient, @Inject('environment') environment) {
    super(httpClient, 'storage', environment);
  }

  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.onLoadingRequest(
      this.httpClient.post(this.endpoint, formData, {
        observe: 'events',
        reportProgress: true
      })
    );
  }

  clonePhoto(storage: Photo): Observable<Photo> {
    return this.onLoadingRequest(
      this.httpClient.post<Storage>(`${this.endpoint}/clone`, storage)
    );
  }

  responseComplete(response: any): boolean {
    return response['body'];
  }
}
