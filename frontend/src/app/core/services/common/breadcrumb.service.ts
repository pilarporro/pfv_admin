import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Event, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Breadcrumb } from '../../model/base/breadcrumb.model';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  breadCrumbs$: BehaviorSubject<Breadcrumb[]> = new BehaviorSubject<Breadcrumb[]>([]);

  constructor(private router: Router) {
    this.router.events.subscribe(routeEvent => {
      this.onRouteEvent(routeEvent);
    });
  }

  /**
   * This method allows you to replace a certain URL parameter value in the breadcrumb with a specific userfriendly value.
   * You can call this method after having done a backend call to fetch data that corresponds to the value of the URL parameter.
   * e.g. if you want to replace the breadcrumb /Persons/1 by /Persons/John Doe (1 is the value of a URL parameter that was replaced
   * by "John Doe" in this example). This method is a less good alternative for using a resolver to fetch the data
   * (it's less good because it causes the breadcrumb to "flicker" upon replace).
   */
  replaceUrlParamValue(urlParamName: string, value: string) {
    const breadcrumb: Breadcrumb = this.breadCrumbs$.getValue().find(breadCrumb => breadCrumb.paramName === urlParamName);
    if (breadcrumb) {
      breadcrumb.paramValue = value;
    }
  }

  private onRouteEvent(event: Event) {
    if (!(event instanceof NavigationEnd)) {
      return;
    }
    let route = this.router.routerState.root.snapshot;
    let url = '';

    const breadcrumbs: Breadcrumb[] = [];

    while (route.firstChild) {
      route = route.firstChild;

      if (route.routeConfig === null
        || !route.routeConfig.path
        || !route.data['breadcrumb']) {
        continue;
      }

      url += `/${this.createUrl(route)}`;
      breadcrumbs.push(this.createBreadcrumb(route, url));
    }
    this.breadCrumbs$.next(breadcrumbs);
  }

  private createBreadcrumb(route: ActivatedRouteSnapshot, url: string): Breadcrumb {
    const isParameterisedUrl = route.routeConfig.path.startsWith(':');
    const urlParamName = isParameterisedUrl ? route.routeConfig.path.substr(1) : undefined;

    let paramValue;
    if (route.params && isParameterisedUrl) {
      // (route.data will contain a key with the same name as the URL parameter + "Data" if the route has a resolver for this property)
      const routeParamData = route.data[`${urlParamName}Data`];
      if (routeParamData) {
        if (route.data.resolvedDataMapper) {
          // if a resolvedDataMapper is defined, then use this mapper to get the value from the routeParamData
          paramValue = route.data.resolvedDataMapper(routeParamData);
        } else {
          // if no resolvedDataMapper is defined, use the routeParamData as paramValue
          paramValue = routeParamData;
        }
      } else {
        // use the value from the URL as paramValue
        paramValue = route.params[urlParamName];
      }
    }

    return new Breadcrumb(
      route.data['breadcrumb'],
      urlParamName,
      paramValue,
      this.isCurrentRoute(route),
      url,
      route.data['active']
    );
  }

  private isCurrentRoute = (route: ActivatedRouteSnapshot): boolean =>
    route.firstChild === null
    || route.firstChild.routeConfig === null
    || !route.firstChild.routeConfig.path;

  private createUrl = (route: ActivatedRouteSnapshot): string =>
    route.url.map(s => s.path).join('/');
}
