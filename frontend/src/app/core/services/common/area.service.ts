import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Observable } from 'rxjs';
import { AliasIdLabel } from '../../model/base/alias-id-label.model';
import { MunicipalityService } from './municipality.service';
import { Area } from '../../model/municipalities/area.model';

@Injectable({
  providedIn: 'root'
})
export class AreaService extends LoadingHttpCrudService<Area> {

  constructor(http: HttpClient, @Inject('environment') environment, private municipalityService: MunicipalityService) {
    super(http, 'municipalities', environment);
  }

  getByIdMinimal(id: string): Observable<AliasIdLabel> {
    return this.http.get<AliasIdLabel>(`${this.endpoint}/${this.municipalityService.defaultMunicipalityId}/${id}/minimal`);
  }

  findByAliasStartsWith(alias: string): Observable<AliasIdLabel[]> {
    const params: HttpParams = new HttpParams()
      .set('name', `${alias}`);

    return this.http.get<AliasIdLabel[]>(`${this.endpoint}/${this.municipalityService.defaultMunicipalityId}/areas`, {params});
  }
}
