import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Animal } from '../../model/animals/animal.model';
import { Activity } from '../../model/actitivities/activity.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActivityService extends LoadingHttpCrudService<Activity> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'activities', environment);
  }

  generateAdoptionContract(adoptionId: string): Observable<Blob> {
    return this.http.get(`${this.endpoint}/${adoptionId}/contract`, {
      observe: 'response',
      responseType: 'blob'
    }).pipe(map(response => response.body));
  }
}
