import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Trap } from '../../model/persons/trap.model';

@Injectable({
  providedIn: 'root'
})
export class TrapService extends LoadingHttpCrudService<Trap> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'traps', environment);
  }
}
