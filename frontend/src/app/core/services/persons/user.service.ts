import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { User } from '../../model/persons/user.model';


@Injectable({
    providedIn: 'root'
})
export class UserService extends LoadingHttpCrudService<User> {

    constructor(http: HttpClient, @Inject('environment') environment) {
        super(http, 'users', environment);
    }
}
