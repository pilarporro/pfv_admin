import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Person } from '../../model/persons/person.model';

@Injectable({
  providedIn: 'root'
})
export class PersonService extends LoadingHttpCrudService<Person> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'persons', environment);
  }
}
