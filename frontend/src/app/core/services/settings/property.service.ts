import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GenericCrudService } from '../base/generic-crud.service';
import { SystemSetting } from '../../model/settings/system-setting.model';
import { Property } from '../../model/settings/property.model';
import { LoadingHttpService } from '../base/loading-http.service';

@Injectable({
  providedIn: 'root'
})
export class PropertyService extends LoadingHttpService {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'settings/properties', environment);
  }

  getProperty(property): Observable<Property> {
    return this.http.get<Property>(`${this.endpoint}/${property}`);
  }
}
