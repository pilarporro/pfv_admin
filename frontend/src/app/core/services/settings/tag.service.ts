import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Observable } from 'rxjs';
import { Tag } from '../../model/settings/tag.model';
import { TagType } from '../../enums/tag-type.enum';

@Injectable({
  providedIn: 'root'
})
export class TagService extends LoadingHttpCrudService<Tag> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'tags', environment);
  }

  findByName(name: string, type?: TagType): Observable<Tag[]> {
    const params: HttpParams = new HttpParams()
      .set('type', `${type}`)
      .set('name', `${name}`);

    return this.http.get<Tag[]>(`${this.endpoint}/search`, {params: params});
  }
}
