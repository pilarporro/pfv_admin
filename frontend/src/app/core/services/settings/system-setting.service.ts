import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GenericCrudService } from '../base/generic-crud.service';
import { SystemSetting } from '../../model/settings/system-setting.model';
import { SystemSettingType } from '../../enums/system-setting-type.enum';

@Injectable({
  providedIn: 'root'
})
export class SystemSettingService extends GenericCrudService<SystemSetting> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'settings/system-properties', environment);
  }

  getByType(systemSettingType: SystemSettingType): Observable<SystemSetting> {
    return this.http.get<SystemSetting>(`${this.endpoint}/${systemSettingType}`);
  }
}
