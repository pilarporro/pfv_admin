import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import * as moment_ from 'moment';

const moment = moment_;

@Injectable({
  providedIn: 'root'
})
export class HttpDatetimeInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

    return next.handle(req).pipe(tap(
      (event: HttpEvent<any>) => {
        /*console.log(event);
        console.log(event['status'] === 200);
        console.log(event['body']);*/
        if (event['status'] === 200 && event['body']) {
          this.reviewDatetimes(event['body']);
          // console.log('END***********************************************');
          // console.log(event['body']);
        }
      }
    ));
  }

  reviewDatetimes(entity: any) {
    // console.log('reviewDatetimes');
    // console.log(entity);
    for (const key in entity) {
      // console.log(key);
      // console.log(typeof entity[key] === 'object');
      if (entity[key] != null && typeof entity[key] === 'object') { // isObject
          // console.log('****************************************************************************');
          // console.log(key);
          this.reviewDatetimes(entity[key]);
      } else if (entity[key] != null && typeof entity[key] === 'string') {
        if (/^(\d{4})\-(\d{2})\-(\d{2})T(\d{2}):(\d{2}):(\d{2})/.test(entity[key])) {
          // console.log('DATETIME PIllado');
          // console.log(key);
          // console.log(entity[key]);
          entity[key] = moment(`${entity[key]}Z`).local().toDate();
          // console.log(entity[key]);
        } else if (/^(\d{4})\-(\d{2})\-(\d{2})$/.test(entity[key])) {
          // console.log('DATE PIllado');
          entity[key] = new Date(entity[key]);
        }
      }
    }
  }
}
