import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoadingHttpService } from '../base/loading-http.service';
import { DataLoaderRequest } from '../../model/data-loader/load-profiles-request.model';
import { DataLoaderResponse } from '../../model/data-loader/load-profiles-response.model';

@Injectable({
  providedIn: 'root'
})
export class DataLoaderService extends LoadingHttpService {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'data-loader', environment);
  }

  load(request: DataLoaderRequest): Observable<DataLoaderResponse> {
    return this.onLoadingRequest(
      this.http.post<DataLoaderResponse>(`${this.endpoint}`, request)
    );
  }

  uploadAccountNotes(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.onLoadingRequest(
        this.http.post(`${this.endpoint}/account-notes`, formData, {
          observe: 'events',
          reportProgress: true
        })
    );
  }
}
