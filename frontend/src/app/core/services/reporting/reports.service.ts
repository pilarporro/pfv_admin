import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoadingHttpService } from '../base/loading-http.service';
import { Utils } from '../../utils/utils';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ReportsService extends LoadingHttpService {

    constructor(http: HttpClient, @Inject('environment') environment) {
        super(http, 'reports', environment);
    }

    generateReport(from: Date, to: Date): Observable<Blob> {
        return this.http.get(`${this.endpoint}/activities`, {
            observe: 'response',
            responseType: 'blob',
            params: {
                from: Utils.formatDate(from),
                to: Utils.formatDate(to)
            }
        }).pipe(map(response => response.body));
    }

    generateAccountsReport(from: Date, to: Date): Observable<Blob> {
        return this.http.get(`${this.endpoint}/accounts`, {
            observe: 'response',
            responseType: 'blob',
            params: {
                from: Utils.formatDate(from),
                to: Utils.formatDate(to)
            }
        }).pipe(map(response => response.body));
    }

    generateCastrationsEmail(invoiceRef: string): Observable<Blob> {
        return this.http.get(`${this.endpoint}/emailcastrations`, {
            observe: 'response',
            responseType: 'blob',
            params: {
                invoiceRef
            }
        }).pipe(map(response => response.body));
    }

    generateColoniesReport(): Observable<Blob> {
        return this.http.get(`${this.endpoint}/colonies`, {
            observe: 'response',
            responseType: 'blob'
        }).pipe(map(response => response.body));
    }

    generateColoniesReportExcel(): Observable<Blob> {
        return this.http.get(`${this.endpoint}/colonies-excel`, {
            observe: 'response',
            responseType: 'blob'
        }).pipe(map(response => response.body));
    }
}
