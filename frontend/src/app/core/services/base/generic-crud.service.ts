import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseHttpService } from './base-http.service';
import { KeyLabel } from '../../model/base/key-label.model';
import { FilterParam } from '../../model/table/filter-param.model';
import { Entity } from '../../model/base/entity.model';
import { PagedWrapper } from '../../model/table/paged-wrapper.model';
import { AliasIdLabel } from '../../model/base/alias-id-label.model';

export abstract class GenericCrudService<T extends Entity> extends BaseHttpService {

  get(entity: T): Observable<T> {
    return this.getById(entity.id);
  }

  getById(id: string): Observable<T> {
    return this.http.get<T>(`${this.endpoint}/${id}`);
  }

  getByIdMinimal(id: string): Observable<AliasIdLabel> {
    return this.http.get<AliasIdLabel>(`${this.endpoint}/${id}/minimal`);
  }

  list(): Observable<T[]> {
    return this.http.get<T[]>(this.endpoint);
  }

  findByAliasStartsWith(alias: string): Observable<AliasIdLabel[]> {
    const params: HttpParams = new HttpParams()
      .set('alias', `${alias}`);

    return this.http.get<AliasIdLabel[]>(`${this.endpoint}/by-alias-starts-with`, {params});
  }

  findByAlias(alias: string): Observable<AliasIdLabel> {
    const params: HttpParams = new HttpParams()
      .set('alias', `${alias}`);

    return this.http.get<AliasIdLabel>(`${this.endpoint}/by-alias`, {params});
  }

  getListAll(): Observable<KeyLabel[]> {
    return this.http.get<KeyLabel[]>(`${this.endpoint}/list`);
  }

  getAllPaginationParams(page: number, size: number, sortField?: string, sortOrder?: number,
                         filterParams?: FilterParam[]) {
    let params: HttpParams = new HttpParams();
    params = params.append('page', `${page}`);
    params = params.append('size', `${size}`);
    if (sortField) {
      params = params.append('sortField', `${sortField}`);
    }
    if (sortOrder) {
      params = params.append('sortOrder', `${sortOrder}`);
    }
    if (filterParams) {
      params = params.append('filters', JSON.stringify(filterParams));
    }

    return params;
  }

  listWithPaging(page: number, size: number, sortField?: string, sortOrder?: number,
                 filterParams?: FilterParam[]): Observable<PagedWrapper<T>> {
    return this.http.get<PagedWrapper<T>>(this.endpoint,
      {params: this.getAllPaginationParams(page, size, sortField, sortOrder, filterParams)});
  }

  add(entity: T): Observable<T> {
    return this.http.post<T>(this.endpoint, entity);
  }

  addMultiple(entities: T[]): Observable<T[]> {
    return this.http.post<T[]>(this.endpoint, entities);
  }

  update(entity: T): Observable<T> {
    return this.http.put<T>(`${this.endpoint}/${entity.id}`, entity);
  }

  updateMultiple(entities: T[]): Observable<T[]> {
    return this.http.put<T[]>(this.endpoint, entities);
  }

  delete(entity: T): any {
    return this.http.delete(`${this.endpoint}/${entity.id}`);
  }
}
