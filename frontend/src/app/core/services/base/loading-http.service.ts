import { catchError, tap } from 'rxjs/operators';
import { LoadingHttp } from './loading-http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { BaseHttpService } from './base-http.service';

export abstract class LoadingHttpService extends BaseHttpService implements LoadingHttp {
  isLoading$: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);

  responseComplete(response: any): boolean {
    return true;
  }

  getIsLoading$(): BehaviorSubject<Boolean> {
    return this.isLoading$;
  }

  protected onLoadingRequest(observable: Observable<any>): Observable<any> {
    this.isLoading$.next(true);

    return observable.pipe(tap(response => {
        if (this.responseComplete(response)) {
          this.isLoading$.next(false);
        }

        return response;
      }
    )).pipe(catchError(this.handleError));
  }

  private handleError = (response: Response) => {
    this.isLoading$.next(false);

    return throwError(response);
  };
}
