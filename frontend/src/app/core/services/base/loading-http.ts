import { BehaviorSubject } from 'rxjs';

export interface LoadingHttp {
  getIsLoading$(): BehaviorSubject<Boolean>;
}
