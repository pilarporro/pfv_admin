import { Inject, Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

export abstract class BaseHttpService {
  apiEndpoint: string = `${this.environment.apiUrl}`;
  endpoint: string;

  constructor(protected http: HttpClient, protected serviceEnpoint: string, @Inject('environment') private environment) {
    this.endpoint = `${this.apiEndpoint}/${serviceEnpoint}`;
  }
}
