import { catchError, tap } from 'rxjs/operators';
import { LoadingHttp } from './loading-http';
import { Injectable } from '@angular/core';
import { GenericCrudService } from './generic-crud.service';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Entity } from '../../model/base/entity.model';

export abstract class LoadingHttpCrudService<T extends Entity> extends GenericCrudService<T> implements LoadingHttp {
  isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  responseComplete(response: any): boolean {
    return true;
  }

  update(entity: T): Observable<T> {
    return this.onLoadingRequest(
      super.update(entity)
    );
  }

  add(entity: T): Observable<T> {
    return this.onLoadingRequest(
      super.add(entity)
    );
  }

  delete(entity: T): Observable<T> {
    return this.onLoadingRequest(
      super.delete(entity)
    );
  }

  getIsLoading$(): BehaviorSubject<Boolean> {
    return this.isLoading$;
  }

  protected onLoadingRequest(observable: Observable<any>): Observable<any> {
    this.isLoading$.next(true);

    return observable.pipe(tap(response => {
        if (this.responseComplete(response)) {
          this.isLoading$.next(false);
        }

        return response;
      }
    )).pipe(catchError(this.handleError));
  }

  private handleError = (response: Response) => {
    this.isLoading$.next(false);

    return throwError(response);
  };
}
