import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../auth.service';
import { REMEMBER_ME_KEY } from '../../../utils/app-initializer';

@Injectable({
    providedIn: 'root'
})
export class CheckRememberMeGuard implements CanActivate {
    constructor(private router: Router,
                private cookieService: CookieService,
                private authService: AuthService) {
    }

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): boolean {
        // console.log('CheckRememberMeGuard canActivate');
        // console.log(`${this.cookieService.check(REMEMBER_ME_KEY)} = ${this.cookieService.get(REMEMBER_ME_KEY)}`);
        // console.log(this.authService.isAuthenticated());

        if (this.cookieService.check(REMEMBER_ME_KEY) && this.authService.isAuthenticated()) {
            this.router.navigateByUrl('/dashboard', {replaceUrl: true});
        }

        return true;
    }
}
