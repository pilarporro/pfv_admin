import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {
    // console.log('AuthGuard canActivate');
    // console.log(this.authService.isAuthenticated());

    if (this.authService.isAuthenticated()) {
      return this.isAccessAllowed(route, this.authService.getLoggedUser().roles);
    } else {
      this.router.navigate(['auth', 'signin']);

      return false;
    }

  }

  isAccessAllowed(route: ActivatedRouteSnapshot,
                  userRoles: string[]): boolean {
    const requiredRoles: string[] = route.data.roles;
    if (!requiredRoles || requiredRoles.length === 0) {
      return true;
    } else if (!userRoles || userRoles.length === 0) {
      return false;
    } else {
      let granted: boolean = false;
      for (const requiredRole of requiredRoles) {
        if (userRoles.indexOf(requiredRole) > -1) {
          granted = true;
          break;
        }
      }

      return granted;
    }
  }
}
