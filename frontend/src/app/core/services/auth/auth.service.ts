import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { User } from '../../model/persons/user.model';
import { BrowserWindowRef } from '../network/window.service';
import { AuthResponse } from '../../model/auth/auth-response.model';
import { tap } from 'rxjs/operators';
import { Role } from '../../enums/role.enum';
import { AliasIdLabel } from '../../model/base/alias-id-label.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends LoadingHttpCrudService<User> {
  userChanges$: BehaviorSubject<User> = new BehaviorSubject(undefined);

  authenticatedKey: string = 'authenticated-key';
  tokenKey: string = 'token-key';
  userKey: string = 'user-key';

  constructor(private windowRef: BrowserWindowRef,
              http: HttpClient,
              @Inject('environment') environment) {
    super(http, 'auth', environment);
    this.getWindow().localStorage.setItem(this.authenticatedKey, 'false');
  }

  getWindow(): Window {
    return this.windowRef.nativeWindow as Window;
  }

  attempAuth(username: string, password: string) {
    return this.onLoadingRequest(
      this.http.post<AuthResponse>(`${this.endpoint}/signin`, {username, password})
        .pipe(
          tap(data => {
            this.saveClient(data);
          })
        )
    );
  }

  saveClient(data: AuthResponse) {
    // console.log('saveClient');
    // console.log(data);
    this.getWindow().localStorage.setItem(this.tokenKey, data.token);
    this.getWindow().localStorage.setItem(this.authenticatedKey, 'true');
    this.updateLoggedUser(data.user);
  }

  updateLoggedUser(user: User) {
    delete user.operationsInfo;
    this.getWindow().localStorage.setItem(this.userKey, JSON.stringify(user));
    this.userChanges$.next(user);
  }

  signout() {
    this.getWindow().localStorage.setItem(this.userKey, undefined);
    this.getWindow().localStorage.setItem(this.authenticatedKey, 'false');
    this.getWindow().localStorage.setItem(this.tokenKey, undefined);
  }

  isAuthenticated(): boolean {
    return this.getWindow().localStorage.getItem(this.authenticatedKey) === 'true';
  }

  getLoggedUser(): User {
    if (this.isAuthenticated()) {
      return JSON.parse(this.getWindow().localStorage.getItem(this.userKey)) as User;
    } else {
      return undefined;
    }
  }

  getLoggedPerson(): AliasIdLabel {
    if (this.isAuthenticated()) {
      return this.getLoggedUser().person;
    } else {
      return undefined;
    }
  }

  getJwtToken(): string {
    if (this.isAuthenticated()) {
      return this.getWindow().localStorage.getItem(this.tokenKey);
    } else {
      return undefined;
    }
  }

  rememberMe(key: string): Observable<AuthResponse> {
    const params: HttpParams = new HttpParams()
      .set('key', `${key}`);

    return this.http.get<AuthResponse>(`${this.endpoint}/remember-me`, {params})
      .pipe(tap(data => {
          if (data) {
            this.saveClient(data['body']);
          }
        })
      );
  }

  isUserInAnyRole(roles) {
    let userInAnyRole: boolean = false;
    roles.forEach(role => userInAnyRole = userInAnyRole || this.isUserInRole(role));

    return userInAnyRole;
  }

  isUserInRole(role: Role): boolean {
    const user: User = this.getLoggedUser();
    // // console.log(user);

    return user && this.getLoggedUser().roles.findIndex(actual => actual === role) > -1;
  }

  isUserAdmin(): boolean {
    return this.isUserInRole(Role.ADMIN);
  }

  isUser(): boolean {
    return this.isUserInRole(Role.USER);
  }

  isUserGuest(): boolean {
    return this.isUserInRole(Role.GUEST);
  }
}
