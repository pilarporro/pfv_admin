import { Injectable } from '@angular/core';
import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpHeaderResponse,
    HttpHeaders,
    HttpInterceptor, HttpParams,
    HttpProgressEvent,
    HttpRequest,
    HttpResponse,
    HttpSentEvent,
    HttpUserEvent
} from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

const TOKEN_HEADER_KEY = 'X-AUTH-TOKEN';
const AUTHORIZATION_HEADER_KEY = 'Authorization';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
    allowedDomains: string[] = [];

    constructor(private authService: AuthService, private router: Router) {
    }

    isAllowedDomain(req: HttpRequest<any>): boolean {
        let allowed: boolean = false;
        this.allowedDomains.some(domain => {
            allowed = req.url.indexOf(domain) > 0;

            return allowed;
        });

        return allowed;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        // console.log('intercept');
        // console.log(this.isAllowedDomain(req));

        if (this.isAllowedDomain(req)) {
            return next.handle(req);
        }

        const jwtToken: string = this.authService.getJwtToken();
        if (jwtToken) {
            const authReq: HttpRequest<any> =
                req.clone({
                    params: req.params
                        .set('token', `${jwtToken}`)
                });

            return next.handle(authReq);
        }

        return next.handle(req).pipe(tap(
            (event: HttpEvent<any>) => {
            },
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    console.log({err});
                    if (err.status === 401) {
                        this.router.navigate(['']);
                    }
                }
            }
        ));
    }

}
