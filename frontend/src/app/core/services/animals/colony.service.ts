import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Person } from '../../model/persons/person.model';
import { Colony } from '../../model/animals/colony.model';

@Injectable({
  providedIn: 'root'
})
export class ColonyService extends LoadingHttpCrudService<Colony> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'colonies', environment);
  }
}
