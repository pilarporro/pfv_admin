import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpCrudService } from '../base/loading-http-crud.service';
import { Animal } from '../../model/animals/animal.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalService extends LoadingHttpCrudService<Animal> {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'animals', environment);
  }
}
