import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingHttpService } from '../base/loading-http.service';

@Injectable({
  providedIn: 'root'
})
export class LogService extends LoadingHttpService {

  constructor(http: HttpClient, @Inject('environment') environment) {
    super(http, 'logs', environment);
  }

  log(value: string, level: string) {
    /* this.http.post<void>(`${this.endpoint}`, { value: value, level: level })
      .pipe(take(1)).subscribe();*/
  }

  error(value: string) {
    this.log(value, 'ERROR');
  }

  debug(value: string) {
    this.log(value, 'DEBUG');
  }

  info(value: string) {
    this.log(value, 'INFO');
  }
}
