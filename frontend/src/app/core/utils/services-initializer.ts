import { MunicipalityService } from '../services/common/municipality.service';
import { tap } from 'rxjs/operators';

export function servicesInitializer(municipalityService: MunicipalityService): () => Promise<any> {
    return () => {
        return municipalityService.loadDefaultMunicipality()
            .pipe(tap(result => municipalityService.defaultMunicipalityId = result.id))
            .toPromise();
    };
}
