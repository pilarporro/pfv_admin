import { FormArray, FormGroup } from '@angular/forms';
import { FilterOperation } from '../enums/table/filter-operation.enum';
import { TableFilters } from '../model/table/table-filters.model';
import { FilterParam } from '../model/table/filter-param.model';

export class Utils {

    static formatDate(date: Date): string {
        const month: number = date.getMonth() + 1;
        const strMonth: string = month <= 9 ? `0${month}` : `${month}`;

        const day: number = date.getDate();
        const strDay: string = day <= 9 ? `0${day}` : `${day}`;

        return `${date.getFullYear()}${strMonth}${strDay}`;
    }

    static isMobile() {
        const screenWidth = screen.width ? screen.width : 5000;
        const windowInnerWidth = window.innerWidth ? window.innerWidth : 5000;

        return (screenWidth < windowInnerWidth ? screenWidth : innerWidth) <= 640;
    }

    static getRandomNum(min: number, max: number): number {
        return Math.trunc(Math.random() * (max - min) + min);
    }

    static removeTimeInfo(date: Date): Date {
        if (date) {
            date.setHours(5); // To avoid date transformation in the backend.
        }

        return date;
    }

    static compare(o1: any, o2: any): boolean {
        try {
            if (Utils.isNotDefined(o1) && Utils.isNotDefined(o2)) {
                return true;
            } else if (Utils.isNotDefined(o1) || Utils.isNotDefined(o2)) {
                return false;
            }
            const o1Clone: any = this.clone(o1);
            const o2Clone: any = this.clone(o2);
            this.checkEmptyValues(o1Clone);
            this.checkEmptyValues(o2Clone);
            // TODO
            // deepEqual(o1Clone, o2Clone);

            return true;
        } catch (error) {
            return false;
        }
    }

    static checkEmptyValues(entity: any) {
        for (const key in entity) {
            if (entity[key] != null && typeof entity[key] === 'object') { // isObject
                if (('id' in entity[key]) && ('alias' in entity[key]) && ('label' in entity[key])) {
                    if (Utils.isNotDefined(entity[key].id)) {
                        delete entity[key];
                    }
                } else {
                    this.checkEmptyValues(entity[key]);
                }
            } else if ((!entity[key] && entity[key] !== false) || entity[key] === '' || entity[key] == null) {
                delete entity[key];
            }
        }
    }

    static clone(obj: any): any {
        const copy: any = {};
        Object.keys(obj).forEach(key => {
            if (obj[key] instanceof Date) {
                const copyDate: Date = new Date();
                copyDate.setTime(obj[key].getTime());
                copy[key] = copyDate;
            } else if (obj[key] instanceof Array) {
                copy[key] = [];
                obj[key].forEach(item => {
                    copy[key].push(this.clone(item));
                });
            } else if (obj[key] instanceof Object) {
                copy[key] = this.clone(obj[key]);
            } else if (this.isDefined(obj[key])) {
                copy[key] = obj[key];
            }
        });

        return copy;
    }

    static isTabletOrMobile(): boolean {
        return window.innerWidth <= 1024;
    }

    static removeTimeInformation(date: Date): Date {
        const dateNoTime: Date = new Date(date);
        dateNoTime.setHours(0);
        dateNoTime.setMilliseconds(0);
        dateNoTime.setSeconds(0);
        dateNoTime.setMinutes(0);

        return dateNoTime;
    }

    static empty(value: string | any[]): boolean {
        return (value === undefined) || (value === null) || (value.length === 0);
    }

    static isNotDefined(value: any): boolean {
        return (value === undefined) || (value === null);
    }

    static isDefined(value: any): boolean {
        return !((value === undefined) || (value === null));
    }

    /* Util while developing */
    static getAllErrors(form: FormGroup | FormArray): { [key: string]: any; } | null {
        let hasError = false;
        const result = Object.keys(form.controls).reduce((acc, key) => {
            const control = form.get(key);
            const errors = (control instanceof FormGroup || control instanceof FormArray)
                ? this.getAllErrors(control)
                : control.errors;
            if (errors) {
                acc[key] = errors;
                hasError = true;
            }

            return acc;
        }, {} as { [key: string]: any; });

        return hasError ? result : null;
    }

    static updateValidity(form: FormGroup | FormArray) {
        Object.keys(form.controls).forEach(key => {
            const control = form.controls[key];
            control.markAsTouched();
            control.markAsDirty();
            if (control instanceof FormGroup || control instanceof FormArray) {
                this.updateValidity(control);
            }
        });
    }

    static markAsClean(form: FormGroup | FormArray) {
        Object.keys(form.controls).forEach(key => {
            const control = form.controls[key];
            control.markAsPristine();
            control.markAsUntouched();
            if (control instanceof FormGroup || control instanceof FormArray) {
                this.markAsClean(control);
            }
        });
    }

    static convertTableFiltersToFilterParams(filters: TableFilters): FilterParam[] {
        const filterParams: FilterParam[] = [];
        for (const filter in filters) {
            if (filters.hasOwnProperty(filter)) {
                const param: FilterParam = {
                    offsetTimezone: (new Date()).getTimezoneOffset(),
                    key: filter,
                    operation: this.convertMatchMode(filters[filter].matchMode),
                    value: filters[filter].value
                };
                filterParams.push(param);
            }
        }

        return filterParams;
    }

    static convertMatchMode(matchMode: string) {
        switch (matchMode) {
            case 'startsWith': {
                return FilterOperation.StartsWith;
            }
            case 'endsWith': {
                return FilterOperation.EndsWith;
            }
            case 'equals': {
                return FilterOperation.Equals;
            }
            case 'notEquals': {
                return FilterOperation.NotEquals;
            }
            case 'contains': {
                return FilterOperation.Contains;
            }
            case 'in': {
                return FilterOperation.In;
            }
            case 'lessThanOrEquals': {
                return FilterOperation.LessThanOrEquals;
            }
            case 'greaterThanOrEquals': {
                return FilterOperation.GreaterThanOrEquals;
            }
            case 'between': {
                return FilterOperation.Between;
            }
            case 'not_null': {
                return FilterOperation.NotNull;
            }
            default: {
                throw new Error(`Matchmode ${matchMode} could not be mapped to a FilterOperation`);
            }
        }
    }
}
