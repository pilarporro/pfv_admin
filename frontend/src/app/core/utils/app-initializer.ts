import { HttpClient } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../services/auth/auth.service';
import { Property } from '../model/settings/property.model';

export const REMEMBER_ME_KEY: string = 'rememberMe0987890';

export function appInitializer(authService: AuthService, cookieService: CookieService): () => Promise<any> {
  return () => {
    // console.log('appInitializer');
    // console.log(`${cookieService.check(REMEMBER_ME_KEY)} = ${cookieService.get(REMEMBER_ME_KEY)}`);

    // To test login : cookieService.delete(REMEMBER_ME_KEY);
    if (cookieService.check(REMEMBER_ME_KEY)) {
      return authService.rememberMe(cookieService.get(REMEMBER_ME_KEY))
        .toPromise();
    } else {
      return of(true).toPromise();
    }
  };
}
