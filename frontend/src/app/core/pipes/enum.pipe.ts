import { Pipe, PipeTransform } from '@angular/core';
import { take } from 'rxjs/operators';
import { EMPTY, Observable } from 'rxjs';
import { EnumTranslationService } from '../services/common/enum-translation.service';

@Pipe({
    name: 'appEnum'
})
export class EnumPipe implements PipeTransform {
    constructor(private enumTranslationService: EnumTranslationService) {
    }

    transform(enumValue: string, enumClass: string): Observable<string> {
        if (enumValue) {
            return this.enumTranslationService.getTranslationForEnumValue(enumClass, enumValue)
                .pipe(take(1));
        } else {
            return EMPTY;
        }
    }

}
