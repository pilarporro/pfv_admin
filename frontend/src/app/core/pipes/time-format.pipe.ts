import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'appTime'
})
export class TimeFormatPipe extends DatePipe implements PipeTransform {

    transform(value: Date): string {
        return super.transform(value, 'HH:mm:ss');
    }
}
