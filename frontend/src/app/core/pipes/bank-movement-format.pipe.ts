import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe, DatePipe, DecimalPipe } from '@angular/common';

@Pipe({
    name: 'appBankMovement'
})
export class BankMovementFormatPipe extends CurrencyPipe implements PipeTransform {

    transform(value: number, args?: any): any {
        const styleClass = value >= 0 ? 'positive' : 'negative';
        value = Math.abs(value);
        return `<span class="${styleClass}">${super.transform(value, 'EUR', 'symbol', '1.2-2', 'es')}</span>`;
    }

}