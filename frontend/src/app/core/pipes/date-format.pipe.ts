import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'appDate'
})
export class DateFormatPipe extends DatePipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (typeof value === 'string') {
            return value;
        } else {
            return super.transform(value, DateFormatConstants.DATE_FMT);
        }
    }

}

export class DateFormatConstants {
    static readonly DATE_FMT: string = 'dd/MM/yyyy';
}
