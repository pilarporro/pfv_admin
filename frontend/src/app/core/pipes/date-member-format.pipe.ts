import { Pipe, PipeTransform } from '@angular/core';
import { DateFormatConstants, DateFormatPipe } from './date-format.pipe';

@Pipe({
    name: 'appDateMember'
})
export class DateMemberFormatPipe extends DateFormatPipe implements PipeTransform {

    transform(value: string, args?: any): any {
        if (value) {
            const parts = value.split('/');
            const date = new Date(parseInt(parts[2], 10),
                parseInt(parts[1], 10) - 1,
                parseInt(parts[0], 10));

            const styleClass = (date > new Date()) ? 'positive' : 'negative';
            return `<span class="${styleClass}">${super.transform(value, args)}</span>`;
        } else {
            return '';
        }
    }

}
