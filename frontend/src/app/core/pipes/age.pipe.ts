import { Pipe, PipeTransform } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import * as moment_ from 'moment';

const moment = moment_;

@Pipe({
  name: 'appAge'
})
export class AgePipe implements PipeTransform {

  transform(value: Date): Observable<string> {
    if (value && value.getFullYear()) {

      return Observable.create(observer => {
        observer.next(`${new Date().getFullYear() - value.getFullYear()}`);
        observer.complete();
      });
    } else {
      return EMPTY;
    }
  }

}
