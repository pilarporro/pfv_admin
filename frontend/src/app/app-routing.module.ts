import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { AuthGuard } from './core/services/auth/guards/auth-guard.service';
import { CheckRememberMeGuard } from './core/services/auth/guards/check-rememberme-guard';


const routes: Routes = [
    {
        path: '', redirectTo: '/auth/signin', pathMatch: 'full'
    },
    {
        path: 'dashboard',
        data: {breadcrumb: 'MENU.Dashboard'},
        component: DashboardComponent,
        pathMatch: 'full', canActivate: [AuthGuard]
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule',
        canActivate: [CheckRememberMeGuard]
    },
    {
        path: 'users',
        data: {breadcrumb: 'MENU.USERS.Title'},
        loadChildren: './users/users.module#UsersModule'
    },
    {
        path: 'association',
        data: {breadcrumb: 'MENU.ASSOCIATION.Title'},
        loadChildren: './association/association.module#AssociationModule'
    },
    {
        path: 'activities',
        data: {breadcrumb: 'MENU.ACTIVITIES.Title'},
        loadChildren: './activities/activities.module#ActivitiesModule'
    },
    {
        path: 'accounting',
        data: {breadcrumb: 'MENU.ACCOUNTING.Title'},
        loadChildren: './accounting/accounting.module#AccountingModule'
    },
    {
        path: 'settings',
        data: {breadcrumb: 'MENU.SETTINGS.Title'},
        loadChildren: './settings/settings.module#SettingsModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
