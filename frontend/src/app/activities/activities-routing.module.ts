import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Role } from '../core/enums/role.enum';
import { AuthGuard } from '../core/services/auth/guards/auth-guard.service';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { AdoptionListComponent } from './activity-list/adoption-list.component';
import { BuySuppliesListComponent } from './activity-list/buy-supplies-list.component';
import { VisitClinicListComponent } from './activity-list/visit-clinic-list.component';
import { MeetingListComponent } from './activity-list/meeting-list.component';
import { RescueListComponent } from './activity-list/rescue-list.component';
import { PickUpKeeperListComponent } from './activity-list/pickup-keeper-list.component';
import { NewMemberListComponent } from './activity-list/new-member-list.component';
import { OtherActivityListComponent } from './activity-list/other-activity-list.component';
import { CastrationListComponent } from './activity-list/castration-list.component';

const routes: Routes = [
  {
    path: '',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.Title'},
    canActivate: [AuthGuard],
    component: ActivityListComponent
  },
  {
    path: 'adoptions',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.Adoptions'},
    canActivate: [AuthGuard],
    component: AdoptionListComponent
  },
  {
    path: 'buy-supplies',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.BuySupplies'},
    canActivate: [AuthGuard],
    component: BuySuppliesListComponent
  },
  {
    path: 'castrations',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.Castrations'},
    canActivate: [AuthGuard],
    component: CastrationListComponent
  },
  {
    path: 'others',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.Others'},
    canActivate: [AuthGuard],
    component: OtherActivityListComponent
  },
  {
    path: 'new-members',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.NewMembers'},
    canActivate: [AuthGuard],
    component: NewMemberListComponent
  },
  {
    path: 'pickup-keepers',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.PickupKeepers'},
    canActivate: [AuthGuard],
    component: PickUpKeeperListComponent
  },
  {
    path: 'rescues',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.Rescues'},
    canActivate: [AuthGuard],
    component: RescueListComponent
  },
  {
    path: 'meetings',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.Meetings'},
    canActivate: [AuthGuard],
    component: MeetingListComponent
  },
  {
    path: 'visit-clinic',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACTIVITIES.VisitClinic'},
    canActivate: [AuthGuard],
    component: VisitClinicListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivitiesRoutingModule {
}
