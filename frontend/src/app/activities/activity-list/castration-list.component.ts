import { Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../core/services/common/translations-utils.service';
import { AuthService } from '../../core/services/auth/auth.service';
import { MunicipalityService } from '../../core/services/common/municipality.service';
import { ActivityType } from '../../core/enums/activity-type.enum';
import { TableColumType } from '../../core/enums/table/table-column-type.enum';
import { ActivityService } from '../../core/services/activities/activity.service';
import { ActivityListComponent } from './activity-list.component';
import { PlaceType } from '../../core/enums/place-type.enum';

@Component({
    selector: 'admin-activity-list',
    templateUrl: './activity-list.component.html',
    styleUrls: ['./activity-list.component.scss']
})
export class CastrationListComponent extends ActivityListComponent {

    constructor(activityService: ActivityService, confirmationService: ConfirmationService,
                translationUtilsService: TranslationsUtilsService,
                authService: AuthService, municipalityService: MunicipalityService) {
        super(activityService, confirmationService, translationUtilsService, authService, municipalityService, ActivityType.CASTRATION);
        this.showListOfTypes = false;
        this.readonlyType = true;
    }

    initColumns() {
        this.columns = [
            ...this.activitiesColumns(),
            {
                field: 'animal.alias', header: 'ANIMALS.Cat', filter: true, filterMatchMode: 'startsWith', filterFields: ['animal.alias']
            },
            {
                field: 'vetClinic.alias',
                header: 'PLACES.VetClinic',
                filter: true,
                filterMatchMode: 'in',
                type: TableColumType.PLACE,
                subtype: PlaceType.VET_CLINIC,
                filterFields: ['vetClinic.id']
            },
            {
                field: 'colony.alias', header: 'ACTIVITIES.Colony', filter: true, filterMatchMode: 'startsWith', filterFields: ['colony.alias']
            },
            {
                field: 'invoiceRef', header: 'ACTIVITIES.InvoiceRef', filter: true, filterMatchMode: 'startsWith'
            },
            {
                field: 'paymentType', header: 'ACTIVITIES.PaymentType', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'PaymentType'
            },
            {
                field: 'chip', header: 'ACTIVITIES.Chip', filter: true, filterMatchMode: 'startsWith'
            }
        ];

        this.selectedColumns = [
            this.columns[2],
            this.columns[3],
            this.columns[7],
            this.columns[8],
            this.columns[9],
            this.columns[10],
            this.columns[12]];
    }
}
