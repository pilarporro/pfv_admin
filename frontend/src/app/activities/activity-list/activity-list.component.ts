import { Component, Optional } from '@angular/core';
import { CommonListComponent } from '../../core/components/common-list.component';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../core/services/common/translations-utils.service';
import { AuthService } from '../../core/services/auth/auth.service';
import { MunicipalityService } from '../../core/services/common/municipality.service';
import { ActivityType } from '../../core/enums/activity-type.enum';
import { TableColumType } from '../../core/enums/table/table-column-type.enum';
import { Activity } from '../../core/model/actitivities/activity.model';
import { ActivityService } from '../../core/services/activities/activity.service';
import { TableColumn } from '../../core/model/table/table-column.model';
import { take } from 'rxjs/operators';
import { saveAs } from 'file-saver';

@Component({
    selector: 'admin-activity-list',
    templateUrl: './activity-list.component.html',
    styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent extends CommonListComponent<Activity> {
    ActivityType: typeof ActivityType = ActivityType;

    readonlyType: boolean = false;

    constructor(private activityService: ActivityService,
                confirmationService: ConfirmationService,
                translationUtilsService: TranslationsUtilsService,
                authService: AuthService,
                private municipalityService: MunicipalityService,
                @Optional() private readonly selectedType?: ActivityType) {
        super(activityService, confirmationService, translationUtilsService, authService);
        if (this.selectedType) {
            this.specificPageType = this.selectedType;
            this.addDefaultFilters();
        } else {
            this.selectedType = ActivityType.CASTRATION;
        }
        this.sortField = 'date';
        this.sortOrder = -1;
    }

    specificPageType: ActivityType;
    showListOfTypes: boolean = true;

    static newActivity(selectedType: ActivityType, authService: AuthService): Activity {
        return {
            active: true,
            person: selectedType === ActivityType.CASTRATION ? undefined : authService.getLoggedPerson(),
            markToDelete: false,
            notification: false,
            date: new Date(),
            type: selectedType
        };
    }

    addDefaultFilters() {
        if (this.specificPageType) {
            this.filters.type = {value: this.specificPageType, matchMode: 'equals'};
        }
    }

    createNewEntity(): Activity {
        return ActivityListComponent.newActivity(this.selectedType, this.authService);
    }

    initColumns() {
        this.columns = this.activitiesColumns();

        this.selectedColumns = [
            this.columns[5],
            this.columns[1],
            this.columns[2],
            this.columns[3]
        ];
    }

    activitiesColumns(): TableColumn[] {
        return [
            {
                field: 'active',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.Active',
                filter: true,
                filterMatchMode: 'equals'
            },
            {
                field: 'type', header: 'COMMON.LABELS.Type', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'ActivityType'
            },
            {
                field: 'date', header: 'ACTIVITIES.Date', filter: true, filterMatchMode: 'in', type: TableColumType.DATE
            },
            {
                field: 'person.alias', header: 'ACTIVITIES.Person', filter: true, filterMatchMode: 'startsWith', filterFields: ['person.alias']
            },
            {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
            {
                field: 'alias', header: 'COMMON.LABELS.Alias', filter: true, filterMatchMode: 'contains'
            },
            {
                field: 'markToDelete',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.MarkToDelete',
                filter: true,
                filterMatchMode: 'equals'
            }
        ];
    }


    downloadAdoptionContract(selectedEntity) {
        // console.log(this.selected);
        this.activityService.generateAdoptionContract(selectedEntity.id)
            .pipe(take(1))
            .subscribe(invoice => {
                saveAs(invoice, `contrato.docx`);
            });
    }
}
