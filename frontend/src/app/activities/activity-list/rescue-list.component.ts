import { Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../core/services/common/translations-utils.service';
import { AuthService } from '../../core/services/auth/auth.service';
import { MunicipalityService } from '../../core/services/common/municipality.service';
import { ActivityType } from '../../core/enums/activity-type.enum';
import { TableColumType } from '../../core/enums/table/table-column-type.enum';
import { Activity } from '../../core/model/actitivities/activity.model';
import { ActivityService } from '../../core/services/activities/activity.service';
import { TableColumn } from '../../core/model/table/table-column.model';
import { ActivityListComponent } from './activity-list.component';
import { Adoption } from '../../core/model/actitivities/adoption.model';
import { of } from 'rxjs';
import { Rescue } from '../../core/model/actitivities/rescue.model';

@Component({
    selector: 'admin-activity-list',
    templateUrl: './activity-list.component.html',
    styleUrls: ['./activity-list.component.scss']
})
export class RescueListComponent extends ActivityListComponent {

    constructor(activityService: ActivityService, confirmationService: ConfirmationService,
                translationUtilsService: TranslationsUtilsService,
                authService: AuthService, municipalityService: MunicipalityService) {
        super(activityService, confirmationService, translationUtilsService, authService, municipalityService, ActivityType.RESCUE);
        this.showListOfTypes = false;
        this.readonlyType = true;
    }

    initColumns() {
        this.columns = [
            ...this.activitiesColumns(),
            {
                field: 'caller.alias', header: 'ACTIVITIES.Caller', filter: true, filterMatchMode: 'startsWith', filterFields: ['caller.alias']
            },
            {
                field: 'id',
                rowToLabelConverter: (row: Rescue) => of(row.animals.map(animal => animal.alias).join(', ')),
                header: 'ACTIVITIES.Animals', filter: true, filterFields: ['animals.alias'], filterMatchMode: 'startsWith'
            }
        ];

        this.selectedColumns = [
            this.columns[2],
            this.columns[3],
            this.columns[7],
            this.columns[8]];
    }
}
