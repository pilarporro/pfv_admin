import { Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../core/services/common/translations-utils.service';
import { AuthService } from '../../core/services/auth/auth.service';
import { MunicipalityService } from '../../core/services/common/municipality.service';
import { ActivityType } from '../../core/enums/activity-type.enum';
import { TableColumType } from '../../core/enums/table/table-column-type.enum';
import { ActivityService } from '../../core/services/activities/activity.service';
import { ActivityListComponent } from './activity-list.component';
import { of } from 'rxjs';
import { VisitClinic } from '../../core/model/actitivities/visit-clinic.model';
import { PlaceType } from '../../core/enums/place-type.enum';

@Component({
    selector: 'admin-activity-list',
    templateUrl: './activity-list.component.html',
    styleUrls: ['./activity-list.component.scss']
})
export class VisitClinicListComponent extends ActivityListComponent {

    constructor(activityService: ActivityService, confirmationService: ConfirmationService,
                translationUtilsService: TranslationsUtilsService,
                authService: AuthService, municipalityService: MunicipalityService) {
        super(activityService, confirmationService, translationUtilsService, authService, municipalityService, ActivityType.VISIT_CLINIC);
        this.showListOfTypes = false;
        this.readonlyType = true;
    }

    initColumns() {
        this.columns = [
            ...this.activitiesColumns(),
            {
                field: 'vetClinic.alias', header: 'PLACES.VetClinic', filter: true, type: TableColumType.PLACE,
                subtype: PlaceType.VET_CLINIC, filterFields: ['vetClinic.id'], filterMatchMode: 'in'
            },
            {
                field: 'visitClinicType', header: 'ACTIVITIES.VisitClinicType', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'VisitClinicType'
            },
            {
                field: 'paymentType', header: 'ACTIVITIES.PaymentType', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'PaymentType'
            },
            {
                field: 'invoiceRef', header: 'ACTIVITIES.InvoiceRef', filter: true, filterMatchMode: 'contains'
            },
            {
                field: 'id',
                rowToLabelConverter: (row: VisitClinic) => of(row.animals.map(animal => animal.alias).join(', ')),
                header: 'ACTIVITIES.Animals', filter: true, filterFields: ['animals.alias'], filterMatchMode: 'startsWith'
            },
            {
                field: 'chip', header: 'ACTIVITIES.Chip', filter: true, filterMatchMode: 'startsWith'
            }
        ];

        this.selectedColumns = [
            this.columns[2],
            this.columns[3],
            this.columns[7],
            this.columns[8],
            this.columns[9],
            this.columns[10],
            this.columns[11]];
    }
}
