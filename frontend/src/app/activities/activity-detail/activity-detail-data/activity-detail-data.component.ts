import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Unsubscribable } from '../../../core/utils/unsubscribable';
import { AnimalFormGroup } from '../../../core/model/forms/animal-form.group.model';
import { ActivityFormGroup } from '../../../core/model/forms/activities/activity-form.group.model';
import { ActivityType } from '../../../core/enums/activity-type.enum';
import { Animal } from '../../../core/model/animals/animal.model';
import { Person } from '../../../core/model/persons/person.model';
import { MunicipalityService } from '../../../core/services/common/municipality.service';

@Component({
  selector: 'admin-activity-detail-data',
  templateUrl: './activity-detail-data.component.html',
  styleUrls: ['./activity-detail-data.component.scss']
})
export class ActivityDetailDataComponent extends Unsubscribable implements OnInit {
  ActivityType: typeof ActivityType = ActivityType;

  @Input()
  form: ActivityFormGroup;

  @Input()
  readonly: boolean = false;

  @Input()
  readonlyType: boolean = false;

  @Input()
  aliasInUse: boolean = false;

  @Output()
  onChangeType: EventEmitter<ActivityType> = new EventEmitter();

  constructor() {
    super();
  }

  ngOnInit() {
  }

  changeActivityType(newType: ActivityType) {
    // console.log('DETAIL DATA changeActivityType');
    // console.log(newType);
    this.onChangeType.emit(newType);
  }

}
