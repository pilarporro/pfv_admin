import { Component, Input, OnInit } from '@angular/core';
import { CommonDetailComponent } from '../../core/components/common-detail.component';
import { Animal } from '../../core/model/animals/animal.model';
import { AnimalFormGroup } from '../../core/model/forms/animal-form.group.model';
import { AnimalService } from '../../core/services/animals/animal.service';
import { TranslateService } from '@ngx-translate/core';
import { Activity } from '../../core/model/actitivities/activity.model';
import { ActivityType } from '../../core/enums/activity-type.enum';
import { AdoptionFormGroup } from '../../core/model/forms/activities/adoption-form.group.model';
import { ActivityFormGroup } from '../../core/model/forms/activities/activity-form.group.model';
import { ActivityService } from '../../core/services/activities/activity.service';
import { BuySuppliesFormGroup } from '../../core/model/forms/activities/buy-supplies-form.group.model';
import { OtherActivityFormGroup } from '../../core/model/forms/activities/other-activity-form.group.model';
import { MeetingFormGroup } from '../../core/model/forms/activities/meeting-form.group.model';
import { RescueFormGroup } from '../../core/model/forms/activities/rescue-form.group.model';
import { NewMemberFormGroup } from '../../core/model/forms/activities/new-member-form.group.model';
import { PickUpKeeperFormGroup } from '../../core/model/forms/activities/pickup-keeper-form.group.model';
import { VisitClinicFormGroup } from '../../core/model/forms/activities/visit-clinic-form.group.model';
import { CastrationFormGroup } from '../../core/model/forms/activities/castration-form.group.model';
import { take } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { ActivityListComponent } from '../activity-list/activity-list.component';
import { AuthService } from '../../core/services/auth/auth.service';

@Component({
  selector: 'admin-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.scss']
})
export class ActivityDetailComponent extends CommonDetailComponent<Activity> implements OnInit {
  ActivityType: typeof ActivityType = ActivityType;

  form: ActivityFormGroup;
  activeTab: number = 0;

  @Input()
  readonlyType: boolean = false;

  constructor(public activityService: ActivityService, translateService: TranslateService, private authService: AuthService) {
    super(activityService, translateService, 'ACTIVITIES.MODAL_TITLE.Add', 'ACTIVITIES.MODAL_TITLE.Edit');
  }

  changeActivityType(newType: ActivityType) {
    // console.log('ActivityDetailComponent changeActivityType');
    // console.log(newType);
    this.data = ActivityListComponent.newActivity(newType, this.authService);
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.createForm();
    this.patchEntity();
  }

  patchEntity() {
    this.form.patchEntity(this.data);
  }

  parseToEntity(): Activity {
    return this.form.parseToEntity();
  }

  createForm() {
    switch (this.data.type) {
      case ActivityType.ADOPTION:
        this.form = new AdoptionFormGroup();
        break;
      case ActivityType.BUY_SUPPLIES:
        this.form = new BuySuppliesFormGroup();
        break;
      case ActivityType.OTHER:
        this.form = new OtherActivityFormGroup();
        break;
      case ActivityType.MEETING:
        this.form = new MeetingFormGroup();
        break;
      case ActivityType.RESCUE:
        this.form = new RescueFormGroup();
        break;
      case ActivityType.NEW_MEMBER:
        this.form = new NewMemberFormGroup();
        break;
      case ActivityType.PICKUP_KEEPER:
        this.form = new PickUpKeeperFormGroup();
        break;
      case ActivityType.VISIT_CLINIC:
        this.form = new VisitClinicFormGroup();
        break;
      case ActivityType.CASTRATION:
        this.form = new CastrationFormGroup();
        break;
    }
  }

  downloadAdoptionContract() {
    // console.log(this.selected);
    this.activityService.generateAdoptionContract(this.form.get('id').value)
        .pipe(take(1))
        .subscribe(invoice => {
          saveAs(invoice, `contrato.docx`);
        });
  }
}
