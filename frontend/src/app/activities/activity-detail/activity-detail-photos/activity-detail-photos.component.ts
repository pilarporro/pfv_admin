import { Component, Input, OnInit } from '@angular/core';
import { Unsubscribable } from '../../../core/utils/unsubscribable';
import { Photo } from '../../../core/model/base/photo.model';
import { FormGroup } from '@angular/forms';
import { GlobalMessageService } from '../../../core/services/global-messages/global-message.service';
import { StorageService } from '../../../core/services/common/storage.service';
import { take } from 'rxjs/operators';
import { Message } from 'primeng/api';
import { ActivityFormGroup } from '../../../core/model/forms/activities/activity-form.group.model';
import { CommonDetailPhotosComponent } from '../../../core/components/common-detail-photos/common-detail-photos.component';

@Component({
    selector: 'admin-activity-detail-photos',
    templateUrl: './../../../core/components/common-detail-photos/common-detail-photos.component.html',
    styleUrls: ['./../../../core/components/common-detail-photos/common-detail-photos.component.scss',
        './activity-detail-photos.component.scss']
})
export class ActivityDetailPhotosComponent extends CommonDetailPhotosComponent implements OnInit {

    constructor(messageService: GlobalMessageService, storageService: StorageService) {
        super(messageService, storageService);
    }
}
