import { NgModule } from '@angular/core';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { AppCommonModule } from '../app-common.module';
import { ActivitiesRoutingModule } from './activities-routing.module';
import { ActivityDetailComponent } from './activity-detail/activity-detail.component';
import { ActivityDetailDataComponent } from './activity-detail/activity-detail-data/activity-detail-data.component';
import { ActivityDetailPhotosComponent } from './activity-detail/activity-detail-photos/activity-detail-photos.component';
import { AdoptionListComponent } from './activity-list/adoption-list.component';
import { BuySuppliesListComponent } from './activity-list/buy-supplies-list.component';
import { MeetingListComponent } from './activity-list/meeting-list.component';
import { RescueListComponent } from './activity-list/rescue-list.component';
import { NewMemberListComponent } from './activity-list/new-member-list.component';
import { CastrationListComponent } from './activity-list/castration-list.component';
import { PickUpKeeperListComponent } from './activity-list/pickup-keeper-list.component';
import { OtherActivityListComponent } from './activity-list/other-activity-list.component';
import { VisitClinicListComponent } from './activity-list/visit-clinic-list.component';


@NgModule({
    declarations: [
        ActivityListComponent,
        AdoptionListComponent,
        BuySuppliesListComponent,
        MeetingListComponent,
        RescueListComponent,
        NewMemberListComponent,
        CastrationListComponent,
        PickUpKeeperListComponent,
        OtherActivityListComponent,
        VisitClinicListComponent
        ],
    imports: [
        AppCommonModule,
        ActivitiesRoutingModule
    ],
  providers: []
})
export class ActivitiesModule {
}
