import { Component, NgZone, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './core/services/auth/auth.service';
import { PropertyService } from './core/services/settings/property.service';

@Component({
  selector: 'admin-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  menuClick: boolean;

  menuButtonClick: boolean;

  topbarMenuButtonClick: boolean;

  topbarMenuClick: boolean;

  topbarMenuActive: boolean;

  activeTopbarItem: Element;

  layoutMode = 'static';

  sidebarActive: boolean;

  mobileMenuActive: boolean;

  darkMenu = false;

  isRTL = false;

  menuHoverActive: boolean;

  resetMenu: boolean;

  configActive: boolean;

  configClick: boolean;

  inputStyle = 'outlined';

  ripple = true;

  compactMode = false;

  constructor(public router: Router, private translateService: TranslateService,
              public authService: AuthService, public renderer: Renderer2, public zone: NgZone, private primengConfig: PrimeNGConfig,
              private propertyService: PropertyService) {
    translateService.addLangs(['es']);
    translateService.setDefaultLang('es');
    // console.log(router.url);
    const browserLang = localStorage.getItem('lang') ? localStorage.getItem('lang') : translateService.getBrowserLang();
    translateService.use(browserLang.match(/es/) ? browserLang : translateService.getDefaultLang());
  }

  get isAuthenticated() {
    return this.router.url !== '/auth/signin' && this.authService.isAuthenticated();
  }

  ngOnInit() {
    this.primengConfig.ripple = true;
  }

  onWrapperClick() {
    if (!this.menuClick && !this.menuButtonClick) {
      this.mobileMenuActive = false;
    }

    if (!this.topbarMenuClick && !this.topbarMenuButtonClick) {
      this.topbarMenuActive = false;
      this.activeTopbarItem = null;
    }

    if (!this.menuClick) {
      if (this.isHorizontal()) {
        this.resetMenu = true;
      }

      this.menuHoverActive = false;
    }

    if (this.configActive && !this.configClick) {
      this.configActive = false;
    }

    this.configClick = false;
    this.menuClick = false;
    this.menuButtonClick = false;
    this.topbarMenuClick = false;
    this.topbarMenuButtonClick = false;
  }

  onMenuButtonClick(event: Event) {
    this.menuButtonClick = true;

    if (this.isMobile()) {
      this.mobileMenuActive = !this.mobileMenuActive;
    }

    event.preventDefault();
  }

  onTopbarMobileMenuButtonClick(event: Event) {
    this.topbarMenuButtonClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;
    event.preventDefault();
  }

  onTopbarRootItemClick(event: Event, item: Element) {
    if (this.activeTopbarItem === item) {
      this.activeTopbarItem = null; } else {
      this.activeTopbarItem = item; }

    event.preventDefault();
  }

  onTopbarMenuClick(event: Event) {
    this.topbarMenuClick = true;
  }

  onSidebarClick(event: Event) {
    this.menuClick = true;
    this.resetMenu = false;
  }

  onConfigClick(event) {
    this.configClick = true;
  }

  onRippleChange(event) {
    this.ripple = event.checked;
  }

  onToggleMenuClick(event: Event) {
    this.layoutMode = this.layoutMode !== 'static' ? 'static' : 'overlay';
    event.preventDefault();
  }

  isMobile() {
    return window.innerWidth <= 1024;
  }

  isTablet() {
    const width = window.innerWidth;
    return width <= 1024 && width > 640;
  }

  isDesktop() {
    return window.innerWidth > 1024;
  }

  isHorizontal() {
    return this.layoutMode === 'horizontal';
  }

  isOverlay() {
    return this.layoutMode === 'overlay';
  }
}
