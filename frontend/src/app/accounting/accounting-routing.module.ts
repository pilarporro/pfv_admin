import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Role } from '../core/enums/role.enum';
import { AuthGuard } from '../core/services/auth/guards/auth-guard.service';
import { AccountNoteListComponent } from './account-note-list/account-note-list.component';

const routes: Routes = [
  {
    path: 'account-notes',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ACCOUNTING.AccountNoteCrud'},
    canActivate: [AuthGuard],
    component: AccountNoteListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule {
}
