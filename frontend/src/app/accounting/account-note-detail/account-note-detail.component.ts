import { Component, OnInit } from '@angular/core';
import { CommonDetailComponent } from '../../core/components/common-detail.component';
import { TranslateService } from '@ngx-translate/core';
import { AccountNoteService } from '../../core/services/accounting/account-note.service';
import { AccountNote } from '../../core/model/accounting/account-note.model';
import { AccountNoteFormGroup } from '../../core/model/forms/account-note-form.group.model';

@Component({
  selector: 'admin-account-note-detail',
  templateUrl: './account-note-detail.component.html',
  styleUrls: ['./account-note-detail.component.scss']
})
export class AccountNoteDetailComponent extends CommonDetailComponent<AccountNote> implements OnInit {
  form: AccountNoteFormGroup;
  activeTab: number = 0;

  constructor(public accountNoteService: AccountNoteService, translateService: TranslateService) {
    super(accountNoteService, translateService, 'ACCOUNT-NOTES.MODAL_TITLE.Add', 'ACCOUNT-NOTES.MODAL_TITLE.Edit');
    this.createForm();
  }

  ngOnInit(): void {
    this.patchEntity();
  }

  patchEntity() {
    this.form.patchEntity(this.data);
  }

  parseToEntity(): AccountNote {
    return this.form.parseToEntity();
  }

  createForm() {
    this.form = new AccountNoteFormGroup();
  }
}
