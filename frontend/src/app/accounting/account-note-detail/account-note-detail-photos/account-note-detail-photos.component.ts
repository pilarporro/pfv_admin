import { Component, OnInit } from '@angular/core';
import { GlobalMessageService } from '../../../core/services/global-messages/global-message.service';
import { StorageService } from '../../../core/services/common/storage.service';
import { CommonDetailPhotosComponent } from '../../../core/components/common-detail-photos/common-detail-photos.component';

@Component({
    selector: 'admin-account-note-detail-photos',
    templateUrl: './../../../core/components/common-detail-photos/common-detail-photos.component.html',
    styleUrls: ['./../../../core/components/common-detail-photos/common-detail-photos.component.scss']
})
export class AccountNoteDetailPhotosComponent extends CommonDetailPhotosComponent implements OnInit {

    constructor(messageService: GlobalMessageService, storageService: StorageService) {
        super(messageService, storageService);
    }
}
