import { Component, Input, OnInit } from '@angular/core';
import { Unsubscribable } from '../../../core/utils/unsubscribable';
import { AccountNoteFormGroup } from '../../../core/model/forms/account-note-form.group.model';

@Component({
  selector: 'admin-account-note-detail-data',
  templateUrl: './account-note-detail-data.component.html',
  styleUrls: ['./account-note-detail-data.component.scss']
})
export class AccountNoteDetailDataComponent extends Unsubscribable implements OnInit {
  @Input()
  form: AccountNoteFormGroup;

  @Input()
  readonly: boolean = false;

  @Input()
  aliasInUse: boolean = false;

  constructor() {
    super();
  }

  ngOnInit() {

  }
}
