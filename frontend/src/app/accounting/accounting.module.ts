import { NgModule } from '@angular/core';
import { AppCommonModule } from '../app-common.module';
import { AccountNoteListComponent } from './account-note-list/account-note-list.component';
import { AccountingRoutingModule } from './accounting-routing.module';
import { AccountNoteDetailComponent } from './account-note-detail/account-note-detail.component';
import { AccountNoteDetailDataComponent } from './account-note-detail/account-note-detail-data/account-note-detail-data.component';
import { AccountNoteDetailPhotosComponent } from './account-note-detail/account-note-detail-photos/account-note-detail-photos.component';


@NgModule({
    declarations: [
        AccountNoteListComponent,
        AccountNoteDetailComponent,
        AccountNoteDetailDataComponent,
        AccountNoteDetailPhotosComponent
    ],
    imports: [
        AppCommonModule,
        AccountingRoutingModule
    ],
  providers: []
})
export class AccountingModule {
}
