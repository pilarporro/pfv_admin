import { Component } from '@angular/core';

import { ConfirmationService } from 'primeng/api';
import { CommonListComponent } from '../../core/components/common-list.component';
import { AuthService } from '../../core/services/auth/auth.service';
import { TranslationsUtilsService } from '../../core/services/common/translations-utils.service';
import { TableColumType } from '../../core/enums/table/table-column-type.enum';
import { MunicipalityService } from '../../core/services/common/municipality.service';
import { AccountNote } from '../../core/model/accounting/account-note.model';
import { AccountNoteService } from '../../core/services/accounting/account-note.service';
import { AccountType } from '../../core/enums/account-type.enum';
import { AccountSubtype } from '../../core/enums/account-subtype.enum';
import { of } from 'rxjs';

@Component({
    selector: 'admin-account-note-list',
    templateUrl: './account-note-list.component.html',
    styleUrls: ['./account-note-list.component.scss']
})
export class AccountNoteListComponent extends CommonListComponent<AccountNote> {

    constructor(accountNoteService: AccountNoteService, confirmationService: ConfirmationService,
                translationUtilsService: TranslationsUtilsService,
                authService: AuthService, private municipalityService: MunicipalityService) {
        super(accountNoteService, confirmationService, translationUtilsService, authService);
        this.sortField = 'date';
        this.sortOrder = -1;
    }

    static newAccountNote(): AccountNote {
        return {
            active: true,
            notification: true,
            markToDelete: false,
            type: AccountType.BANK,
            subtype: AccountSubtype.VETCLINIC_INVOICE,
            date: new Date()
        };
    }

    createNewEntity(): AccountNote {
        return AccountNoteListComponent.newAccountNote();
    }

    initColumns() {
        this.columns = [
            {
                field: 'notification',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.Notification',
                filter: true,
                filterMatchMode: 'equals'
            },
            {
                field: 'active',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.Active',
                filter: true,
                filterMatchMode: 'equals'
            },
            {
                field: 'date', header: 'ACCOUNT-NOTES.Date', filter: true, filterMatchMode: 'in', type: TableColumType.DATE
            },
            {
                field: 'type', header: 'COMMON.LABELS.Type', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'AccountType'
            },
            {
                field: 'subtype', header: 'COMMON.LABELS.Subtype', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'AccountSubtype'
            },
            {field: 'total', header: 'ACCOUNT-NOTES.Total', filter: true, filterMatchMode: 'equals', type: TableColumType.BANK},
            {
                field: 'person.alias', header: 'ACCOUNT-NOTES.Person', filter: true, filterMatchMode: 'startsWith', filterFields: ['person.alias']
            },
            {
                field: 'place.alias', header: 'PLACES.Title', filter: true, type: TableColumType.PLACE, filterFields: ['place.id'],
                filterMatchMode: 'in'
            },
            {
                field: 'id',
                rowToLabelConverter: (row: AccountNote) => of(row.activities.map(activity => activity.alias).join(', ')),
                header: 'ACCOUNT-NOTES.Activities', filter: true, filterFields: ['activities.alias'], filterMatchMode: 'contains'
            },
            {field: 'notes', header: 'COMMON.LABELS.Notes', filter: true, filterMatchMode: 'contains', class: 'ac-notes', width: '800px'},
            {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
            {field: 'bankBalance', header: 'ACCOUNT-NOTES.BankBalance', filter: true, filterMatchMode: 'equals', type: TableColumType.CASH},
            {field: 'cashBalance', header: 'ACCOUNT-NOTES.CashBalance', filter: true, filterMatchMode: 'equals', type: TableColumType.CASH},
            {
                field: 'grantBalance', header: 'ACCOUNT-NOTES.GrantBalance', filter: true, filterMatchMode: 'equals',
                type: TableColumType.CASH
            }
            ,
            {
                field: 'withPhotos',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.WithPhotos',
                filter: true,
                filterMatchMode: 'equals'
            },
            {
                field: 'manualNote',
                type: TableColumType.BOOLEAN,
                header: 'ACCOUNT-NOTES.ManualNote',
                filter: true,
                filterMatchMode: 'equals'
            }

        ];

        this.selectedColumns = [
            this.columns[0],
            this.columns[2],
            this.columns[3],
            this.columns[4],
            this.columns[6],
            this.columns[9],
            this.columns[5],
            this.columns[14]];
    }
}
