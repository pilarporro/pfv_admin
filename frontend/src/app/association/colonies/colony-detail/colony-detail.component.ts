import { Component, OnInit } from '@angular/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { Utils } from '../../../core/utils/utils';
import { ColonyFormGroup } from '../../../core/model/forms/colony-form.group.model';
import { ColonyService } from '../../../core/services/animals/colony.service';
import { Colony } from '../../../core/model/animals/colony.model';

@Component({
  selector: 'admin-colony-detail',
  templateUrl: './colony-detail.component.html',
  styleUrls: ['./colony-detail.component.scss']
})
export class ColonyDetailComponent extends CommonDetailComponent<Colony> implements OnInit {
  form: ColonyFormGroup;
  activeTab: number = 0;

  constructor(public colonyService: ColonyService, translateService: TranslateService) {
    super(colonyService, translateService, 'COLONIES.MODAL_TITLE.Add', 'COLONIES.MODAL_TITLE.Edit');
    this.createForm();
  }

  ngOnInit(): void {
    this.patchEntity();
  }

  patchEntity() {
    this.form.patchEntity(this.data);
  }

  parseToEntity(): Colony {
    return this.form.parseToEntity();
  }

  createForm() {
    this.form = new ColonyFormGroup();
  }
}
