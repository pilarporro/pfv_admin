import { Component, Input, OnInit } from '@angular/core';
import { Unsubscribable } from '../../../../core/utils/unsubscribable';
import { AnimalFormGroup } from '../../../../core/model/forms/animal-form.group.model';
import { ColonyFormGroup } from '../../../../core/model/forms/colony-form.group.model';

@Component({
  selector: 'admin-colony-detail-data',
  templateUrl: './colony-detail-data.component.html',
  styleUrls: ['./colony-detail-data.component.scss']
})
export class ColonyDetailDataComponent extends Unsubscribable implements OnInit {
  @Input()
  form: ColonyFormGroup;

  @Input()
  readonly: boolean = false;

  @Input()
  aliasInUse: boolean = false;

  constructor() {
    super();
  }

  ngOnInit() {

  }
}
