import { Component, OnInit } from '@angular/core';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import { MunicipalityService } from '../../../core/services/common/municipality.service';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';
import { Colony } from '../../../core/model/animals/colony.model';
import { ColonyService } from '../../../core/services/animals/colony.service';
import { AliasIdLabel } from '../../../core/model/base/alias-id-label.model';
import { Person } from '../../../core/model/persons/person.model';

@Component({
  selector: 'admin-colony-list',
  templateUrl: './colony-list.component.html',
  styleUrls: ['./colony-list.component.scss']
})
export class ColonyListComponent extends CommonListComponent<Colony> {

  constructor(colonyService: ColonyService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService,
              authService: AuthService, private municipalityService: MunicipalityService) {
    super(colonyService, confirmationService, translationUtilsService, authService);
    this.sortField = 'created';
    this.sortOrder = -1;
  }

  static newColony(municipalityService: MunicipalityService, personId?: string): Colony {
    const colony: Colony = {
      active: true,
      markToDelete: false,
      notification: false,
      location: {
        municipalityId: municipalityService.defaultMunicipalityId
      }
    };
    if (personId) {
      colony.person = {
        id: personId
      };
    }

    return colony;
  }

  createNewEntity(): Colony {
    return ColonyListComponent.newColony(this.municipalityService);
  }

  initColumns() {
    this.columns = [
      {
        field: 'active',
        type: TableColumType.BOOLEAN,
        header: 'COMMON.LABELS.Active',
        filter: true,
        filterMatchMode: 'equals'
      },
      {field: 'alias', header: 'COMMON.LABELS.Alias', filter: true, filterMatchMode: 'contains'},
      {field: 'name', header: 'COMMON.LABELS.Name', filter: true, filterMatchMode: 'contains'},
      {field: 'person.alias', header: 'COLONIES.Person', filter: true, filterMatchMode: 'startsWith'},
      {
        field: 'location.areaName', filterFields: ['location.area.id'], header: 'LOCATION.Area',
        filter: true, filterMatchMode: 'in', type: TableColumType.AREA
      },
      {field: 'totalAnimals', header: 'COLONIES.TotalAnimals', filter: true, filterMatchMode: 'equals'},
      {field: 'totalNeuteredAnimals', header: 'COLONIES.TotalNeuteredAnimals', filter: true, filterMatchMode: 'equals'},
      {field: 'totalFemaleAnimals', header: 'COLONIES.TotalFemaleAnimals', filter: true, filterMatchMode: 'equals'},
      {field: 'totalNeuteredFemaleAnimals', header: 'COLONIES.TotalNeuteredFemaleAnimals', filter: true, filterMatchMode: 'equals'},
      {field: 'initDate', header: 'COLONIES.InitDate', filter: true, filterMatchMode: 'in', type: TableColumType.DATE},
      {field: 'location.url', header: 'LOCATION.Address', type: TableColumType.MAP, filter: false },
      {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
      {
        field: 'markToDelete',
        type: TableColumType.BOOLEAN,
        header: 'COMMON.LABELS.MarkToDelete',
        filter: true,
        filterMatchMode: 'equals'
      }
    ];

    this.selectedColumns = [
      this.columns[1],
      this.columns[2],
      this.columns[3],
      this.columns[4],
      this.columns[5],
      this.columns[6],
      this.columns[7],
      this.columns[8],
      this.columns[10]];
  }
}
