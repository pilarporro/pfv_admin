import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonListComponent } from './persons/person-list/person-list.component';
import { AssociationRoutingModule } from './association-routing.module';
import { AppCommonModule } from '../app-common.module';
import { environment } from '../../environments/environment';
import { PersonDetailDataComponent } from './persons/person-detail/person-detail-data/person-detail-data.component';
import { PersonDetailComponent } from './persons/person-detail/person-detail.component';
import { PersonDetailPhotosComponent } from './persons/person-detail/person-detail-photos/person-detail-photos.component';
import { PhotoEditorComponent } from './persons/person-detail/photo-editor/photo-editor.component';
import { PlaceListComponent } from './places/place-list/place-list.component';
import { PlaceDetailComponent } from './places/place-detail/place-detail.component';
import { PlaceDetailDataComponent } from './places/place-detail/place-detail-data/place-detail-data.component';
import { AnimalDetailComponent } from './animals/animal-detail/animal-detail.component';
import { AnimalListComponent } from './animals/animal-list/animal-list.component';
import { AnimalDetailDataComponent } from './animals/animal-detail/animal-detail-data/animal-detail-data.component';
import { AnimalDetailPhotosComponent } from './animals/animal-detail/animal-detail-photos/animal-detail-photos.component';
import { ColonyListComponent } from './colonies/colony-list/colony-list.component';
import { ColonyDetailComponent } from './colonies/colony-detail/colony-detail.component';
import { ColonyDetailDataComponent } from './colonies/colony-detail/colony-detail-data/colony-detail-data.component';
import { ColonyDetailPhotosComponent } from './colonies/colony-detail/colony-detail-photos/colony-detail-photos.component';
import { ReportsComponent } from './reports/reports.component';
import { TrapListComponent } from './traps/trap-list/trap-list.component';
import { TrapDetailComponent } from './traps/trap-detail/trap-detail.component';
import { TrapDetailPhotosComponent } from './traps/trap-detail/trap-detail-photos/trap-detail-photos.component';
import { TrapDetailDataComponent } from './traps/trap-detail/trap-detail-data/trap-detail-data.component';

@NgModule({
    imports: [
        AppCommonModule,
        AssociationRoutingModule
    ],
    declarations: [
        PersonListComponent,
        PlaceListComponent,
        AnimalListComponent,
        ColonyListComponent,
        ReportsComponent,
        TrapListComponent,
        TrapDetailComponent,
        TrapDetailPhotosComponent,
        TrapDetailDataComponent
        ]
})
export class AssociationModule {
}
