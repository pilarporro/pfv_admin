import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonListComponent } from './persons/person-list/person-list.component';
import { Role } from '../core/enums/role.enum';
import { AuthGuard } from '../core/services/auth/guards/auth-guard.service';
import { PlaceListComponent } from './places/place-list/place-list.component';
import { AnimalListComponent } from './animals/animal-list/animal-list.component';
import { ColonyListComponent } from './colonies/colony-list/colony-list.component';
import { ReportsComponent } from './reports/reports.component';
import { TrapListComponent } from './traps/trap-list/trap-list.component';

const routes: Routes = [
  {
    path: 'persons',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ASSOCIATION.PersonsCrud'},
    canActivate: [AuthGuard],
    component: PersonListComponent
  },
  {
    path: 'colonies',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ASSOCIATION.ColoniesCrud'},
    canActivate: [AuthGuard],
    component: ColonyListComponent
  },
  {
    path: 'traps',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ASSOCIATION.TrapsCrud'},
    canActivate: [AuthGuard],
    component: TrapListComponent
  },
  {
    path: 'animals',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ASSOCIATION.AnimalsCrud'},
    canActivate: [AuthGuard],
    component: AnimalListComponent
  },
  {
    path: 'places',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ASSOCIATION.PlacesCrud'},
    canActivate: [AuthGuard],
    component: PlaceListComponent
  },
  {
    path: 'reports',
    data: {roles: [Role.USER], breadcrumb: 'MENU.ASSOCIATION.Reports'},
    canActivate: [AuthGuard],
    component: ReportsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssociationRoutingModule {
}
