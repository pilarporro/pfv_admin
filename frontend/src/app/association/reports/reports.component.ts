import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { finalize, take } from 'rxjs/operators';
import { Utils } from '../../core/utils/utils';
import { ReportsService } from '../../core/services/reporting/reports.service';
import { saveAs } from 'file-saver';

@Component({
    selector: 'admin-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
    form: FormGroup;
    generatingReport: boolean = false;

    constructor(private reportsService: ReportsService) {
        this.form = new FormGroup({
            from: new FormControl(),
            to: new FormControl(),
            invoiceRef: new FormControl()
        });
    }

    ngOnInit() {

    }

    validForm() {
        if (this.form.valid) {
            return true;
        } else {
            Utils.updateValidity(this.form);

            return false;
        }
    }

    downloadActivitiesReport() {
        if (this.validForm()) {
            this.generatingReport = true;
            this.reportsService.generateReport(this.form.get('from').value, this.form.get('to').value)
                .pipe(take(1), finalize(() => this.generatingReport = false))
                .subscribe(data => {
                    saveAs(data, `informe.docx`);
                });
        }
    }

    downloadAccountsReport() {
        if (this.validForm()) {
            this.generatingReport = true;
            this.reportsService.generateAccountsReport(this.form.get('from').value, this.form.get('to').value)
                .pipe(take(1), finalize(() => this.generatingReport = false))
                .subscribe(data => {
                    saveAs(data, `libro_cuentas.docx`);
                });
        }
    }

    downloadCastrationsEmail() {
        if (this.validForm()) {
            this.generatingReport = true;
            this.reportsService.generateCastrationsEmail(this.form.get('invoiceRef').value)
                .pipe(take(1), finalize(() => this.generatingReport = false))
                .subscribe(data => {
                    saveAs(data, `resumen_y_fichas.zip`);
                });
        }
    }

    downloadColoniesReport() {
        if (this.validForm()) {
            this.generatingReport = true;
            this.reportsService.generateColoniesReport()
                .pipe(take(1), finalize(() => this.generatingReport = false))
                .subscribe(data => {
                    saveAs(data, `censo.docx`);
                });
        }
    }

    downloadColoniesReportExcel() {
        if (this.validForm()) {
            this.generatingReport = true;
            this.reportsService.generateColoniesReportExcel()
                .pipe(take(1), finalize(() => this.generatingReport = false))
                .subscribe(data => {
                    saveAs(data, `censo.xlsx`);
                });
        }
    }
}
