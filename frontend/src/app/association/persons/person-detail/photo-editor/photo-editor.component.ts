import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';
import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { CommonDialogComponent } from '../../../../core/components/common-dialog.component';
import { Photo } from '../../../../core/model/base/photo.model';
import { StorageService } from '../../../../core/services/common/storage.service';
import { PersonService } from '../../../../core/services/persons/person.service';

@Component({
  selector: 'admin-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.scss']
})
export class PhotoEditorComponent extends CommonDialogComponent<Photo> implements OnInit {
  @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageFileChanged: any;
  maxWidthSize: number;
  imageBase64: any;

  rotation: number = 0;
  scale: number = 1;
  maintainAspectRatio: boolean = false;

  constructor(public storageService: StorageService, personService: PersonService,
              translateService: TranslateService, private httpClient: HttpClient) {
    super(personService, translateService);
  }

  ngOnInit(): void {
    this.httpClient.get(this.data.url, {responseType: 'blob'})
      .pipe(take(1))
      .subscribe(data => {
        // console.log(data);
        this.imageFileChanged = data;
      });

  }

  resize(scale: number) {
    this.scale += scale;
    this.modifyImage();
  }

  rotate(rotation: number) {
    this.rotation += rotation;
    this.modifyImage();
  }

  modifyImage() {
    /*
    Jimp.read(this.data.url)
      .then(image => {
        image
          .rotate(this.rotation)
          .scale(this.scale)
          .getBase64Async(Jimp.MIME_PNG)
          .then(imageBase64 => {
            // console.log(imageBase64);
            this.imageBase64 = imageBase64;
          });
      })
      .catch(err => {
        console.log(err);
      });*/
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    // Change the extension
    let nameWithoutExtension = this.data.diskName;
    if (this.data.diskName.lastIndexOf('.') > 0) {
      nameWithoutExtension = nameWithoutExtension.substr(0, this.data.diskName.lastIndexOf('.'));
    }

    this.storageService.upload(this.blobToFile(this.dataURItoBlob(event.base64), `${nameWithoutExtension}.png`))
      .pipe(
        take(1),
        finalize(() => {
          this.hideDialog();
        })
      ).subscribe((httpEvent: HttpEvent<any>) => {
      if (httpEvent instanceof HttpResponse) {
        this.successDialog.emit(httpEvent.body);
      }
    });
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });
    return blob;
  }

  blobToFile(theBlob: Blob, fileName: string): File {
    const fileBlob: any = theBlob;
    fileBlob.lastModifiedDate = new Date();
    fileBlob.name = fileName;

    return theBlob as File;
  }

  loadImageFailed() {
    // show message
  }

  save(): void {
    this.imageCropper.crop();
  }
}
