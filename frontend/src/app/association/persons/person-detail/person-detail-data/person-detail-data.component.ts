import { Component, Input, OnInit } from '@angular/core';
import * as moment_ from 'moment';
import { Unsubscribable } from '../../../../core/utils/unsubscribable';
import { PersonFormGroup } from '../../../../core/model/forms/person-form.group.model';
import { AuthService } from '../../../../core/services/auth/auth.service';

const moment = moment_;

@Component({
  selector: 'admin-person-detail-data',
  templateUrl: './person-detail-data.component.html',
  styleUrls: ['./person-detail-data.component.scss']
})
export class PersonDetailDataComponent extends Unsubscribable implements OnInit {
  @Input()
  form: PersonFormGroup;

  @Input()
  readonly: boolean = false;

  @Input()
  aliasInUse: boolean = false;

  isAdmin: boolean = false;

  constructor(private authService: AuthService) {
    super();
    this.isAdmin = authService.isUserAdmin();
  }

  ngOnInit() {

  }
}
