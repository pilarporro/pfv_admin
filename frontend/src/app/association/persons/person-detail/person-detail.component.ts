import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { take } from 'rxjs/operators';
import { Person } from '../../../core/model/persons/person.model';
import { PersonFormGroup } from '../../../core/model/forms/person-form.group.model';
import { PersonService } from '../../../core/services/persons/person.service';
import { Utils } from '../../../core/utils/utils';

@Component({
    selector: 'admin-person-detail',
    templateUrl: './person-detail.component.html',
    styleUrls: ['./person-detail.component.scss']
})
export class PersonDetailComponent extends CommonDetailComponent<Person> implements OnInit {
    form: PersonFormGroup;
    activeTab: number = 0;

    constructor(public personService: PersonService, translateService: TranslateService) {
        super(personService, translateService, 'PERSONS.MODAL_TITLE.Add', 'PERSONS.MODAL_TITLE.Edit');
        this.createForm();
    }

    ngOnInit(): void {
        this.patchEntity();
    }

    patchEntity() {
        this.form.patchEntity(this.data);
    }

    parseToEntity(): Person {
        return this.form.parseToEntity();
    }

    createForm() {
        this.form = new PersonFormGroup();
    }
}
