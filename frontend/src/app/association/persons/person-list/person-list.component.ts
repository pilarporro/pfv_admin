import { Component } from '@angular/core';

import { ConfirmationService } from 'primeng/api';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { PersonService } from '../../../core/services/persons/person.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import { Person } from '../../../core/model/persons/person.model';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';
import { of } from 'rxjs';
import { MunicipalityService } from '../../../core/services/common/municipality.service';
import { Animal } from '../../../core/model/animals/animal.model';
import { AnimalStatus } from '../../../core/enums/animal-status.enum';
import { AnimalType } from '../../../core/enums/animal-type.enum';

@Component({
    selector: 'admin-art-profiles-list',
    templateUrl: './person-list.component.html',
    styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent extends CommonListComponent<Person> {

    constructor(personService: PersonService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService,
                authService: AuthService, private municipalityService: MunicipalityService) {
        super(personService, confirmationService, translationUtilsService, authService);
        this.sortField = 'alias';
        this.sortOrder = 1;
    }

    static newPerson(municipalityService: MunicipalityService): Person {
        return {
            active: true,
            markToDelete: false,
            notification: true,
            isMember: false,
            location: {
                municipalityId: municipalityService.defaultMunicipalityId
            }
        };
    }

    createNewEntity(): Person {
        return PersonListComponent.newPerson(this.municipalityService);
    }

    initColumns() {
        this.columns = [
            {
                field: 'active',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.Active',
                filter: true,
                filterMatchMode: 'equals'
            },
            {field: 'isMember', header: 'PERSONS.IsMember', filter: true, filterMatchMode: 'equals', type: TableColumType.BOOLEAN},
            {field: 'alias', header: 'COMMON.LABELS.Alias', filter: true, filterMatchMode: 'contains'},
            {field: 'name', header: 'COMMON.LABELS.Name', filter: true, filterMatchMode: 'contains'},
            {field: 'lastName', header: 'COMMON.LABELS.LastName', filter: true, filterMatchMode: 'contains'},
            {field: 'phone', header: 'COMMON.LABELS.Phone', filter: true, filterMatchMode: 'contains'},
            {
                field: 'location.areaName', filterFields: ['location.area.id'], header: 'LOCATION.Area',
                filter: true, filterMatchMode: 'in', type: TableColumType.AREA
            },
            {field: 'location.url', header: 'LOCATION.Address', type: TableColumType.MAP, filter: false },
            {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
            {
                field: 'markToDelete',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.MarkToDelete',
                filter: true,
                filterMatchMode: 'equals'
            },
            {field: 'nextPayment', header: 'PERSONS.NextPayment', filter: true, filterMatchMode: 'in', type: TableColumType.DATE_MEMBER},

        ];

        this.selectedColumns = [
            this.columns[1],
            this.columns[2],
            this.columns[3],
            this.columns[4],
            this.columns[5],
            this.columns[6],
            this.columns[7],
            this.columns[10]];
    }
}
