import { Component, Input, OnInit } from '@angular/core';
import { Unsubscribable } from '../../../../core/utils/unsubscribable';
import { ColonyFormGroup } from '../../../../core/model/forms/colony-form.group.model';
import { TrapFormGroup } from '../../../../core/model/forms/trap-form.group.model';

@Component({
  selector: 'admin-trap-detail-data',
  templateUrl: './trap-detail-data.component.html',
  styleUrls: ['./trap-detail-data.component.scss']
})
export class TrapDetailDataComponent extends Unsubscribable implements OnInit {
  @Input()
  form: TrapFormGroup;

  @Input()
  readonly: boolean = false;

  @Input()
  aliasInUse: boolean = false;

  constructor() {
    super();
  }

  ngOnInit() {

  }
}