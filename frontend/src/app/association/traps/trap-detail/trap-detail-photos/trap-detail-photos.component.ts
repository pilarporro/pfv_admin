import { Component, OnInit } from '@angular/core';
import { CommonDetailPhotosComponent } from '../../../../core/components/common-detail-photos/common-detail-photos.component';
import { GlobalMessageService } from '../../../../core/services/global-messages/global-message.service';
import { StorageService } from '../../../../core/services/common/storage.service';

@Component({
  selector: 'admin-trap-detail-photos',
  templateUrl: './../../../../core/components/common-detail-photos/common-detail-photos.component.html',
  styleUrls: ['./../../../../core/components/common-detail-photos/common-detail-photos.component.scss']
})
export class TrapDetailPhotosComponent extends CommonDetailPhotosComponent implements OnInit {

  constructor(messageService: GlobalMessageService, storageService: StorageService) {
    super(messageService, storageService);
  }

}
