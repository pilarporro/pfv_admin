import { Component, OnInit } from '@angular/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { TranslateService } from '@ngx-translate/core';
import { TrapService } from '../../../core/services/persons/trap.service';
import { Trap } from '../../../core/model/persons/trap.model';
import { TrapFormGroup } from '../../../core/model/forms/trap-form.group.model';

@Component({
  selector: 'admin-trap-detail',
  templateUrl: './trap-detail.component.html',
  styleUrls: ['./trap-detail.component.scss']
})
export class TrapDetailComponent extends CommonDetailComponent<Trap> implements OnInit {
  form: TrapFormGroup;
  activeTab: number = 0;

  constructor(public trapService: TrapService, translateService: TranslateService) {
    super(trapService, translateService, 'TRAPS.MODAL_TITLE.Add', 'TRAPS.MODAL_TITLE.Edit');
    this.createForm();
  }

  ngOnInit(): void {
    this.patchEntity();
  }

  patchEntity() {
    this.form.patchEntity(this.data);
  }

  parseToEntity(): Trap {
    return this.form.parseToEntity();
  }

  createForm() {
    this.form = new TrapFormGroup();
  }
}
