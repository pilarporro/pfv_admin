import { Component } from '@angular/core';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import { MunicipalityService } from '../../../core/services/common/municipality.service';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';
import { Trap } from '../../../core/model/persons/trap.model';
import { TrapService } from '../../../core/services/persons/trap.service';

@Component({
  selector: 'admin-trap-list',
  templateUrl: './trap-list.component.html',
  styleUrls: ['./trap-list.component.scss']
})
export class TrapListComponent extends CommonListComponent<Trap> {

  constructor(trapService: TrapService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService,
              authService: AuthService, private municipalityService: MunicipalityService) {
    super(trapService, confirmationService, translationUtilsService, authService);
    this.sortField = 'created';
    this.sortOrder = -1;
  }

  static newTrap(): Trap {
    const trap: Trap = {
      active: true,
      markToDelete: false
    };

    return trap;
  }

  createNewEntity(): Trap {
    return TrapListComponent.newTrap();
  }

  initColumns() {
    this.columns = [
      {
        field: 'active',
        type: TableColumType.BOOLEAN,
        header: 'COMMON.LABELS.Active',
        filter: true,
        filterMatchMode: 'equals'
      },
      {field: 'alias', header: 'COMMON.LABELS.Alias', filter: true, filterMatchMode: 'contains'},
      {field: 'name', header: 'COMMON.LABELS.Name', filter: true, filterMatchMode: 'contains'},
      {field: 'owner.alias', header: 'TRAPS.Owner', filter: true, filterMatchMode: 'startsWith'},
      {field: 'person.alias', header: 'TRAPS.Person', filter: true, filterMatchMode: 'startsWith'},
      {field: 'usingFrom', header: 'TRAPS.UsingFrom', filter: true, filterMatchMode: 'in', type: TableColumType.DATE},
      { field: 'notes', header: 'COMMON.LABELS.Notes', filter: true, filterMatchMode: 'contains' },
      {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
      {
        field: 'markToDelete',
        type: TableColumType.BOOLEAN,
        header: 'COMMON.LABELS.MarkToDelete',
        filter: true,
        filterMatchMode: 'equals'
      }
    ];

    this.selectedColumns = [
      this.columns[0],
      this.columns[2],
      this.columns[3],
      this.columns[4],
      this.columns[5],
      this.columns[6]];
  }
}
