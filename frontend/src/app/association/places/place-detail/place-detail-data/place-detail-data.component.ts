import { Component, Input, OnInit } from '@angular/core';
import { Unsubscribable } from '../../../../core/utils/unsubscribable';
import { PersonFormGroup } from '../../../../core/model/forms/person-form.group.model';

@Component({
  selector: 'admin-place-detail-data',
  templateUrl: './place-detail-data.component.html',
  styleUrls: ['./place-detail-data.component.scss']
})
export class PlaceDetailDataComponent extends Unsubscribable implements OnInit {
  @Input()
  form: PersonFormGroup;

  @Input()
  readonly: boolean = false;

  @Input()
  aliasInUse: boolean = false;

  constructor() {
    super();
  }

  ngOnInit() {

  }
}
