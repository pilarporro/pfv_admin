import { Component, OnInit } from '@angular/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { Place } from '../../../core/model/places/place.model';
import { PlaceFormGroup } from '../../../core/model/forms/place-form.group.model';
import { PlaceService } from '../../../core/services/places/place.service';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { Utils } from '../../../core/utils/utils';

@Component({
    selector: 'admin-place-detail',
    templateUrl: './place-detail.component.html',
    styleUrls: ['./place-detail.component.scss']
})
export class PlaceDetailComponent extends CommonDetailComponent<Place> implements OnInit {
    form: PlaceFormGroup;
    activeTab: number = 0;

    constructor(public placeService: PlaceService, translateService: TranslateService) {
        super(placeService, translateService, 'PLACES.MODAL_TITLE.Add', 'PLACES.MODAL_TITLE.Edit');
        this.createForm();
    }

    ngOnInit(): void {
        this.patchEntity();
    }

    patchEntity() {
        this.form.patchEntity(this.data);
    }

    parseToEntity(): Place {
        return this.form.parseToEntity();
    }

    createForm() {
        this.form = new PlaceFormGroup();
    }
}
