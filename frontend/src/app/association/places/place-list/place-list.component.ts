import { Component } from '@angular/core';

import { ConfirmationService } from 'primeng/api';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { PlaceService } from '../../../core/services/places/place.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import { Place } from '../../../core/model/places/place.model';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';
import { MunicipalityService } from '../../../core/services/common/municipality.service';
import { Animal } from '../../../core/model/animals/animal.model';
import { AnimalStatus } from '../../../core/enums/animal-status.enum';
import { AnimalType } from '../../../core/enums/animal-type.enum';

@Component({
  selector: 'admin-place-list',
  templateUrl: './place-list.component.html',
  styleUrls: ['./place-list.component.scss']
})
export class PlaceListComponent extends CommonListComponent<Place> {

  constructor(placeService: PlaceService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService,
              authService: AuthService, private municipalityService: MunicipalityService) {
    super(placeService, confirmationService, translationUtilsService, authService);
    this.sortField = 'alias';
    this.sortOrder = 1;
  }

  static newPlace(municipalityService: MunicipalityService): Place {
    return {
      active: true,
      markToDelete: false,
      notification: false,
      location: {
        municipalityId: municipalityService.defaultMunicipalityId
      }
    };
  }

  createNewEntity(): Place {
    return PlaceListComponent.newPlace(this.municipalityService);
  }

  initColumns() {
    this.columns = [
      {
        field: 'active',
        type: TableColumType.BOOLEAN,
        header: 'COMMON.Active',
        filter: true,
        filterMatchMode: 'equals'
      },
      {field: 'type', header: 'COMMON.LABELS.Type', filter: true, filterMatchMode: 'in', type: TableColumType.ENUM, enumClass: 'PlaceType'},
      {field: 'alias', header: 'COMMON.LABELS.Alias', filter: true, filterMatchMode: 'contains'},
      {field: 'name', header: 'COMMON.LABELS.Name', filter: true, filterMatchMode: 'contains'},
      {field: 'phone', header: 'COMMON.LABELS.Phone', filter: true, filterMatchMode: 'contains'},
      {
        field: 'location.municipalityName', filterFields: ['location.municipality.id'], header: 'LOCATION.Municipality',
        filter: true, filterMatchMode: 'contains', type: TableColumType.MUNICIPALITY
      },
      {
        field: 'location.areaName', filterFields: ['location.area.id'], header: 'LOCATION.Area',
        filter: true, filterMatchMode: 'in', type: TableColumType.AREA
      },
      {field: 'location.url', header: 'LOCATION.Address', type: TableColumType.MAP, filter: false },
      {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
      {
        field: 'markToDelete',
        type: TableColumType.BOOLEAN,
        header: 'COMMON.LABELS.MarkToDelete',
        filter: true,
        filterMatchMode: 'equals'
      }
    ];

    this.selectedColumns = [
      this.columns[1],
      this.columns[2],
      this.columns[3],
      this.columns[4],
      this.columns[5],
      this.columns[6],
      this.columns[7]];
  }
}
