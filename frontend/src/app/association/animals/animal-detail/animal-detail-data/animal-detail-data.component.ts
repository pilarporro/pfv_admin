import { Component, Input, OnInit } from '@angular/core';
import { Unsubscribable } from '../../../../core/utils/unsubscribable';
import { AnimalFormGroup } from '../../../../core/model/forms/animal-form.group.model';

@Component({
    selector: 'admin-animal-detail-data',
    templateUrl: './animal-detail-data.component.html',
    styleUrls: ['./animal-detail-data.component.scss']
})
export class AnimalDetailDataComponent extends Unsubscribable implements OnInit {
    @Input()
    form: AnimalFormGroup;

    @Input()
    readonly: boolean = false;

    @Input()
    aliasInUse: boolean = false;

    constructor() {
        super();
    }

    ngOnInit() {

    }
}
