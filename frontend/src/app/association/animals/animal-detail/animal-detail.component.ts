import { Component, OnInit } from '@angular/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { Animal } from '../../../core/model/animals/animal.model';
import { AnimalFormGroup } from '../../../core/model/forms/animal-form.group.model';
import { AnimalService } from '../../../core/services/animals/animal.service';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { Utils } from '../../../core/utils/utils';

@Component({
    selector: 'admin-animal-detail',
    templateUrl: './animal-detail.component.html',
    styleUrls: ['./animal-detail.component.scss']
})
export class AnimalDetailComponent extends CommonDetailComponent<Animal> implements OnInit {
    form: AnimalFormGroup;
    activeTab: number = 0;

    constructor(public animalService: AnimalService, translateService: TranslateService) {
        super(animalService, translateService, 'ANIMALS.MODAL_TITLE.Add', 'ANIMALS.MODAL_TITLE.Edit');
        this.createForm();
    }

    ngOnInit(): void {
        this.patchEntity();
    }

    patchEntity() {
        this.form.patchEntity(this.data);
    }

    parseToEntity(): Animal {
        return this.form.parseToEntity();
    }

    createForm() {
        this.form = new AnimalFormGroup();
    }
}
