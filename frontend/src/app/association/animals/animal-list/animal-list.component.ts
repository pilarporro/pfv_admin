import { Component } from '@angular/core';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { Animal } from '../../../core/model/animals/animal.model';
import { AnimalService } from '../../../core/services/animals/animal.service';
import { ConfirmationService } from 'primeng/api';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import { MunicipalityService } from '../../../core/services/common/municipality.service';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';
import { AnimalStatus } from '../../../core/enums/animal-status.enum';
import { AnimalType } from '../../../core/enums/animal-type.enum';
import { Gender } from '../../../core/enums/gender.enum';

@Component({
    selector: 'admin-animal-list',
    templateUrl: './animal-list.component.html',
    styleUrls: ['./animal-list.component.scss']
})
export class AnimalListComponent extends CommonListComponent<Animal> {

    constructor(animalService: AnimalService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService,
                authService: AuthService, private municipalityService: MunicipalityService) {
        super(animalService, confirmationService, translationUtilsService, authService);
        this.sortField = 'alias';
        this.sortOrder = 1;
    }

    static newAnimal(): Animal {
        return {
            active: true,
            neutered: false,
            markToDelete: false,
            notification: false,
            tags: [],
            animalStatus: AnimalStatus.FREE_IN_COLONY,
            gender: Gender.UNKNOWN,
            type: AnimalType.CAT
        };
    }

    createNewEntity(): Animal {
        return AnimalListComponent.newAnimal();
    }

    initColumns() {
        this.columns = [
            {
                field: 'active',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.Active',
                filter: true,
                filterMatchMode: 'equals'
            },
            {
                field: 'neutered',
                type: TableColumType.BOOLEAN,
                header: 'ANIMALS.Neutered',
                filter: true,
                filterMatchMode: 'equals'
            },
            {
                field: 'type', header: 'COMMON.LABELS.Type', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'AnimalType'
            },
            {
                field: 'gender', header: 'ANIMALS.Gender', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'Gender'
            },
            {field: 'alias', header: 'COMMON.LABELS.Alias', filter: true, filterMatchMode: 'contains'},
            {field: 'animalStatus', header: 'ANIMALS.Status', filter: true, filterMatchMode: 'in', type: TableColumType.ENUM, enumClass: 'AnimalStatus'},
            {field: 'name', header: 'COMMON.LABELS.Name', filter: true, filterMatchMode: 'contains'},
            {
                field: 'owner.alias', header: 'ANIMALS.Owner', filter: true, type: TableColumType.PERSON, filterFields: ['owner.id']
            },
            {
                field: 'keeper.alias', header: 'ANIMALS.Keeper', filter: true, type: TableColumType.PERSON, filterFields: ['keeper.id']
            },
            {
                field: 'adopter.alias', header: 'ANIMALS.Adopter', filter: true, type: TableColumType.PERSON, filterFields: ['adopter.id']
            },
            {field: 'id', header: 'COMMON.LABELS.Id', filter: true, filterMatchMode: 'equals'},
            {
                field: 'color', header: 'ANIMALS.Color', filter: true, filterMatchMode: 'in',
                type: TableColumType.ENUM, enumClass: 'Color'
            },
            {
                field: 'markToDelete',
                type: TableColumType.BOOLEAN,
                header: 'COMMON.LABELS.MarkToDelete',
                filter: true,
                filterMatchMode: 'equals'
            }
        ];

        this.selectedColumns = [
            this.columns[1],
            this.columns[3],
            this.columns[4],
            this.columns[5],
            this.columns[6],
            this.columns[7],
            this.columns[8],
            this.columns[9]];
    }
}
