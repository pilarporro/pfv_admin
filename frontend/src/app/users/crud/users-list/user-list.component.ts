import { Component } from '@angular/core';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { ConfirmationService } from 'primeng/api';
import { FormControl } from '@angular/forms';
import { UserService } from '../../../core/services/persons/user.service';
import { User } from '../../../core/model/persons/user.model';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { Country } from '../../../core/enums/country.enum';
import { Role } from '../../../core/enums/role.enum';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';

@Component({
  selector: 'admin-users-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent extends CommonListComponent<User> {

  constructor(userService: UserService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService) {
    super(userService, confirmationService, translationUtilsService);
    // this.filters['admin'] = {value: false, matchMode: 'equals'};
  }

  createNewEntity(): User {
    return {
      password: 'cambiame',
      roles: [ Role.USER ]
    };
  }

  initColumns() {
    this.columns = [
      {field: 'username', header: 'USERS.Username', filter: true, filterMatchMode: 'contains'},
      {field: 'name', header: 'COMMON.LABELS.Name', filter: true, filterMatchMode: 'contains'},
      {field: 'lastName', header: 'COMMON.LABELS.LastName', filter: true, filterMatchMode: 'contains'},
      {
        field: 'roles',
        type: TableColumType.ENUM_ARRAY,
        enumClass: 'Role',
        header: 'USERS.Roles',
        filter: true,
        filterMatchMode: 'in'
      },
      {field: 'phone', header: 'COMMON.LABELS.Phone', filter: true, filterMatchMode: 'contains'},
      {field: 'email', header: 'COMMON.LABELS.Email', filter: true, filterMatchMode: 'contains'}
    ];
  }
}
