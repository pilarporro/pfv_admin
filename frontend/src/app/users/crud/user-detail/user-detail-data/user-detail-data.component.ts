import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../../../../core/services/auth/auth.service';
import { Role } from '../../../../core/enums/role.enum';
import { Utils } from '../../../../core/utils/utils';

@Component({
  selector: 'admin-user-detail-data',
  templateUrl: './user-detail-data.component.html',
  styleUrls: ['./user-detail-data.component.scss']
})
export class UserDetailDataComponent implements OnInit {
  Role: typeof Role = Role;

  @Input()
  form: FormGroup;

  @Input()
  userInUse: boolean = false;

  @Input()
  emailInUse: boolean = false;

  readonly: boolean = true;
  newUser: boolean = false;
  admin: boolean = false;

  constructor(public authService: AuthService) {

  }

  ngOnInit(): void {
    this.admin = this.authService.isUserAdmin();
    this.newUser = Utils.isNotDefined(this.form.get('id').value);
    this.readonly = !(this.admin || Utils.isNotDefined(this.form.get('id').value)
      || (this.authService.getLoggedUser().id === this.form.get('id').value)
      || (this.admin && (!this.form.get('superadmin').value))
    );
  }
}
