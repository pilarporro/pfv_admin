import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { take, takeUntil } from 'rxjs/operators';
import { User } from '../../../core/model/persons/user.model';
import { AuthService } from '../../../core/services/auth/auth.service';
import { UserService } from '../../../core/services/persons/user.service';
import { AuditableFormGroup } from '../../../core/model/forms/auditable-form-group.model';
import { Utils } from '../../../core/utils/utils';

@Component({
  selector: 'admin-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent extends CommonDetailComponent<User> implements OnInit {
  userInUse: boolean = false;
  emailInUse: boolean = false;
  activeTab: number = 0;
  admin: boolean = false;

  constructor(public userService: UserService, translateService: TranslateService, private authService: AuthService) {
    super(userService, translateService, 'USERS.MODAL_TITLE.Add', 'USERS.MODAL_TITLE.Edit');
    this.admin = this.authService.isUserAdmin();
    this.createForm();
  }

  ngOnInit(): void {
    this.form.patchValue(this.data);
  }

  createForm() {
    this.form = new AuditableFormGroup({
      id: new FormControl(),
      password: new FormControl(),
      username: new FormControl(undefined, { validators: [Validators.required] }),
      name: new FormControl(undefined, { validators: [Validators.required] }),
      lastName: new FormControl(undefined, { validators: [Validators.required] }),
      phone: new FormControl(),
      email: new FormControl(undefined, { validators: [Validators.required, Validators.email] }),
      roles: new FormControl(undefined, { validators: [Validators.required] }),
      admin: new FormControl(),
      user: new FormControl(),
      guest: new FormControl()
    });
  }

  save(): void {

    this.userInUse = false;
    this.emailInUse = false;
    if (this.formValid()) {
      this.checkUserInUse();
    } else {
      // console.log(Utils.getAllErrors(this.form));
      Utils.updateValidity(this.form);
    }
  }

  checkUserInUse() {
    // TODO
    /*
    this.userService.findByUsername(this.form.get('username').value)
      .pipe(take(1))
      .subscribe(response => {
        if (response && (response !== this.data.id)) {
          this.aliasInUse = true;
        } else {
          this.checkEmailInUse();
        }
      });*/
  }

  checkEmailInUse() {
    // TODO
    /*
    this.userService.findByEmail(this.form.get('email').value)
      .pipe(take(1))
      .subscribe(response => {
        if (response && (response.id !== this.data.id)) {
          this.emailInUse = true;
        } else {
          super.save();
        }
      });*/
  }
}
