import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { UserListComponent } from './crud/users-list/user-list.component';
import { AppCommonModule } from '../app-common.module';
import { environment } from '../../environments/environment';

@NgModule({
    imports: [
        AppCommonModule,
        UsersRoutingModule
    ],
    declarations: [UserListComponent]
})
export class UsersModule {
}
