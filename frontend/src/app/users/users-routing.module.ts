import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './crud/users-list/user-list.component';
import { AuthGuard } from '../core/services/auth/guards/auth-guard.service';
import { Role } from '../core/enums/role.enum';

const routes: Routes = [
  {
    path: 'crud',
    data: {roles: [Role.ADMIN], breadcrumb: 'MENU.USERS.Crud'},
    canActivate: [AuthGuard],
    component: UserListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
