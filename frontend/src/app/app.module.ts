import { APP_INITIALIZER, DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppCommonModule } from './app-common.module';
import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { servicesInitializer } from './core/utils/services-initializer';
import { MunicipalityService } from './core/services/common/municipality.service';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, registerLocaleData } from '@angular/common';
import { AppCoreModule, HttpLoaderFactory } from './app-core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { appInitializer } from './core/utils/app-initializer';
import { AuthService } from './core/services/auth/auth.service';

import localeEsEs from '@angular/common/locales/es';
import { AgmCoreModule } from '@agm/core';
import { HttpClient } from '@angular/common/http';

// registrar los locales con el nombre que quieras utilizar a la hora de proveer
registerLocaleData(localeEsEs, 'es');

// @ts-ignore
@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        AppCoreModule,
        AppCommonModule,
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpLoaderFactory),
                deps: [HttpClient]
            }
        })
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'es-ES' },
        {provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR'},
        CookieService,
        {
            provide: 'environment',
            useValue: environment
        },
        {
            provide: APP_INITIALIZER,
            useFactory: appInitializer,
            multi: true,
            deps: [AuthService, CookieService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: servicesInitializer,
            multi: true,
            deps: [MunicipalityService]
        }
    ],
    exports: [
        TranslateModule
    ],
    bootstrap: [AppComponent]
})
// @ts-ignore
export class AppModule {

}
