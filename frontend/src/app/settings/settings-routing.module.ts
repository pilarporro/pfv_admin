import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SystemSettingListComponent } from './system-crud/system-settings-list/system-setting-list.component';

import { TagListComponent } from './tags-crud/tags-list/tag-list.component';
import { Role } from '../core/enums/role.enum';
import { AuthGuard } from '../core/services/auth/guards/auth-guard.service';

const routes: Routes = [
  {
    path: 'system-crud',
    data: {roles: [Role.ADMIN], breadcrumb: 'MENU.SETTINGS.SystemCrud'},
    canActivate: [AuthGuard],
    component: SystemSettingListComponent
  },
  {
    path: 'tags-crud',
    data: {roles: [Role.ADMIN], breadcrumb: 'MENU.SETTINGS.Tags'},
    canActivate: [AuthGuard],
    component: TagListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {
}
