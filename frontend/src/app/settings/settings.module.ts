import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { SystemSettingDetailComponent } from './system-crud/system-settings-detail/system-setting-detail.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SystemSettingListComponent } from './system-crud/system-settings-list/system-setting-list.component';
import { TagListComponent } from './tags-crud/tags-list/tag-list.component';
import { TagDetailComponent } from './tags-crud/tags-detail/tag-detail.component';
import { AppCommonModule } from '../app-common.module';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    AppCommonModule,
    SettingsRoutingModule
  ],
  declarations: [SystemSettingListComponent,
    SystemSettingDetailComponent, TagListComponent, TagDetailComponent]
})
export class SettingsModule {
}
