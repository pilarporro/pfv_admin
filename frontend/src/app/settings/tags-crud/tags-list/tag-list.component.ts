import { Component } from '@angular/core';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { ConfirmationService } from 'primeng/api';
import { Tag } from '../../../core/model/settings/tag.model';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';
import { TagService } from '../../../core/services/settings/tag.service';
import { TableColumType } from '../../../core/enums/table/table-column-type.enum';

@Component({
  selector: 'admin-tags-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent extends CommonListComponent<Tag> {

  constructor(service: TagService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService) {
    super(service, confirmationService, translationUtilsService);
  }

  createNewEntity(): Tag {
    return {
      uses: 0,
      terms: ''
    };
  }

  initColumns() {
    this.columns = [
      {field: 'name', header: 'SETTINGS.TAGS.Name', filter: true, filterMatchMode: 'contains'} // TODO
    ];
  }
}
