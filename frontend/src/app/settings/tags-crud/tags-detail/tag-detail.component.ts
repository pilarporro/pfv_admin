import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Tag } from '../../../core/model/settings/tag.model';
import { TagService } from '../../../core/services/settings/tag.service';
import { AuditableFormGroup } from '../../../core/model/forms/auditable-form-group.model';
import { KeyLabel } from '../../../core/model/base/key-label.model';

@Component({
  selector: 'admin-tags-detail',
  templateUrl: './tag-detail.component.html',
  styleUrls: ['./tag-detail.component.scss']
})
export class TagDetailComponent extends CommonDetailComponent<Tag> implements OnInit {
  constructor(public tagService: TagService, translateService: TranslateService) {
    super(tagService, translateService, 'SETTINGS.TAGS.MODAL_TITLE.Add', 'SETTINGS.TAGS.MODAL_TITLE.Edit');
    this.createForm();
  }

  ngOnInit(): void {
    this.form.patchValue(this.data);
  }

  parseToEntity(): Tag {
    const tag: Tag = this.form.getRawValue();

    return tag;
  }

  createForm() {
    this.form = new AuditableFormGroup({
      terms: new FormControl([], {validators: [Validators.required]}),
      interest: new FormControl(undefined, {validators: [Validators.required]}),
      validForClients: new FormControl(undefined, {validators: [Validators.required]}),
      name: new FormControl(undefined, {validators: [Validators.required]}),
      webIds: new FormControl()
    });
  }
}
