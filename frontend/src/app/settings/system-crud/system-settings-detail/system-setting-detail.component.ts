import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonDetailComponent } from '../../../core/components/common-detail.component';
import { FormControl, Validators } from '@angular/forms';
import { SystemSetting } from '../../../core/model/settings/system-setting.model';
import { SystemSettingService } from '../../../core/services/settings/system-setting.service';
import { AuditableFormGroup } from '../../../core/model/forms/auditable-form-group.model';

@Component({
    selector: 'admin-system-settings-detail',
    templateUrl: './system-setting-detail.component.html',
    styleUrls: ['./system-setting-detail.component.scss']
})
export class SystemSettingDetailComponent extends CommonDetailComponent<SystemSetting> implements OnInit {

    constructor(public systemSettingService: SystemSettingService, translateService: TranslateService) {
        super(systemSettingService, translateService, 'SETTINGS.SYSTEM.MODAL_TITLE.Add', 'SETTINGS.SYSTEM.MODAL_TITLE.Edit');
        this.createForm();
    }

    ngOnInit(): void {
        this.form.patchValue(this.data);
    }

    parseToEntity(): SystemSetting {
        const systemSetting: SystemSetting = this.form.getRawValue();

        return systemSetting;
    }

    createForm() {
        this.form = new AuditableFormGroup({
            property: new FormControl(undefined, {validators: [Validators.required]}),
            value: new FormControl(undefined, {validators: [Validators.required]})
        });
        this.form.get('property').disable();
    }
}
