import { Component } from '@angular/core';
import { CommonListComponent } from '../../../core/components/common-list.component';
import { ConfirmationService } from 'primeng/api';
import { SystemSetting } from '../../../core/model/settings/system-setting.model';
import { SystemSettingService } from '../../../core/services/settings/system-setting.service';
import { TranslationsUtilsService } from '../../../core/services/common/translations-utils.service';

@Component({
  selector: 'admin-system-settings-list',
  templateUrl: './system-setting-list.component.html',
  styleUrls: ['./system-setting-list.component.scss']
})
export class SystemSettingListComponent extends CommonListComponent<SystemSetting> {

  constructor(systemSettingService: SystemSettingService, confirmationService: ConfirmationService, translationUtilsService: TranslationsUtilsService) {
    super(systemSettingService, confirmationService, translationUtilsService);
  }

  createNewEntity(): SystemSetting {
    return {};
  }

  initColumns() {
    this.columns = [
      {field: 'property', header: 'SETTINGS.SYSTEM.Property', filter: true, filterMatchMode: 'contains'},
      {field: 'value', header: 'SETTINGS.SYSTEM.Value', filter: true, filterMatchMode: 'contains'}
      ];
  }
}
