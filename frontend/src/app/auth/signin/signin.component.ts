import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Unsubscribable } from '../../core/utils/unsubscribable';
import { AuthService } from '../../core/services/auth/auth.service';
import { REMEMBER_ME_KEY } from '../../core/utils/app-initializer';
import { CookieService } from 'ngx-cookie-service';
import { take } from 'rxjs/operators';

@Component({
    selector: 'admin-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})
export class SigninComponent extends Unsubscribable {
    form: FormGroup;
    errorLogin: boolean = false;

    constructor(public authService: AuthService, private cookieService: CookieService, private router: Router) {
        super();
        this.form = new FormGroup({
            username: new FormControl(undefined, {validators: Validators.required}),
            password: new FormControl(undefined, {validators: Validators.required})
        });
    }

    submit() {
        this.authService.attempAuth(
            this.form.get('username').value,
            this.form.get('password').value
        )
            .pipe(take(1))
            .subscribe(authResponse => {
                    // console.log('SAVING IN COOKIE SERVICE');
                    // console.log(authResponse.user.id);
                    this.cookieService.set(REMEMBER_ME_KEY, authResponse.user.id);
                    // console.log(`${this.cookieService.check(REMEMBER_ME_KEY)} = ${this.cookieService.get(REMEMBER_ME_KEY)}`);
                    this.router.navigateByUrl('/dashboard', {replaceUrl: true});
                },
                err => {
                    this.errorLogin = true;

                    return;
                }
            );
    }

}
