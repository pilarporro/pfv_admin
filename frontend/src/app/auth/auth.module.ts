import { NgModule } from '@angular/core';

import { AuthRoutingModule } from './auth-routing.module';
import { SigninComponent } from './signin/signin.component';
import { TranslateModule } from '@ngx-translate/core';
import { AppCommonModule } from '../app-common.module';

@NgModule({
    imports: [
        AppCommonModule,
        AuthRoutingModule
    ],
    declarations: [SigninComponent]
})
export class AuthModule {
}
