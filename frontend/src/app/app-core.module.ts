import { NgModule, Optional, SkipSelf } from '@angular/core';

import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AuthInterceptor } from './core/services/auth/auth-inteceptor';
import { EnumPipe } from './core/pipes/enum.pipe';
import { TranslateLoader, TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { TopbarComponent } from './layout/topbar/topbar.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { MenuComponent } from './layout/menu/menu.component';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';
import { SubmenuComponent } from './layout/submenu/submenu.component';
import { FooterComponent } from './layout/footer/footer.component';
import { AppCommonModule } from './app-common.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpDatetimeInterceptor } from './core/services/interceptors/http-datetime-interceptor.service';

const LAYOUT_COMPONENTS: any[] = [
    TopbarComponent,
    DashboardComponent,
    MenuComponent,
    BreadcrumbComponent,
    SubmenuComponent,
    FooterComponent
];

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        HttpClientModule,
        AppCommonModule
    ],
    declarations: [
        LAYOUT_COMPONENTS
    ],
    exports: [
        LAYOUT_COMPONENTS
    ],
    providers: [
        EnumPipe,
        DatePipe,
        MessageService,
        ConfirmationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ]
})
export class AppCoreModule {
    constructor(@Optional()
                @SkipSelf()
                    parentModule: AppCoreModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppPhotosModule only');
        }
    }
}
