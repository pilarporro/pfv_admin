import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { AnimalType } from './app/core/enums/animal-type.enum';
import { AnimalStatus } from './app/core/enums/animal-status.enum';
import { Gender } from './app/core/enums/gender.enum';
import { Role } from './app/core/enums/role.enum';
import { Country } from './app/core/enums/country.enum';
import { SystemSettingType } from './app/core/enums/system-setting-type.enum';
import { TagType } from './app/core/enums/tag-type.enum';
import { PlaceType } from './app/core/enums/place-type.enum';
import { ActivityType } from './app/core/enums/activity-type.enum';
import { PaymentPeriodicity } from './app/core/enums/payment-periodicity.enum';
import { AccountSubtype } from './app/core/enums/account-subtype.enum';
import { AccountType } from './app/core/enums/account-type.enum';
import { PaymentType } from './app/core/enums/payment-type.enum';
import { Color } from './app/core/enums/color.enum';
import { VisitClinicType } from './app/core/enums/visit-clinic-type.enum';

window['VisitClinicType'] = VisitClinicType;
window['PaymentType'] = PaymentType;
window['PaymentPeriodicity'] = PaymentPeriodicity;
window['Gender'] = Gender;
window['Country'] = Country;
window['AnimalType'] = AnimalType;
window['AnimalStatus'] = AnimalStatus;
window['Role'] = Role;
window['SystemSettingType'] = SystemSettingType;
window['TagType'] = TagType;
window['PlaceType'] = PlaceType;
window['ActivityType'] = ActivityType;
window['AccountSubtype'] = AccountSubtype;
window['AccountType'] = AccountType;
window['Color'] = Color;


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule, {
  preserveWhitespaces: true
}).catch(err => console.log(err));
