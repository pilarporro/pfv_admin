package com.pfv.admin.common.exceptions.resources;

import com.pfv.admin.common.exceptions.ApiException;
import com.pfv.admin.common.exceptions.CustomFieldError;
import com.pfv.admin.common.exceptions.ErrorMessages;
import com.pfv.admin.common.model.dto.ListDto;

import java.util.List;
import java.util.stream.Collectors;

public class ResourceErrorsException extends ApiException {

    public static final String MESSAGE =  "Resource validation failed";
    public static final String KEY_MESSAGE =  "error.resource.invalid";

    public ResourceErrorsException(final ErrorMessages errorMessages) {
        super(MESSAGE, KEY_MESSAGE, errorMessages);
    }

    public ResourceErrorsException(final List<Integer> idxs, final String field, final String message) {
        super(MESSAGE, KEY_MESSAGE);
        getErrors().getFieldErrors().addAll(
                idxs.stream().map(idx -> new CustomFieldError(
                        ListDto.generatePrefix(idx) + "." + field, message
                )).collect(Collectors.toList())
        );
    }
}
