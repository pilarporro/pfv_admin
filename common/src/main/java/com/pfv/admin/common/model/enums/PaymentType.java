package com.pfv.admin.common.model.enums;

public enum PaymentType implements EnumTranslationMessage{
    PAYED_BY_PERSON,
    PAYED_BY_ADOPTER,
    ASSOCIATION,
    TOWNHALL_GRANT,
    CERRO2_GRANT,
    MIRADOR_GRANT,
    MONTEMORILLO_GRANT,
    PUENTELASIERRA_GRANT

    ;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
