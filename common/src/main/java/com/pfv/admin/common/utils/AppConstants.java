package com.pfv.admin.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.time.format.DateTimeFormatter;

@Slf4j
public final class AppConstants {

    private AppConstants() {
    }

    public static final String APP_PROFILE_DEV = "dev";
    public static final String APP_PROFILE_INTEGRATION = "int";
    public static final String APP_PROFILE_PRODUCTION = "prod";

    // Paging max limitations for the REST resources to minimize stress on the server
    public static final int MAX_PAGE_SIZE = 50;

    public static final DateTimeFormatter REPORT_DATE_TIME_FORMATTER= DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public static final String AUTHORIZE_ADMIN =
            "hasAuthority(T(com.pfv.admin.common.model.enums.Role).ADMIN.name())";

    public static final String AUTHORIZE_USER =
            "hasAuthority(T(com.pfv.admin.common.model.enums.Role).USER.name())";

    public static final String AUTHORIZE_GUEST =
            "hasAuthority(T(ccom.pfv.admin.common.model.enums.Role).GUEST.name())";

    public static final String USER_SYSTEM = "SYSTEM";


    public static final String REQUEST_DATE_FORMAT = "yyyyMMdd";

    public static final String DEFAULT_COLONY = "BASE";

}
