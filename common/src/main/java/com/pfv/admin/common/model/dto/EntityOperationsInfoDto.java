package com.pfv.admin.common.model.dto;

import com.pfv.admin.common.model.dto.operations.OperationsInfoDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class EntityOperationsInfoDto extends AuditableDto {
    @ApiModelProperty(readOnly = true, hidden = true)
    private OperationsInfoDto operationsInfo;
}
