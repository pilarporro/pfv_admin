package com.pfv.admin.common.model.querydsl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

public class SearchCriteriaUtils {

    private static ObjectMapper objectMapper;

    private SearchCriteriaUtils() {
        // should never be called
    }

    public static final ArrayList<SearchCriteria> convertStringToSearchCriterias(final String fields) throws IOException {
        if (objectMapper == null) {
            createObjectMapper();
        }
        ArrayList<SearchCriteria> searchCriterias = objectMapper.readValue(fields, new TypeReference<ArrayList<SearchCriteria>>(){});
        return searchCriterias;
    }

    private static void createObjectMapper(){
        objectMapper = new ObjectMapper();
    }

}
