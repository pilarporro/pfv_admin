package com.pfv.admin.common.mappers;

import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.services.Service;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Generic mapper.
 *
 * @param <ENTITY>
 * @param <DTO>
 */
public abstract class OperationsInfoMapper<DTO extends EntityOperationsInfoDto, ENTITY extends Entity> extends Mapper<DTO, ENTITY> {

    @Autowired
    private Service<DTO, ENTITY> service;

    @AfterMapping
    public void addOperationsInfoToDto(final ENTITY entity, @MappingTarget final DTO dto) {
        dto.setOperationsInfo(service.calculateOperationsInfo(entity));
    }

}
