package com.pfv.admin.common.utils;

public final class ApiUris {

    // Application base path for the REST resources.
    public static final String API_BASE = "/api";

    public static final String API_USERS_URI = API_BASE + "/users";
    public static final String API_PERSONS_URI = API_BASE + "/persons";
    public static final String API_TRAPS_URI = API_BASE + "/traps";
    public static final String API_ANIMALS_URI = API_BASE + "/animals";
    public static final String API_COLONIES_URI = API_BASE + "/colonies";
    public static final String API_ACTIVITIES_URI = API_BASE + "/activities";
    public static final String API_PLACES_URI = API_BASE + "/places";
    public static final String API_ACCOUNT_NOTES_URI = API_BASE + "/account-notes";

    public static final String API_SERVER_STATUS = API_BASE + "/server-status";

    public static final String API_REPORTS_URI = API_BASE + "/reports";
    public static final String API_LOAD_URI = API_BASE + "/data-loader";
    public static final String API_STORAGE_URI = API_BASE + "/storage";
    public static final String API_MUNICIPALITIES_URI = API_BASE + "/municipalities";
    public static final String API_TAGS_URI = API_BASE + "/tags";
    public static final String API_LOGS_URI = API_BASE + "/logs";
    public static final String API_SETTINGS_URI = API_BASE + "/settings";
    public static final String API_SETTINGS_PROPERTIES_URI = API_SETTINGS_URI + "/properties";
    public static final String API_SETTINGS_SYSTEM_PROPERTIES_URI = API_SETTINGS_URI + "/system-properties";

    private ApiUris() {
    }
}