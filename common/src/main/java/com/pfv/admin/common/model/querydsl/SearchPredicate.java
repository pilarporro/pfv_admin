package com.pfv.admin.common.model.querydsl;

import com.pfv.admin.common.model.querydsl.pathstrategy.BooleanPathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.CollectionPathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.DatePathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.DateTimePathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.EnumPathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.NumberPathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.ObjectPathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.PathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.PointPathStrategy;
import com.pfv.admin.common.model.querydsl.pathstrategy.StringPathStrategy;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@AllArgsConstructor
@Slf4j
public class SearchPredicate<T> {

    private SearchCriteria criteria;

    private static Field getField(final Class clazz, final String fieldName)
        throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (final NoSuchFieldException e) {
            final Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }

    public static Class getClass(final Class clazz, final String fieldName) throws NoSuchFieldException {
        final String[] nestedFields = fieldName.split("\\.");
        Class componentClass = clazz;

        for (final String nestedField : nestedFields) {
            final Field field = getField(componentClass, nestedField);
            if (field.getType() == List.class) {
                componentClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            } else {
                componentClass = field.getType();
            }
        }

        return componentClass;

    }

    public BooleanExpression getPredicate(final Class classOfPath, final String variable) {
        final PathBuilder<T> entityPath = new PathBuilder<>(classOfPath, variable);

        try {
            final Class propertyClass = getClass(classOfPath, criteria.getKey());
            PathStrategy pathStrategy = null;
            if (propertyClass.isEnum()) {
                pathStrategy = new EnumPathStrategy();
            } else if (double[].class.isAssignableFrom(propertyClass)) {
                pathStrategy = new PointPathStrategy();
            } else if (String.class.isAssignableFrom(propertyClass)) {
                pathStrategy = new StringPathStrategy();
            } else if (Number.class.isAssignableFrom(propertyClass)) {
                pathStrategy = new NumberPathStrategy();
            } else if (Boolean.class.isAssignableFrom(propertyClass) || boolean.class.isAssignableFrom(propertyClass)) {
                pathStrategy = new BooleanPathStrategy();
            } else if (LocalDateTime.class.isAssignableFrom(propertyClass)) {
                pathStrategy = new DateTimePathStrategy();
            } else if (LocalDate.class.isAssignableFrom(propertyClass)) {
                pathStrategy = new DatePathStrategy();
            } else if (Set.class.isAssignableFrom(propertyClass) || List.class.isAssignableFrom(propertyClass))  {
                pathStrategy = new CollectionPathStrategy();
            } else if (Object.class.isAssignableFrom(propertyClass)) {
                pathStrategy = new ObjectPathStrategy();
            }

            if (pathStrategy != null) {
                final BooleanExpression expression = pathStrategy.getBooleanExpression(entityPath, propertyClass, criteria);

                if (expression != null) {
                    return expression;
                }
            }
        } catch (final NoSuchFieldException e) {
            log.error("nosuchfieldexception", e);
        }
        throw new UnsupportedOperationException("filter is missing");
    }
}
