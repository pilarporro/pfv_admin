package com.pfv.admin.common.model.enums;

public enum AccountSubtype implements EnumTranslationMessage{
    BANK_COMISSION,
    DONATION,
    EXPENSE,
    VETCLINIC_INVOICE,
    // TRANSFER,
    // OUT_CASH,
    LOTTERY,
    OTHER,
    MEMBER_SUBSCRIPTION;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
