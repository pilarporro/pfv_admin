package com.pfv.admin.common.exceptions;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CustomGlobalError extends CustomError {
    @Getter
    @Setter
    private String index;

    public CustomGlobalError(final String index, final String description) {
        super(description);
        this.index = index;
    }

    public CustomGlobalError(final String description) {
        this("", description);
    }

    public void addPrefix(final String prefix) {
        this.index = prefix.concat(index);
    }
}
