package com.pfv.admin.common.model.enums;

public enum AnimalType implements EnumTranslationMessage {
    CAT,
    DOG;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }

    public static final String CAT_TYPE = "CAT";
    public static final String DOG_TYPE = "DOG";
}
