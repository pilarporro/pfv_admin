package com.pfv.admin.common.utils;

import com.pfv.admin.common.model.enums.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.concurrent.ThreadLocalRandom;

@Slf4j
public final class AppUtil {
    private static final Boolean[] BOOLEAN_70 = { true, true, true, true, true, true, true, false, false, false };
    public static Integer MAX_PAGE_SIZE = 2000;

    private AppUtil() {
    }

    public static void printBanner(final String message) {
        String banner = "\n"
                + "###################################################################################################\n"
                + "###################################################################################################\n"
                + "##### " + message + "\n"
                + "###################################################################################################\n"
                + "###################################################################################################";
        log.info(banner);
    }

    public static Integer getRandomNumber(final Integer from, final Integer to) {
        return ThreadLocalRandom.current().nextInt(from, to);
    }

    public static Long getRandomLong(final Long from, final Long to) {
        return ThreadLocalRandom.current().nextLong(from, to);
    }

    public static Boolean getRandomBoolean() {
        return BOOLEAN_70[getRandomNumber(0, BOOLEAN_70.length)];
    }

    public static Double roundDouble(Double value) {
        return (int) (value * 100 + 0.5) / 100.0;
    }


    public static Boolean hasAuthorization(final String username) {
        boolean hasAuthorization = false;
        User user = getAuthorizedUser();
        if (user != null) {
            hasAuthorization = user.getAuthorities().stream().filter(auth -> {
                return auth.getAuthority().equals(Role.ADMIN.name())
                        || (auth.getAuthority().equals(Role.USER.name()) && (user.getUsername().equalsIgnoreCase(username)));
            }).findFirst().isPresent();
        }

        return hasAuthorization;
    }

    public static Boolean isSuperadmin() {
        boolean hasAuthorization = false;
        User user = getAuthorizedUser();
        if (user != null) {
            hasAuthorization = user.getAuthorities().stream().filter(auth -> {
                return auth.getAuthority().equals(Role.ADMIN.name());
            }).findFirst().isPresent();
        }

        return hasAuthorization;
    }

    public static User getAuthorizedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            return (User) authentication.getPrincipal();
        }

        return null;
    }
}
