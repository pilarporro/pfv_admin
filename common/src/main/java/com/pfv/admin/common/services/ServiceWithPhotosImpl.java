package com.pfv.admin.common.services;

import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import com.pfv.admin.common.repositories.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

public abstract class ServiceWithPhotosImpl<DTO extends WithPhotosDto, ENTITY extends AuditableEntityWithPhotos, REPOSITORY extends Repository<ENTITY>>
        extends ServiceImpl<DTO, ENTITY, REPOSITORY> {
    @Autowired protected StorageService storageService;
    protected void preSave(ENTITY oldEntity, ENTITY newEntity) {
        if ((oldEntity != null) && !CollectionUtils.isEmpty(oldEntity.getPhotos())) {
            oldEntity.getPhotos().forEach(photo -> {
                if (!newEntity.getPhotos().stream().filter(cur -> photo.getDiskName().equals(cur.getDiskName())).findFirst().isPresent()) {
                    storageService.delete(photo.getFolder(), photo.getDiskName());
                    storageService.delete(photo.getFolder(), photo.getDiskNameThumbnail());
                }
            });
        }

        if (!CollectionUtils.isEmpty(newEntity.getPhotos())) {
            newEntity.getPhotos().forEach(photo -> {
                if (photo.getTemporary()) {
                    storageService.save(photo.getFolder(), photo.getDiskName(), photo.getDiskNameThumbnail());
                    photo.setTemporary(false);
                }
            });
        }

        super.preSave(oldEntity, newEntity);
    }

}