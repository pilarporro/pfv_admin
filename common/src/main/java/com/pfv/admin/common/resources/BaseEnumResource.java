package com.pfv.admin.common.resources;

import com.pfv.admin.common.model.dto.EnumTranslationDto;
import com.pfv.admin.common.model.enums.EnumTranslationMessage;
import com.pfv.admin.common.utils.EnumTypesFactory;
import com.pfv.admin.common.utils.ResourceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Slf4j
public abstract class BaseEnumResource {
    private static final String WS_ENUMS_BASE_PACKAGE = "com.pfv.admin.common.model.enums";

    public ResponseEntity<List<EnumTranslationDto>> getEnumTypeTranslations(final String type) {
        List<EnumTranslationDto> enumTypes = null;
        Class enumClass = null;
        try {
            enumClass = Class.forName(String.format("%s.%s", WS_ENUMS_BASE_PACKAGE, type));
        } catch (ClassNotFoundException e) {
            log.error("Type enum incorrect " + type);
        }
        if (enumClass != null) {
            enumTypes = Arrays.stream((EnumTranslationMessage[]) enumClass.getEnumConstants()).map(this::createDTO).collect(Collectors.toList());
        }
        final ResponseEntity<List<EnumTranslationDto>> responseEntity = new ResponseEntity<>(enumTypes, HttpStatus.OK);

        return responseEntity;
    }

    public Map<String, String> getAllTypes() {
        return EnumTypesFactory.getAllTypes(WS_ENUMS_BASE_PACKAGE);
    }

    private EnumTranslationDto createDTO(final EnumTranslationMessage enumType) {
        return EnumTranslationDto.builder()
            .key(enumType.name())
            .label(ResourceUtil.getMessage(enumType.getMessageKey(), null, LocaleContextHolder.getLocale()))
            .build();
    }

}