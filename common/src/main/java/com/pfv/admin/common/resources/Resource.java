package com.pfv.admin.common.resources;

import com.pfv.admin.common.exceptions.resources.ResourceNotFoundException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.dto.ListDto;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchFields;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.common.utils.AppConstants;
import com.pfv.admin.common.utils.ResourceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class Resource<DTO extends EntityOperationsInfoDto, ENTITY extends Entity, SERVICE extends Service<DTO, ENTITY>> {

    @Autowired
    protected SERVICE service;

    @GetMapping("{id}/minimal")
    public AliasIdLabelDto getByIdMinimal(final @PathVariable String id) {
        return service.findMinimalById(id);
    }

    @GetMapping("by-alias-starts-with")
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public List<AliasIdLabelDto> findByAliasStartsWith(final @RequestParam("alias") String alias) {
        return service.findByAliasStartsWith(alias);
    }

    @GetMapping("by-alias")
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public AliasIdLabelDto findByAlias(final @RequestParam("alias") String alias) {
        return service.findByAlias(alias);
    }


    protected List<DTO> genericGetAll() {
        return service.getAll();
    }

    protected ResponseEntity<DTO> genericCreate(final DTO dto,
                                                final BindingResult result) {
        ResourceUtil.checkFieldErrors(result);
        ResourceUtil.checkIdIsNull(dto);
        return new ResponseEntity<>(service.create(dto), HttpStatus.CREATED);
    }

    protected Page<DTO> getAllWithFiltersAndPaging(final Integer page,
                                                   final Integer size,
                                                   final String sortField,
                                                   final Integer sortOrder,
                                                   final @SearchFields List<SearchCriteria> filters) {
        ResourceUtil.checkMaxPageSize(size);

        return this.service.getAllWithFiltersAndPaging(this.createPageRequest(page, size, sortField, sortOrder), filters);
    }

    protected ResponseEntity<List<DTO>> genericCreate(final ListDto<DTO> listDtos,
                                                      final BindingResult result) {
        ResourceUtil.checkNotEmptyResource(listDtos);
        ResourceUtil.checkFieldErrors(result);
        ResourceUtil.checkIdIsNull(listDtos);
        return new ResponseEntity<>(service.create(listDtos.getDtos()), HttpStatus.CREATED);
    }

    protected ResponseEntity<DTO> genericGetById(final String id) {
        return service.findById(id).map(
                item -> new ResponseEntity<>(item, HttpStatus.OK)
        ).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    protected ResponseEntity<DTO> genericUpdate(final String id,
                                                final DTO dto,
                                                final BindingResult result) {
        ResourceUtil.checkFieldErrors(result);
        dto.setId(getIfItemExists(id).getId());
        return new ResponseEntity<>(service.update(dto), HttpStatus.OK);
    }

    protected ResponseEntity<List<DTO>> genericUpdate(final ListDto<DTO> listDtos,
                                                      final BindingResult result) {
        ResourceUtil.checkNotEmptyResource(listDtos);
        ResourceUtil.checkFieldErrors(result);
        List<DTO> dtos = listDtos.getDtos();
        checkIfDtosExist(dtos);
        return new ResponseEntity<>(service.update(dtos), HttpStatus.OK);
    }

    protected ResponseEntity<DTO> genericDelete(final String id) {
        service.delete(getIfItemExists(id).getId());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    protected DTO getIfItemExists(final String id) {
        return service.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    private void checkIfDtosExist(final List<DTO> dtos) {
        List<Integer> idxs = IntStream.range(0, dtos.size())
                .filter(idx -> !service.findById(dtos.get(idx).getId()).isPresent())
                .boxed().collect(Collectors.toList());
        if (!idxs.isEmpty()) {
            throw new ResourceNotFoundException(idxs, ResourceUtil.getMessage(ResourceNotFoundException.KEY_MESSAGE));
        }
    }

    protected PageRequest createPageRequest(final Integer page, final Integer size, final String sortField, final Integer sortOrder) {
       return ResourceUtil.createPageRequest(page, size, sortField, sortOrder);
    }
}
