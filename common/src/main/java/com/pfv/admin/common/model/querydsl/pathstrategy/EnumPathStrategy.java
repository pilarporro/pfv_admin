package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.PathBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EnumPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        final EnumPath enumPath = entityPath.getEnum(searchCriteria.getKey(), propertyClass);
        switch (searchCriteria.getOperation()) {
            case EQUALS:
                return enumPath.eq(Enum.valueOf(propertyClass, searchCriteria.getValue().toString()));
            case NOT_EQUALS:
                return enumPath.ne(Enum.valueOf(propertyClass, searchCriteria.getValue().toString()));
            case IN:
                if (searchCriteria.getValue() instanceof String[]) {
                    final String[] stringArray = (String[]) searchCriteria.getValue();
                    final List<Enum> arrayList = new ArrayList<>();
                    for (final String strElement : stringArray) {
                        arrayList.add(Enum.valueOf(propertyClass, strElement));
                    }
                    return enumPath.in(arrayList);
                } else if (searchCriteria.getValue() instanceof Collection<?>) {
                    final Collection<String> stringCollection = (Collection<String>) searchCriteria.getValue();
                    final List<Enum> arrayList = new ArrayList<>();
                    for (final String strElement : stringCollection) {
                        arrayList.add(Enum.valueOf(propertyClass, strElement));
                    }
                    return enumPath.in(arrayList);
                }
            default:
                return null;
        }
    }
}