package com.pfv.admin.common.model.enums;

public enum Color implements EnumTranslationMessage {
    WHITE,
    BLACK,
    TRICOLOR,
    WHITE_BLACK,
    GREY,
    WHITE_YELLOW,
    WHITE_BROWN,
    GREY_BROWN,
    CAREY,
    WHITE_GREY;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
