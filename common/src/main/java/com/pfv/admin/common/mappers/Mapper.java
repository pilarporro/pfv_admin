package com.pfv.admin.common.mappers;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.Dto;
import com.pfv.admin.common.model.entity.AuditableEntity;
import com.pfv.admin.common.model.entity.Entity;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.MappingTarget;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class Mapper<DTO extends Dto, ENTITY extends Entity> {
    public abstract ENTITY dtoToEntity(DTO dto);

    @InheritConfiguration(name = "dtoToEntity")
    public abstract ENTITY merge(DTO dto, @MappingTarget ENTITY entity);

    @InheritInverseConfiguration(name = "dtoToEntity")
    public abstract DTO entityToDto(ENTITY entity);

    @InheritConfiguration(name = "dtoToEntity")
    public abstract List<ENTITY> dtosToEntities(List<DTO> dtos);

    @InheritInverseConfiguration
    public abstract List<DTO> entitiesToDtos(List<ENTITY> entities);

    public abstract AliasIdLabelDto toResultDto(ENTITY entity);

    protected <ENTITYOBJECT extends AuditableEntity> List<String> entityToDtoObjects(final List<ENTITYOBJECT> entities) {
        List<String> ids = new ArrayList<>();
        if (!CollectionUtils.isEmpty(entities)) {
            entities.forEach(entity -> ids.add(entity.getId()));
        }
        return ids;
    }
}
