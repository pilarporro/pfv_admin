package com.pfv.admin.common.utils;

import com.pfv.admin.common.model.dto.Dto;
import com.pfv.admin.common.exceptions.ApiException;
import com.pfv.admin.common.exceptions.CustomFieldError;
import com.pfv.admin.common.exceptions.CustomGlobalError;
import com.pfv.admin.common.exceptions.ErrorMessages;
import com.pfv.admin.common.exceptions.ErrorMessagesFactory;
import com.pfv.admin.common.exceptions.resources.ResourceErrorsException;
import com.pfv.admin.common.model.dto.ResourceResponseMessageDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class ResourceUtil {
    private static final Logger LOGGER = LogManager.getLogger();

    private static ResourceBundleMessageSource MESSAGE_SOURCE;


    public static ResourceBundleMessageSource createMessageSource(String...baseNames) {
        if (MESSAGE_SOURCE == null) {
            MESSAGE_SOURCE = new ResourceBundleMessageSource();
            MESSAGE_SOURCE.setDefaultEncoding("UTF-8");
            MESSAGE_SOURCE.setCacheSeconds(-1);
            MESSAGE_SOURCE.setBasenames(baseNames);
        }
        return MESSAGE_SOURCE;
    }

    private ResourceUtil() {
    }

    public static PageRequest createPageRequest(final Integer page, final Integer size, final String sortField, final Integer sortOrder) {
        return createPageRequest(page, size, sortOrder, sortField == null ? "id" : sortField);
    }

    public static PageRequest createPageRequest(final Integer page, final Integer size, final Integer sortOrder, final String...sortFields) {
        Sort.Direction sortDirection = Sort.Direction.ASC;
        if (sortOrder != null && sortOrder < 0) {
            sortDirection = Sort.Direction.DESC;
        }
        return PageRequest.of(page, size, sortDirection, sortFields);
    }

    public static ResponseEntity<Object> getErrorResponse(
            final WebRequest request, final Exception exception, final HttpStatus httpStatus,
            final boolean showStackTrace, final ErrorMessages messages
    ) {
        return getErrorResponse(request, exception.getClass().getName() + ": " + exception.getMessage(), exception, httpStatus, showStackTrace, messages);
    }

    @SuppressWarnings("unchecked")
    public static ResponseEntity<Object> getErrorResponse(
            final WebRequest request, final ApiException exception, final HttpStatus httpStatus
    ) {
        return getErrorResponse(request, exception.getResponseMessage(), exception, httpStatus, exception.isShowStackTrace(), exception.getErrors());
    }

    @SuppressWarnings("unchecked")
    public static ResponseEntity<Object> getErrorResponse(
            final WebRequest request, final String responseMessage, final Exception exception, final HttpStatus httpStatus,
            final boolean showStackTrace, final ErrorMessages errorMessages
    ) {
        String localeResponseMessage;
        if (responseMessage != null) {
            try {
                localeResponseMessage = MESSAGE_SOURCE.getMessage(responseMessage, null, LocaleContextHolder.getLocale());
            } catch (NoSuchMessageException noSuchMessageException) {
                LOGGER.warn("The message is not found", noSuchMessageException);
                localeResponseMessage = responseMessage;
            }
        } else {
            localeResponseMessage = "";
        }
        final ResourceResponseMessageDto errorMessage = new ResourceResponseMessageDto(
                httpStatus.value(),
                localeResponseMessage,
                errorMessages,
                ((ServletWebRequest) request).getRequest().getRequestURL().toString()
        );
        LOGGER.error("ERROR : {} - {}", errorMessage.getPath(), exception.getMessage());
        if (showStackTrace) {
            LOGGER.error("ERROR : ", exception);
        }
        return new ResponseEntity<>(errorMessage, httpStatus);
    }

    public static ResponseEntity<Object> getSimpleErrorResponse(final HttpStatus httpStatus, final String path) {
        final ResourceResponseMessageDto errorMessage = new ResourceResponseMessageDto(
                httpStatus.value(),
                httpStatus.getReasonPhrase(),
                ErrorMessagesFactory.newWithGlobalMessage(httpStatus.getReasonPhrase()),
                path
        );
        return new ResponseEntity<>(errorMessage, httpStatus);
    }

    public static ErrorMessages generateWithGlobalErrorMessages(final String globalErrorCode) {
        return ErrorMessagesFactory.newWithGlobalMessage(getMessage(globalErrorCode));
    }

    public static ErrorMessages generateWithFieldErrorMessages(final String field, final String code) {
        return ErrorMessagesFactory.newWithFieldMessage(field, getMessage(code));
    }

    public static void checkFieldErrors(final BindingResult result) {
        if (result.hasErrors()) {
            throw new ResourceErrorsException(composeErrors(result));
        }
    }

    public static void checkMaxPageSize(final int pageSize) {
        if (pageSize > AppUtil.MAX_PAGE_SIZE) {
            final String[] params = {Integer.toString(AppUtil.MAX_PAGE_SIZE)};
            final String msg = MESSAGE_SOURCE.getMessage("error.max.page.size.limit", params, LocaleContextHolder.getLocale());
            throw new IllegalArgumentException(msg);
        }
    }

    public static ErrorMessages composeErrors(final BindingResult bindingResult) {
        List<CustomFieldError> fieldErrorMessages = new ArrayList<>();
        List<CustomGlobalError> globalErrorMessages = new ArrayList<>();
        ErrorMessages errorMessages = new ErrorMessages(globalErrorMessages, fieldErrorMessages);
        fieldErrorMessages.addAll(
                bindingResult.getFieldErrors().stream()
                        .map(fieldError -> new CustomFieldError(fieldError.getField(), fieldError.getDefaultMessage()))
                        .collect(Collectors.toList()));

        globalErrorMessages.addAll(
                bindingResult.getGlobalErrors().stream()
                        .map(error -> new CustomGlobalError(error.getDefaultMessage()))
                        .collect(Collectors.toList()));

        return errorMessages;
    }

    public static String getMessage(final String code) {
        return MESSAGE_SOURCE.getMessage(code, null, LocaleContextHolder.getLocale());
    }

    public static String getMessage(final String code, final Object... args) {
        return MESSAGE_SOURCE.getMessage(code, args, LocaleContextHolder.getLocale());
    }

    public static <DTO extends Dto> void checkNotEmptyResource(final List<DTO> listDtos) {
        if (listDtos.isEmpty()) {
            throw new ResourceErrorsException(generateWithGlobalErrorMessages("validation.resource.empty.message"));
        }
    }

    public static <DTO extends Dto> void checkIdIsNull(final DTO dto) {
        if (dto.getId() != null) {
            throw new ResourceErrorsException(generateWithFieldErrorMessages("id", "validation.null"));
        }
    }

    public static <DTO extends Dto> void checkIdIsNull(final List<DTO> dtos) {
        List<Integer> idxs = IntStream.range(0, dtos.size())
                .filter(idx -> dtos.get(idx).getId() != null)
                .boxed().collect(Collectors.toList());
        if (!idxs.isEmpty()) {
            throw new ResourceErrorsException(idxs, "id", getMessage("validation.null"));
        }
    }
}