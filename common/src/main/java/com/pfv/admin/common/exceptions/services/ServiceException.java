package com.pfv.admin.common.exceptions.services;

import com.pfv.admin.common.exceptions.ApiException;
import com.pfv.admin.common.exceptions.ErrorMessages;
import com.pfv.admin.common.exceptions.resources.ResourceErrorsException;

public class ServiceException extends ApiException {
    public ServiceException(final String message, final ErrorMessages errors) {
        super(message, ResourceErrorsException.KEY_MESSAGE, errors);
    }

    public ServiceException(final Exception exception) {
        super(exception);
    }

    public ServiceException(final String message) {
        super(message);
    }
}
