package com.pfv.admin.common.exceptions;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CustomConstraintError extends CustomGlobalError {
    @Getter
    private List<String> reasons;

    public CustomConstraintError(final String description, final List<String> reasons) {
        super(description);
        this.reasons = reasons;
    }
}
