package com.pfv.admin.common.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDate;

import static com.pfv.admin.common.utils.DateUtil.parseLocalDate;

public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {
    @Override public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return parseLocalDate(jsonParser.getText());
    }
}