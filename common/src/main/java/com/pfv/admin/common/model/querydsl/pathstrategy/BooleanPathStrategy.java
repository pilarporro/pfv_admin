package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.PathBuilder;

import java.util.List;

public class BooleanPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        final BooleanPath booleanPath = entityPath.getBoolean(searchCriteria.getKey());
        switch (searchCriteria.getOperation()) {
            case EQUALS:
                final boolean value = Boolean.valueOf(searchCriteria.getValue().toString());
                return booleanPath.eq(value);
            case IN:
                List<Boolean> values = ((List<Boolean>) searchCriteria.getValue());
                return booleanPath.in(values);
            default:
                return null;
        }
    }
}