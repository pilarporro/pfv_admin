package com.pfv.admin.common.exceptions;

import lombok.Getter;

@Getter
public class ApiException extends RuntimeException {
    protected String responseMessage;
    protected ErrorMessages errors = new ErrorMessages();
    protected boolean showStackTrace = false;

    public ApiException(final String message, final String responseMessage, final ErrorMessages errors) {
        super(message);
        this.errors = errors;
        this.responseMessage = responseMessage;
    }

    public ApiException(final String message) {
        super(message);
    }

    public ApiException(final Exception exception) {
        super(exception);
    }

    public ApiException(final String message, final String responseMessage, final Throwable cause) {
        super(message, cause);
        this.responseMessage = responseMessage;
    }

    public ApiException(final String message, final String responseMessage) {
        super(message);
        this.responseMessage = responseMessage;
    }

    public ApiException(final String responseMessage, final Throwable cause) {
        super(cause);
        this.responseMessage = responseMessage;
    }
}
