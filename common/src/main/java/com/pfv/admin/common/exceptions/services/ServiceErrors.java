package com.pfv.admin.common.exceptions.services;

import com.pfv.admin.common.exceptions.CustomFieldError;
import com.pfv.admin.common.exceptions.CustomGlobalError;
import com.pfv.admin.common.exceptions.ErrorMessages;

public class ServiceErrors {
    private ErrorMessages errors = new ErrorMessages();

    public void newFieldError(final String field, final String message) {
        errors.getFieldErrors().add(new CustomFieldError(field, message));
    }

    public void newGlobalError(final String message) {
        errors.getGlobalErrors().add(new CustomGlobalError(message));
    }

    public void addErrors(final ErrorMessages errors) {
        this.errors.getGlobalErrors().addAll(errors.getGlobalErrors());
        this.errors.getFieldErrors().addAll(errors.getFieldErrors());
    }

    public ErrorMessages getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return !errors.getFieldErrors().isEmpty() || !errors.getGlobalErrors().isEmpty();
    }
}
