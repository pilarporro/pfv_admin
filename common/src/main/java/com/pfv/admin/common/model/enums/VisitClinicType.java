package com.pfv.admin.common.model.enums;

public enum VisitClinicType implements EnumTranslationMessage{
    REVISION,
    OTHER,
    SURGERY,
    HANDING_OVER;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }

    public static final String BUY_SUPPLIES_TYPE = "BUY_SUPPLIES";
    public static final String VISIT_CLINIC_TYPE = "VISIT_CLINIC";
    public static final String NEW_MEMBER_TYPE = "NEW_MEMBER";
    public static final String ADOPTION_TYPE = "ADOPTION";
    public static final String RESCUE_TYPE = "RESCUE";
    public static final String PICKUP_KEEPER_TYPE = "PICKUP_KEEPER";
    public static final String CASTRATION_TYPE = "CASTRATION";
    public static final String MEETING_TYPE = "MEETING";
    public static final String OTHER_TYPE = "OTHER";
}
