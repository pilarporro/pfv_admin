package com.pfv.admin.common.exceptions.resources;

import com.pfv.admin.common.exceptions.ApiException;
import com.pfv.admin.common.exceptions.CustomFieldError;
import com.pfv.admin.common.exceptions.CustomGlobalError;
import com.pfv.admin.common.model.dto.ListDto;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ResourceNotFoundException extends ApiException {
    private static final String MESSAGE = "Resource not found";
    public static final String KEY_MESSAGE = "error.resource.not.found";

    public ResourceNotFoundException() {
        super(MESSAGE, KEY_MESSAGE);
    }

    public ResourceNotFoundException(final String notFoundMessage, final String... errorCodes) {
        super(MESSAGE, KEY_MESSAGE);
        getErrors().getFieldErrors().addAll(
                Arrays.stream(errorCodes).map(code -> new CustomFieldError(
                        code, notFoundMessage
                )).collect(Collectors.toList()));
    }

    public ResourceNotFoundException(final List<Integer> idxs, final String notFoundMessage) {
        super(MESSAGE, KEY_MESSAGE);
        getErrors().getGlobalErrors().addAll(
                idxs.stream().map(idx -> new CustomGlobalError(
                        ListDto.generatePrefix(idx), notFoundMessage
                )).collect(Collectors.toList()));
    }

    public ResourceNotFoundException(final Integer idx, final String notFoundMessage) {
        super(MESSAGE, KEY_MESSAGE);
        getErrors().getGlobalErrors().add(new CustomGlobalError(
                ListDto.generatePrefix(idx), notFoundMessage
        ));
    }
}
