package com.pfv.admin.common.services;

import com.google.common.collect.Lists;
import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.exceptions.services.ServiceErrors;
import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.dto.ListDto;
import com.pfv.admin.common.model.dto.operations.OperationDto;
import com.pfv.admin.common.model.dto.operations.OperationsInfoDto;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.model.querydsl.PredicateBuilder;
import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.common.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class ServiceImpl<DTO extends EntityOperationsInfoDto, ENTITY extends Entity, REPOSITORY extends Repository<ENTITY>>
        implements Service<DTO, ENTITY> {
    public static final String MESSAGE_ENTITY_CANNOT_BE_SAVED = "The entity cannot be saved";
    public static final String MESSAGE_ENTITIES_CANNOT_BE_SAVED = "The entities cannot be saved";
    public static final String MESSAGE_ENTITY_CANNOT_BE_DELETED = "The entity cannot be deleted";
    public static final String MESSAGE_ENTITIES_CANNOT_BE_DELETED = "The entities cannot be deleted";

    @Autowired
    protected Mapper<DTO, ENTITY> mapper;
    @Autowired
    protected REPOSITORY repository;
    @Autowired(required = false)
    protected Validator<ENTITY> validator;
    @Autowired(required = false)
    protected ConstraintsChecker<ENTITY> constraintsChecker;

    private Class<ENTITY> classEntity;

    public ServiceImpl() {
        this.classEntity = (Class<ENTITY>) GenericTypeResolver.resolveTypeArguments(getClass(), ServiceImpl.class)[1];
    }

    public PredicateBuilder<ENTITY> buildPredicateBuilder() {
        return new PredicateBuilder<>(classEntity, StringUtils.uncapitalize(classEntity.getSimpleName()));
    }

    @Override
    public List<DTO> getAll() {
        return this.getAll(null);
    }

    @Override
    public Page<DTO> getAllWithFiltersAndPaging(final Pageable pageable, final List<SearchCriteria> searchCriteria) {
        Page<ENTITY> results;
        final PredicateBuilder<ENTITY> predicateBuilder = buildPredicateBuilder();

        if (!ObjectUtils.isEmpty(searchCriteria)) {
            predicateBuilder.with(searchCriteria);
            results = this.repository.findAll(predicateBuilder.build(), pageable);
        } else {
            results = this.repository.findAll(pageable);
        }
        return new PageImpl<>(entitiesToDtos(results.getContent()), pageable, results.getTotalElements());
    }

    @Override
    public Long countWithFiltering(final List<SearchCriteria> searchCriteria) {
        final PredicateBuilder<ENTITY> predicateBuilder = buildPredicateBuilder();

        if (!ObjectUtils.isEmpty(searchCriteria)) {
            predicateBuilder.with(searchCriteria);
            return this.repository.count(predicateBuilder.build());
        } else {
            return this.repository.count();
        }
    }

    @Override
    public List<DTO> getAll(final String sortField) {
        List<ENTITY> entities;

        if (sortField == null) {
            entities = repository.findAll();
        } else {
            entities = repository.findAll(Sort.by(Sort.Direction.ASC, sortField));
        }

        return entitiesToDtos(entities);
    }

    @Override
    public List<ENTITY> getAllEntities() {
        return repository.findAll();
    }

    @Override
    public Page<DTO> getAllWithPaging(final Pageable pageable) {
        final Page<ENTITY> pagedEntities = this.repository.findAll(pageable);
        return new PageImpl<>(entitiesToDtos(pagedEntities.getContent()),
                pageable, pagedEntities.getTotalElements()
        );
    }

    @Override
    public Optional<DTO> findById(final String id) {
        Optional<ENTITY> entity = repository.findById(id);
        if (entity.isPresent()) {
            return Optional.ofNullable(entityToDto(entity.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<ENTITY> findEntityById(final String id) {
        return repository.findById(id);
    }

    @Override
    public void delete(final String id) {
        Optional<ENTITY> entity = repository.findById(id);
        if (entity.isPresent()) {
            if (constraintsChecker != null) {
                constraintsChecker.checkDeleteConstraints(entity.get());
            }
            preDelete(entity.get());
            repository.delete(entity.get());
        }
    }

    protected void preDelete(final ENTITY entity) {

    }

    @Override
    public AliasIdLabelDto findMinimalById(String id) {
        return mapper.toResultDto(repository.findById(id).orElseThrow(NotFoundException::new));
    }

    @Override
    public DTO update(final DTO dto) {
        this.preUpdate(dto);
        ENTITY entity = repository.findById(dto.getId()).get();
        ENTITY entitySaved = save(mapper.merge(dto, entity));
        return entityToDto(entitySaved, calculateOperationsInfo(entitySaved));
    }

    private List<DTO> entitiesToDtos(final List<ENTITY> entities) {
        return entities.stream().map(
                entity -> entityToDto(entity)).collect(Collectors.toList());
    }

    private DTO entityToDto(final ENTITY entity) {
        return entityToDto(entity, calculateOperationsInfo(entity));
    }

    private DTO entityToDto(final ENTITY entity, final OperationsInfoDto operationsInfoDto) {
        DTO dto = mapper.entityToDto(entity);
        dto.setOperationsInfo(operationsInfoDto);
        return dto;
    }

    public void preUpdate(final DTO dto) {

    }

    @Override
    @Transactional
    public List<DTO> update(final List<DTO> dtos) {
        return persist(dtos, this::update);
    }

    public void preCreate(final DTO dto) {

    }

    @Override
    public DTO create(final DTO dto) {
        this.preCreate(dto);
        ENTITY entity = mapper.dtoToEntity(dto);
        return entityToDto(save(entity), emptyOperationsInfo());
    }

    @Override
    @Transactional
    public List<DTO> create(final List<DTO> dtos) {
        return persist(dtos, this::create);
    }

    private List<DTO> persist(final List<DTO> dtos, final Function<DTO, DTO> functionPersist) {
        int idx = 0;
        List<DTO> dtosCreated = new ArrayList<>();
        ServiceErrors errors = new ServiceErrors();
        for (DTO dto : dtos) {
            try {
                dtosCreated.add(functionPersist.apply(dto));
            } catch (ServiceException exception) {
                exception.getErrors().addPrefix(ListDto.generatePrefix(idx));
                errors.addErrors(exception.getErrors());
            }
            idx++;
        }
        if (errors.hasErrors()) {
            throw new ServiceException(MESSAGE_ENTITIES_CANNOT_BE_SAVED, errors.getErrors());
        }
        return dtosCreated;
    }

    protected ENTITY save(final ENTITY entity) {
        preValidate(entity);
        if (validator != null) {
            validator.validate(entity, new ServiceErrors());
        }
        ENTITY oldEntity = null;
        if (entity.getId() != null) {
            oldEntity = repository.findById(entity.getId()).get();
            if (constraintsChecker != null) {
                constraintsChecker.checkUpdateConstraints(oldEntity, entity);
            }
        }
        preSave(oldEntity, entity);
        ENTITY entitySaved = repository.save(entity);
        postSave(oldEntity, entity);
        return entitySaved;
    }

    protected void preValidate(final ENTITY oldEntity) {

    }

    protected void preSave(final ENTITY oldEntity, final ENTITY entity) {
        if (StringUtils.isEmpty(entity.getAlias())) {
            String alias = createAlias(entity);
            if (StringUtils.hasLength(alias)) {
                Long total = repository.countByAliasStartingWithIgnoreCase(alias);
                if (total > 0) {
                    alias += (total + 1);
                }
                entity.setAlias(alias);
            }
        }
    }

    protected String createAlias(ENTITY entity) {
        return null;
    };

    protected void postSave(final ENTITY oldEntity, final ENTITY newEntity) {

    }

    public Mapper<DTO, ENTITY> getMapper() {
        return mapper;
    }

    @Override
    public OperationsInfoDto calculateOperationsInfo(final ENTITY entity) {
        List<String> reasons = Lists.newArrayList();
        if (constraintsChecker != null) {
            constraintsChecker.checkDeleteConstraints(entity, reasons);
        }
        Boolean isDeletingAllowed = reasons.isEmpty();
        return OperationsInfoDto.builder()
                .delete(OperationDto.builder().allowed(isDeletingAllowed).reasons(reasons).build())
                .build();
    }

    public OperationsInfoDto emptyOperationsInfo() {
        return OperationsInfoDto.builder()
                .delete(OperationDto.builder().allowed(Boolean.TRUE).reasons(Lists.newArrayList()).build())
                .build();
    }

    @Override
    public AliasIdLabelDto findByAlias(String alias) {
        return  mapper.toResultDto(repository.findFirstByAliasAndActiveIsTrue(alias));
    }

    @Override
    public List<AliasIdLabelDto> findByAliasStartsWith(String alias) {
        return repository.findAllByActiveIsTrueAndAliasStartsWithIgnoreCase(alias).stream()
                .map(person -> mapper.toResultDto(person))
                .collect(Collectors.toList());
    }

}