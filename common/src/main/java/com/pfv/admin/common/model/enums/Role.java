package com.pfv.admin.common.model.enums;

public enum Role implements EnumTranslationMessage {
    ADMIN,
    USER,
    GUEST;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}