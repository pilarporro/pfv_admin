package com.pfv.admin.common.model.entity;

import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.common.services.Service;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Slf4j
public class EntityResults<DTO extends EntityOperationsInfoDto, ENTITY extends AuditableEntity> {
    private final Service<DTO, ENTITY> service;
    private final List<SearchCriteria> criteria;
    private final int pageSize;
    private final String sortField;

    private Page<DTO> currentPage;

    private int totalPages;
    private int nextPage;
    private int nextIdxDto;
    @Getter
    private long totalElements;

    public <SERVICE extends Service<DTO, ENTITY>> EntityResults(final SERVICE service, final List<SearchCriteria> criteria) {
        this(service, criteria, 2000, "lastModified");
    }

    public <SERVICE extends Service<DTO, ENTITY>> EntityResults(final SERVICE service, final List<SearchCriteria> criteria, final int pageSize,  final String sortField) {
        this.service = service;
        this.sortField = sortField;
        this.pageSize = pageSize;
        this.criteria = criteria;
        currentPage = getDtos(0);
        nextPage = 1;
        nextIdxDto = 0;
        totalElements = currentPage.getTotalElements();
        totalPages = currentPage.getTotalPages();
    }

    public DTO next() {
        DTO dto = null;
        if (totalElements > 0) {
            if (nextIdxDto < currentPage.getContent().size()) {
                dto = currentPage.getContent().get(nextIdxDto);
                nextIdxDto += 1;
            } else if (nextPage < totalPages) {
                currentPage = getDtos(nextPage);
                nextPage += 1;
                dto = currentPage.getContent().get(0);
                nextIdxDto = 1;
            }
        }

        return dto;
    }

    private Page<DTO> getDtos(int numPage) {
        Pageable pageable = ResourceUtil.createPageRequest(numPage, this.pageSize, sortField, 1);
        return service.getAllWithFiltersAndPaging(pageable, criteria);
    }
}
