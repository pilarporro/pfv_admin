package com.pfv.admin.common.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Getter
@Setter
@AllArgsConstructor
public class ListDto<DTO extends Dto> implements List<DTO> {
    private static final String CONTAINER_NAME = "dtos";

    @NotEmpty
    @Valid
    protected List<DTO> dtos;

    public ListDto() {
        this.dtos = new ArrayList<>();
    }

    @JsonIgnore
    public static String generatePrefix(final Integer idx) {
        return String.format("%s[%s]", ListDto.CONTAINER_NAME, idx);
    }

    @Override
    public int size() {
        return dtos.size();
    }

    @Override
    public boolean isEmpty() {
        return dtos.isEmpty();
    }

    @Override
    public boolean contains(final Object o) {
        return dtos.contains(o);
    }

    @Override
    public Iterator<DTO> iterator() {
        return dtos.iterator();
    }

    @Override
    public Object[] toArray() {
        return dtos.toArray();
    }

    @Override
    public <T> T[] toArray(final T[] a) {
        return dtos.toArray(a);
    }

    @Override
    public boolean add(final DTO dto) {
        return dtos.add(dto);
    }

    @Override
    public boolean remove(final Object o) {
        return dtos.remove(o);
    }

    @Override
    public boolean containsAll(final Collection<?> c) {
        return dtos.containsAll(c);
    }

    @Override
    public boolean addAll(final Collection<? extends DTO> c) {
        return dtos.addAll(c);
    }

    @Override
    public boolean addAll(final int index, final Collection<? extends DTO> c) {
        return dtos.addAll(index, c);
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
        return dtos.removeAll(c);
    }

    @Override
    public boolean retainAll(final Collection<?> c) {
        return dtos.retainAll(c);
    }

    @Override
    public void clear() {
        dtos.clear();
    }

    @Override
    public DTO get(final int index) {
        return dtos.get(index);
    }

    @Override
    public DTO set(final int index, final DTO element) {
        return dtos.set(index, element);
    }

    @Override
    public void add(final int index, final DTO element) {
        dtos.add(index, element);
    }

    @Override
    public DTO remove(final int index) {
        return dtos.remove(index);
    }

    @Override
    public int indexOf(final Object o) {
        return dtos.indexOf(o);
    }

    @Override
    public int lastIndexOf(final Object o) {
        return dtos.lastIndexOf(o);
    }

    @Override
    public ListIterator<DTO> listIterator() {
        return dtos.listIterator();
    }

    @Override
    public ListIterator<DTO> listIterator(final int index) {
        return dtos.listIterator(index);
    }

    @Override
    public List<DTO> subList(final int fromIndex, final int toIndex) {
        return dtos.subList(fromIndex, toIndex);
    }
}
