/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.common.repositories;

import com.pfv.admin.common.model.entity.Entity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface Repository<ENTITY extends Entity> extends MongoRepository<ENTITY, String>, QuerydslPredicateExecutor<ENTITY> {
    Long countByAliasStartingWithIgnoreCase(String alias);

    List<ENTITY> findAllByActiveIsTrueAndAliasStartsWithIgnoreCase(String alias);

    ENTITY findFirstByAliasAndActiveIsTrue(String alias);
}
