package com.pfv.admin.common.mappers;

import com.pfv.admin.common.exceptions.ErrorMessagesFactory;
import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.exceptions.resources.ResourceErrorsException;
import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.common.utils.ResourceUtil;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.repository.support.Repositories;
import org.springframework.util.StringUtils;


@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public abstract class EntityToIdMapper {
    @Autowired
    private ListableBeanFactory listableBeanFactory;

    protected Repositories repositories;

    @EventListener
    public void handleContextRefresh(final ContextRefreshedEvent event) {
        this.repositories = new Repositories(listableBeanFactory);
    }

    public <T extends Entity> T resolve(final String id, @TargetType final Class<T> entityClass) {
        T entry = null;
        if (id != null) {
            if (repositories == null) {
                handleContextRefresh(null);
            }
            Repository<T> repository = (Repository<T>) repositories.getRepositoryFor(entityClass).get();
            entry = repository.findById(id).orElseThrow(() ->
                    new ServiceException(ResourceErrorsException.MESSAGE,
                            ErrorMessagesFactory.newWithFieldMessage(
                                    StringUtils.uncapitalize(entityClass.getSimpleName()) + "Id",
                                    ResourceUtil.getMessage(NotFoundException.KEY_MESSAGE))));

        }
        return entry;
    }

    public <T extends Entity> T resolve(final AliasIdLabelDto aliasIdLabelDto, @TargetType final Class<T> entityClass) {
        T entry = null;
        if (aliasIdLabelDto != null && StringUtils.hasLength(aliasIdLabelDto.getId())) {
            if (repositories == null) {
                handleContextRefresh(null);
            }
            Repository<T> repository = (Repository<T>) repositories.getRepositoryFor(entityClass).get();
            entry = repository.findById(aliasIdLabelDto.getId()).orElseThrow(() ->
                    new ServiceException(ResourceErrorsException.MESSAGE,
                            ErrorMessagesFactory.newWithFieldMessage(
                                    StringUtils.uncapitalize(entityClass.getSimpleName()) + "Id",
                                    ResourceUtil.getMessage(NotFoundException.KEY_MESSAGE))));

        }
        return entry;
    }
}
