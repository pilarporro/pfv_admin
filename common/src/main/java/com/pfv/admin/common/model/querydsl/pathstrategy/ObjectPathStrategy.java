package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;

public class ObjectPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        final PathBuilder<Object> path = entityPath.get(searchCriteria.getKey());
        switch (searchCriteria.getOperation()) {
            case EQUALS:
                if (searchCriteria.getValue() == null) {
                    return path.isNull();
                }
                return path.eq(searchCriteria.getValue());
            case NOT_EQUALS:
                if (searchCriteria.getValue() == null) {
                    return path.isNotNull();
                }
                return path.ne(searchCriteria.getValue());
            default:
                return null;
        }
    }

}
