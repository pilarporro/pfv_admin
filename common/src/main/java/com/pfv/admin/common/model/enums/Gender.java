package com.pfv.admin.common.model.enums;

public enum Gender implements EnumTranslationMessage {
    MALE,
    FEMALE,
    UNKNOWN;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
