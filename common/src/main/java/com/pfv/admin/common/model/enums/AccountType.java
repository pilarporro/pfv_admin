package com.pfv.admin.common.model.enums;

public enum AccountType implements EnumTranslationMessage{
    BANK,
    CASH,
    PUBLIC_GRANT;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
