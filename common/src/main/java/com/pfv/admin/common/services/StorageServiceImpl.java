package com.pfv.admin.common.services;

import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.dto.PhotoDto;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Transactional
@Service
@Slf4j
@Setter
public class StorageServiceImpl implements StorageService {
    private static final DateTimeFormatter FOLDER_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    @Value("${storage.path}")
    private String storagePath;
    @Value("${storage.tmp-path}")
    private String temporaryPath;
    @Value("${storage.url}")
    private String urlStorage;

    @Override
    public void buildUrls(final PhotoDto storageDto) {
        String assetsPath = storageDto.getTemporary() ? "pfv/tmp" : "pfv/" + storageDto.getFolder();
        storageDto.setUrl(String.format("%s/%s/%s", urlStorage, assetsPath, storageDto.getDiskName()));
        storageDto.setUrlThumbnail(String.format("%s/%s/%s", urlStorage, assetsPath, storageDto.getDiskNameThumbnail()));
    }

    @Override
    public PhotoDto saveTemporary(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (extension == null) {
            // TODO
        } else {
            extension = extension.toLowerCase();
        }
        String uniqueDiskName = getUniqueDiskName();
        PhotoDto storageDto = PhotoDto.builder()
                .diskNameThumbnail("thumbnail." + uniqueDiskName + "." + extension)
                .diskName(uniqueDiskName + "." + extension).temporary(true).build();
        try {
            String newPath = temporaryPath + "/" + storageDto.getDiskName();
            file.transferTo(new File(newPath));
            resize(extension, newPath);
            storageDto.setFolder(createFolder());
            buildUrls(storageDto);
        } catch (IOException exception) {
            log.error("Could not store the file", exception);
            throw new ServiceException(exception);
        }
        return storageDto;
    }

    private String createFolder() throws IOException {
        String folder = LocalDate.now().format(FOLDER_FORMATTER);
        Path pathFolder = Paths.get(String.format("%s/%s", storagePath, folder));
        Files.createDirectories(pathFolder);
        return folder;
    }

    @Override
    public PhotoDto saveTemporary(Path path) {
        String strPath = path.toString();
        String extension = "";
        if (strPath.lastIndexOf(".") > 0) {
            extension = strPath.substring(strPath.lastIndexOf(".") + 1);
        }

        String uniqueDiskName = getUniqueDiskName();
        PhotoDto storageDto = PhotoDto.builder()
                .diskNameThumbnail("thumbnail." + uniqueDiskName + "." + extension)
                .diskName(uniqueDiskName + "." + extension).temporary(true).build();
        try {
            Path newPath = Paths.get(getTemporaryFilePath(storageDto.getDiskName()));
            Files.copy(path, newPath, StandardCopyOption.REPLACE_EXISTING);
            storageDto.setFolder(createFolder());
            buildUrls(storageDto);
        } catch (IOException exception) {
            log.error("Could not store the file", exception);
            throw new ServiceException(exception);
        }
        return storageDto;
    }

    public static String getUniqueDiskName() {
        return String.format("%s%s", RandomString.make(5), Long.valueOf(System.currentTimeMillis()));
    }

    @Override
    public void delete(String folder, final String diskName) {
        try {
            if ((diskName != null) && (!diskName.equals("null"))) {
                Files.delete(Paths.get(getFilePath(folder, diskName)));
            }
        } catch (IOException exception) {
            log.warn("Could not delete the file", exception);
        }
    }

    @Override
    public void save(final String folder, final String diskName, final String diskNameThumbnail) {
        try {
            Files.move(Paths.get(temporaryPath + "/" + diskName),
                    Paths.get(getFilePath(folder, diskName)),
                    StandardCopyOption.REPLACE_EXISTING);
            if (!StringUtils.isEmpty(diskNameThumbnail)) {
                createThumbnail(folder, diskName, diskNameThumbnail);
            }
        } catch (IOException exception) {
            log.error("Could not store the tmp file", exception);
            throw new ServiceException(exception);
        }
    }

    private String getFilePath(final String folder, final String fileName) {
        return getFilePath(storagePath, folder, fileName);
    }

    public static String getFilePath(final String storagePath, final String folder, final String fileName) {
        return String.format("%s/%s/%s", storagePath, folder, fileName);
    }

    private String getTemporaryFilePath(final String fileName) {
        return String.format("%s/%s", temporaryPath, fileName);
    }

    @Override
    public void createThumbnail(final String folder, final String diskName, final String diskNameThumbnail) throws IOException {
        File inputFile = new File(getFilePath(folder, diskName));
        BufferedImage inputImage = ImageIO.read(inputFile);


        int scaledWidth = 150;
        int scaledHeight = (150 * inputImage.getHeight()) / inputImage.getWidth();

        Thumbnails.of(inputFile)
                .size(scaledWidth, scaledHeight)
                .toFiles(Rename.PREFIX_DOT_THUMBNAIL);

        // resize(inputFile.getAbsolutePath(), storagePath + "/" + diskNameThumbnail, scaledWidth, scaledHeight);
    }

    @Override
    public PhotoDto clonePhoto(PhotoDto storageDto) {
        Path path = Paths.get(storageDto.getTemporary()
                ? getTemporaryFilePath(storageDto.getDiskName())
                : getFilePath(storageDto.getFolder(), storageDto.getDiskName()));
        return saveTemporary(path);
    }

    @Override
    public void deleteTemporary() {

    }


    public static void compress(File input, File output, final Float quality) throws IOException {

        BufferedImage image = ImageIO.read(input);
        OutputStream out = new FileOutputStream(output);

        ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);

        ImageWriteParam param = writer.getDefaultWriteParam();

        if (param.canWriteCompressed()) {
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);
        }

        writer.write(null, new IIOImage(image, null, null), param);

        out.close();
        ios.close();
        writer.dispose();
    }

    public static void resize(String inputImagePath,
                              String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);


        // scales the input image to the output image
        BufferedImage outputImage = getScaledInstance(inputImage, scaledWidth, scaledHeight);

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    public static BufferedImage getScaledInstance(BufferedImage img,
                                                  int targetWidth,
                                                  int targetHeight) {
        int type = (img.getTransparency() == Transparency.OPAQUE) ?
                BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage) img;
        int currentWidth = img.getWidth();
        int currentHeight = img.getHeight();

        do {
            if (currentWidth > targetWidth) {
                currentWidth *= 0.5;
                if (currentWidth < targetWidth) {
                    currentWidth = targetWidth;
                }
            }

            if (currentHeight > targetHeight) {
                currentHeight *= 0.5;
                if (currentHeight < targetHeight) {
                    currentHeight = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(currentWidth, currentHeight, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.drawImage(ret, 0, 0, currentWidth, currentHeight, null);
            g2.dispose();

            ret = tmp;
        } while (currentWidth != targetWidth || currentHeight != targetHeight);

        return ret;
    }

    public static void resize(final String extension, final String inputImagePath) throws IOException {
        resize(extension, inputImagePath, 1024, 0.82f);
    }

    public static void resize(final String extension, final String inputImagePath, final Integer newSize, final Float quality) throws IOException {
        if (StringUtils.hasLength(extension) &&
                (extension.equalsIgnoreCase("JPG") || extension.equalsIgnoreCase("JPEG"))) {
            File inputFile = new File(inputImagePath);
            BufferedImage inputImage = ImageIO.read(inputFile);

            if (inputImage.getWidth() > newSize) {
                int scaledWidth = newSize;
                int scaledHeight = (newSize * inputImage.getHeight()) / inputImage.getWidth();
                resize(inputImagePath, inputImagePath, scaledWidth, scaledHeight);
            }

            compress(new File(inputImagePath), new File(inputImagePath), quality);
        }
    }

}
