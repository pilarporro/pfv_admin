package com.pfv.admin.common.mappers;


import org.mapstruct.Builder;
import org.mapstruct.MapperConfig;
import org.mapstruct.MappingInheritanceStrategy;
import org.mapstruct.ReportingPolicy;

@MapperConfig(
        builder = @Builder(disableBuilder = true),
        uses = { EntityToIdMapper.class },
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_ALL_FROM_CONFIG
)
public interface MapperConfiguration {

    // public abstract Entity dtoToEntity(Dto dto);
}
