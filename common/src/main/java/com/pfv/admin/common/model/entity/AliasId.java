/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.common.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AliasId {
    private String id;
    private String alias;

    public static AliasId ofIdAndAlias(String id, String alias) {
        AliasId aliasId = new AliasId();
        aliasId.setId(id);
        aliasId.setAlias(alias);
        return  aliasId;
    }
}
