package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.ArrayPath;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.mongodb.MongodbExpressions;

public class CirclePath extends ArrayPath<Double[], Double> {

    public CirclePath(String variable) {
        super(Double[].class, variable);
    }

    public CirclePath(Path<?> parent, String property) {
        super(Double[].class, parent, property);
    }

    public CirclePath(PathMetadata metadata) {
        super(Double[].class, metadata);
    }

    public BooleanExpression near(double latVal, double longVal) {
        return MongodbExpressions.near(this, latVal, longVal);
    }
}
