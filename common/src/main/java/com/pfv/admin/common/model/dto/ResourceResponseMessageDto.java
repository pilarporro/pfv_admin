package com.pfv.admin.common.model.dto;


import com.pfv.admin.common.exceptions.ErrorMessages;

import java.time.LocalDateTime;

public final class ResourceResponseMessageDto {

    private final LocalDateTime timestamp = LocalDateTime.now();
    private int status;
    private String error;
    private ErrorMessages messages = new ErrorMessages();
    private String path;

    private ResourceResponseMessageDto() {
    }

    public ResourceResponseMessageDto(final int status,
                                      final String error,
                                      final ErrorMessages messages,
                                      final String path) {
        this.status = status;
        this.error = error;
        this.messages = messages;
        this.path = path;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public ErrorMessages getMessages() {
        return messages;
    }

    public String getPath() {
        return path;
    }

}