package com.pfv.admin.common.model.enums;

public enum PlaceType implements EnumTranslationMessage{
    VET_CLINIC,
    SHOP;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }

    public static final String VET_CLINIC_TYPE = "VET_CLINIC";
    public static final String SHOP_TYPE = "SHOP";
}
