package com.pfv.admin.common.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDate;

import static com.pfv.admin.common.utils.DateUtil.formatDate;

public class LocalDateSerializer extends JsonSerializer<LocalDate> {

    @Override public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(formatDate(localDate));
    }
}