package com.pfv.admin.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhotoDto extends Dto {
    private String folder;
    private String diskName;
    private String diskNameThumbnail;
    private String url;
    private String urlThumbnail;
    private Boolean temporary;

}
