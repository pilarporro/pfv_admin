package com.pfv.admin.common.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Photo extends Entity {
    private String folder;
    private String diskName;
    private String diskNameThumbnail;
    private Boolean temporary;
}
