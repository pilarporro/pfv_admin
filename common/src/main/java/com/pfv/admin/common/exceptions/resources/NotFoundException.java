package com.pfv.admin.common.exceptions.resources;


import com.pfv.admin.common.exceptions.ApiException;

public class NotFoundException extends ApiException {
    private static final String MESSAGE = "Resource not found";
    public static final String KEY_MESSAGE = "error.not.found";

    public NotFoundException() {
        super(MESSAGE, KEY_MESSAGE);
    }
}
