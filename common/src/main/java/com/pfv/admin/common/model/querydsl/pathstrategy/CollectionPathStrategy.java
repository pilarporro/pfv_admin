package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;

import java.lang.reflect.Field;
import java.util.List;

public class CollectionPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        try {
            final Field fieldSet = getField(entityPath, searchCriteria);
            final Class classSet = Class.forName(fieldSet.getGenericType().getTypeName().split("[<>]")[1]);
            final PathBuilder anyEntityPathBuilder = ((PathBuilder) entityPath.getCollection(searchCriteria.getKey(), classSet).any());
            BooleanExpression filter = null;
            switch (searchCriteria.getOperation()) {
                case IN:
                    List<String> values = ((List<String>) searchCriteria.getValue());
                    filter = anyEntityPathBuilder.in(values);
                    break;
                case NOT_IN:
                    List<String> valuesNotIn = ((List<String>) searchCriteria.getValue());
                    filter = anyEntityPathBuilder.notIn(valuesNotIn);
                    break;
                case CONTAINS:
                    for (int index = 0; index < searchCriteria.getFields().length; index++) {
                        String field = searchCriteria.getFields()[index];
                        filter = (filter == null) ? anyEntityPathBuilder.getString(field).containsIgnoreCase(searchCriteria.getValue().toString())
                                : filter.or(anyEntityPathBuilder.getString(field).containsIgnoreCase(searchCriteria.getValue().toString()));
                    }
                    break;
                case STARTS_WITH:
                    for (int index = 0; index < searchCriteria.getFields().length; index++) {
                        String field = searchCriteria.getFields()[index];
                        filter = (filter == null) ? anyEntityPathBuilder.getString(field).startsWithIgnoreCase(searchCriteria.getValue().toString())
                                : filter.or(anyEntityPathBuilder.getString(field).startsWithIgnoreCase(searchCriteria.getValue().toString()));
                    }
                    break;
                case ENDS_WITH:
                    for (int index = 0; index < searchCriteria.getFields().length; index++) {
                        String field = searchCriteria.getFields()[index];
                        filter = (filter == null) ? anyEntityPathBuilder.getString(field).endsWithIgnoreCase(searchCriteria.getValue().toString())
                                : filter.or(anyEntityPathBuilder.getString(field).endsWithIgnoreCase(searchCriteria.getValue().toString()));
                    }
                    break;
                case EQUALS:
                    for (int index = 0; index < searchCriteria.getFields().length; index++) {
                        String field = searchCriteria.getFields()[index];
                        filter = (filter == null) ? anyEntityPathBuilder.getString(field).equalsIgnoreCase(searchCriteria.getValue().toString())
                                : filter.or(anyEntityPathBuilder.getString(field).equalsIgnoreCase(searchCriteria.getValue().toString()));
                    }
                    break;
                case NOT_EQUALS:
                    for (int index = 0; index < searchCriteria.getFields().length; index++) {
                        String field = searchCriteria.getFields()[index];
                        filter = (filter == null) ? anyEntityPathBuilder.getString(field).notEqualsIgnoreCase(searchCriteria.getValue().toString())
                                : filter.and(anyEntityPathBuilder.getString(field).notEqualsIgnoreCase(searchCriteria.getValue().toString()));
                    }
                    break;
                default:
            }
            return filter;

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Field getField(final PathBuilder entityPath, final SearchCriteria searchCriteria) throws NoSuchFieldException {
        try {
            return entityPath.getType().getDeclaredField(searchCriteria.getKey());
        } catch (NoSuchFieldException e) {
            return entityPath.getType().getSuperclass().getDeclaredField(searchCriteria.getKey());
        }
    };
}