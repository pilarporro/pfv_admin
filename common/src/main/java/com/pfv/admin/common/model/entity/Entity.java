package com.pfv.admin.common.model.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public abstract class Entity implements Serializable {
    @Id
    protected String id;

    @Indexed(unique = true, name = "alias", direction = IndexDirection.ASCENDING, dropDups = true)
    private String alias;

    @Field
    private Boolean markToDelete;

    @Field
    private Boolean notification;
}
