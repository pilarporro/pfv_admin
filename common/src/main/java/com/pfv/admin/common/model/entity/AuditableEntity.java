package com.pfv.admin.common.model.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(callSuper = true)
public abstract class AuditableEntity extends Entity {

    @CreatedDate
    protected LocalDateTime created;

    @CreatedBy
    protected String createdBy = "No user";

    @Indexed(direction = IndexDirection.DESCENDING)
    @LastModifiedDate
    protected LocalDateTime lastModified;

    @LastModifiedBy
    protected String lastModifiedBy;

    @Indexed
    private boolean active = true;
}
