package com.pfv.admin.common.exceptions;

import java.util.List;

public class ErrorMessagesFactory {

    private ErrorMessagesFactory() {
    }

    public static ErrorMessages newWithGlobalMessage(final String message) {
        ErrorMessages errorMessages = new ErrorMessages();
        errorMessages.getGlobalErrors().add(new CustomGlobalError(message));
        return errorMessages;
    }

    public static ErrorMessages newWithFieldMessage(final String field, final String message) {
        ErrorMessages errorMessages = new ErrorMessages();
        errorMessages.getFieldErrors().add(new CustomFieldError(field, message));
        return errorMessages;
    }

    public static ErrorMessages newWithConstraintMessage(final String message, final List<String> reasons) {
        ErrorMessages errorMessages = new ErrorMessages();
        errorMessages.getGlobalErrors().add(new CustomConstraintError(message, reasons));
        return errorMessages;
    }
}
