package com.pfv.admin.common.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public abstract class WithPhotosDto extends EntityOperationsInfoDto {
    private List<PhotoDto> photos;
    private Boolean withPhotos;
}
