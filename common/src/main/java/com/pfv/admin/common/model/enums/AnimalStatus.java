package com.pfv.admin.common.model.enums;

public enum AnimalStatus implements EnumTranslationMessage{
    FREE_IN_COLONY,
    ADOPTED,
    PRE_ADOPTION,
    AVAILABLE_ADOPTION,
    WITH_KEEPER,
    HOSPITALIZED;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
