package com.pfv.admin.common.model.enums;

public interface EnumTranslationMessage {
    String getMessageKey();
    String name();

    default String getMessageKey(String name, String type) {
        return "types." + name + "." + type;
    }
}
