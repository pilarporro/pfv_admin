package com.pfv.admin.common.utils;

import com.google.common.base.CaseFormat;
import com.pfv.admin.common.model.enums.EnumTranslationMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EnumTypesFactory {

    private static final Logger LOGGER = LogManager.getLogger();
    private static Set<Class<? extends EnumTranslationMessage>> enumClasses;

    private EnumTypesFactory() {
    }

    public static Class getEnumClass(final String className, final String enumsBasePackage) {
        try {
            Set<Class<? extends EnumTranslationMessage>> enumClasses =  getAllEnumTranslationMessageEnums(enumsBasePackage);
            for (Class<? extends EnumTranslationMessage> enumClass : enumClasses) {
                if (enumClass.getSimpleName().equals(className)) {
                    return enumClass;
                }
            }
        } catch (IllegalArgumentException notFound) {
            LOGGER.warn("Type enum incorrect " + className, notFound);
            return null;
        }

        return null;
    }

    public static Map<String, String> getAllTypes(final String enumsBasePackage) {
        Map<String, String> enumList = new HashMap<>();
        getAllEnumTranslationMessageEnums(enumsBasePackage).forEach(
                typeEnum -> enumList.put(
                        typeEnum.getSimpleName(),
                        CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, typeEnum.getSimpleName())
                ));
        return enumList;
    }

    private static Set<Class<? extends EnumTranslationMessage>> getAllEnumTranslationMessageEnums(final String enumsBasePackage) {
        if (enumClasses == null) {
            Reflections reflections = new Reflections(enumsBasePackage);
            enumClasses= reflections.getSubTypesOf(EnumTranslationMessage.class);
        }

        return enumClasses;
    }
}
