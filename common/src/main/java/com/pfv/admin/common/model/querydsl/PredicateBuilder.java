package com.pfv.admin.common.model.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.ArrayList;
import java.util.List;

public class PredicateBuilder<T> {

    private List<SearchCriteria> params;

    private Class entityClass;

    private String variable;

    private List<BooleanExpression> customPredicates;

    public PredicateBuilder(final Class entityClass, final String variable) {
        this.entityClass = entityClass;
        this.variable = variable;
        params = new ArrayList<>();
        customPredicates = new ArrayList<>();
    }

    private PredicateBuilder() {

    }

    public PredicateBuilder with(final SearchCriteria searchCriteria) {
        params.add(searchCriteria);
        return this;
    }

    public PredicateBuilder with(final List<SearchCriteria> searchCriterias){
        params.addAll(searchCriterias);
        return this;
    }

    public PredicateBuilder withCustomPredicate(final BooleanExpression customPredicate) {
        this.customPredicates.add(customPredicate);
        return this;
    }

    public BooleanExpression build() {
        if (params.size() == 0) {
            return null;
        }

        final List<BooleanExpression> predicates = new ArrayList<>();
        SearchPredicate<T> predicate;
        for (final SearchCriteria param : params) {
            predicate = new SearchPredicate(param);
            final BooleanExpression exp = predicate.getPredicate(entityClass, variable);
            if (exp != null) {
                predicates.add(exp);
            }
        }

        predicates.addAll(customPredicates);

        BooleanExpression result = predicates.get(0);
        for (int i = 1; i < predicates.size(); i++) {
            result = result.and(predicates.get(i));
        }
        return result;
    }
}
