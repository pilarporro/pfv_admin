package com.pfv.admin.common.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessages {

    private List<CustomGlobalError> globalErrors = new ArrayList<>();
    private List<CustomFieldError> fieldErrors = new ArrayList<>();

    public void addPrefix(final String prefix) {
        globalErrors.stream().forEach(ce -> ce.addPrefix(prefix));
        fieldErrors.stream().forEach(ce -> ce.addPrefix(prefix.concat(".")));
    }
}
