package com.pfv.admin.common.model.querydsl;

public enum SearchOperation {
    NEAR,
    EQUALS,
    BETWEEN,
    NOT_EQUALS,
    STARTS_WITH,
    NOT_NULL,
    NULL,
    ENDS_WITH,
    CONTAINS,
    IN,
    NOT_IN,
    GREATER_THAN_OR_EQUALS,
    LESS_THAN_OR_EQUALS
}
