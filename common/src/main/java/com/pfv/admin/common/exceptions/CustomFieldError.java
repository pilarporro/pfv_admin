package com.pfv.admin.common.exceptions;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CustomFieldError extends CustomError {
    @Getter
    @Setter
    private String field;

    public CustomFieldError(final String field, final String description) {
        super(description);
        this.field = field;
    }

    public void addPrefix(final String prefix) {
        this.field = prefix.concat(field);
    }
}
