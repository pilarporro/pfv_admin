package com.pfv.admin.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Dto {
    private String id;
    private String alias;
    private Boolean markToDelete;
    private Boolean notification;
}
