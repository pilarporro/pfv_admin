package com.pfv.admin.common.services;

import com.pfv.admin.common.exceptions.ErrorMessagesFactory;
import com.pfv.admin.common.exceptions.services.ServiceErrors;
import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.utils.ResourceUtil;

import java.util.ArrayList;
import java.util.List;

public abstract class ConstraintsChecker<ENTITY extends Entity> {
    public static final String MESSAGE_FIELD_CANNOT_BE_UPDATED = "validation.constraints.update";

    protected String getMessageKeyPrefix(final ENTITY entity) {
        return String.format("validation.constraints.%s.", entity.getClass().getSimpleName());
    }

    public void checkUpdateConstraints(final ENTITY oldEntity, final ENTITY newEntity) {
        ServiceErrors errors = new ServiceErrors();
        checkUpdateConstraints(oldEntity, newEntity, errors);
        if (errors.hasErrors()) {
            throw new ServiceException(ServiceImpl.MESSAGE_ENTITY_CANNOT_BE_SAVED, errors.getErrors());
        }
    }

    public void checkUpdateConstraints(final ENTITY oldEntity, final ENTITY newEntity, final ServiceErrors errors) {

    }

    public void checkDeleteConstraints(final ENTITY entity) {
        List<String> reasons = new ArrayList<>();
        checkDeleteConstraints(entity, reasons);
        if (!reasons.isEmpty()) {
            throw new ServiceException(ServiceImpl.MESSAGE_ENTITY_CANNOT_BE_DELETED,
                    ErrorMessagesFactory.newWithConstraintMessage(ResourceUtil.getMessage(getMessageKeyPrefix(entity) + "delete"), reasons));
        }
    }

    public void checkDeleteConstraints(final ENTITY entity, final List<String> reasons) {

    }

    public boolean isDeletingAllowed(final ENTITY entity) {
        return true;
    }
}