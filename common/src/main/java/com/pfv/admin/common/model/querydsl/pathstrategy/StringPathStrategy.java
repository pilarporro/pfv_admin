package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;

import java.util.List;

public class StringPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        final StringPath stringPath = entityPath.getString(searchCriteria.getKey());
        switch (searchCriteria.getOperation()) {
            case CONTAINS:
                return stringPath.containsIgnoreCase(searchCriteria.getValue().toString());
            case STARTS_WITH:
                return stringPath.startsWithIgnoreCase(searchCriteria.getValue().toString());
            case ENDS_WITH:
                return stringPath.endsWithIgnoreCase(searchCriteria.getValue().toString());
            case EQUALS:
                return stringPath.equalsIgnoreCase(searchCriteria.getValue().toString());
            case NOT_EQUALS:
                return stringPath.notEqualsIgnoreCase(searchCriteria.getValue().toString());
            case NOT_NULL:
                return stringPath.isNotNull();
            case NULL:
                return stringPath.isNull();
            case IN:
                return stringPath.in((List) searchCriteria.getValue());
            case NOT_IN:
                return stringPath.notIn((List) searchCriteria.getValue());
            default:
                return null;
        }
    }

}
