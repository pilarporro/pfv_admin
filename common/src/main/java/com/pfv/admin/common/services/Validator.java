package com.pfv.admin.common.services;

import com.pfv.admin.common.exceptions.services.ServiceErrors;
import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.utils.ResourceUtil;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Set;

@Slf4j
public abstract class Validator<ENTITY extends Entity> {

    protected String getEntityName(final ENTITY entity) {
        return entity.getClass().getSimpleName();
    }

    protected javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public void validate(final ENTITY entity, final ServiceErrors errors) {
        Set<ConstraintViolation<ENTITY>> result = validator.validate(entity);

        result.stream()
                .filter(error -> !error.getPropertyPath().toString().isEmpty())
                .forEach(error -> errors.newFieldError(error.getPropertyPath().toString(),
                        ResourceUtil.getMessage(getMessageKeyFromTemplate(error.getMessageTemplate()))));

        result.stream()
                .filter(error -> error.getPropertyPath().toString().isEmpty())
                .forEach(error -> errors.newGlobalError(ResourceUtil.getMessage(getMessageKeyFromTemplate(error.getMessageTemplate()))));

        if (errors.hasErrors()) {
            errors.getErrors().getGlobalErrors().forEach(
                    customError -> log.debug(customError.getDescription())
            );
            throw new ServiceException(ServiceImpl.MESSAGE_ENTITY_CANNOT_BE_SAVED, errors.getErrors());
        }
    }

    private String getMessageKeyFromTemplate(final String messageTemplate) {
        return messageTemplate.substring(1, messageTemplate.length() - 1);
    }

}
