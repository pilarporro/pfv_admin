package com.pfv.admin.common.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public final class DateUtil {
    public static final DateTimeFormatter SDP_DATE_FORMATTER_WITH_HYPHENS = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter SDP_DATE_FORMATTER_WITH_SLASHES = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static LocalDateTime getDateIntervalLeft(LocalDate date, Integer offsetTimezone) {
        return LocalDateTime.of(date, LocalTime.MIN).plusMinutes(offsetTimezone);
    }

    public static LocalDateTime getDateIntervalRight(LocalDate date, Integer offsetTimezone) {
        return LocalDateTime.of(date, LocalTime.MAX).plusMinutes(offsetTimezone);
    }

    public static LocalDate parseLocalDate(String str) {
        if (str.length() > 10) {
            str = str.substring(0, 10);
        }
        if (str.contains("-")) {
            return LocalDate.parse(str, SDP_DATE_FORMATTER_WITH_HYPHENS);
        } else if (str.contains("/")) {
            return LocalDate.parse(str, SDP_DATE_FORMATTER_WITH_SLASHES);
        } else {
            throw new IllegalArgumentException(String.format("The date %s cannot be formatted", str)) {};
        }
    }

    public static String formatDate(LocalDate localDate) {
        return SDP_DATE_FORMATTER_WITH_SLASHES.format(localDate);
    }
}
