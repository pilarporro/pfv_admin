package com.pfv.admin.common.services;

import com.pfv.admin.common.model.dto.PhotoDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

public interface StorageService {
    PhotoDto saveTemporary(MultipartFile file);
    PhotoDto saveTemporary(Path path);

    void delete(String folder, final String diskName);

    void save(String folder, String diskName, String diskNameThumnail);

    void createThumbnail(String folder, String diskName, String diskNameThumbnail) throws IOException;

    PhotoDto clonePhoto(PhotoDto storageDto);

    void deleteTemporary();

    void buildUrls(PhotoDto storageDto);
}
