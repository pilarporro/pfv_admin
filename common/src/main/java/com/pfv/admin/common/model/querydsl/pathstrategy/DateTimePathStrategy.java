package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchOperation;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.PathBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DateTimePathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        final DatePath datePath = entityPath.getDate(searchCriteria.getKey(), propertyClass);

        LocalDateTime localDate = null;
        if ((searchCriteria.getOperation() != SearchOperation.BETWEEN) && (searchCriteria.getValue() != null)) {
            localDate = getLocalDateTime(searchCriteria.getValue().toString(), searchCriteria.getOffsetTimezone());
        }
        switch (searchCriteria.getOperation()) {
            case EQUALS:
                return localDate == null ? datePath.isNull() : datePath.eq(localDate);
            case NOT_EQUALS:
                return localDate == null ? datePath.isNotNull() : datePath.ne(localDate);
            case GREATER_THAN_OR_EQUALS:
                return datePath.goe(localDate);
            case LESS_THAN_OR_EQUALS:
                return datePath.loe(localDate);
            case NOT_NULL:
                return datePath.isNotNull();
            case NULL:
                return datePath.isNull();
            case BETWEEN:
                List<String> dates = (List) searchCriteria.getValue();
                LocalDateTime firstDate = getLocalDateTime(dates.get(0), searchCriteria.getOffsetTimezone());
                LocalDateTime secondDate = getLocalDateTime(dates.get(1), searchCriteria.getOffsetTimezone());

                if ((firstDate != null) || (secondDate != null)) {
                    if (firstDate == null) {
                        return datePath.loe(secondDate);
                    } else if (secondDate == null) {
                        return datePath.goe(firstDate);
                    } else {
                        return datePath.between(firstDate, secondDate);
                    }
                } else {
                    return datePath.isNotNull();
                }
            default:
                return null;
        }
    }

    private LocalDateTime getLocalDateTime(String value, Integer offsetTimezone) {
        LocalDateTime localDate = null;
        if (value != null) {
            localDate = LocalDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME);
            // localDate.plusMinutes(offsetTimezone);
        }

        return localDate;
    }

}
