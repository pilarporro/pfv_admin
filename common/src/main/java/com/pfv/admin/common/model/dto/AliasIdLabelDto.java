/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AliasIdLabelDto {
    private String label;
    private String id;
    private String alias;

    public static AliasIdLabelDto ofId(String id) {
        AliasIdLabelDto dto = new AliasIdLabelDto();
        dto.setId(id);
        return  dto;
    }

    public static AliasIdLabelDto ofIdAlias(String id, String alias) {
        AliasIdLabelDto dto = new AliasIdLabelDto();
        dto.setId(id);
        dto.setAlias(alias);
        return  dto;
    }
}
