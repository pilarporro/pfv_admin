package com.pfv.admin.common.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AuditableDto extends Dto {

    private LocalDateTime created;

    private String createdBy;

    private LocalDateTime lastModified;

    private String lastModifiedBy;

    private boolean active = true;
}
