package com.pfv.admin.common.model.querydsl;

import org.springframework.context.support.EmbeddedValueResolutionSupport;
import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Formatter;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class SearchCriteriaFormatter extends EmbeddedValueResolutionSupport
        implements AnnotationFormatterFactory<SearchFields> {

    @Override
    public Set<Class<?>> getFieldTypes() {
        Set<Class<?>> fieldTypes = new HashSet<>();
        fieldTypes.add(List.class);
        return Collections.unmodifiableSet(fieldTypes);
    }

    @Override
    public Printer<List<SearchCriteria>> getPrinter(final SearchFields annotation, final Class<?> fieldType) {
        return configureFormatterFrom(annotation);
    }

    @Override
    public Parser<List<SearchCriteria>> getParser(final SearchFields annotation, final Class<?> fieldType) {
        return configureFormatterFrom(annotation);
    }

    private Formatter<List<SearchCriteria>> configureFormatterFrom(final SearchFields annotation) {
        return new Formatter<List<SearchCriteria>>() {
            @Override
            public String print(final List<SearchCriteria> object, final Locale locale) {
                return null;
            }

            @Override
            public List<SearchCriteria> parse(final String text, final Locale locale) throws ParseException {
                try {
                    return SearchCriteriaUtils.convertStringToSearchCriterias(text);
                } catch (IOException e) {
                    throw new ParseException(e.getMessage(), 0);
                }
            }
        };
    }
}
