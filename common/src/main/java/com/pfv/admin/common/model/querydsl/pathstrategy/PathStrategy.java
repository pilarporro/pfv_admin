package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;

public interface PathStrategy {

    BooleanExpression getBooleanExpression(PathBuilder entityPath, Class propertyClass, SearchCriteria searchCriteria);
}
