package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import org.springframework.data.geo.Point;


public class PointPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        CirclePath circle = new CirclePath(searchCriteria.getKey());
        //final PathBuilder<Object> path = entityPath.get(searchCriteria.getKey());
        Point point = (Point) searchCriteria.getValue();
        switch (searchCriteria.getOperation()) {
            case NEAR:
                return circle.near(point.getX(), point.getY());
            default:
                return null;
        }
    }

}
