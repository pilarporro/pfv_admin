package com.pfv.admin.common.model.dto.operations;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Information about an operation.
 */
@Getter
@Setter
@Builder
public class OperationDto {
    private Boolean allowed;
    private List<String> reasons;
}