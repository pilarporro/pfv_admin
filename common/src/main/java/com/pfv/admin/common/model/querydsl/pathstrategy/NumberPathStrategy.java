package com.pfv.admin.common.model.querydsl.pathstrategy;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchOperation;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;

public class NumberPathStrategy implements PathStrategy {

    @Override
    public BooleanExpression getBooleanExpression(final PathBuilder entityPath, final Class propertyClass, final SearchCriteria searchCriteria) {
        final NumberPath numberPath = entityPath.getNumber(searchCriteria.getKey(), propertyClass);
        String strValue = searchCriteria.getValue().toString();
        SearchOperation operation = searchCriteria.getOperation();
        if (strValue.startsWith("<")) {
            operation = SearchOperation.LESS_THAN_OR_EQUALS;
            if (strValue.length() > 1) {
                strValue = strValue.substring(1).trim();
            } else {
                strValue = "0";
            }
        } else if (strValue.startsWith(">")) {
            operation = SearchOperation.GREATER_THAN_OR_EQUALS;
            if (strValue.length() > 1) {
                strValue = strValue.substring(1).trim();
            } else {
                strValue = "0";
            }
        }

        final double value = Double.parseDouble(strValue);
        switch (operation) {
            case EQUALS:
                return numberPath.eq(value);
            case GREATER_THAN_OR_EQUALS:
                return numberPath.goe(value);
            case LESS_THAN_OR_EQUALS:
                return numberPath.loe(value);
            default:
                return null;
        }
    }
}
