package com.pfv.admin.common.model.enums;


public enum PaymentPeriodicity implements EnumTranslationMessage {
    MONTLHY,
    BIANNUAL,
    THREEMONTHS,
    ANNUAL,
    OTHER;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }
}
