package com.pfv.admin.common.model.enums;

public enum TagType implements EnumTranslationMessage {
    CAT,
    ACTIVITY;

    @Override
    public String getMessageKey() {
        return getMessageKey(getClass().getSimpleName(), name());
    }

}
