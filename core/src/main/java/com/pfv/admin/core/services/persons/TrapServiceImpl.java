package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.services.ServiceWithPhotosImpl;
import com.pfv.admin.core.model.persons.TrapDto;
import com.pfv.admin.data.model.persons.Trap;
import com.pfv.admin.data.repositories.persons.TrapRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class TrapServiceImpl extends ServiceWithPhotosImpl<TrapDto, Trap, TrapRepository> implements TrapService {

    protected String createAlias(Trap entity) {
        return entity.getName().replaceAll(" ", "").toLowerCase();
    };
}
