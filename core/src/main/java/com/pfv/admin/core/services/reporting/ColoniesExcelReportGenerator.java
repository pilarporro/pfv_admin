package com.pfv.admin.core.services.reporting;

public interface ColoniesExcelReportGenerator {
    byte[] generateColonyReport();

}
