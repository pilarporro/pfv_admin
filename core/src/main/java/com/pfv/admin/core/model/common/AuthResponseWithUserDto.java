package com.pfv.admin.core.model.common;

import com.pfv.admin.core.model.persons.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthResponseWithUserDto {
	private AuthResponseDto authResponse;
	private UserDto user;
	private boolean withUserAnswers;

}