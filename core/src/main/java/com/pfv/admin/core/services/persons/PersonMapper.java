package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.common.LocationMapper;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.persons.Person;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, ColonyMapper.class, LocationMapper.class },
        config = MapperConfiguration.class)
public abstract class PersonMapper extends com.pfv.admin.common.mappers.Mapper<PersonDto, Person> {

    @AfterMapping
    public void addLabel(final Person entity, @MappingTarget final AliasIdLabelDto dto) {
        dto.setLabel(String.format("%s (%s%s)", entity.getAlias(), entity.getFullName(), getArea(entity)));
    }

    public static String getArea(final Person entity) {
        if (entity.getLocation() != null && entity.getLocation().getArea() != null) {
            return String.format(" - %s", entity.getLocation().getArea().getName());
        } else {
            return "";
        }
    }
}
