package com.pfv.admin.core.model.persons;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends EntityOperationsInfoDto implements UserDetails {
    private List<String> roles;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String name;
    private String lastName;
    private AliasIdLabelDto person;


    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role))
                .collect(Collectors.toList());
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return isActive();
    }


    public Boolean getAdmin() {
        return roles.contains(Role.ADMIN.name());
    }

    public Boolean getGuest() {
        return roles.contains(Role.GUEST.name());
    }

    public Boolean getUser() {
        return roles.contains(Role.USER.name());
    }

}
