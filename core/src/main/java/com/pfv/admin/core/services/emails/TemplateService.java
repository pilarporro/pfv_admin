package com.pfv.admin.core.services.emails;

import java.util.Map;

public interface TemplateService {
    String generate(String templateName, Map<String, String> data);
}
