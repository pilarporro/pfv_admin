package com.pfv.admin.core.services.municipalities;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.municipalities.MunicipalityDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.municipalities.Municipality;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class MunicipalityMapper extends com.pfv.admin.common.mappers.Mapper<MunicipalityDto, Municipality> {

    @Mappings({
            @Mapping(target = "label", source = "city")
    })
    public abstract AliasIdLabelDto toResultDto(Municipality entity);
}
