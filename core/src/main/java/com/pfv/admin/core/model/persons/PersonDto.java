package com.pfv.admin.core.model.persons;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.model.enums.PaymentPeriodicity;
import com.pfv.admin.common.utils.LocalDateDeserializer;
import com.pfv.admin.common.utils.LocalDateSerializer;
import com.pfv.admin.core.model.common.LocationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto extends WithPhotosDto {
    private String email;
    private String fullName;
    @NotNull
    private String name;
    private String lastName;
    private String phone;
    private String bankAccount;
    private String notes;
    private String bankMatcher;
    private LocationDto location;


    @NotNull
    private Boolean isMember;
    private String idNumber;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate memberFrom;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate memberTo;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate nextPayment;

    private PaymentPeriodicity paymentPeriodicity;

    private List<AliasIdLabelDto> colonies;
}
