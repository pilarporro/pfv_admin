package com.pfv.admin.core.services.reporting;

public interface ColonyReportGenerator {
    byte[] generateColonyReport();
}
