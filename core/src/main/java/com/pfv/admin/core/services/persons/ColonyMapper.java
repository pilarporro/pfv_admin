package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.services.common.LocationMapper;
import com.pfv.admin.data.model.animals.Colony;
import com.pfv.admin.data.model.persons.Person;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

@Mapper(componentModel = "spring",
        uses = { LocationMapper.class },
        config = MapperConfiguration.class)
public abstract class ColonyMapper extends com.pfv.admin.common.mappers.Mapper<ColonyDto, Colony> {
    @Autowired private PersonService personService;

    @AfterMapping
    public void addLabel(final Colony entity, @MappingTarget final AliasIdLabelDto dto) {
        String label = entity.getName();
        if (entity.getPerson() != null) {
            label += getFullName(entity);
        }
        dto.setLabel(label);
    }

    private String getFullName(final Colony entity) {
        if (entity.getPerson() != null && StringUtils.hasLength(entity.getPerson().getId())) {
            Person person = personService.findEntityById(entity.getPerson().getId()).orElseThrow(NotFoundException::new);
            return " - " + person.getFullName() + PersonMapper.getArea(person);
        }

        return "";
    }
}
