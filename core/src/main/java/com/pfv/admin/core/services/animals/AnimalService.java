package com.pfv.admin.core.services.animals;

import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.data.model.animals.Animal;

public interface AnimalService extends Service<AnimalDto, Animal> {

}
