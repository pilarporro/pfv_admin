package com.pfv.admin.core.services.emails;

import com.pfv.admin.data.model.emails.EmailToRead;
import com.pfv.admin.data.model.emails.EmailToSend;
import com.pfv.admin.data.repositories.emails.EmailRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

@Slf4j(topic = "emaillogger")
@Service
public class EmailServiceImpl implements EmailService {
    private final EmailRepository emailRepository;
    private List<EmailReader> emailReaders = new LinkedList<>();

    public EmailServiceImpl(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @Override
    public void sendMail(final EmailToSend sendMailData) {
        sendMailData.setRetry(4);
        emailRepository.save(sendMailData);
    }

    @Override
    public void deletePendingMessagesTo(final String to) {
        emailRepository.deleteByToEmail(to);
    }

    @Override
    public boolean sendEffectiveMail(final EmailToSend emailToSend) {
        log.info(String.format("Sending mail %s to %s", emailToSend.getSubject(), emailToSend.getTo().getEmail()));
        /*WebDto web = webService.getActiveWeb();
        try {
            HtmlEmail email = new HtmlEmail();
            email.setCharset("utf-8");
            email.setHostName(web.getSmtpHost());
            email.setSslSmtpPort(web.getSmtpPort());
            email.setAuthenticator(new DefaultAuthenticator(web.getSmtpUsername(), web.getSmtpPassword()));
            email.setSSLOnConnect(false);
            email.setStartTLSEnabled(false);
            email.setFrom(web.getDefaultEmailSenderEmail(), web.getDefaultEmailSenderName());
            email.addTo(emailToSend.getTo().getEmail(), "utf-8");
            //email.addBcc("virtualogysoluciones@gmail.com");
            email.setHtmlMsg(emailToSend.getMessage());
            // set the alternative message
            email.setTextMsg("Your email client does not support HTML messages");
            email.setSubject(emailToSend.getSubject());
            email.send();

            log.info(String.format("Mail %s sent to %s", email.getSubject(), email.getToAddresses().get(0).toString()));
        } catch (Exception e) {
            log.error("Unexpected error sending emails", e);
            return false;
        }*/
        return true;
    }

    /*@Override
    public boolean sendEffectiveMail(final EmailToSend email) {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        log.info(String.format("Mail %s sent to %s", email.getSubject(), email.getTo().getEmail()));
        try {
            InternetAddress addressTo = new InternetAddress(email.getTo().getEmail(), email.getTo().getName());
            InternetAddress addressFrom = new InternetAddress(email.getFrom().getEmail(), email.getFrom().getName());

            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(addressFrom);
            helper.setTo(addressTo);
            helper.setBcc("virtualogysoluciones@gmail.com");
            helper.setSubject(email.getSubject());
            helper.setText(email.getMessage(), true);
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            log.error("Unexpected error sending emails", e);
            return false;
        }

        return true;
    }*/

    @Override
    public void addEmailReader(final EmailReader reader) {
        emailReaders.add(reader);
    }

    @Override
    public void readLastEmails() {
        // WebDto web = webService.getActiveWeb();

        try {
                log.info("readLastEmails ");
                Session session = Session.getDefaultInstance(new Properties());
                Store store = session.getStore("imaps");
                /*
                store.connect("imap." + web.getSmtpUsername().substring( web.getSmtpUsername().indexOf("@") + 1),
                        993,  web.getSmtpUsername(),  web.getSmtpPassword()); //143
*/
                Folder inbox = store.getFolder("inbox");
                inbox.open(Folder.READ_WRITE);

                Flags seen = new Flags(Flags.Flag.SEEN);
                FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
                Message messages[] = inbox.search(unseenFlagTerm);
                for (int idxMessage = 0, totalMessages = messages.length; idxMessage < totalMessages; idxMessage++) {
                    Message message = messages[idxMessage];
                    String content = getTextFromMimeMultipart((MimeMultipart) message.getContent());
                    String from = message.getFrom()[0].toString();
                    String subject = message.getSubject();
                    this.emailReaders.forEach(emailReader -> {
                        boolean processed = emailReader.processEmailRead(EmailToRead.builder()
                                .from(from)
                                .message(content)
                                .subject(subject)
                                .build());
                        try {
                            if (!processed) {
                                message.setFlag(Flags.Flag.SEEN, false);
                            } else {
                                message.setFlag(Flags.Flag.DELETED, true);
                            }
                        } catch (MessagingException exception) {
                            log.error("Error receiving emails", exception);
                        }

                    });

                }

                //close the store and folder objects
                inbox.close(true);
                store.close();

            } catch (Exception exception) {
                log.error("Error receiving emails", exception);
            }
    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        String result = "";
        int partCount = mimeMultipart.getCount();
        for (int i = 0; i < partCount; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                // result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
                result = html;
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }

}
