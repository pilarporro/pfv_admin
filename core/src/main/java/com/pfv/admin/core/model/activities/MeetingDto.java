package com.pfv.admin.core.model.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MeetingDto extends ActivityDto {

    private List<AliasIdLabelDto> persons;
}
