package com.pfv.admin.core.services.settings;

import com.pfv.admin.core.model.settings.SystemSettingDto;
import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.data.model.settings.SystemSetting;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        config = MapperConfiguration.class)
public abstract class SystemSetttingMapper extends com.pfv.admin.common.mappers.Mapper<SystemSettingDto, SystemSetting> {

}
