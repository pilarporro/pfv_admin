package com.pfv.admin.core.model.settings;

import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.data.model.settings.SystemSettingType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SystemSettingDto extends EntityOperationsInfoDto {
    private SystemSettingType property;
    private String value;
}
