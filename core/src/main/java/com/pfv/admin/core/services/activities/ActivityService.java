package com.pfv.admin.core.services.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.model.activities.NewMemberDto;
import com.pfv.admin.data.model.activities.Activity;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ActivityService extends Service<ActivityDto, Activity> {

    Optional<AliasIdLabelDto> findByTypeAndDateAndPlace(ActivityType type, LocalDate date, String placeAlias);

    List<ActivityDto> findByTypeAndDates(ActivityType type, LocalDate from, LocalDate to);

    List<ActivityDto> findByTypeAndInvoiceRef(ActivityType type, String invoiceRef);

    byte[] generateAdoptionContract(AdoptionDto adoptionDto);

    byte[] generateNewMemberContract(NewMemberDto newMemberDto);
}
