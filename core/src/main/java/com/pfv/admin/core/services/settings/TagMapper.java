package com.pfv.admin.core.services.settings;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.core.model.settings.TagDto;
import com.pfv.admin.data.model.settings.Tag;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring",
        config = MapperConfiguration.class)
public abstract class TagMapper extends com.pfv.admin.common.mappers.Mapper<TagDto, Tag> {

}
