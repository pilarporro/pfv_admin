package com.pfv.admin.core.services.accounting;

import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.accounting.AccountNoteDto;
import com.pfv.admin.core.model.settings.LoadResponseDto;
import com.pfv.admin.data.model.accounting.AccountNote;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface AccountNoteService extends Service<AccountNoteDto, AccountNote> {

    LoadResponseDto importAccountNotes(MultipartFile csv) throws IOException;

    List<AccountNoteDto> findAll();

    List<AccountNoteDto> findByDates(LocalDate from, LocalDate to);

    AccountNoteDto findPreviousNote(LocalDate from);
}
