package com.pfv.admin.core.model.persons;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.utils.LocalDateDeserializer;
import com.pfv.admin.common.utils.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrapDto extends WithPhotosDto {
    private String name;
    private String notes;
    private AliasIdLabelDto owner;
    private AliasIdLabelDto person;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate usingFrom;
}
