package com.pfv.admin.core.services.settings;

import com.pfv.admin.common.services.StorageService;
import com.pfv.admin.core.jobs.AccountingMaitananceJob;
import com.pfv.admin.core.jobs.PersonsMaitananceJob;
import com.pfv.admin.core.model.settings.LoadRequestDto;
import com.pfv.admin.core.model.settings.LoadResponseDto;
import com.pfv.admin.core.services.persons.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

@Service
@Slf4j
public class DataLoaderServiceImpl implements DataLoaderService {
    @Autowired
    private UserService userService;
    @Value("${storage.path}")
    private String storagePath;
    @Value("${storage.tmp-path}")
    private String temporaryPath;

    @Autowired
    private StorageService storageService;

    @Autowired
    private PersonsMaitananceJob personsMaitananceJob;

    @Autowired
    private AccountingMaitananceJob accountingMaitananceJob;


    @Override
    public LoadResponseDto load(LoadRequestDto request) {
        return customMethod(request);
    }

    public LoadResponseDto customMethod(LoadRequestDto request) {
        final LoadResponseDto response = LoadResponseDto.builder().errors(new LinkedList<>()).total(0).build();

        if (request.getType().equalsIgnoreCase("job")) {
            if (request.getValue().equalsIgnoreCase("persons")) {
                personsMaitananceJob.execute();
            } else if (request.getValue().equalsIgnoreCase("accounting")) {
                accountingMaitananceJob.execute();
            }
        }
        return response;
    }
}
