package com.pfv.admin.core.services.status;

import com.pfv.admin.core.model.status.ServerStatusDto;

public interface ServerStatusService {
    ServerStatusDto getServerStatus(String userId);
}
