package com.pfv.admin.core.services.reporting;

import lombok.Getter;
import lombok.Setter;

@Getter
public class AccountTotals {
    private Double incomes;
    private Double expenses;
    private Double total;
    private Double incomesGrant;
    private Double expensesGrant;

    @Setter
    private Double balance;
    @Setter
    private Double balanceGrant;

    public void incrementTotal(Double total) {
        if (total != null) {
            this.total += total;
        }
    }

    public void incrementIncomes(Double incomes) {
        if (incomes != null) {
            this.incomes += Math.abs(incomes);
        }
    }

    public void incrementIncomesGrant(Double incomesGrant) {
        if (incomesGrant != null) {
            this.incomesGrant += Math.abs(incomesGrant);
        }
    }

    public void incrementExpenses(Double expenses) {
        if (expenses != null) {
            this.expenses += Math.abs(expenses);
        }
    }

    public void incrementExpensesGrant(Double expensesGrant) {
        if (expensesGrant != null) {
            this.expensesGrant += Math.abs(expensesGrant);
        }
    }

    public void incrementBalance(Double balance) {
        if (balance != null) {
            this.balance += balance;
        }
    }

    public void incrementBalanceGrant(Double balanceGrant) {
        if (balanceGrant != null) {
            this.balanceGrant +=balanceGrant;
        }
    }

    public static AccountTotals init() {
        AccountTotals totals = new AccountTotals();
        totals.incomes = 0d;
        totals.total = 0d;
        totals.expenses = 0d;
        totals.incomesGrant = 0d;
        totals.expensesGrant = 0d;
        totals.balance = 0d;
        totals.balanceGrant = 0d;
        return totals;
    }
}
