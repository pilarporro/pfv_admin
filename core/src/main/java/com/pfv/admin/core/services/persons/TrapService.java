package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.persons.TrapDto;
import com.pfv.admin.data.model.persons.Trap;

public interface TrapService extends Service<TrapDto, Trap> {

}
