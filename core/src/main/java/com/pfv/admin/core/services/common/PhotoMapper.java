package com.pfv.admin.core.services.common;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.PhotoDto;
import com.pfv.admin.common.model.entity.Photo;
import com.pfv.admin.common.services.StorageService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        config = MapperConfiguration.class)
public abstract class PhotoMapper extends com.pfv.admin.common.mappers.Mapper<PhotoDto, Photo> {
    @Autowired
    private StorageService storageService;

    @AfterMapping
    public void addUrl(final Photo entity, @MappingTarget final PhotoDto dto) {
        storageService.buildUrls(dto);
    }
}
