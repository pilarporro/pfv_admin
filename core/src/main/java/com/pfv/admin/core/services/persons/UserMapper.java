package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.core.model.persons.UserDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.persons.User;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class UserMapper extends com.pfv.admin.common.mappers.Mapper<UserDto, User> {
    @InheritConfiguration(name = "dtoToEntity")
    @Mappings({
            @Mapping(target = "password", ignore = true)
    })
    public abstract User merge(UserDto dto, @MappingTarget User entity);


    @Mappings({
            @Mapping(target = "password", ignore = true)
    })
    public abstract UserDto entityToDto(User entity);
}
