package com.pfv.admin.core.services.reporting;

import com.pfv.admin.core.model.activities.NewMemberDto;

public interface MembershipContractGenerator {
    byte[] generateMembershipContract(NewMemberDto newMemberDto);
}
