package com.pfv.admin.core.services.reporting;

import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.dto.EnumTranslationDto;
import com.pfv.admin.common.model.dto.PhotoDto;
import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.model.enums.EnumTranslationMessage;
import com.pfv.admin.common.model.enums.PaymentType;
import com.pfv.admin.common.services.StorageServiceImpl;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.model.activities.CastrationDto;
import com.pfv.admin.core.model.activities.RescueDto;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.model.common.CatFile;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.activities.ActivityService;
import com.pfv.admin.core.services.animals.AnimalService;
import com.pfv.admin.core.services.persons.ColonyService;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.core.services.places.PlaceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.pfv.admin.common.services.StorageServiceImpl.getFilePath;
import static com.pfv.admin.core.services.reporting.ReportUtils.LONG_DATE_FORMATTER;
import static com.pfv.admin.core.services.reporting.ReportUtils.SHORT_DATE_FORMATTER;

@Component
@Slf4j
public class ActivityReportGeneratorImpl implements ActivityReportGenerator {
    private static final int REPORT_FROM = 0;
    private static final int REPORT_TO = 1;
    private static final int TOTAL_CASTRATIONS = 2;
    private static final int TOTAL_ADOPTIONS = 3;
    private static final int TOTAL_RESCUES = 4;
    private static final int TOTAL_MEMBERS = 5;
    private static final int TOTAL_CATS = 6;
    private static final int TOTAL_NEUTERED_CATS = 7;

    @Value("${storage.path}")
    private String storagePath;

    @Value("${storage.tmp-path}")
    private String storagePathTmp;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private PersonService personService;

    @Autowired
    private AnimalService animalService;

    @Autowired
    private ColonyService colonyService;

    @Autowired
    private PlaceService placeService;

    @Autowired
    private ColonyReportGenerator colonyReportGenerator;

    @Override
    public byte[] generateActivityReport(LocalDate requestFrom, LocalDate requestTo) {

        try {
            LocalDate from = requestFrom == null ? LocalDate.of(LocalDate.now().getYear(), 1, 1) : requestFrom;
            LocalDate to = requestTo == null ? LocalDate.of(LocalDate.now().getYear(), 12, 31) : requestTo;

            String args[] = new String[8];
            args[REPORT_FROM] = LONG_DATE_FORMATTER.format(from);
            args[REPORT_TO] = LONG_DATE_FORMATTER.format(to);

            List<ActivityDto> castrations = activityService.findByTypeAndDates(ActivityType.CASTRATION, from, to);
            args[TOTAL_CASTRATIONS] = String.valueOf(castrations.size());

            List<ActivityDto> adoptions = activityService.findByTypeAndDates(ActivityType.ADOPTION, from, to);
            args[TOTAL_ADOPTIONS] = String.valueOf(adoptions.size());

            List<ActivityDto> rescues = activityService.findByTypeAndDates(ActivityType.RESCUE, from, to);
            args[TOTAL_RESCUES] = String.valueOf(rescues.size());

            List<PersonDto> members = personService.findMembers();
            args[TOTAL_MEMBERS] = String.valueOf(members.size());

            /* Count the cats */
            AtomicInteger totalAnimals = new AtomicInteger(0);
            AtomicInteger totalNeuteredAnimals = new AtomicInteger(0);

            members.forEach(member -> {
                if (!CollectionUtils.isEmpty(member.getColonies())) {
                    member.getColonies().forEach(colonyAliasId -> {
                        colonyService.findById(colonyAliasId.getId()).ifPresent(colony -> {
                            if (colony.getTotalAnimals() != null) {
                                totalAnimals.addAndGet(colony.getTotalAnimals());
                            }

                            if (colony.getTotalNeuteredAnimals() != null) {
                                totalNeuteredAnimals.addAndGet(colony.getTotalNeuteredAnimals());
                            }
                        });
                    });
                }
            });

            args[TOTAL_CATS] = String.valueOf(totalAnimals.get());
            args[TOTAL_NEUTERED_CATS] = String.valueOf(totalNeuteredAnimals.get());

            for (int idx = 0; idx < args.length; idx++) {
                if (args[idx] == null) {
                    args[idx] = "_______________";
                }
            }

            Map<String, String> vetClinics = new HashMap<>();
            placeService.getAll().forEach(place -> {
                vetClinics.put(place.getId(), place.getName());
            });
            return generate(vetClinics, castrations, rescues, adoptions, args);
        } catch (Exception exception) {
            throw new ServiceException(exception);
        }
    }

    @Override
    public byte[] generateEmailCastrationsReport(String invoiceRef) {
        try {
            List<ActivityDto> castrations = activityService.findByTypeAndInvoiceRef(ActivityType.CASTRATION, invoiceRef);

            Map<String, String> vetClinics = new HashMap<>();
            placeService.getAll().forEach(place -> {
                vetClinics.put(place.getId(), place.getName());
            });
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("documents/castrations_email.docx").getFile());

            XWPFDocument document = new XWPFDocument(OPCPackage.open(new FileInputStream(file)));

            addCastrationsInfo(document, 0, vetClinics, castrations);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            document.write(out);
            return out.toByteArray();
        } catch (Exception exception) {
            throw new ServiceException(exception);
        }
    }

    @Override
    public List<CatFile> generateCatFiles(String invoiceRef) {
        List<CatFile> files = new LinkedList<>();

        List<ActivityDto> castrations = activityService.findByTypeAndInvoiceRef(ActivityType.CASTRATION, invoiceRef).stream()
                .filter(castration -> StringUtils.hasLength(castration.getChip()))
                .collect(Collectors.toList());

        Map<String, String> vetClinics = new HashMap<>();
        placeService.getAll().forEach(place -> {
            vetClinics.put(place.getId(), place.getName());
        });
        ClassLoader classLoader = getClass().getClassLoader();

        Map<String, Integer> totals = new HashMap<>();

        castrations.forEach(castration -> {
            try {
                File file = new File(classLoader.getResource("documents/cat_details.docx").getFile());
                XWPFDocument document = new XWPFDocument(OPCPackage.open(new FileInputStream(file)));

                addCastrationsInfoEachCat(document, 0, vetClinics, castration);

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                document.write(out);

                String chip = castration.getChip() == null ? "" : castration.getChip();
                int total = (totals.get(chip) == null) ? 0 : totals.get(chip);
                total++;
                totals.put(chip, total);
                String suffix = total > 1 ? "_" + total : "";
                log.info(String.format("%s   %s   %s   %s",chip, totals.get(chip), total, suffix));
                files.add(CatFile.builder()
                        .content(out.toByteArray())
                        .name(String.format("%s%s",castration.getChip(), suffix))
                        .build());

            } catch (Exception exception) {
                throw new ServiceException(exception);
            }
        });


        return files;

    }


    @Override
    public byte[] generateColonyReport() {
        return colonyReportGenerator.generateColonyReport();
    }

    public byte[] generate
            (Map<String, String> vetClinics, List<ActivityDto> castrations, List<ActivityDto> rescues, List<ActivityDto> adoptions, String...
                    args)
            throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("documents/activity_report.docx").getFile());

        XWPFDocument document = new XWPFDocument(OPCPackage.open(new FileInputStream(file)));
        ReportUtils.replacePlaceholders(document, args);
        addCastrationsInfo(document, 1, vetClinics, castrations);
        addAdoptionsInfo(document, 3, adoptions);

        addRescuesInfo(document, rescues);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.write(out);
        return out.toByteArray();
    }

    private void addAdoptionsInfo(final XWPFDocument document, final int idxTable, List<ActivityDto> adoptions) throws
            Exception {
        XWPFTable table = document.getTables().get(idxTable);
        AtomicInteger rowIdx = new AtomicInteger(1);
        adoptions.forEach(adoption -> {
            try {
                XWPFTableRow row = table.insertNewTableRow(rowIdx.incrementAndGet());
                addAdoptionInfo(row, (AdoptionDto) adoption);
            } catch (Exception e) {
                log.error("Unexpected exception adding a castration", e);
            }
        });

    }

    private void addAdoptionInfo(final XWPFTableRow row, AdoptionDto adoption) throws Exception {
        row.createCell().setText(SHORT_DATE_FORMATTER.format(adoption.getDate()));

        AnimalDto animalDto = animalService.findById(adoption.getAnimal().getId()).orElse(CatDto.emptyData());
        String gender = ResourceUtil.getMessage(animalDto.getGender().getMessageKey());

        row.createCell().setText(gender);
        if (StringUtils.hasLength(adoption.getNotes())) {
            row.createCell().setText(adoption.getNotes());
        } else {
            row.createCell().setText("");
        }
        addPhoto(row, adoption.getPhotos(), 75);
    }

    private void addCastrationsInfo(final XWPFDocument document, final int idxTable, Map<
            String, String> vetClinics, List<ActivityDto> castrations) throws Exception {
        Map<PaymentType, Integer> paymentTypes = new HashMap<>();
        XWPFTable table = document.getTables().get(idxTable);
        AtomicInteger rowIdx = new AtomicInteger(1);
        castrations.forEach(castration -> {
            try {
                // Add the payment type total
                PaymentType type = ((CastrationDto) castration).getPaymentType();
                if (paymentTypes.get(type) == null) {
                    paymentTypes.put(type, 0);
                }
                paymentTypes.put(type, paymentTypes.get(type) + 1);

                XWPFTableRow row = table.insertNewTableRow(rowIdx.incrementAndGet());
                addCastrationInfo(idxTable, row, vetClinics, (CastrationDto) castration);
            } catch (Exception e) {
                log.error("Unexpected exception adding a castration", e);
            }
        });
        if (idxTable == 1) {
            XWPFTable tableTotals = document.getTables().get(0);
            Arrays.stream(PaymentType.values()).forEach(paymentType -> {
                Integer totalType = paymentTypes.get(paymentType);
                if (totalType != null) {
                    XWPFTableRow row = tableTotals.insertNewTableRow(2);
                    row.createCell().setText("\t" + ResourceUtil.getMessage(paymentType.getMessageKey()));
                    row.createCell().setText(totalType.toString());
                }
            });
        }
    }

    private void addCastrationsInfoEachCat(final XWPFDocument document, final int idxTable, Map<
            String, String> vetClinics, ActivityDto castration) throws Exception {
        XWPFTable table = document.getTables().get(idxTable);
        try {
            addCastrationInfoOneCat(table, vetClinics, (CastrationDto) castration);
        } catch (Exception e) {
            log.error("Unexpected exception adding a castration", e);
        }
    }

    private void addCastrationInfoOneCat(final XWPFTable table, Map<String, String> vetClinics, CastrationDto
            castration) throws Exception {
        AnimalDto animalDto = animalService.findById(castration.getAnimal().getId()).orElse(CatDto.emptyData());
        String catName = StringUtils.hasLength(animalDto.getName()) ? animalDto.getName() : "";
        String gender = animalDto.getGender() == null ? "" : ResourceUtil.getMessage(animalDto.getGender().getMessageKey());

        table.getRows().get(0).getTableCells().get(0).setText(catName);
        table.getRows().get(0).getTableCells().get(1).setText(gender);


        String chip = StringUtils.hasLength(castration.getChip()) ? castration.getChip() : "";
        table.getRows().get(1).getTableCells().get(1).setText(chip);

        String place = vetClinics.get(castration.getVetClinic().getId());

        String castrationDate = SHORT_DATE_FORMATTER.format(castration.getDate());
        String liberationDate = SHORT_DATE_FORMATTER.format(castration.getDate().plusDays(1));
        table.getRows().get(4).getTableCells().get(1).setText(castrationDate);
        table.getRows().get(4).getTableCells().get(3).setText(castrationDate);
        table.getRows().get(5).getTableCells().get(1).setText(liberationDate);

        String person = "";
        String colony = "";
        if (colonyService != null) { // Avoid test NP
            PersonDto personDto = null;

            if (castration.getColony() != null) {
                ColonyDto colonyDto = colonyService.findById(castration.getColony().getId()).orElse(null);
                if (colonyDto != null) {
                    personDto = personService.findById(colonyDto.getPerson().getId()).orElse(null);
                    colony = String.format("%s, %s", colonyDto.getLocation().getStreet(), colonyDto.getLocation().getAreaName());
                }
            }
            if (personDto == null) {
                personDto = personService.findById(castration.getPerson().getId()).orElse(null);
            }
            if (!StringUtils.hasLength(colony)) {
                colony = personDto.getLocation() == null ? null : String.format("%s, %s", personDto.getLocation().getStreet(), personDto.getLocation().getAreaName());
            }
            if (!StringUtils.hasLength(colony)) {
                colony = "---";
            }
            if (personDto != null) {
                person = personDto.getFullName();
            }
        }
        table.getRows().get(2).getTableCells().get(1).setText(person);
        table.getRows().get(3).getTableCells().get(1).setText(colony);

        XWPFTableCell cellPhotos = table.getRows().get(6).getTableCells().get(0);


        List<String> allowedExtensions = Arrays.asList("jpg", "jpeg");

        if (!CollectionUtils.isEmpty(castration.getPhotos())) {
            List<PhotoDto> images = castration.getPhotos().stream()
                    .filter(photoDto -> photoDto.getDiskName().toLowerCase().endsWith(allowedExtensions.get(0))
                    || photoDto.getDiskName().toLowerCase().endsWith(allowedExtensions.get(1)))
                    .collect(Collectors.toList());
            for (int idx = 0; idx < 1; idx++) {
                addRiacPhoto(cellPhotos, images.get(idx), 450);
            }

            for (int idx = 1; idx < images.size(); idx++) {
                addRiacPhoto(cellPhotos, images.get(idx), 550);
            }

            /*if (images.size() > 0) {
                addFirstPhoto(cellPhotos, images.get(0), 450);
                if (images.size() > 1) {
                    for (int idx = 1; idx < images.size() - 1; idx++) {
                        addFirstPhoto(cellPhotos, images.get(idx), 400);
                    }
                    addRiacPhoto(cellPhotos, images.get(images.size() - 1), 550);
                }
            }*/
        } else {
            cellPhotos.setText("");
        }

    }

    private void addRiacPhoto(final XWPFTableCell cell, PhotoDto photoDto, int scaledWidth) throws Exception {

        if (photoDto != null) {
            String pathImgStorage = getFilePath(storagePath, photoDto.getFolder(), photoDto.getDiskName());

            Path tmpFile = Files.createTempFile(Paths.get(storagePathTmp), "TMP" + LocalDateTime.now().getNano(), ".jpg");

            Files.copy(Paths.get(pathImgStorage), tmpFile, StandardCopyOption.REPLACE_EXISTING);

            // StorageServiceImpl.resize("jpg", tmpFile.toString(), scaledWidth, 1.00f);

            FileInputStream is = new FileInputStream(tmpFile.toString());

            BufferedImage inputImage = ImageIO.read(new File(tmpFile.toString()));
            int scaledHeight = (scaledWidth * inputImage.getHeight()) / inputImage.getWidth();
            cell.addParagraph().createRun()
                    .addPicture(is, XWPFDocument.PICTURE_TYPE_JPEG, photoDto.getDiskName(), Units.toEMU(scaledWidth), Units.toEMU(scaledHeight)); // 200x200 pixels
            is.close();

            Files.delete(tmpFile);
        }

    }

    private void addFirstPhoto(final XWPFTableCell cell, PhotoDto photoDto, int scaledWidth) throws Exception {

        if (photoDto != null) {
            String pathImgStorage = getFilePath(storagePath, photoDto.getFolder(), photoDto.getDiskName());

            Path tmpFile = Files.createTempFile(Paths.get(storagePathTmp), "TMP" + LocalDateTime.now().getNano(), ".jpg");

            Files.copy(Paths.get(pathImgStorage), tmpFile, StandardCopyOption.REPLACE_EXISTING);

            // StorageServiceImpl.resize("jpg", tmpFile.toString(), scaledWidth, 0.95f);

            FileInputStream is = new FileInputStream(tmpFile.toString());

            BufferedImage inputImage = ImageIO.read(new File(tmpFile.toString()));
            int scaledHeight = (scaledWidth * inputImage.getHeight()) / inputImage.getWidth();
            cell.addParagraph().createRun()
                    .addPicture(is, XWPFDocument.PICTURE_TYPE_JPEG, photoDto.getDiskName(), Units.toEMU(scaledWidth), Units.toEMU(scaledHeight)); // 200x200 pixels
            is.close();

            Files.delete(tmpFile);
        }

    }

    private void addCastrationInfo(final int idxTable, final XWPFTableRow row, Map<
            String, String> vetClinics, CastrationDto castration) throws Exception {

        row.createCell().setText(SHORT_DATE_FORMATTER.format(castration.getDate()));

        AnimalDto animalDto = animalService.findById(castration.getAnimal().getId()).orElse(CatDto.emptyData());
        String catName = animalDto.getName() == null ? "" : animalDto.getName() + "- ";

        String gender = ResourceUtil.getMessage(animalDto.getGender().getMessageKey());
        String place = vetClinics.get(castration.getVetClinic().getId());

        row.createCell().setText(gender);
        if (idxTable == 1) {
            row.createCell().setText(place + " - " + ResourceUtil.getMessage(castration.getPaymentType().getMessageKey()));
        }
        String person = "";
        String colony = "";
        if (colonyService != null) { // Avoid test NP
            PersonDto personDto = null;

            if (castration.getColony() != null) {
                ColonyDto colonyDto = colonyService.findById(castration.getColony().getId()).orElse(null);
                if (colonyDto != null) {
                    personDto = personService.findById(colonyDto.getPerson().getId()).orElse(null);
                    colony = colonyDto.getLocation().getAreaName();
                }
            }
            if (personDto == null) {
                personDto = personService.findById(castration.getPerson().getId()).orElse(null);
            }
            if (!StringUtils.hasLength(colony)) {
                colony = personDto.getLocation() == null ? null : personDto.getLocation().getAreaName();
            }
            if (!StringUtils.hasLength(colony)) {
                colony = "---";
            }
            if (personDto != null) {
                person = personDto.getFullName();
            }
        }
        if (idxTable == 1) {
            row.createCell().setText(String.format("%s, %s", person, colony.toUpperCase()));
        } else {
            row.createCell().setText(String.format("%s", person));
            row.createCell().setText(String.format("%s", colony.toUpperCase()));
        }
        addPhoto(row, castration.getPhotos());
    }

    private void addRescuesInfo(final XWPFDocument document, List<ActivityDto> rescues) throws Exception {
        XWPFTable table = document.getTables().get(2);
        AtomicInteger rowIdx = new AtomicInteger(1);
        rescues.forEach(rescue -> {
            try {
                XWPFTableRow row = table.insertNewTableRow(rowIdx.incrementAndGet());
                addRescueInfo(row, (RescueDto) rescue);
            } catch (Exception e) {
                log.error("Unexpected exception adding a rescue", e);
            }
        });
    }

    private void addRescueInfo(final XWPFTableRow row, RescueDto rescue) throws Exception {

        row.createCell().setText(SHORT_DATE_FORMATTER.format(rescue.getDate()));
        row.createCell().setText(rescue.getNotes());

        addPhoto(row, rescue.getPhotos());
    }

    private void addPhoto(final XWPFTableRow row, List<PhotoDto> photos) throws Exception {
        addPhoto(row, photos, 150);
    }

    private void addPhoto(final XWPFTableRow row, List<PhotoDto> photos, int scaledWidth) throws Exception {
        if (!CollectionUtils.isEmpty(photos)) {

            PhotoDto photoDto = photos.get(0);
            int idxPhoto = 0;
            while ((idxPhoto < photos.size()) && (photoDto == null)) {
                if (photos.get(idxPhoto).getDiskName().toLowerCase().endsWith("jpg")
                        || photos.get(idxPhoto).getDiskName().toLowerCase().endsWith("jpeg")) {
                    photoDto = photos.get(idxPhoto);
                }
                idxPhoto++;
            }
            if (photoDto != null) {
                String pathImgStorage = getFilePath(storagePath, photoDto.getFolder(), photoDto.getDiskName());

                Path tmpFile = Files.createTempFile(Paths.get(storagePathTmp), "TMP" + LocalDateTime.now().getNano(), ".jpg");

                Files.copy(Paths.get(pathImgStorage), tmpFile, StandardCopyOption.REPLACE_EXISTING);

                StorageServiceImpl.resize("jpg", tmpFile.toString(), scaledWidth, 0.95f);

                FileInputStream is = new FileInputStream(tmpFile.toString());

                BufferedImage inputImage = ImageIO.read(new File(tmpFile.toString()));
                int scaledHeight = (scaledWidth * inputImage.getHeight()) / inputImage.getWidth();
                row.createCell().addParagraph().createRun()
                        .addPicture(is, XWPFDocument.PICTURE_TYPE_JPEG, photoDto.getDiskName(), Units.toEMU(scaledWidth), Units.toEMU(scaledHeight)); // 200x200 pixels
                is.close();

                Files.delete(tmpFile);
            }
        } else {
            row.createCell().setText("");
        }
    }

    private EnumTranslationDto createDTO(final EnumTranslationMessage enumType) {
        return EnumTranslationDto.builder()
                .key(enumType.name())
                .label(ResourceUtil.getMessage(enumType.getMessageKey(), null, LocaleContextHolder.getLocale()))
                .build();
    }

    private void copyTable(XWPFTable source, XWPFTable target) {
        target.getCTTbl().setTblPr(source.getCTTbl().getTblPr());
        target.getCTTbl().setTblGrid(source.getCTTbl().getTblGrid());
        for (int r = 0; r < source.getRows().size(); r++) {
            XWPFTableRow targetRow = target.createRow();
            XWPFTableRow row = source.getRows().get(r);
            targetRow.getCtRow().setTrPr(row.getCtRow().getTrPr());
            for (int c = 0; c < row.getTableCells().size(); c++) {
                //newly created row has 1 cell
                XWPFTableCell targetCell = c == 0 ? targetRow.getTableCells().get(0) : targetRow.createCell();
                XWPFTableCell cell = row.getTableCells().get(c);
                targetCell.getCTTc().setTcPr(cell.getCTTc().getTcPr());
                XmlCursor cursor = targetCell.getParagraphArray(0).getCTP().newCursor();
                for (int p = 0; p < cell.getBodyElements().size(); p++) {
                    IBodyElement elem = cell.getBodyElements().get(p);
                    if (elem instanceof XWPFParagraph) {
                        XWPFParagraph targetPar = targetCell.insertNewParagraph(cursor);
                        cursor.toNextToken();
                        XWPFParagraph par = (XWPFParagraph) elem;
                        copyParagraph(par, targetPar);
                    } else if (elem instanceof XWPFTable) {
                        XWPFTable targetTable = targetCell.insertNewTbl(cursor);
                        XWPFTable table = (XWPFTable) elem;
                        copyTable(table, targetTable);
                        cursor.toNextToken();
                    }
                }
                //newly created cell has one default paragraph we need to remove
                targetCell.removeParagraph(targetCell.getParagraphs().size() - 1);
            }
        }
        //newly created table has one row by default. we need to remove the default row.
        target.removeRow(0);
    }

    private void copyParagraph(XWPFParagraph source, XWPFParagraph target) {
        target.getCTP().setPPr(source.getCTP().getPPr());
        for (int i = 0; i < source.getRuns().size(); i++) {
            XWPFRun run = source.getRuns().get(i);
            XWPFRun targetRun = target.createRun();
            //copy formatting
            targetRun.getCTR().setRPr(run.getCTR().getRPr());
            //no images just copy text
            targetRun.setText(run.getText(0));
        }
    }
}
