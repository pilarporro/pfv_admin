package com.pfv.admin.core.model.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdoptionDto extends ActivityDto {

    private AliasIdLabelDto adopter;
    private AliasIdLabelDto animal;

}
