package com.pfv.admin.core.model.animals;

import com.pfv.admin.common.model.enums.Gender;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CatDto extends AnimalDto {

    public static CatDto emptyData() {
        CatDto cat = new CatDto();
        cat.setGender(Gender.UNKNOWN);
        cat.setName("");
        return cat;
    }
}
