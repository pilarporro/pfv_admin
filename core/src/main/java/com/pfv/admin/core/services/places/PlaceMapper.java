package com.pfv.admin.core.services.places;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.places.PlaceDto;
import com.pfv.admin.core.model.places.ShopDto;
import com.pfv.admin.core.model.places.VetClinicDto;
import com.pfv.admin.core.services.common.LocationMapper;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.places.Place;
import com.pfv.admin.data.model.places.Shop;
import com.pfv.admin.data.model.places.VetClinic;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, LocationMapper.class },
        config = MapperConfiguration.class)
public abstract class PlaceMapper extends com.pfv.admin.common.mappers.Mapper<PlaceDto, Place> {
    @Autowired private ShopMapper shopMapper;
    @Autowired private VetClinicMapper vetClinicMapper;

    public Place dtoToEntity(final PlaceDto dto) {
        if (dto instanceof ShopDto) {
            return shopMapper.dtoToEntity((ShopDto) dto);
        } else if (dto instanceof VetClinicDto) {
            return vetClinicMapper.dtoToEntity((VetClinicDto) dto);
        } else {
            return null;
        }
    }

    @InheritConfiguration(name = "dtoToEntity")
    public Place merge(final PlaceDto dto, @MappingTarget final Place entity) {
        if (dto instanceof ShopDto) {
            return shopMapper.merge((ShopDto) dto, (Shop) entity);
        } else if (dto instanceof VetClinicDto) {
            return vetClinicMapper.merge((VetClinicDto) dto, (VetClinic) entity);
        } else {
            return null;
        }
    }

    @InheritInverseConfiguration(name = "dtoToEntity")
    public PlaceDto entityToDto(final Place entity) {
        if (entity instanceof Shop) {
            return shopMapper.entityToDto((Shop) entity);
        } else if (entity instanceof VetClinic) {
            return vetClinicMapper.entityToDto((VetClinic) entity);
        } else {
            return null;
        }
    }

    @Mappings({
            @Mapping(target = "label", source = "name")
    })
    @Override
    public abstract AliasIdLabelDto toResultDto(Place entity);

}
