package com.pfv.admin.core.services.settings;

import com.pfv.admin.core.model.settings.PropertyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PropertyServiceImpl implements PropertyService {
    @Autowired
    private Environment environment;

    @Override
    public Optional<PropertyDto> getProperty(final String property) {
        String value = environment.getProperty(property);
        if (value == null) {
            return Optional.empty();
        } else {
            return Optional.of(PropertyDto.builder().property(property).value(value).build());
        }
    }
}
