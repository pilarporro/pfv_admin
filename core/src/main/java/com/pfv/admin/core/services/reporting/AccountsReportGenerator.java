package com.pfv.admin.core.services.reporting;

import java.time.LocalDate;

public interface AccountsReportGenerator {
    byte[] generateAccountsReport(LocalDate from, LocalDate to);


}
