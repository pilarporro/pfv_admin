/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.core.model.emails;

import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalLinkDataDto extends EntityOperationsInfoDto {
    private String key;
    private String data;
    private String[] aData;
}
