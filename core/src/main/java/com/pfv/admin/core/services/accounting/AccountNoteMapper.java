package com.pfv.admin.core.services.accounting;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.core.model.accounting.AccountNoteDto;
import com.pfv.admin.core.services.activities.ActivityService;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.accounting.AccountNote;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class AccountNoteMapper extends com.pfv.admin.common.mappers.Mapper<AccountNoteDto, AccountNote> {
    @Autowired
    private ActivityService activityService;


    @AfterMapping
    public void addWithPhotos(final AccountNote entity, @MappingTarget final AccountNoteDto dto) {
        /* We should add this to every mapper or find another generic way */
        dto.setWithPhotos(!CollectionUtils.isEmpty(dto.getPhotos()) ||
                        (withPhotosInActivities(entity.getActivities()) )
                );

        dto.setManualNote(entity.getBankBalance() == null);
    }


    private boolean withPhotosInActivities(List<AliasId> activities) {
        return activities.stream()
                .anyMatch(aliasId -> {
                    AtomicBoolean withPhotos = new AtomicBoolean(false);
                    activityService.findById(aliasId.getId()).ifPresent(activityDto -> {
                        if (!CollectionUtils.isEmpty(activityDto.getPhotos())) {
                            withPhotos.set(true);
                        }
                    });
                    return withPhotos.get();
                });
    }
}
