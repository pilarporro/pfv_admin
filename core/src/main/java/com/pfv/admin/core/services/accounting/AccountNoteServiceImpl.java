package com.pfv.admin.core.services.accounting;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.AccountSubtype;
import com.pfv.admin.common.model.enums.AccountType;
import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.model.enums.PaymentPeriodicity;
import com.pfv.admin.common.services.ServiceWithPhotosImpl;
import com.pfv.admin.common.utils.DateUtil;
import com.pfv.admin.core.jobs.AccountingMaitananceJob;
import com.pfv.admin.core.jobs.PersonsMaitananceJob;
import com.pfv.admin.core.model.accounting.AccountNoteDto;
import com.pfv.admin.core.model.common.LocationDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.model.settings.LoadErrorItemResponseDto;
import com.pfv.admin.core.model.settings.LoadResponseDto;
import com.pfv.admin.core.services.activities.ActivityServiceImpl;
import com.pfv.admin.core.services.municipalities.MunicipalityServiceImpl;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.data.model.accounting.AccountNote;
import com.pfv.admin.data.repositories.accounting.AccountNoteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class AccountNoteServiceImpl extends ServiceWithPhotosImpl<AccountNoteDto, AccountNote, AccountNoteRepository> implements AccountNoteService {
    @Autowired
    private PersonService personService;

    @Autowired
    private MunicipalityServiceImpl municipalityService;

    @Autowired
    private ActivityServiceImpl activityService;

    @Autowired
    private AccountingMaitananceJob accountingMaitananceJob;

    @Autowired
    private PersonsMaitananceJob personsMaitananceJob;

    @Override
    public LoadResponseDto importAccountNotes(MultipartFile csv) throws IOException {
        log.info("Loading file");
        InputStream inputStream = csv.getInputStream();
        AtomicInteger idxLine = new AtomicInteger(0);
        final LoadResponseDto response = LoadResponseDto.builder().total(0).errors(new LinkedList<>()).build();
        List<PersonDto> allPersons = personService.getAll().stream()
                .filter(person -> Objects.nonNull(person.getBankMatcher()) && person.isActive())
                .map(person -> {
                    person.setBankMatcher(person.getBankMatcher().toLowerCase());
                    return person;
                })
                .collect(Collectors.toList());
        List<String> lines = new LinkedList<>();

        new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .forEach(line -> lines.add(line));
        Collections.reverse(lines);
        idxLine.set(lines.size());
        lines.stream()
                .peek(log::info)
                .forEach(line -> {
                    try {
                        if ((idxLine.get() == 95) || (idxLine.get() == 156))  {
                            int i = 9;
                        }
                        String aData[] = line.trim().split(";");
                        boolean created = false;
                        if (aData.length == 4) {
                            created = createPerson(line);
                        } else if (aData.length > 2) {
                            created = createAccountNote(allPersons, line);
                        }
                        if (created) {
                            response.setTotal(response.getTotal() + 1);
                        }
                    } catch (Exception exception) {
                        response.getErrors().add(LoadErrorItemResponseDto.builder()
                                .numLine(idxLine.get())
                                .message(exception.getMessage())
                                .build());
                    }
                    idxLine.decrementAndGet();
                });

        accountingMaitananceJob.execute();
        personsMaitananceJob.execute();

        return response;
    }

    @Transactional
    private boolean createPerson(String line) {
        String aData[] = line.split(";");


        PersonDto personDto = PersonDto.builder()
                .name(aData[0])
                .isMember(true)
                .lastName(aData[1])
                .location(LocationDto.builder()
                        .municipalityId(municipalityService.getDefaultMunicipality().getId())
                        .build())
                .memberFrom(LocalDate.of(2021, 1, 1))
                .bankMatcher(aData[3])
                .build();
        personDto.setActive(true);
        personDto.setMarkToDelete(false);
        personDto.setPhotos(new LinkedList<>());
        personDto.setNotification(personDto.getBankMatcher().equalsIgnoreCase("XXX"));
        personDto.setAlias(aData[2]);
        personDto.setNextPayment(LocalDate.now());
        personDto.setPaymentPeriodicity(PaymentPeriodicity.MONTLHY);
        PersonDto created = personService.create(personDto);

        return created.getId() != null;
    }

    @Transactional
    private boolean createAccountNote(List<PersonDto> persons, String line) {
        //05/02/2021;05/02/2021;TRANSFERENCIA DE MARIA BLANCA SALGUERO CARMENA, CONCEPTO Socio LAURA S.NCHEZ SALGUERO ;6;2.628,59
        String aData[] = line.split(";");
        String concept = aData[2];
        LocalDate date = DateUtil.parseLocalDate(aData[0]);
        BigDecimal total = fromString(aData[3]);

        if (repository.countByDateAndNotesAndTotal(date, concept, total) > 0) {
            return false;
        }


        Optional<PersonDto> opPerson = persons.stream()
                .filter(current -> concept.toLowerCase().contains(current.getBankMatcher()))
                .findFirst();

        AccountNoteDto newAccountDto = AccountNoteDto.builder()
                .date(date)
                .type(AccountType.BANK)
                .total(total)
                .activities(new LinkedList<>())
                .notes(concept)
                .bankBalance(fromString(aData[4]))
                .build();

        Boolean notification = true;
        PersonDto person = null;
        AliasIdLabelDto aliasIdLabelPerson = null;

        if (opPerson.isPresent()) {
            person = opPerson.get();
            aliasIdLabelPerson = AliasIdLabelDto.ofIdAlias(person.getId(), person.getAlias());
        }

        if (concept.toLowerCase().contains("lotería") || concept.toLowerCase().contains("loteria")
                || concept.toLowerCase().contains("decimo") || concept.toLowerCase().contains("décimo")) {
            if (person != null) {
                newAccountDto.setPerson(aliasIdLabelPerson);
            }
            newAccountDto.setSubtype(AccountSubtype.LOTTERY);
            notification = false;
        } else if ((person != null) && (person.getIsMember())) {
            newAccountDto.setSubtype(AccountSubtype.MEMBER_SUBSCRIPTION);
            newAccountDto.setPerson(aliasIdLabelPerson);
            boolean monthlySubscription =
                    (person.getPaymentPeriodicity() == PaymentPeriodicity.MONTLHY)
                            && validSubscription(total, person);
            if (monthlySubscription) {
                LocalDate nextPayment = date.plusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
                person.setNextPayment(nextPayment);
                personService.update(person);
            }

            notification = !monthlySubscription;
        } else if (concept.toLowerCase().contains("teaming")) {
            newAccountDto.setSubtype(AccountSubtype.DONATION);
            notification = false;
        } else if (concept.startsWith("LIQUIDACION DEL CONTRATO")) {
            newAccountDto.setSubtype(AccountSubtype.BANK_COMISSION);
            notification = false;
        } else if (concept.startsWith("DISPOSICION EN CAJERO")) {
            newAccountDto.setSubtype(AccountSubtype.VETCLINIC_INVOICE);
            notification = false;
        } else if (concept.contains("PEREZ MARIN MARIA LUZ, CONCEPTO")) {
            newAccountDto.setSubtype(AccountSubtype.DONATION);
            newAccountDto.setPerson(personService.findByAlias("marialuz"));
            notification = false;
        } else if (concept.contains("ALEJANDRA MARIA SOLER")) {
            newAccountDto.setSubtype(AccountSubtype.DONATION);
            newAccountDto.setPerson(personService.findByAlias("alejandradonante"));
            notification = false;
        } else if (concept.startsWith("COMPRA EN")) {
            newAccountDto.setSubtype(AccountSubtype.EXPENSE);
            newAccountDto.setPerson(personService.findByAlias("olga"));
            if (concept.startsWith("COMPRA EN MERCADONA VALDEMORILLO")) {
                Optional<AliasIdLabelDto> opActivity = activityService.findByTypeAndDateAndPlace(ActivityType.BUY_SUPPLIES, date, "mercadona");
                if (opActivity.isPresent()) {
                    newAccountDto.getActivities().add(opActivity.get());
                }
            } else if (concept.startsWith("COMPRA EN KIWOKO MADRID.LA")) {
                Optional<AliasIdLabelDto> opActivity = activityService.findByTypeAndDateAndPlace(ActivityType.BUY_SUPPLIES, date, "kiwoko");
                if (opActivity.isPresent()) {
                    newAccountDto.getActivities().add(opActivity.get());
                }
            }
        } else if (concept.startsWith("TRANSFERENCIA A FAVOR DE PORRO SANCHEZ MARIA DEL PILAR")) {
            newAccountDto.setPerson(personService.findByAlias("pilar"));
            newAccountDto.setSubtype(AccountSubtype.EXPENSE);
        }  else if ((concept.toLowerCase().contains("donación") || concept.toLowerCase().contains("donacion"))) {
            newAccountDto.setSubtype(AccountSubtype.DONATION);
        } else {
            newAccountDto.setSubtype(AccountSubtype.OTHER);
        }

        newAccountDto.setNotification(notification);
        newAccountDto.setActive(true);
        newAccountDto.setMarkToDelete(false);
        newAccountDto.setPhotos(new LinkedList<>());

        AccountNoteDto created = this.create(newAccountDto);

        return created.getId() != null;
    }

    private boolean validSubscription(BigDecimal total, PersonDto person) {
        return (total.compareTo(BigDecimal.valueOf(6L)) == 0)
                || (total.compareTo(BigDecimal.valueOf(10L)) == 0)
                || (person.getAlias().equals("mariapita") && total.compareTo(BigDecimal.valueOf(12L)) == 0)
                || (person.getAlias().equals("isabel") && total.compareTo(BigDecimal.valueOf(12L)) == 0)
                || (person.getAlias().equals("miguelangel") && total.compareTo(BigDecimal.valueOf(12L)) == 0)
                || (person.getAlias().equals("rafaela") && total.compareTo(BigDecimal.valueOf(10L)) == 0);
    }

    private BigDecimal fromString(String str) {
        str = str.replaceAll("\\.", "").replaceAll(",", ".");
        return new BigDecimal(str);
    }

    @Override
    public List<AccountNoteDto> findAll() {
        return repository.findAllByActiveIsTrueOrderByDate().stream()
                .map(accountNote -> mapper.entityToDto(accountNote))
                .collect(Collectors.toList());
    }

    @Override
    public List<AccountNoteDto> findByDates(LocalDate from, LocalDate to) {
        return repository.findByActiveIsTrueAndDateBetweenOrderByDate(from, to).stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public AccountNoteDto findPreviousNote(LocalDate from) {
        return mapper.entityToDto(repository.findFirstByActiveIsTrueAndDateIsBeforeOrderByDateDesc(from));
    }
}
