package com.pfv.admin.core.services.animals;

import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.data.model.animals.Animal;
import com.pfv.admin.data.model.animals.Cat;
import org.mapstruct.AfterMapping;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class AnimalMapper extends com.pfv.admin.common.mappers.Mapper<AnimalDto, Animal> {
    @Autowired private CatMapper catMapper;
    @Autowired private PersonService personService;

    public Animal dtoToEntity(final AnimalDto dto) {
        if (dto instanceof CatDto) {
            return catMapper.dtoToEntity((CatDto) dto);
        } else {
            return null;
        }
    }

    @InheritConfiguration(name = "dtoToEntity")
    public Animal merge(final AnimalDto dto, @MappingTarget final Animal entity) {
        if (dto instanceof CatDto) {
            return catMapper.merge((CatDto) dto, (Cat) entity);
        } else {
            return null;
        }
    }

    @InheritInverseConfiguration(name = "dtoToEntity")
    public AnimalDto entityToDto(final Animal entity) {
        if (entity instanceof Cat) {
            return catMapper.entityToDto((Cat) entity);
        } else {
            return null;
        }
    }

    @Override
    public abstract AliasIdLabelDto toResultDto(Animal entity);

    @AfterMapping
    public void addLabel(final Animal entity, @MappingTarget final AliasIdLabelDto dto) {
        String label = entity.getName();
        if (entity.getOwner() != null) {
            label += getFullName(entity);
        }
        dto.setLabel(label);
    }

    private String getFullName(final Animal entity) {
        if (entity.getAdopter() != null && StringUtils.hasLength(entity.getAdopter().getId())) {
            return " - " + personService.findEntityById(entity.getAdopter().getId()).orElseThrow(NotFoundException::new).getFullName();
        } else if (entity.getKeeper() != null && StringUtils.hasLength(entity.getKeeper().getId())) {
            return " - " + personService.findEntityById(entity.getKeeper().getId()).orElseThrow(NotFoundException::new).getFullName();
        } else if (entity.getOwner() != null && StringUtils.hasLength(entity.getOwner().getId())) {
            return " - " + personService.findEntityById(entity.getOwner().getId()).orElseThrow(NotFoundException::new).getFullName();
        }

        return "";
    }
}
