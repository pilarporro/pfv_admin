package com.pfv.admin.core.jobs;


import com.google.common.util.concurrent.AtomicDouble;
import com.pfv.admin.core.services.accounting.AccountNoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Slf4j
public class AccountingMaitananceJob {
    @Autowired
    private AccountNoteService accountNoteService;

    @Scheduled(cron = "${scheduler.periodicity.accountingMaintanance}")
    public void execute() {
        log.info("Starting AccountingMaitananceJob");

        final AtomicDouble cashInit = new AtomicDouble(0);
        final AtomicDouble bankInit = new AtomicDouble(100);
        final AtomicDouble grantInit = new AtomicDouble(100);


        accountNoteService.findAll().stream()
                .forEach(accountNoteDto -> {
                    switch (accountNoteDto.getType()) {
                        case BANK:
                            if (accountNoteDto.getBankBalance() == null) {
                                log.error(String.format("Invalid account note %s", accountNoteDto));
                            } else {
                                bankInit.set(accountNoteDto.getBankBalance().doubleValue());
                            }
                            break;

                        case CASH:
                            accountNoteDto.setBankBalance(BigDecimal.valueOf(bankInit.get()));
                            cashInit.set(cashInit.get() + accountNoteDto.getTotal().doubleValue());
                            break;

                        case PUBLIC_GRANT:
                            accountNoteDto.setBankBalance(BigDecimal.valueOf(bankInit.get()));
                            grantInit.set(grantInit.get() + accountNoteDto.getTotal().doubleValue());
                            break;
                    }

                    accountNoteDto.setCashBalance(BigDecimal.valueOf(cashInit.get()));
                    accountNoteDto.setGrantBalance(BigDecimal.valueOf(grantInit.get()));
                    accountNoteService.update(accountNoteDto);
                });

    }
}