package com.pfv.admin.core.services.emails;

import com.pfv.admin.core.model.emails.ExternalLinkDataDto;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.data.model.emails.ExternalLinkData;

public interface ExternalLinkDataService extends Service<ExternalLinkDataDto, ExternalLinkData> {
    public String[] findDataByKey(String key);

    void deleteExpired();

    ExternalLinkDataDto createWithData(String data);
}
