package com.pfv.admin.core.services.activities;

import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.model.enums.AnimalStatus;
import com.pfv.admin.common.model.enums.PaymentPeriodicity;
import com.pfv.admin.common.services.ServiceWithPhotosImpl;
import com.pfv.admin.common.utils.DateUtil;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.model.activities.BuySuppliesDto;
import com.pfv.admin.core.model.activities.CastrationDto;
import com.pfv.admin.core.model.activities.MeetingDto;
import com.pfv.admin.core.model.activities.NewMemberDto;
import com.pfv.admin.core.model.activities.OtherActivityDto;
import com.pfv.admin.core.model.activities.PickUpKeeperDto;
import com.pfv.admin.core.model.activities.RescueDto;
import com.pfv.admin.core.model.activities.VisitClinicDto;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.animals.AnimalService;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.core.services.reporting.AdoptionContractGenerator;
import com.pfv.admin.core.services.reporting.MembershipContractGenerator;
import com.pfv.admin.data.model.activities.Activity;
import com.pfv.admin.data.model.activities.Adoption;
import com.pfv.admin.data.model.activities.BuySupplies;
import com.pfv.admin.data.model.activities.Castration;
import com.pfv.admin.data.model.activities.Meeting;
import com.pfv.admin.data.model.activities.NewMember;
import com.pfv.admin.data.model.activities.PickUpKeeper;
import com.pfv.admin.data.model.activities.Rescue;
import com.pfv.admin.data.model.activities.VisitClinic;
import com.pfv.admin.data.repositories.activities.ActivityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class ActivityServiceImpl extends ServiceWithPhotosImpl<ActivityDto, Activity, ActivityRepository> implements ActivityService {
    @Autowired
    private AnimalService animalService;

    @Autowired
    private PersonService personService;

    @Autowired
    private MembershipContractGenerator membershipContractGenerator;

    @Autowired
    private AdoptionContractGenerator adoptionContractGenerator;

    @Override
    public void preUpdate(ActivityDto dto) {
        switch (dto.getType()) {
            case ADOPTION:
                adoptionPreCreate((AdoptionDto) dto);
                break;
            case RESCUE:
                rescuePreCreate((RescueDto) dto);
                break;
            case VISIT_CLINIC:
                visitClinicPreCreate((VisitClinicDto) dto);
                break;
            case PICKUP_KEEPER:
                pickUpKeeperPreCreate((PickUpKeeperDto) dto);
                break;
            case CASTRATION:
                castrationPreCreate((CastrationDto) dto);
                break;
            case OTHER:
                otherPreCreate((OtherActivityDto) dto);
                break;
            case MEETING:
                meetingPreCreate((MeetingDto) dto);
                break;
            case BUY_SUPPLIES:
                buySuppliesPreCreate((BuySuppliesDto) dto);
                break;
            case NEW_MEMBER:
                newMemberPreCreate((NewMemberDto) dto);
                break;
        }
    }

    @Override
    public void preCreate(ActivityDto dto) {
        super.preCreate(dto);
        preUpdate(dto);
    }

    public void rescuePreCreate(RescueDto dto) {

    }

    public void visitClinicPreCreate(VisitClinicDto dto) {

    }

    public void pickUpKeeperPreCreate(PickUpKeeperDto dto) {
        dto.getAnimals().forEach(animal -> {
            AnimalDto animalDto = animalService.findById(animal.getId()).orElseThrow(NotFoundException::new);
            animalDto.setKeeper(dto.getKeeper());
            animalDto.setAdoptionDate(dto.getDate());
            animalDto.setAnimalStatus(AnimalStatus.WITH_KEEPER);
            animalService.update(animalDto);
        });
    }

    public void otherPreCreate(OtherActivityDto dto) {

    }

    public void castrationPreCreate(CastrationDto dto) {
        AnimalDto animalDto = animalService.findById(dto.getAnimal().getId()).orElseThrow(NotFoundException::new);
        animalDto.setNeutered(true);
        animalService.update(animalDto);

    }

    public void adoptionPreCreate(AdoptionDto dto) {
        AnimalDto animalDto = animalService.findById(dto.getAnimal().getId()).orElseThrow(NotFoundException::new);
        animalDto.setAdopter(dto.getAdopter());
        animalDto.setAdoptionDate(dto.getDate());
        animalDto.setAnimalStatus(AnimalStatus.ADOPTED);
        animalService.update(animalDto);
    }

    public void meetingPreCreate(MeetingDto dto) {

    }

    public void newMemberPreCreate(NewMemberDto dto) {
        PersonDto personDto = personService.findById(dto.getNewMember().getId()).orElseThrow(NotFoundException::new);
        personDto.setMemberFrom(dto.getDate());
        personDto.setIsMember(true);
        personDto.setPaymentPeriodicity(PaymentPeriodicity.MONTLHY);
        personService.update(personDto);
    }

    public void buySuppliesPreCreate(BuySuppliesDto dto) {

    }

    protected String createAlias(Activity entity) {
        String type = ResourceUtil.getMessage("types.ActivityType." + entity.getType().name()) + " " + DateUtil.formatDate(entity.getDate()) + ": ";
        switch (entity.getType()) {
            case ADOPTION:
                Adoption adoption = (Adoption) entity;
                type += "(" + adoption.getPerson().getAlias() + ") " + adoption.getAnimal().getAlias();
                break;
            case RESCUE:
                Rescue rescue = (Rescue) entity;
                type += "(" + rescue.getPerson().getAlias() + ") " + getListOfAlias(rescue.getAnimals());

                break;
            case VISIT_CLINIC:
                VisitClinic visitClinic = (VisitClinic) entity;
                type += "(" + visitClinic.getPerson().getAlias() + ") " + getListOfAlias(visitClinic.getAnimals());

                break;
            case PICKUP_KEEPER:
                PickUpKeeper pickUpKeeper = (PickUpKeeper) entity;
                type += "(" + pickUpKeeper.getPerson().getAlias() + ") " + getListOfAlias(pickUpKeeper.getAnimals());

                break;
            case CASTRATION:
                Castration castration = (Castration) entity;
                type += "(" + castration.getPerson().getAlias() + ") " + castration.getAnimal().getAlias();

                break;
            case OTHER:
                type += entity.getPerson().getAlias();

                break;
            case MEETING:
                Meeting meeting = (Meeting) entity;
                type += getListOfAlias(meeting.getPersons());

                break;
            case BUY_SUPPLIES:
                BuySupplies buySupplies = (BuySupplies) entity;
                type += "(" + buySupplies.getPerson().getAlias() + ") " + buySupplies.getPlace().getAlias();

                break;
            case NEW_MEMBER:
                NewMember newMember = (NewMember) entity;
                type += newMember.getNewMember().getAlias();

                break;
        }

        return type;
    }

    private String getListOfAlias(List<AliasId> list) {
        if (CollectionUtils.isEmpty(list)) {
            return "";
        }
        return String.join("_", list.stream().map(item -> item.getAlias()).collect(Collectors.toList()));
    }

    @Override
    public Optional<AliasIdLabelDto> findByTypeAndDateAndPlace(ActivityType type, LocalDate date, String placeAlias) {
        return Optional.ofNullable(mapper.toResultDto(repository.findFirstByActiveIsTrueAndTypeAndDateAndPlaceAlias(type, date, placeAlias)));
    }

    @Override public List<ActivityDto> findByTypeAndDates(ActivityType type, LocalDate from, LocalDate to) {
        return repository.findByActiveIsTrueAndTypeAndDateBetweenOrderByDate(type, from, to).stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override public List<ActivityDto> findByTypeAndInvoiceRef(ActivityType type, String invoiceRef) {
        return repository.findByActiveIsTrueAndTypeAndInvoiceRefIgnoreCase(type, invoiceRef).stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public byte[] generateAdoptionContract(AdoptionDto adoptionDto) {
        return adoptionContractGenerator.generateAdoptionContract(adoptionDto);
    }

    @Override
    public byte[] generateNewMemberContract(NewMemberDto newMemberDto) {
        return membershipContractGenerator.generateMembershipContract(newMemberDto);
    }
}
