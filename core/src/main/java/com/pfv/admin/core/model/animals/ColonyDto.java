package com.pfv.admin.core.model.animals;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.utils.LocalDateDeserializer;
import com.pfv.admin.common.utils.LocalDateSerializer;
import com.pfv.admin.core.model.common.LocationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ColonyDto extends WithPhotosDto {
    @Indexed(unique = true, name = "name", direction = IndexDirection.ASCENDING, dropDups = true)
    private String name;
    private String notes;

    private LocationDto location;

    @Valid
    @NotNull
    private AliasIdLabelDto person;

    private Integer totalAnimals;
    private Integer totalNeuteredAnimals;

    private Integer totalFemaleAnimals;
    private Integer totalNeuteredFemaleAnimals;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate initDate;

    public static ColonyDto emptyData() {
        ColonyDto colony = new ColonyDto();

        return colony;
    }
}
