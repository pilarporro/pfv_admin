package com.pfv.admin.core.model.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.PaymentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CastrationDto extends ActivityDto {

    private AliasIdLabelDto animal;

    private AliasIdLabelDto vetClinic;

    private PaymentType paymentType;

    private String invoiceRef;

    private AliasIdLabelDto colony;

}
