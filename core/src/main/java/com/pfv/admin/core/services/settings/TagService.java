package com.pfv.admin.core.services.settings;

import com.pfv.admin.common.model.enums.TagType;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.settings.TagDto;
import com.pfv.admin.data.model.settings.Tag;

import java.util.List;

public interface TagService extends Service<TagDto, Tag> {
    List<TagDto> getTags(TagType type, String name);

    List<String> searchTagsByTerms(TagType type, String term);

    boolean existTag(TagType type, String name);

    void useTag(TagType type, String name);
}
