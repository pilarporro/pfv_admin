package com.pfv.admin.core.services.common;

import com.pfv.admin.common.mappers.Mapper;
import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.dto.operations.OperationsInfoDto;
import com.pfv.admin.common.model.entity.Entity;
import com.pfv.admin.common.model.querydsl.SearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface WithPhotosService<DTO extends EntityOperationsInfoDto, ENTITY extends Entity> {

    List<DTO> getAll();

    Long countWithFiltering(List<SearchCriteria> searchCriteria);

    List<DTO> getAll(String sortField);

    List<ENTITY> getAllEntities();

    Page<DTO> getAllWithPaging(Pageable pageable);

    Optional<DTO> findById(String id);

    DTO create(DTO dto);

    List<DTO> create(List<DTO> dtos);

    DTO update(DTO dto);

    List<DTO> update(List<DTO> dtos);

    void delete(String id);

    Optional<ENTITY> findEntityById(String id);

    Page<DTO> getAllWithFiltersAndPaging(Pageable pageable, List<SearchCriteria> searchCriteria);

    Mapper<DTO, ENTITY> getMapper();

    OperationsInfoDto calculateOperationsInfo(ENTITY entity);
}
