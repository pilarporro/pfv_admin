package com.pfv.admin.core.services.reporting;

import com.pfv.admin.core.model.activities.NewMemberDto;
import com.pfv.admin.core.services.animals.AnimalService;
import com.pfv.admin.core.services.persons.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MembershipContractGeneratorImpl implements MembershipContractGenerator {
    @Autowired
    private PersonService personService;

    @Autowired
    private AnimalService animalService;

    @Override
    public byte[] generateMembershipContract(NewMemberDto newMemberDto) {
        return null;
    }




}
