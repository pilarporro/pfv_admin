package com.pfv.admin.core.model.activities;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.utils.LocalDateDeserializer;
import com.pfv.admin.common.utils.LocalDateSerializer;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = AdoptionDto.class, name = ActivityType.ADOPTION_TYPE),
        @JsonSubTypes.Type(value = BuySuppliesDto.class, name = ActivityType.BUY_SUPPLIES_TYPE),
        @JsonSubTypes.Type(value = CastrationDto.class, name = ActivityType.CASTRATION_TYPE),
        @JsonSubTypes.Type(value = MeetingDto.class, name = ActivityType.MEETING_TYPE),
        @JsonSubTypes.Type(value = NewMemberDto.class, name = ActivityType.NEW_MEMBER_TYPE),
        @JsonSubTypes.Type(value = OtherActivityDto.class, name = ActivityType.OTHER_TYPE),
        @JsonSubTypes.Type(value = PickUpKeeperDto.class, name = ActivityType.PICKUP_KEEPER_TYPE),
        @JsonSubTypes.Type(value = RescueDto.class, name = ActivityType.RESCUE_TYPE),
        @JsonSubTypes.Type(value = VisitClinicDto.class, name = ActivityType.VISIT_CLINIC_TYPE)
}
)
@ApiModel(value = "activity", subTypes = {
        AdoptionDto.class,
        BuySuppliesDto.class,
        CastrationDto.class,
        MeetingDto.class,
        NewMemberDto.class,
        OtherActivityDto.class,
        PickUpKeeperDto.class,
        RescueDto.class,
        VisitClinicDto.class
}, discriminator = "type")
public abstract class ActivityDto extends WithPhotosDto {
    private String notes;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;
    private List<String> tags;
    private AliasIdLabelDto person;
    @NotNull
    private ActivityType type;
    private String chip;
}
