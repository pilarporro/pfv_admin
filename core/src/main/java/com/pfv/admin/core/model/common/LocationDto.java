package com.pfv.admin.core.model.common;

import com.pfv.admin.common.model.enums.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class LocationDto {
    private String street;
    private String number;
    private String postalCode;
    private String areaId;
    private String areaName;
    private String municipalityId;
    private String municipalityName;
    private Country country;
    private String placeId;
    private String url;
    private String fullAddress;


    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private double[] geoLocation;
}
