package com.pfv.admin.core.services.activities;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.model.activities.BuySuppliesDto;
import com.pfv.admin.core.model.activities.CastrationDto;
import com.pfv.admin.core.model.activities.MeetingDto;
import com.pfv.admin.core.model.activities.NewMemberDto;
import com.pfv.admin.core.model.activities.OtherActivityDto;
import com.pfv.admin.core.model.activities.PickUpKeeperDto;
import com.pfv.admin.core.model.activities.RescueDto;
import com.pfv.admin.core.model.activities.VisitClinicDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.activities.Activity;
import com.pfv.admin.data.model.activities.Adoption;
import com.pfv.admin.data.model.activities.BuySupplies;
import com.pfv.admin.data.model.activities.Castration;
import com.pfv.admin.data.model.activities.Meeting;
import com.pfv.admin.data.model.activities.NewMember;
import com.pfv.admin.data.model.activities.OtherActivity;
import com.pfv.admin.data.model.activities.PickUpKeeper;
import com.pfv.admin.data.model.activities.Rescue;
import com.pfv.admin.data.model.activities.VisitClinic;
import org.mapstruct.AfterMapping;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class ActivityMapper extends com.pfv.admin.common.mappers.Mapper<ActivityDto, Activity> {
    @Autowired private AdoptionMapper adoptionMapper;
    @Autowired private BuySuppliesMapper buySuppliesMapper;
    @Autowired private CastrationMapper castrationMapper;
    @Autowired private MeetingMapper meetingMapper;
    @Autowired private NewMemberMapper newMemberMapper;
    @Autowired private OtherActivityMapper otherActivityMapper;
    @Autowired private PickUpKeeperMapper pickUpKeeperMapper;
    @Autowired private RescueMapper rescueMapper;
    @Autowired private VisitClinicMapper visitClinicMapper;

    @AfterMapping
    public void addLabel(final Activity entity, @MappingTarget final AliasIdLabelDto dto) {
        dto.setLabel(entity.getAlias());
    }

    public Activity dtoToEntity(final ActivityDto dto) {
        if (dto instanceof AdoptionDto) {
            return adoptionMapper.dtoToEntity((AdoptionDto) dto);
        } else if (dto instanceof BuySuppliesDto) {
            return buySuppliesMapper.dtoToEntity((BuySuppliesDto) dto);
        } else if (dto instanceof CastrationDto) {
            return castrationMapper.dtoToEntity((CastrationDto) dto);
        } else if (dto instanceof MeetingDto) {
            return meetingMapper.dtoToEntity((MeetingDto) dto);
        } else if (dto instanceof NewMemberDto) {
            return newMemberMapper.dtoToEntity((NewMemberDto) dto);
        } else if (dto instanceof OtherActivityDto) {
            return otherActivityMapper.dtoToEntity((OtherActivityDto) dto);
        } else if (dto instanceof PickUpKeeperDto) {
            return pickUpKeeperMapper.dtoToEntity((PickUpKeeperDto) dto);
        } else if (dto instanceof RescueDto) {
            return rescueMapper.dtoToEntity((RescueDto) dto);
        } else if (dto instanceof VisitClinicDto) {
            return visitClinicMapper.dtoToEntity((VisitClinicDto) dto);
        } else {
            return null;
        }
    }

    @InheritConfiguration(name = "dtoToEntity")
    public Activity merge(final ActivityDto dto, @MappingTarget final Activity entity) {
        if (dto instanceof AdoptionDto) {
            return adoptionMapper.merge((AdoptionDto) dto, (Adoption) entity);
        } else if (dto instanceof BuySuppliesDto) {
            return buySuppliesMapper.merge((BuySuppliesDto) dto, (BuySupplies) entity);
        } else if (dto instanceof CastrationDto) {
            return castrationMapper.merge((CastrationDto) dto, (Castration) entity);
        } else if (dto instanceof MeetingDto) {
            return meetingMapper.merge((MeetingDto) dto, (Meeting) entity);
        } else if (dto instanceof NewMemberDto) {
            return newMemberMapper.merge((NewMemberDto) dto, (NewMember) entity);
        } else if (dto instanceof OtherActivityDto) {
            return otherActivityMapper.merge((OtherActivityDto) dto, (OtherActivity) entity);
        } else if (dto instanceof PickUpKeeperDto) {
            return pickUpKeeperMapper.merge((PickUpKeeperDto) dto, (PickUpKeeper) entity);
        } else if (dto instanceof RescueDto) {
            return rescueMapper.merge((RescueDto) dto, (Rescue) entity);
        } else if (dto instanceof VisitClinicDto) {
            return visitClinicMapper.merge((VisitClinicDto) dto, (VisitClinic) entity);
        } else {
            return null;
        }
    }

    @InheritInverseConfiguration(name = "dtoToEntity")
    public ActivityDto entityToDto(final Activity entity) {
        if (entity instanceof Adoption) {
            return adoptionMapper.entityToDto((Adoption) entity);
        } else if (entity instanceof BuySupplies) {
            return buySuppliesMapper.entityToDto((BuySupplies) entity);
        } else if (entity instanceof Castration) {
            return castrationMapper.entityToDto((Castration) entity);
        } else if (entity instanceof Meeting) {
            return meetingMapper.entityToDto((Meeting) entity);
        } else if (entity instanceof NewMember) {
            return newMemberMapper.entityToDto((NewMember) entity);
        } else if (entity instanceof OtherActivity) {
            return otherActivityMapper.entityToDto((OtherActivity) entity);
        } else if (entity instanceof PickUpKeeper) {
            return pickUpKeeperMapper.entityToDto((PickUpKeeper) entity);
        } else if (entity instanceof Rescue) {
            return rescueMapper.entityToDto((Rescue) entity);
        } else if (entity instanceof VisitClinic) {
            return visitClinicMapper.entityToDto((VisitClinic) entity);
        } else {
            return null;
        }
    }
}
