/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.core.model.municipalities;

import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MunicipalityDto extends EntityOperationsInfoDto {
    private String name;
    private String province;
    private String city;
    private double[] location;
    private Integer population;
}
