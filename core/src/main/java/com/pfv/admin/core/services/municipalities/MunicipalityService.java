package com.pfv.admin.core.services.municipalities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.municipalities.MunicipalityDto;
import com.pfv.admin.data.model.municipalities.Municipality;

import java.util.List;


public interface MunicipalityService extends Service<MunicipalityDto, Municipality> {
    List<AliasIdLabelDto> getMunipalities(String name);

    List<AliasIdLabelDto> getAreas(String municipalityId, String name);

    AliasIdLabelDto findMinimalByName(String name);

    AliasIdLabelDto findAreaMinimalById(String id);

    MunicipalityDto getDefaultMunicipality();
}
