package com.pfv.admin.core.services.emails;

import com.pfv.admin.common.services.ConstraintsChecker;
import com.pfv.admin.data.model.emails.ExternalLinkData;
import org.springframework.stereotype.Component;

@Component
public class ExternalLinkDataConstraintsChecker extends ConstraintsChecker<ExternalLinkData> {

}
