package com.pfv.admin.core.services.settings;

import com.pfv.admin.common.model.enums.TagType;
import com.pfv.admin.common.services.DataInit;
import com.pfv.admin.common.services.ServiceImpl;
import com.pfv.admin.core.model.settings.TagDto;
import com.pfv.admin.data.model.settings.Tag;
import com.pfv.admin.data.repositories.settings.TagRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TagServiceImpl extends ServiceImpl<TagDto, Tag, TagRepository> implements TagService, DataInit {
    @Override
    public List<TagDto> getTags(final TagType type, final String name) {
        List<TagDto> dtos = mapper.entitiesToDtos(repository.findByTypeAndNameStartsWithIgnoreCaseOrderByUsesDesc(type, name));
        dtos.forEach(tag -> {
            tag.setName(tag.getName().toUpperCase());
        });

        if (dtos.isEmpty()) {
            // dtos.add(TagDto.builder().name(name.toUpperCase()).build());
        }
        // TODO It is not going to work for now.
        return dtos.stream().sorted((o1, o2)->o1.getName().
                compareTo(o2.getName())).
                collect(Collectors.toList());
    }

    @Override
    public void init() {
        if (repository.count() == 0) {
            create(TagDto.builder().type(TagType.CAT).name("CASTRADO").terms("CASTRADO,CASTRADA").build());
            create(TagDto.builder().type(TagType.CAT).name("TIÑA").terms("TIÑA").build());
            create(TagDto.builder().type(TagType.CAT).name("GIARDIAS").terms("GIARDIAS").build());
            create(TagDto.builder().type(TagType.CAT).name("NEGATIVO LEUCEMIA").terms("NEGATIVO,LEUCEMIA").build());

            create(TagDto.builder().type(TagType.ACTIVITY).name("ANTIBIOTICO").terms("ANTIBIOTICO,ANTIBIÓTICO").build());
        }
    }

    @Override
    public List<String> searchTagsByTerms(final TagType type, final String term) {
        return mapper.entitiesToDtos(repository.findByTypeAndTermsRegexOrderByUsesDesc(type,
                MongoRegexCreator.INSTANCE.toRegularExpression(term, MongoRegexCreator.MatchMode.CONTAINING)))
                .stream().map(tag -> tag.getName()).collect(Collectors.toList());
    }

    @Override
    public boolean existTag(final TagType type, String name) {
        return repository.countByTypeAndNameIgnoreCase(type, name) > 0;
    }

    @Override public void useTag(TagType type, String name) {
        Tag tag = repository.findFirstByTypeAndName(type, name);
        if (tag != null) {
            Integer uses = tag.getUses() == null ? 0 : tag.getUses();
            tag.setUses(uses + 1);
            repository.save(tag);
        }
    }

}
