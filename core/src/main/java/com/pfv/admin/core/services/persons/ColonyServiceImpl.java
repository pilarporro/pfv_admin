package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.services.ServiceWithPhotosImpl;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.data.model.animals.Colony;
import com.pfv.admin.data.repositories.animals.ColonyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;

@Service
@Slf4j
@Transactional
public class ColonyServiceImpl extends ServiceWithPhotosImpl<ColonyDto, Colony, ColonyRepository> implements ColonyService {
    @Autowired
    private PersonService personService;

    @Override
    protected void postSave(final Colony oldEntity, final Colony newEntity) {
        PersonDto person = personService.findById(newEntity.getPerson().getId()).orElseThrow(NotFoundException::new);
        if (person.getColonies() == null) {
            person.setColonies(new LinkedList<>());
        }

        if (!person.getColonies().stream()
                .anyMatch(colony -> colony.getId().equals(newEntity.getId()))) {
            person.getColonies().add(AliasIdLabelDto.ofIdAlias(newEntity.getId(), newEntity.getAlias()));
            personService.update(person);
        }
    }

    protected String createAlias(Colony entity) {
        return entity.getPerson().getAlias() + "_" + entity.getName().replaceAll(" ", "").toLowerCase();
    }


}
