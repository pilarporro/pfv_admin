package com.pfv.admin.core.model.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.PaymentType;
import com.pfv.admin.common.model.enums.VisitClinicType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VisitClinicDto extends ActivityDto {

    private AliasIdLabelDto vetClinic;

    private List<AliasIdLabelDto> animals;

    private PaymentType paymentType;

    private String invoiceRef;

    private VisitClinicType visitClinicType;
}
