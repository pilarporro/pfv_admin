package com.pfv.admin.core.services.settings;

import com.pfv.admin.core.model.settings.PropertyDto;

import java.util.Optional;

public interface PropertyService {
    Optional<PropertyDto> getProperty(String property);
}
