package com.pfv.admin.core.jobs;


import com.pfv.admin.core.services.persons.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PersonsMaitananceJob {
    @Autowired
    private PersonService personService;

    @Scheduled(cron = "${scheduler.periodicity.personsMaintanance}")
    public void execute() {
        log.info("Starting PersonsMaitananceJob");



    }
}