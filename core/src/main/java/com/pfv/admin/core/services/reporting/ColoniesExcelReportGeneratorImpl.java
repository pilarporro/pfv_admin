package com.pfv.admin.core.services.reporting;

import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.model.common.LocationDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.persons.ColonyService;
import com.pfv.admin.core.services.persons.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.pfv.admin.core.services.reporting.ReportUtils.SHORT_DATE_FORMATTER;

@Component
@Slf4j
public class ColoniesExcelReportGeneratorImpl implements ColoniesExcelReportGenerator {
    private static final String AREA_EMPTY = "?";

    @Autowired
    private PersonService personService;

    @Autowired
    private ColonyService colonyService;

    @Override
    public byte[] generateColonyReport() {

        try {
            List<ColonyDto> colonies = new LinkedList<>();
            Map<String, PersonDto> members = new HashMap<>();
            Map<String, List<ColonyDto>> coloniesAreas = new HashMap<>();

            /* Count the cats */
            AtomicInteger totalAnimals = new AtomicInteger(0);
            AtomicInteger totalNeuteredAnimals = new AtomicInteger(0);
            List<PersonDto> membersTotal =  personService.findMembers();
            membersTotal.forEach(member -> {
                if (!CollectionUtils.isEmpty(member.getColonies())) {
                    member.getColonies().forEach(colonyAliasId -> {
                        colonyService.findById(colonyAliasId.getId()).ifPresent(colony -> {
                            if ((colony.getTotalAnimals() != null) && (colony.getTotalAnimals() > 0)) {
                                colonies.add(colony);
                                members.put(colony.getId(), member);

                                String area = getColonyArea(colony, members);

                                if (coloniesAreas.get(area) == null) {
                                    coloniesAreas.put(area, new LinkedList<>());
                                }
                                coloniesAreas.get(area).add(colony);

                                if (colony.getTotalAnimals() != null) {
                                    totalAnimals.addAndGet(colony.getTotalAnimals());
                                }

                                if (colony.getTotalNeuteredAnimals() != null) {
                                    totalNeuteredAnimals.addAndGet(colony.getTotalNeuteredAnimals());
                                }
                            }
                        });
                    });
                }
            });

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("documents/censo.xlsx").getFile());
            FileInputStream inputStream = new FileInputStream(file);
            Workbook document = WorkbookFactory.create(inputStream);

            addTotalColonies(document, colonies, members, coloniesAreas);

            inputStream.close();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            document.write(out);
            document.close();

            return out.toByteArray();

        } catch (Exception exception) {
            throw new ServiceException(exception);
        }
    }

    private void addTotalColonies(final Workbook document, List<ColonyDto> colonies, Map<String, PersonDto> members, Map<String, List<ColonyDto>> coloniesAreas)
            throws Exception {
        Sheet sheet = document.getSheetAt(0);

        int firstRow = 3;
        AtomicInteger rowIdx = new AtomicInteger(firstRow);

        colonies.stream()
                .sorted((c1, c2) -> getColonyArea(c1, members).compareTo(getColonyArea(c2, members)))
                .forEach(colony -> {
                    try {
                        int lastRow = sheet.getLastRowNum();
                        sheet.shiftRows(rowIdx.incrementAndGet(), lastRow, 1, true, true);

                        Row row = sheet.createRow(rowIdx.get());
                        addColonyInfo(document, (rowIdx.get() - firstRow) == colonies.size(), row, colony, members);
                        row.setHeight((short) (row.getHeight() * 2));

                    } catch (Exception e) {
                        log.error("Unexpected exception adding a colony", e);
                    }

                });

        int lastRow = sheet.getLastRowNum();
        Row row = sheet.getRow(lastRow);
        row.getCell(Columns.TOTAL.ordinal()).setCellFormula(String.format("SUM(G%s:G%s)", firstRow + 2, lastRow));
        row.getCell(Columns.FEMALE_TOTAL.ordinal()).setCellFormula(String.format("SUM(H%s:H%s)", firstRow + 2, lastRow));
        row.getCell(Columns.FEMALE_NEUTERED_TOTAL.ordinal()).setCellFormula(String.format("SUM(I%s:I%s)", firstRow + 2, lastRow));
        row.getCell(Columns.MALE_TOTAL.ordinal()).setCellFormula(String.format("SUM(J%s:J%s)", firstRow + 2, lastRow));
        row.getCell(Columns.MALE_NEUTERED_TOTAL.ordinal()).setCellFormula(String.format("SUM(K%s:K%s)", firstRow + 2, lastRow));

    }

    private LocationDto getColonyLocation(ColonyDto colony, Map<String, PersonDto> members) {
        PersonDto personDto = members.get(colony.getId());
        LocationDto location = colony.getLocation();

        if (location == null || !StringUtils.hasLength(location.getAreaName())) {
            location = personDto.getLocation();
        }

        return location;
    }

    private String getColonyArea(ColonyDto colony, Map<String, PersonDto> members) {
        LocationDto location = getColonyLocation(colony, members);

        return location != null && StringUtils.hasLength(location.getAreaName()) ? location.getAreaName() : AREA_EMPTY;
    }


    enum Columns {
        EMPTY,
        ADDRESS_STREET,
        ADDRESS_NUMBER,
        ADDRESS_AREA,
        MEMBER,
        INIT_DATE,
        TOTAL,
        FEMALE_TOTAL,
        FEMALE_NEUTERED_TOTAL,
        MALE_TOTAL,
        MALE_NEUTERED_TOTAL,
        NOTES
    }

    private Cell createCell(final Workbook document, final Row row, int idxCell, boolean rightBorder, boolean bottomBorder, boolean underline) {
        Cell cell = row.createCell(idxCell);
        XSSFCellStyle cellStyle = (XSSFCellStyle) document.createCellStyle();

        if (underline) {
            Font font = document.createFont();
            font.setUnderline(HSSFFont.U_SINGLE);
            font.setColor(IndexedColors.BLUE.getIndex());
            cellStyle.setFont(font);
        }

        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
        cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        if (rightBorder) {
            cellStyle.setBorderRight(BorderStyle.MEDIUM);
            cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        }

        if (bottomBorder) {
            cellStyle.setBorderBottom(BorderStyle.MEDIUM);
            cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        }

        cell.setCellStyle(cellStyle);
        return cell;
    }

    private void addColonyInfo(final Workbook document, final boolean lastRow, final Row row, final ColonyDto colonyDto, final Map<String, PersonDto> members) throws Exception {
        createCell(document, row, Columns.MEMBER.ordinal(), false, !lastRow, false).setCellValue(members.get(colonyDto.getId()).getFullName());
        createCell(document, row, Columns.NOTES.ordinal(), true, !lastRow, false)
                .setCellValue(colonyDto.getNotes() == null ? "" : colonyDto.getNotes());

        createCell(document, row, Columns.INIT_DATE.ordinal(), false, !lastRow, false)
                .setCellValue(colonyDto.getInitDate() == null ? "" : SHORT_DATE_FORMATTER.format(colonyDto.getInitDate()));


        Integer total = 0;
        Integer totalFemale = 0;
        Integer totalMale = 0;
        Integer totalNeuteredFemale = 0;
        Integer totalNeuteredMale = 0;


        if (colonyDto.getTotalAnimals() != null) {
            total = colonyDto.getTotalAnimals();

            if (colonyDto.getTotalFemaleAnimals() == null) {
                colonyDto.setTotalFemaleAnimals(new Double(Math.floor(colonyDto.getTotalAnimals() * 0.7d)).intValue());
            }

            totalFemale = colonyDto.getTotalFemaleAnimals();
            totalMale = colonyDto.getTotalAnimals() - colonyDto.getTotalFemaleAnimals();

            if (colonyDto.getTotalNeuteredAnimals() != null) {
                if (colonyDto.getTotalNeuteredFemaleAnimals() == null) {
                    colonyDto.setTotalNeuteredFemaleAnimals(new Double(Math.floor(colonyDto.getTotalFemaleAnimals() * 0.5d)).intValue());
                }

                totalNeuteredFemale = colonyDto.getTotalNeuteredFemaleAnimals();
                totalNeuteredMale = colonyDto.getTotalNeuteredAnimals() - colonyDto.getTotalNeuteredFemaleAnimals();
            }
        }

        createCell(document, row, Columns.TOTAL.ordinal(), false, !lastRow, false)
                .setCellValue(total);
        createCell(document, row, Columns.FEMALE_TOTAL.ordinal(), false, !lastRow, false)
                .setCellValue(totalFemale);
        createCell(document, row, Columns.MALE_TOTAL.ordinal(), false, !lastRow, false)
                .setCellValue(totalMale);
        createCell(document, row, Columns.FEMALE_NEUTERED_TOTAL.ordinal(), false, !lastRow, false)
                .setCellValue(totalNeuteredFemale);
        createCell(document, row, Columns.MALE_NEUTERED_TOTAL.ordinal(), false, !lastRow, false)
                .setCellValue(totalNeuteredMale);

        LocationDto locationDto = getColonyLocation(colonyDto, members);
        String street = "";
        String number = "";
        String area = "";
        Hyperlink link = null;

        if (locationDto != null) {
            String areaName = getColonyArea(colonyDto, members);

            if (StringUtils.hasLength(locationDto.getStreet()) ) {
                street = locationDto.getStreet();
                number = locationDto.getNumber();

                if (StringUtils.hasLength(locationDto.getUrl()) ) {
                    link = document.getCreationHelper().createHyperlink(HyperlinkType.URL);
                    link.setAddress(locationDto.getUrl());
                }
            }

            area = areaName.equalsIgnoreCase(AREA_EMPTY) ? "" : areaName;
        }

        Cell cellAddressStreet = createCell(document, row, Columns.ADDRESS_STREET.ordinal(), false, !lastRow, link != null);
        cellAddressStreet.setCellValue(street);
        if (link != null) {
            cellAddressStreet.setHyperlink(link);
        }
        createCell(document, row, Columns.ADDRESS_NUMBER.ordinal(), false, !lastRow, false).setCellValue(number);
        createCell(document, row, Columns.ADDRESS_AREA.ordinal(), false, !lastRow, false).setCellValue(area);
    }
}
