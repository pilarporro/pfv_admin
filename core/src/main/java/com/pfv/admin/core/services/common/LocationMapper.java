package com.pfv.admin.core.services.common;

import com.pfv.admin.common.mappers.EntityToIdMapper;
import com.pfv.admin.core.model.common.LocationDto;
import com.pfv.admin.data.model.common.Location;
import org.mapstruct.Builder;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring",
        uses = { EntityToIdMapper.class },
        builder = @Builder(disableBuilder = true))
public abstract class LocationMapper  {
    @Mappings({
            @Mapping(source = "municipality.id", target = "municipalityId"),
            @Mapping(source = "municipality.name", target = "municipalityName"),
            @Mapping(source = "area.id", target = "areaId"),
            @Mapping(source = "area.name", target = "areaName"),
    })
    @InheritConfiguration(name = "dtoToEntity")
    public abstract LocationDto entityToDto(Location entity);

    @Mappings({
            @Mapping(source = "municipalityId", target = "municipality"),
            @Mapping(source = "areaId", target = "area")
    })
    public abstract Location dtoToEntity(LocationDto dto);

    @InheritConfiguration(name = "dtoToEntity")
    public abstract Location merge(LocationDto dto, @MappingTarget Location entity);
}
