package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.persons.TrapDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.persons.Trap;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class TrapMapper extends com.pfv.admin.common.mappers.Mapper<TrapDto, Trap> {

    @AfterMapping
    public void addLabel(final Trap entity, @MappingTarget final AliasIdLabelDto dto) {
        dto.setLabel(entity.getName());
    }

}
