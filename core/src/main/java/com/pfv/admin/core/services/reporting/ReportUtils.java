package com.pfv.admin.core.services.reporting;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;

public class ReportUtils {
    public static final DateTimeFormatter LONG_DATE_FORMATTER = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).withLocale(Locale.forLanguageTag("ES"));
    public static final DateTimeFormatter SHORT_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/YYYY");

    public static void replacePlaceholders(XWPFDocument document, String... args) {
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        for (XWPFParagraph p : paragraphs) {
            List<XWPFRun> runs = p.getRuns();
            if (runs != null) {
                for (XWPFRun r : runs) {
                    replaceText(r, args);
                }
            }
        }
        for (XWPFTable tbl : document.getTables()) {
            for (XWPFTableRow row : tbl.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    for (XWPFParagraph p : cell.getParagraphs()) {
                        for (XWPFRun r : p.getRuns()) {
                            replaceText(r, args);
                        }
                    }
                }
            }
        }
    }

    public static void replaceText(XWPFRun run, String... args) {
        String text = run.getText(0);
        if (text != null) {
            System.out.println(text);
            boolean changed = false;
            for (int idx = 0; idx < args.length; idx++) {
                String contains = "#" + idx + "#";
                if (text.contains(contains)) {
                    text = text.replace(contains, args[idx]);
                    changed = true;
                }
            }
            if (changed) {
                run.setText(text, 0);
            }
        }
    }
}
