package com.pfv.admin.core.services.places;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.PlaceType;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.places.PlaceDto;
import com.pfv.admin.data.model.places.Place;

import java.util.List;

public interface PlaceService extends Service<PlaceDto, Place> {
    List<AliasIdLabelDto> findByTypeAndAliasStartsWith(PlaceType type, String alias);

}
