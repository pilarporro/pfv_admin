package com.pfv.admin.core.services.settings;

import com.pfv.admin.common.exceptions.services.ServiceErrors;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.common.services.Validator;
import com.pfv.admin.data.model.settings.SystemSetting;
import com.pfv.admin.data.model.settings.SystemSettingType;
import org.springframework.stereotype.Component;

@Component
public class SystemSettingValidator extends Validator<SystemSetting> {
    @Override
    public void validate(final SystemSetting systemSettting, final ServiceErrors errors) {
        if (!validValuesFreeCredits(systemSettting)) {
            errors.newFieldError("value", ResourceUtil.getMessage("settings.system.free-credits.invalid.values"));
        }
        super.validate(systemSettting, errors);
    }

    private boolean validValuesFreeCredits(final SystemSetting systemSettting) {
        if (systemSettting.getProperty() == SystemSettingType.SAMPLE) {
            return validInteger(systemSettting.getValue());
        } else {
            return true;
        }
    }

    private boolean validInteger(final String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException exception) {
            return false;
        }
    }

    private boolean validBoolean(final String value) {
        return (value != null) && ("true".equals(value) || "false".equals(value));
    }
}
