package com.pfv.admin.core.services.activities;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.activities.PickUpKeeperDto;
import com.pfv.admin.core.services.animals.AnimalMapper;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.core.services.persons.PersonMapper;
import com.pfv.admin.core.services.places.PlaceMapper;
import com.pfv.admin.data.model.activities.PickUpKeeper;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, PersonMapper.class, AnimalMapper.class, PlaceMapper.class },
        config = MapperConfiguration.class)
public abstract class PickUpKeeperMapper extends com.pfv.admin.common.mappers.Mapper<PickUpKeeperDto, PickUpKeeper> {
    @AfterMapping
    public void addLabel(final PickUpKeeper entity, @MappingTarget final AliasIdLabelDto dto) {
        dto.setLabel(entity.getAlias());
    }
}
