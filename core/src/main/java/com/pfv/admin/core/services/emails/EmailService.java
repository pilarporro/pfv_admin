package com.pfv.admin.core.services.emails;

import com.pfv.admin.data.model.emails.EmailToSend;

public interface EmailService {
    void sendMail(EmailToSend sendMailData);

    void deletePendingMessagesTo(String to);

    boolean sendEffectiveMail(EmailToSend email);

    void addEmailReader(EmailReader reader);

    void readLastEmails();
}
