package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.persons.UserDto;
import com.pfv.admin.data.model.persons.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public interface UserService extends Service<UserDto, User> {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    UserDto findByUsername(String username);
}
