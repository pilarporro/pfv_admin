package com.pfv.admin.core.services.reporting;

import com.pfv.admin.common.model.dto.PhotoDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class PhotoDate {
    private PhotoDto photoDto;
    private LocalDate date;
}
