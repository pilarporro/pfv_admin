package com.pfv.admin.core.services.reporting;

import com.pfv.admin.core.model.common.CatFile;

import java.time.LocalDate;
import java.util.List;

public interface ActivityReportGenerator {
    byte[] generateActivityReport(LocalDate from, LocalDate to);
    byte[] generateEmailCastrationsReport(String refInvoice);

    List<CatFile> generateCatFiles(String invoiceRef);

    byte[] generateColonyReport();

}
