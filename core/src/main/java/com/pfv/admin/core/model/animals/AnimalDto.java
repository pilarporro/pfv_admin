package com.pfv.admin.core.model.animals;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.model.enums.AnimalStatus;
import com.pfv.admin.common.model.enums.AnimalType;
import com.pfv.admin.common.model.enums.Color;
import com.pfv.admin.common.model.enums.Gender;
import com.pfv.admin.common.utils.LocalDateDeserializer;
import com.pfv.admin.common.utils.LocalDateSerializer;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CatDto.class, name = AnimalType.CAT_TYPE),
        @JsonSubTypes.Type(value = DogDto.class, name = AnimalType.DOG_TYPE)
})
@ApiModel(value = "animal", subTypes = {
        CatDto.class,
        DogDto.class
}, discriminator = "type")
public abstract class AnimalDto extends WithPhotosDto {
    private String name;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate birthDate;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate deathDate;
    private Gender gender;
    private Color color;
    private AnimalStatus animalStatus;
    private List<String> tags;
    private String notes;
    private String colonyId;
    private AliasIdLabelDto owner;
    private AliasIdLabelDto adopter;
    private AliasIdLabelDto keeper;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate adoptionDate;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate pickUpDate;
    @NotNull
    private AnimalType type;

    @NotNull
    private Boolean neutered;
}
