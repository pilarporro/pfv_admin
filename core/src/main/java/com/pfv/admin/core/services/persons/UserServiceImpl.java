package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.services.DataInit;
import com.pfv.admin.common.services.ServiceImpl;
import com.pfv.admin.common.model.enums.Role;
import com.pfv.admin.core.model.common.LocationDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.model.persons.UserDto;
import com.pfv.admin.core.services.municipalities.MunicipalityServiceImpl;
import com.pfv.admin.data.model.persons.User;
import com.pfv.admin.data.repositories.persons.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service("mongoUserService")
@Slf4j
@Transactional
@DependsOn("municipalityService")
public class UserServiceImpl extends ServiceImpl<UserDto, User, UserRepository>
        implements UserDetailsService, UserService, DataInit {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PersonService personService;

    @Autowired
    private MunicipalityServiceImpl municipalityService;

    protected void preSave(User oldEntity, User newEntity) {
        if (oldEntity == null) {
            newEntity.setPassword(passwordEncoder.encode(newEntity.getPassword()));
        }
    }

    @Override
    public void init() {
        log.info("Start users initialization  ...");
        if (repository.count() == 0) {
            this.addAdmin("pilarp", "Pilar", "Porro", "kokoko", "pilar.porro@gmail.com", "51943011H");
            this.addUser("olga", "Olga", "Mormeneo", "Olga2020", "olga.mormeneo@gmail.com", "50107350X");
            this.addGuest("invitado", "Invitado", "", "pfv", "");
        }

        if (repository.count() == 3) {
            this.addUser("patricia", "Patricia", "del Amo Velasco", "Patri2021", "-", "-");
        }

        if (repository.count() == 4) {
            this.addUser("carlos", "Carlos", "Rivas Angulo", "Carlos2022", "email@server.com", "--");
            this.addUser("ana", "Ana", "Picazo", "Ana2022", "email@server.com", "--");
        }

        if (repository.count() == 6) {
            this.addUser("gemma", "Gemma", "Sánchez Pedrero", "GemmaPFV", "email@server.com", "--");
        }

        if (repository.count() == 7) {
            this.addUser("cristina", "Cristina", "- -", "crispu", "email@server.com", "--");
        }

        if (repository.count() == 8) {
            this.addAdmin("adminpfv", "Admin", "- -", "Patri2021", "email@server.com", "--");
        }

        if (repository.count() == 9) {
            this.addUser("nerea", "Nerea", "Alonso Durán", "NereaPFV", "email@server.com", "--");
            this.addUser("pablo", "Pablo", "Piñeiro Sabalete", "Pablo2023", "email@server.com", "--");
        }

        municipalityService.init();
    }

    @Transactional
    private AliasIdLabelDto createPerson(String name, String lastName, String email, String dni) {
        AliasIdLabelDto person = personService.findByAlias(name.toLowerCase());
        if (person != null) {
            return person;
        }

        PersonDto personDto = PersonDto.builder()
                .name(name)
                .idNumber(dni)
                .isMember(true)
                .lastName(lastName)
                .location(LocationDto.builder()
                        .municipalityId(municipalityService.getDefaultMunicipality().getId())
                        .build())
                .email(email)
                .build();
        personDto.setActive(true);
        personDto.setMarkToDelete(false);
        personDto.setNotification(false);
        personDto.setPhotos(new LinkedList<>());

        PersonDto created = personService.create(personDto);
        return personService.findMinimalById(created.getId());
    }

    @Transactional
    public UserDto addAdmin(String username, String name, String lastName, String password, String email, String idNumber) {
        List<String> roles = Arrays.asList(Role.USER.name(), Role.ADMIN.name());

        UserDto user = UserDto.builder()
                .roles(roles)
                .person(createPerson(name, lastName, email, idNumber))
                .username(username)
                .lastName(lastName)
                .name(name)
                .password(password)
                .email(email)
                .build();

        return create(user);
    }

    private void addUser(String username, String name, String lastName, String password, String email, String dni) {
        List<String> roles = Arrays.asList(Role.USER.name());

        UserDto user = UserDto.builder()
                .roles(roles)
                .person(createPerson(name, lastName, email, dni))
                .username(username)
                .lastName(lastName)
                .name(name)
                .password(password)
                .email(email)
                .build();

        create(user);
    }

    private void addGuest(String username, String name, String lastName, String password, String email) {
        List<String> roles = Arrays.asList(Role.GUEST.name());

        UserDto user = UserDto.builder()
                .roles(roles)
                .username(username)
                .lastName(lastName)
                .person(createPerson(name, lastName, email, null))
                .name(name)
                .password(password)
                .email(email)
                .build();

        create(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User dbUser = repository.findByUsernameIgnoreCase(username);
        UserDetails userFound = null;
        if ((dbUser == null) || (!dbUser.isActive())) {
            throw new UsernameNotFoundException("User not found");
        } else {
            List<SimpleGrantedAuthority> authorities = dbUser.getRoles().stream()
                    .map(role -> new SimpleGrantedAuthority(role.name()))
                    .collect(Collectors.toList());
            userFound = new org.springframework.security.core.userdetails.User(dbUser.getUsername(), dbUser.getPassword(), authorities);
        }

        return userFound;
    }

    @Override
    public UserDto findByUsername(String username) {
        return mapper.entityToDto(repository.findByUsernameIgnoreCase(username));
    }
}
