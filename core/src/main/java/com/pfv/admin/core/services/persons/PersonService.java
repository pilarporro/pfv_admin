package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.data.model.persons.Person;

import java.util.List;

public interface PersonService extends Service<PersonDto, Person> {

    List<PersonDto> findMembers();
}
