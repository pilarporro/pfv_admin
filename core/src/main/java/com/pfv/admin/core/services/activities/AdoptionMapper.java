package com.pfv.admin.core.services.activities;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.services.animals.AnimalMapper;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.core.services.persons.PersonMapper;
import com.pfv.admin.data.model.activities.Adoption;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, PersonMapper.class, AnimalMapper.class },
        config = MapperConfiguration.class)
public abstract class AdoptionMapper extends com.pfv.admin.common.mappers.Mapper<AdoptionDto, Adoption> {
    @AfterMapping
    public void addLabel(final Adoption entity, @MappingTarget final AliasIdLabelDto dto) {
        dto.setLabel(entity.getAlias());
    }
}
