package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.services.Service;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.data.model.animals.Colony;

public interface ColonyService extends Service<ColonyDto, Colony> {

}
