package com.pfv.admin.core.services.emails;

import com.pfv.admin.common.exceptions.services.ServiceErrors;
import com.pfv.admin.common.services.Validator;
import com.pfv.admin.data.model.emails.ExternalLinkData;
import org.springframework.stereotype.Component;

@Component
public class ExternalLinkDataValidator extends Validator<ExternalLinkData> {

    @Override
    public void validate(final ExternalLinkData entity, final ServiceErrors errors) {
        super.validate(entity, errors);
    }
}
