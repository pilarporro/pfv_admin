package com.pfv.admin.core.services.municipalities;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.municipalities.AreaDto;
import com.pfv.admin.data.model.municipalities.Area;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        config = MapperConfiguration.class)
public abstract class AreaMapper extends com.pfv.admin.common.mappers.Mapper<AreaDto, Area> {

    @Override
    @Mappings({
            @Mapping(source = "municipality.id", target = "municipalityId")
    })
    public abstract AreaDto entityToDto(Area entity);

    @Override
    @Mappings({
            @Mapping(source = "municipalityId", target = "municipality")
    })
    public abstract Area dtoToEntity(AreaDto dto);

    @Mappings({
            @Mapping(target = "label", source = "name")
    })
    public abstract AliasIdLabelDto toResultDto(Area entity);
}
