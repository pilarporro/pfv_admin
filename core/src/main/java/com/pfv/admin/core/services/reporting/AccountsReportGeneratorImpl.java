package com.pfv.admin.core.services.reporting;

import com.mysema.codegen.StringUtils;
import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.PhotoDto;
import com.pfv.admin.common.model.enums.AccountSubtype;
import com.pfv.admin.common.model.enums.AccountType;
import com.pfv.admin.common.services.StorageServiceImpl;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.core.model.accounting.AccountNoteDto;
import com.pfv.admin.core.services.accounting.AccountNoteService;
import com.pfv.admin.core.services.activities.ActivityService;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.data.repositories.accounting.AccountNoteRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHyperlinkRun;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.pfv.admin.common.services.StorageServiceImpl.getFilePath;
import static com.pfv.admin.core.services.reporting.ColonyReportGeneratorImpl.createHyperlinkRun;
import static com.pfv.admin.core.services.reporting.ReportUtils.LONG_DATE_FORMATTER;

@Component
@Slf4j
public class AccountsReportGeneratorImpl implements AccountsReportGenerator {
    private static final int REPORT_FROM = 0;
    private static final int REPORT_TO = 1;

    private static Locale localeSpain = new Locale.Builder().setLanguage("es").setRegion("ES").build();
    public static final DateTimeFormatter SDP_REPORT_MONTH_FORMATTER = DateTimeFormatter.ofPattern("MMMM", localeSpain);
    public static final DateTimeFormatter SDP_REPORT_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static DecimalFormat SDP_REPORT_DECIMAL_FORMATTER =
            new DecimalFormat("#,##0.00", new DecimalFormatSymbols(localeSpain));

    @Value("${storage.path}")
    private String storagePath;

    @Value("${storage.tmp-path}")
    private String storagePathTmp;

    @Autowired
    private PersonService personService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private AccountNoteService accountNoteService;

    @Autowired
    private AccountNoteRepository accountNoteRepository;

    @Override
    public byte[] generateAccountsReport(LocalDate requestFrom, LocalDate requestTo) {


        try {
            LocalDate from = requestFrom == null ? LocalDate.of(LocalDate.now().getYear(), 1, 1) : requestFrom;
            LocalDate to = requestTo == null ? LocalDate.of(LocalDate.now().getYear(), 12, 31) : requestTo;

            /*
            accountNoteRepository.findByActiveIsTrueAndDateBetweenOrderByDate(from, to).stream()
                    .forEach(accountNote -> {
                        if (accountNote.getBankBalance().doubleValue() > 400000) {
                            log.info("Prev: " +  accountNote.getBankBalance().doubleValue() + "BankBalance: " + BigDecimal.valueOf(accountNote.getBankBalance().doubleValue() / 100));
                            accountNote.setBankBalance(BigDecimal.valueOf(accountNote.getBankBalance().doubleValue() / 100));
                            accountNoteRepository.save(accountNote);
                        }
                    });
*/
            String args[] = new String[8];
            args[REPORT_FROM] = LONG_DATE_FORMATTER.format(from);
            args[REPORT_TO] = LONG_DATE_FORMATTER.format(to);

            List<AccountNoteDto> notes = accountNoteService.findByDates(from.minusDays(1), to.plusDays(1));

            for (int idx = 0; idx < args.length; idx++) {
                if (args[idx] == null) {
                    args[idx] = "_______________";
                }
            }
            ;

            return generate(accountNoteService.findPreviousNote(from), notes, args);
        } catch (Exception exception) {
            throw new ServiceException(exception);
        }
    }

    public byte[] generate(AccountNoteDto previousNote, List<AccountNoteDto> notes, String... args)
            throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("documents/accounts_report.docx").getFile());

        XWPFDocument document = new XWPFDocument(OPCPackage.open(new FileInputStream(file)));
        addTotals(previousNote, document, notes);

        log.info("The previous note is " + previousNote);

        List<PhotoDate> photosToAdd = new LinkedList<>();
        addDetailsTotals(previousNote, document, notes, photosToAdd);
        // addPhotos(document, photosToAdd);

        ReportUtils.replacePlaceholders(document, args);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.write(out);
        return out.toByteArray();
    }

    private void addTotals(AccountNoteDto previousNote, final XWPFDocument document, List<AccountNoteDto> notes)
            throws Exception {
        XWPFTable tableTotals = document.getTables().get(0);
        XWPFTable tableByType = document.getTables().get(1);

        Map<Integer, AccountTotals> totals = new HashMap<>();
        Map<AccountSubtype, Map<Integer, AccountTotals>> totalsByType = new HashMap<>();

        Arrays.stream(AccountSubtype.values()).forEach(type -> {
            Map<Integer, AccountTotals> totalsByTrimester = new HashMap<>();
            IntStream.range(0, 5).forEach(idx -> totalsByTrimester.put(idx, AccountTotals.init()));
            totalsByType.put(type, totalsByTrimester);
        });



        IntStream.range(0, 13).forEach(idx -> totals.put(idx, AccountTotals.init()));

        AccountTotals allTotals = totals.get(totals.size() - 1);
        notes.stream().forEach(note -> {
            AccountTotals totalsMonth = totals.get(note.getDate().getMonthValue() - 1);
            Double amount = note.getTotal().doubleValue();

            if (((note.getType() == AccountType.PUBLIC_GRANT && (amount < 0)) || (note.getType() == AccountType.BANK)) && (note.getSubtype() != null)) {
                // Group by trimestrer
                int trimester = new Double(Math.floor((note.getDate().getMonthValue() - 1) / 3)).intValue();
                Map<Integer, AccountTotals> totalsByTrimester = totalsByType.get(note.getSubtype());
                totalsByTrimester.get(trimester).incrementTotal(amount);
                totalsByTrimester.get(4).incrementTotal(amount);
            }

            if (note.getType() == AccountType.PUBLIC_GRANT) {
                allTotals.incrementBalanceGrant(amount);
                totalsMonth.setBalanceGrant(allTotals.getBalanceGrant());
                if (amount > 0) {
                    totalsMonth.incrementIncomesGrant(amount);
                    allTotals.incrementIncomesGrant(amount);
                } else {
                    totalsMonth.incrementExpensesGrant(amount);
                    allTotals.incrementExpensesGrant(amount);
                }
            } else if (note.getType() == AccountType.BANK) {
                // allTotals.setBalance(note.getBankBalance() != null ? note.getBankBalance().doubleValue() : 0d);
                //totalsMonth.setBalance(note.getBankBalance() != null ? note.getBankBalance().doubleValue() : 0d);
                allTotals.incrementBalance(amount);
                totalsMonth.setBalance(allTotals.getBalance());
                if (amount > 0) {
                    totalsMonth.incrementIncomes(amount);
                    allTotals.incrementIncomes(amount);
                } else {
                    totalsMonth.incrementExpenses(amount);
                    allTotals.incrementExpenses(amount);
                }
            }
        });



        AtomicInteger rowIdxType = new AtomicInteger(1);
        Arrays.stream(AccountSubtype.values()).forEach(type -> {
            Map<Integer, AccountTotals> totalsByTrimester = totalsByType.get(type);
            XWPFTableRow row = tableByType.insertNewTableRow(rowIdxType.incrementAndGet());

            addCell(row, ResourceUtil.getMessage(type.getMessageKey()), true, ParagraphAlignment.LEFT, 11);
            IntStream.range(0, 5).forEach(idx -> {
                Double total = totalsByTrimester.get(idx).getTotal();
                String color = null;
                if (idx == 4) {
                    color = total < 0 ? "B00020" : "689F38";
                }
                addCell(row, format(total), true, ParagraphAlignment.RIGHT, 11, color);
            });
        });

        AtomicInteger rowIdx = new AtomicInteger(1);
        IntStream.range(0, 13)
                .forEach(month -> {
                    try {
                        XWPFTableRow row = tableTotals.insertNewTableRow(rowIdx.incrementAndGet());
                        String titleText;
                        if (month == totals.size() - 1) {
                            titleText = "TOTAL";
                        } else {
                            titleText = StringUtils.capitalize(SDP_REPORT_MONTH_FORMATTER.format(LocalDate.now().withMonth(month + 1)));
                        }
                        addCell(row, titleText, true, ParagraphAlignment.LEFT, 11);

                        AccountTotals currentTotals = totals.get(month);

                        currentTotals.incrementBalance(previousNote.getBankBalance().doubleValue());
                        addCell(row, format(currentTotals.getIncomes()), false, ParagraphAlignment.RIGHT, 11);
                        addCell(row, format(currentTotals.getExpenses()), false, ParagraphAlignment.RIGHT, 11);
                        addCell(row, format(currentTotals.getBalance()), true, ParagraphAlignment.RIGHT, 11);
                        addCell(row, format(currentTotals.getIncomesGrant()), false, ParagraphAlignment.RIGHT, 11);
                        addCell(row, format(currentTotals.getExpensesGrant()), false, ParagraphAlignment.RIGHT, 11);
                        addCell(row, format(currentTotals.getBalanceGrant()), true, ParagraphAlignment.RIGHT, 11);

                    } catch (Exception e) {
                        log.error("Unexpected exception adding the totals of a month", e);
                    }
                });
    }

    private void addPhotos(final XWPFDocument document, List<PhotoDate> photosToAdd) {
        XWPFTable tablePhotos = document.getTables().get(2);
        AtomicInteger rowIdx = new AtomicInteger(1);
        photosToAdd.stream().forEach(photoDate -> {
            XWPFTableRow row = tablePhotos.insertNewTableRow(rowIdx.incrementAndGet());
            addCell(row, SDP_REPORT_FORMATTER.format(photoDate.getDate()), false, ParagraphAlignment.LEFT, 11);
            try {
                addPhoto(row, photoDate.getPhotoDto(), 300);
            } catch (Exception e) {
                log.error("Error adding the photo", e);
            }
        });
    }

    private void addDetailsTotals(AccountNoteDto previousNote, final XWPFDocument document, List<AccountNoteDto> notes, final List<PhotoDate> photosToAdd)
            throws Exception {
        XWPFTable tableDetails = document.getTables().get(2);

        Map<Integer, AccountTotals> totals = new HashMap<>();
        IntStream.range(0, 13).forEach(idx -> totals.put(idx, AccountTotals.init()));

        AccountTotals allTotals = AccountTotals.init();
        allTotals.setBalance(previousNote.getBankBalance().doubleValue());
        AtomicInteger rowIdx = new AtomicInteger(1);
        notes.stream().forEach(note -> {
            if (note.getType() != AccountType.CASH) {
                XWPFTableRow row = tableDetails.insertNewTableRow(rowIdx.incrementAndGet());

                addCell(row, SDP_REPORT_FORMATTER.format(note.getDate()), false, ParagraphAlignment.LEFT, 9);

                String type;
                if (note.getType() == AccountType.PUBLIC_GRANT) {
                    type = ResourceUtil.getMessage(note.getType().getMessageKey());
                    allTotals.incrementBalanceGrant(note.getTotal().doubleValue());
                } else {
                    type = ResourceUtil.getMessage(note.getSubtype().getMessageKey());
                    allTotals.incrementBalance(note.getTotal().doubleValue());
                }

                addCell(row, type, false, ParagraphAlignment.LEFT, 9);
                addCell(row, getPersonName(note.getPerson()), false, ParagraphAlignment.LEFT, 9);
                addCell(row, note.getNotes(), false, ParagraphAlignment.LEFT, 9);
                addCell(row, format(note.getTotal()), true, ParagraphAlignment.RIGHT, 9);
                addCell(row, format(allTotals.getBalance()), false, ParagraphAlignment.RIGHT, 9);
                //addCell(row, format(note.getBankBalance()), false, ParagraphAlignment.RIGHT, 9);
                addCell(row, format(allTotals.getBalanceGrant()), false, ParagraphAlignment.RIGHT, 9);

                /* We get the main photo */
                photosToAdd.clear();
                if (!CollectionUtils.isEmpty(note.getPhotos())) {
                    photosToAdd.addAll(note.getPhotos().stream().map(photoDto -> new PhotoDate(photoDto, note.getDate())).collect(Collectors.toList()));
                }

                if (CollectionUtils.isEmpty(note.getActivities())) {
                    note.getActivities().forEach(aliasIdLabelDto -> {
                        if (aliasIdLabelDto.getId() != null) {
                            photosToAdd.addAll(activityService.findById(aliasIdLabelDto.getId())
                                    .filter(activityDto -> !CollectionUtils.isEmpty(activityDto.getPhotos()))
                                    .map(activityDto -> activityDto.getPhotos()).orElse(new LinkedList<>()).stream()
                                    .map(photoDto -> new PhotoDate(photoDto, note.getDate())).collect(Collectors.toList()));
                        }
                    });
                }

                AtomicInteger idxPhoto = new AtomicInteger(0);
                XWPFParagraph paragraph = row.createCell().addParagraph();

                photosToAdd.forEach(photoDate -> {
                    XWPFHyperlinkRun hyperlinkrun = createHyperlinkRun(paragraph, photoDate.getPhotoDto().getUrl());
                    hyperlinkrun.setText(idxPhoto.incrementAndGet() + " ");
                    hyperlinkrun.setColor("0000FF");
                    hyperlinkrun.setUnderline(UnderlinePatterns.SINGLE);
                });
            }
        });
    }

    private void addCell(XWPFTableRow row, String text, boolean bold, ParagraphAlignment alignment, Integer fontSize) {
       addCell(row, text, bold, alignment, fontSize, null);
    }

    private void addCell(XWPFTableRow row, String text, boolean bold, ParagraphAlignment alignment, Integer fontSize, String color) {
        XWPFTableCell cell = row.createCell();
        cell.setText(text);

        XWPFParagraph paragraph = cell.getParagraphs().get(0);
        paragraph.getRuns().get(0).setBold(bold);
        if (color != null) {
            paragraph.getRuns().get(0).setColor(color);
        }
        paragraph.getRuns().get(0).setFontSize(fontSize);
        paragraph.setAlignment(alignment);
    }

    private String getPersonName(AliasIdLabelDto aliasIdLabelDto) {
        String name = "";
        if ((aliasIdLabelDto != null) && (aliasIdLabelDto.getId() != null)) {
            name = personService.findById(aliasIdLabelDto.getId()).map(personDto -> personDto.getFullName()).orElse("");
        }

        return name;
    }

    private String format(Double value) {
        return SDP_REPORT_DECIMAL_FORMATTER.format(value);
    }

    private String format(BigDecimal value) {
        if (value != null) {
            return format(value.doubleValue());
        } else {
            return "";
        }
    }

    private void addPhoto(final XWPFTableRow row, PhotoDto photoDto, int scaledWidth) throws Exception {
        if ((photoDto != null) && (
                photoDto.getDiskName().toLowerCase().endsWith("jpg")
                        || photoDto.getDiskName().toLowerCase().endsWith("jpeg")
        )) {
            String pathImgStorage = getFilePath(storagePath, photoDto.getFolder(), photoDto.getDiskName());

            Path tmpFile = Files.createTempFile(Paths.get(storagePathTmp), "TMP" + LocalDateTime.now().getNano(), ".jpg");

            Files.copy(Paths.get(pathImgStorage), tmpFile, StandardCopyOption.REPLACE_EXISTING);

            StorageServiceImpl.resize("jpg", tmpFile.toString(), scaledWidth, 0.95f);

            FileInputStream is = new FileInputStream(tmpFile.toString());

            BufferedImage inputImage = ImageIO.read(new File(tmpFile.toString()));
            int scaledHeight = (scaledWidth * inputImage.getHeight()) / inputImage.getWidth();
            row.createCell().addParagraph().createRun()
                    .addPicture(is, XWPFDocument.PICTURE_TYPE_JPEG, photoDto.getDiskName(), Units.toEMU(scaledWidth), Units.toEMU(scaledHeight)); // 200x200 pixels
            is.close();
            Files.delete(tmpFile);

        } else {
            XWPFParagraph paragraph = row.createCell().addParagraph();
            XWPFHyperlinkRun hyperlinkrun = createHyperlinkRun(paragraph, photoDto.getUrl());
            hyperlinkrun.setText(photoDto.getUrl());
            hyperlinkrun.setColor("0000FF");
            hyperlinkrun.setUnderline(UnderlinePatterns.SINGLE);
        }
    }
}
