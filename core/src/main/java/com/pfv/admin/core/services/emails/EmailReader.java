package com.pfv.admin.core.services.emails;

import com.pfv.admin.data.model.emails.EmailToRead;

public interface EmailReader {
    boolean processEmailRead(EmailToRead email);
}