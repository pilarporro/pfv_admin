package com.pfv.admin.core.services.emails;

import com.pfv.admin.core.model.emails.ExternalLinkDataDto;
import com.pfv.admin.common.services.ServiceImpl;
import com.pfv.admin.data.model.emails.ExternalLinkData;
import com.pfv.admin.data.repositories.emails.ExternalLinkDataRepository;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Base64;

@Service
@Slf4j
public class ExternalLinkDataServiceImpl extends ServiceImpl<ExternalLinkDataDto, ExternalLinkData, ExternalLinkDataRepository> implements ExternalLinkDataService {
    public static final Integer LINK_EXPIRY_TIME_IN_MONTHS = 6;
    @Override
    public String[] findDataByKey(String key) {
        ExternalLinkData externalLinkData = repository.findFirstByKeyAndCreatedAfter(key, LocalDateTime.now().minusMonths(LINK_EXPIRY_TIME_IN_MONTHS));
        if (externalLinkData == null) {
            return new String(Base64.getDecoder().decode(key)).split(";"); // Previous implementation
        } else {
            return mapper.entityToDto(externalLinkData).getAData();
        }
    }

    @Override
    public void deleteExpired() {
        repository.findAllByCreatedBefore(LocalDateTime.now().minusMonths(LINK_EXPIRY_TIME_IN_MONTHS))
                .forEach(entity -> {
                    repository.deleteById(entity.getId());
                });
    }

    @Override
    public ExternalLinkDataDto createWithData(final String data) {
        ExternalLinkDataDto dataDto = new ExternalLinkDataDto();
        dataDto.setKey(RandomString.make(40));
        dataDto.setData(new String(Base64.getEncoder().encode(data.getBytes())));
        return create(dataDto);
    }
}
