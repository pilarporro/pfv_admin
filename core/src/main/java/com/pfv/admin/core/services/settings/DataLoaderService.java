package com.pfv.admin.core.services.settings;

import com.pfv.admin.core.model.settings.LoadRequestDto;
import com.pfv.admin.core.model.settings.LoadResponseDto;

public interface DataLoaderService {

    LoadResponseDto load(LoadRequestDto request);
}
