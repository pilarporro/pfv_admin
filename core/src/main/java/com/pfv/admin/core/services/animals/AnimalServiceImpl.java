package com.pfv.admin.core.services.animals;

import com.pfv.admin.common.model.querydsl.PredicateBuilder;
import com.pfv.admin.common.services.ServiceWithPhotosImpl;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.data.model.animals.Animal;
import com.pfv.admin.data.repositories.animals.AnimalRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class AnimalServiceImpl extends ServiceWithPhotosImpl<AnimalDto, Animal, AnimalRepository> implements AnimalService {

    @Override
    public void preCreate(AnimalDto dto) {
        super.preCreate(dto);
    }

    @Override
    public void preUpdate(AnimalDto dto) {
        super.preUpdate(dto);
    }

    protected String createAlias(Animal entity) {
        return entity.getName().replaceAll(" ", "").toLowerCase();
    };

    @Override
    public PredicateBuilder<Animal> buildPredicateBuilder() {
        return new PredicateBuilder<>(Animal.class, "animal");
    }

}
