package com.pfv.admin.core.services.activities;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.activities.BuySuppliesDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.core.services.persons.PersonMapper;
import com.pfv.admin.core.services.places.PlaceMapper;
import com.pfv.admin.data.model.activities.BuySupplies;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, PersonMapper.class, PlaceMapper.class },
        config = MapperConfiguration.class)
public abstract class BuySuppliesMapper extends com.pfv.admin.common.mappers.Mapper<BuySuppliesDto, BuySupplies> {
    @AfterMapping
    public void addLabel(final BuySupplies entity, @MappingTarget final AliasIdLabelDto dto) {
        dto.setLabel(entity.getAlias());
    }
}
