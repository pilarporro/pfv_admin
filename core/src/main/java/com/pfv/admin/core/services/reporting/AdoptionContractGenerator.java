package com.pfv.admin.core.services.reporting;

import com.pfv.admin.core.model.activities.AdoptionDto;

public interface AdoptionContractGenerator {
    byte[] generateAdoptionContract(AdoptionDto adoptionDto);
}
