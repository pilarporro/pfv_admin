package com.pfv.admin.core.services.reporting;

import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.model.common.LocationDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.animals.AnimalService;
import com.pfv.admin.core.services.persons.ColonyService;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.core.services.places.PlaceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHyperlinkRun;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRelation;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHyperlink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class ColonyReportGeneratorImpl implements ColonyReportGenerator {
    private static final int TOTAL_MEMBERS = 0;
    private static final int TOTAL_COLONIES = 1;
    private static final int TOTAL_CATS = 2;
    private static final int TOTAL_NEUTERED_CATS = 3;
    private static final String AREA_EMPTY = "?";

    @Autowired
    private PersonService personService;

    @Autowired
    private AnimalService animalService;

    @Autowired
    private ColonyService colonyService;

    @Autowired
    private PlaceService placeService;

    @Override
    public byte[] generateColonyReport() {

        try {
            String args[] = new String[4];
            List<ColonyDto> colonies = new LinkedList<>();
            Map<String, PersonDto> members = new HashMap<>();
            Map<String, List<ColonyDto>> coloniesAreas = new HashMap<>();

            /* Count the cats */
            AtomicInteger totalAnimals = new AtomicInteger(0);
            AtomicInteger totalNeuteredAnimals = new AtomicInteger(0);
            List<PersonDto> membersTotal = personService.findMembers();
            membersTotal.forEach(member -> {
                if (!CollectionUtils.isEmpty(member.getColonies())) {
                    member.getColonies().forEach(colonyAliasId -> {
                        colonyService.findById(colonyAliasId.getId()).ifPresent(colony -> {
                            if ((colony.getTotalAnimals() != null) && (colony.getTotalAnimals() > 0)) {
                                colonies.add(colony);
                                members.put(colony.getId(), member);

                                String area = getColonyArea(colony, members);

                                if (coloniesAreas.get(area) == null) {
                                    coloniesAreas.put(area, new LinkedList<>());
                                }
                                coloniesAreas.get(area).add(colony);

                                if (colony.getTotalAnimals() != null) {
                                    totalAnimals.addAndGet(colony.getTotalAnimals());
                                }

                                if (colony.getTotalNeuteredAnimals() != null) {
                                    totalNeuteredAnimals.addAndGet(colony.getTotalNeuteredAnimals());
                                }
                            }
                        });
                    });
                }
            });
            args[TOTAL_MEMBERS] = String.valueOf(membersTotal.size());
            args[TOTAL_COLONIES] = String.valueOf(colonies.size());
            args[TOTAL_CATS] = String.valueOf(totalAnimals.get());
            args[TOTAL_NEUTERED_CATS] = String.valueOf(totalNeuteredAnimals.get());

            for (int idx = 0; idx < args.length; idx++) {
                if (args[idx] == null) {
                    args[idx] = "_______________";
                }
            }

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("documents/censo.docx").getFile());

            XWPFDocument document = new XWPFDocument(OPCPackage.open(new FileInputStream(file)));
            ReportUtils.replacePlaceholders(document, args);
            addTotalColonies(document, colonies, members, coloniesAreas);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            document.write(out);
            return out.toByteArray();

        } catch (Exception exception) {
            throw new ServiceException(exception);
        }
    }

    private void addTotalColonies(final XWPFDocument document, List<ColonyDto> colonies, Map<String, PersonDto> members, Map<String, List<ColonyDto>> coloniesAreas)
            throws Exception {
        XWPFTable tableTotals = document.getTables().get(0);
        XWPFTable tableDetail = document.getTables().get(1);

        AtomicInteger rowIdx = new AtomicInteger(1);
        colonies.stream()
                .sorted((c1, c2) -> getColonyArea(c1, members).compareTo(getColonyArea(c2, members)))
                .forEach(colony -> {
                    try {
                        XWPFTableRow row = tableDetail.insertNewTableRow(rowIdx.incrementAndGet());
                        addColonyInfo(row, colony, members);
                    } catch (Exception e) {
                        log.error("Unexpected exception adding a colony", e);
                    }
                });

        coloniesAreas.keySet().forEach(areaName -> {
            AtomicInteger totalAnimals = new AtomicInteger(0);
            AtomicInteger totalNeuteredAnimals = new AtomicInteger(0);
            Set<String> membersArea = new HashSet<>();

            members.values().forEach(member -> {
                String areaMember = member.getLocation() != null && StringUtils.hasLength(member.getLocation().getAreaName()) ?
                        member.getLocation().getAreaName() : AREA_EMPTY;
                if (areaMember.equals(areaName)) {
                    membersArea.add(member.getId());
                }
            });

            List<ColonyDto> coloniesInThisArea = coloniesAreas.get(areaName);
            coloniesInThisArea.forEach(colony -> {
                if (colony.getTotalAnimals() != null) {
                    totalAnimals.addAndGet(colony.getTotalAnimals());
                }

                if (colony.getTotalNeuteredAnimals() != null) {
                    totalNeuteredAnimals.addAndGet(colony.getTotalNeuteredAnimals());
                }
            });
            XWPFTableRow row = tableTotals.insertNewTableRow(2);
            row.createCell().setText("\t" + areaName);
            row.createCell().setText(membersArea.size() + "");
            row.createCell().setText(coloniesInThisArea.size() + "");
            row.createCell().setText(totalAnimals.get() + "");
            row.createCell().setText(totalNeuteredAnimals.get() + "");
        });

    }

    private LocationDto getColonyLocation(ColonyDto colony, Map<String, PersonDto> members) {
        PersonDto personDto = members.get(colony.getId());
        LocationDto location = colony.getLocation();

        if (location == null || !StringUtils.hasLength(location.getAreaName())) {
            location = personDto.getLocation();
        }

        return location;
    }

    private String getColonyArea(ColonyDto colony, Map<String, PersonDto> members) {
        LocationDto location = getColonyLocation(colony, members);

        return location != null && StringUtils.hasLength(location.getAreaName()) ? location.getAreaName() : AREA_EMPTY;
    }

    public static XWPFHyperlinkRun createHyperlinkRun(XWPFParagraph paragraph, String uri) {
        String rId = paragraph.getDocument().getPackagePart().addExternalRelationship(
                uri,
                XWPFRelation.HYPERLINK.getRelation()
        ).getId();

        CTHyperlink cthyperLink = paragraph.getCTP().addNewHyperlink();
        cthyperLink.setId(rId);
        cthyperLink.addNewR();

        return new XWPFHyperlinkRun(
                cthyperLink,
                cthyperLink.getRArray(0),
                paragraph
        );
    }

    private void addColonyInfo(final XWPFTableRow row, final ColonyDto colonyDto, final Map<String, PersonDto> members) throws Exception {
        row.createCell().setText(members.get(colonyDto.getId()).getFullName());

        LocationDto locationDto = getColonyLocation(colonyDto, members);
        if (locationDto == null) {
            row.createCell().setText("");
        } else {
            String areaName = getColonyArea(colonyDto, members);
            if (StringUtils.hasLength(locationDto.getStreet())) {
                if (StringUtils.hasLength(locationDto.getUrl())) {
                    XWPFParagraph paragraph = row.createCell().addParagraph();
                    XWPFHyperlinkRun hyperlinkrun = createHyperlinkRun(paragraph, locationDto.getUrl());
                    hyperlinkrun.setText(locationDto.getStreet());
                    hyperlinkrun.setColor("0000FF");
                    hyperlinkrun.setUnderline(UnderlinePatterns.SINGLE);
                } else {
                    row.createCell().setText(locationDto.getStreet());
                }
            } else {
                row.createCell().setText("");
            }
            row.createCell().setText(areaName.equalsIgnoreCase(AREA_EMPTY) ? "" : areaName);
        }

        row.createCell().setText(colonyDto.getTotalAnimals() + "");
        row.createCell().setText(colonyDto.getTotalNeuteredAnimals() + "");
    }

}
