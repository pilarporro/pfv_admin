package com.pfv.admin.core.services.places;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.core.model.places.ShopDto;
import com.pfv.admin.core.services.common.LocationMapper;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.places.Shop;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, LocationMapper.class },
        config = MapperConfiguration.class)
public abstract class ShopMapper extends com.pfv.admin.common.mappers.Mapper<ShopDto, Shop> {

}
