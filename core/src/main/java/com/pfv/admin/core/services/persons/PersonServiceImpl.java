package com.pfv.admin.core.services.persons;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.services.ServiceWithPhotosImpl;
import com.pfv.admin.common.utils.AppConstants;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.common.LocationMapper;
import com.pfv.admin.data.model.persons.Person;
import com.pfv.admin.data.repositories.persons.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class PersonServiceImpl extends ServiceWithPhotosImpl<PersonDto, Person, PersonRepository> implements PersonService {

    @Autowired
    private ColonyService colonyService;

    @Autowired
    private LocationMapper locationMapper;

    private void addDefaultColoy(Person person) {
        /* Default colony */
        if (CollectionUtils.isEmpty(person.getColonies()) && (person.getIsMember())) {
            ColonyDto colonyDto = colonyService.create(ColonyDto.builder()
                    .name(AppConstants.DEFAULT_COLONY)
                    .location(locationMapper.entityToDto(person.getLocation()))
                    .person(AliasIdLabelDto.ofIdAlias(person.getId(), person.getAlias()))
                    .totalAnimals(0)
                    .totalNeuteredAnimals(0).build());
            colonyDto.setActive(true);
            colonyDto.setMarkToDelete(false);
            colonyDto.setNotification(false);
            colonyDto .setPhotos(new LinkedList<>());
            ColonyDto colonyCreated = colonyService.create(colonyDto);

            person.setColonies(Arrays.asList(AliasId.ofIdAndAlias(colonyCreated.getId(), colonyCreated.getAlias())));
            repository.save(person);
        }
    }

    protected void postSave(final Person oldEntity, final Person newEntity) {
        addDefaultColoy(newEntity);
    }

    protected String createAlias(Person entity) {
        return entity.getName().replaceAll(" ", "").toLowerCase();
    };

    @Override
    public List<PersonDto> findMembers() {
        return repository.findByActiveIsTrueAndIsMemberIsTrue().stream()
                .map(mapper::entityToDto)
                .collect(Collectors.toList());
    }
}
