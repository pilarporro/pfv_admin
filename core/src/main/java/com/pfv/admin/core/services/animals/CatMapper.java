package com.pfv.admin.core.services.animals;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.animals.Cat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class },
        config = MapperConfiguration.class)
public abstract class CatMapper extends com.pfv.admin.common.mappers.Mapper<CatDto, Cat> {
    @Override
    @Mappings({
            @Mapping(source = "colony.id", target = "colonyId")
    })
    public abstract CatDto entityToDto(Cat entity);

    @Override
    @Mappings({
            @Mapping(source = "colonyId", target = "colony")
    })
    public abstract Cat dtoToEntity(CatDto dto);
}
