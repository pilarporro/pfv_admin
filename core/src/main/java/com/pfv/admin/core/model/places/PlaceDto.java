package com.pfv.admin.core.model.places;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.model.enums.PlaceType;
import com.pfv.admin.core.model.common.LocationDto;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ShopDto.class, name = PlaceType.SHOP_TYPE),
        @JsonSubTypes.Type(value = VetClinicDto.class, name = PlaceType.VET_CLINIC_TYPE)
}
)
@ApiModel(value = "place", subTypes = {
        ShopDto.class,
        VetClinicDto.class
}, discriminator = "type")
public abstract class PlaceDto extends EntityOperationsInfoDto {
    private String email;
    private String name;
    private String phone;
    private String notes;
    private LocationDto location;
    @NotNull
    private PlaceType type;
}
