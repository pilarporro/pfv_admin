package com.pfv.admin.core.services.places;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.PlaceType;
import com.pfv.admin.common.services.ServiceImpl;
import com.pfv.admin.core.model.places.PlaceDto;
import com.pfv.admin.data.model.places.Place;
import com.pfv.admin.data.repositories.places.PlaceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class PlaceServiceImpl extends ServiceImpl<PlaceDto, Place, PlaceRepository> implements PlaceService {

    @Override
    protected String createAlias(Place entity) {
        return entity.getName().replaceAll(" ", "").toLowerCase();
    };

    @Override public List<AliasIdLabelDto> findByTypeAndAliasStartsWith(PlaceType type, String alias) {
        return repository.findAllByActiveIsTrueAndTypeAndAliasStartsWithIgnoreCase(type, alias).stream()
                .map(person -> mapper.toResultDto(person))
                .collect(Collectors.toList());
    }
}
