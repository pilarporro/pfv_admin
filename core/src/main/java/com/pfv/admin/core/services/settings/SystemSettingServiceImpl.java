package com.pfv.admin.core.services.settings;


import com.pfv.admin.common.services.DataInit;
import com.pfv.admin.data.repositories.settings.SystemSettingRepository;
import com.pfv.admin.core.model.settings.SystemSettingDto;
import com.pfv.admin.common.services.ServiceImpl;
import com.pfv.admin.data.model.settings.SystemSetting;
import com.pfv.admin.data.model.settings.SystemSettingType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class SystemSettingServiceImpl extends ServiceImpl<SystemSettingDto, SystemSetting, SystemSettingRepository>
        implements SystemSettingService, DataInit {

    @Override
    public SystemSettingDto create(final SystemSettingDto dto) {
        return null;
    }

    @Override
    public void delete(final String id) {
        // It is not possible to delete a system setting
    }

    @Override
    public void init() {
        if (repository.count() == 0) {
            super.create(SystemSettingDto.builder()
                    .property(SystemSettingType.SAMPLE).value("3").build());
        }
    }

    @Override
    public Optional<SystemSettingDto> findByProperty(final SystemSettingType propertyName) {
        return Optional.ofNullable(mapper.entityToDto(repository.findByProperty(propertyName)));
    }

    @Override
    public Boolean getBooleanValue(final SystemSettingType propertyName) {
        return Boolean.valueOf(findByProperty(propertyName).get().getValue());
    }

    @Override
    public String getValue(final SystemSettingType propertyName) {
        return findByProperty(propertyName).get().getValue();
    }

    @Override
    public Integer getIntegerValue(final SystemSettingType propertyName) {
        return Integer.valueOf(findByProperty(propertyName).get().getValue());
    }
}
