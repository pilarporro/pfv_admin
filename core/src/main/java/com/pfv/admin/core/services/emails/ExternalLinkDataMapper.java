package com.pfv.admin.core.services.emails;

import com.pfv.admin.core.model.emails.ExternalLinkDataDto;
import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.data.model.emails.ExternalLinkData;
import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Base64;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        config = MapperConfiguration.class)
public abstract class ExternalLinkDataMapper extends com.pfv.admin.common.mappers.Mapper<ExternalLinkDataDto, ExternalLinkData> {
    @AfterMapping
    public void addExtraData(ExternalLinkData entity, @MappingTarget ExternalLinkDataDto dto) {
        dto.setAData(new String(Base64.getDecoder().decode(entity.getData())).split(";"));
    }
}
