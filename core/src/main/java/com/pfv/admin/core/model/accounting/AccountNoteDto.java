package com.pfv.admin.core.model.accounting;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.WithPhotosDto;
import com.pfv.admin.common.model.enums.AccountSubtype;
import com.pfv.admin.common.model.enums.AccountType;
import com.pfv.admin.common.utils.LocalDateDeserializer;
import com.pfv.admin.common.utils.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AccountNoteDto extends WithPhotosDto {
    private AccountType type;
    private AccountSubtype subtype;
    private String notes;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;
    private BigDecimal total;
    private BigDecimal bankBalance;
    private BigDecimal cashBalance;
    private BigDecimal grantBalance;

    private Boolean manualNote;

    private AliasIdLabelDto place;

    private AliasIdLabelDto person;

    private List<AliasIdLabelDto> activities;
}
