package com.pfv.admin.core.services.places;

import com.pfv.admin.common.mappers.MapperConfiguration;
import com.pfv.admin.core.model.places.VetClinicDto;
import com.pfv.admin.core.services.common.LocationMapper;
import com.pfv.admin.core.services.common.PhotoMapper;
import com.pfv.admin.data.model.places.VetClinic;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
        uses = { PhotoMapper.class, LocationMapper.class },
        config = MapperConfiguration.class)
public abstract class VetClinicMapper extends com.pfv.admin.common.mappers.Mapper<VetClinicDto, VetClinic> {

}
