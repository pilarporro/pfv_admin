package com.pfv.admin.core.services.municipalities;

import com.pfv.admin.common.exceptions.resources.NotFoundException;
import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.querydsl.PredicateBuilder;
import com.pfv.admin.common.services.DataInit;
import com.pfv.admin.common.services.ServiceImpl;
import com.pfv.admin.core.model.municipalities.MunicipalityDto;
import com.pfv.admin.data.model.municipalities.Area;
import com.pfv.admin.data.model.municipalities.Municipality;
import com.pfv.admin.data.repositories.municipalities.AreaRepository;
import com.pfv.admin.data.repositories.municipalities.MunicipalityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("municipalityService")
@Slf4j
public class MunicipalityServiceImpl extends ServiceImpl<MunicipalityDto, Municipality, MunicipalityRepository>
        implements MunicipalityService, DataInit {
    @Value("${config.country}")
    private String country;

    @Value("${config.municipality}")
    private String municipality;

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private AreaMapper areaMapper;

    private void initSpanishMunicipalities() {
        try {
            Set<String> provinces = new HashSet<>();
            Files.lines(Paths.get(this.getClass().getClassLoader().getResource("database/municipios_spain.csv").toURI())).forEach(line -> {
                String data[] = line.split(";");
                double[] position = new double[2];
                position[0] = Double.parseDouble(data[3]);
                position[1] = Double.parseDouble(data[4]);
                provinces.add(data[1]);
                create(MunicipalityDto.builder()
                        .population(Integer.parseInt(data[6]))
                        .province(data[1])
                        .city(data[2])
                        .location(position)
                        .name(String.format("%s (%s)", data[2], data[1])).build());
            });

            Files.lines(Paths.get(this.getClass().getClassLoader().getResource("database/areas_spain.csv").toURI())).forEach(line -> {
                String data[] = line.split(";");
                areaRepository.save(
                        Area.builder()
                                .municipality(municipalityRepository.findByCity(data[0]))
                                .name(data[1]).build());
            });
        } catch (Exception e) {
            log.error("Error loading municipalities", e);
        }
    }

    @Override
    public void init() {
        log.info("Start municipalities initialization  ...");
        //areaRepository.deleteAll();
        //municipalityRepository.deleteAll();
        if (repository.count() == 0) {
            switch (country) {
                case "es":
                    initSpanishMunicipalities();
                    break;
            }
        } else if (areaRepository.findByMunicipalityAndNameStartsWithIgnoreCase(
                municipalityRepository.findByCity("Valdemorillo").getId(), "Isla Blanca").size() == 0) {
            areaRepository.save(
                    Area.builder()
                            .municipality(municipalityRepository.findByCity("Valdemorillo"))
                            .name("Isla Blanca").build());
        }
    }

    @Override
    public List<AliasIdLabelDto> getMunipalities(String name) {
        return repository.findByNameStartsWithIgnoreCase(name).stream()
                .map(municipality -> ((MunicipalityMapper) mapper).toResultDto(municipality))
                .collect(Collectors.toList());
    }

    @Override
    public List<AliasIdLabelDto> getAreas(String municipalityId, String name) {
        return areaRepository.findByMunicipalityAndNameStartsWithIgnoreCase(municipalityId, name).stream()
                .map(area -> areaMapper.toResultDto(area))
                .collect(Collectors.toList());
    }

    @Override
    public AliasIdLabelDto findMinimalByName(String name) {
        return mapper.toResultDto(repository.findByCityIgnoreCase(name));
    }

    @Override
    public AliasIdLabelDto findAreaMinimalById(String id) {
        return areaMapper.toResultDto(areaRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    @Override public MunicipalityDto getDefaultMunicipality() {
        return mapper.entityToDto(repository.findByCityIgnoreCase(municipality));
    }

    @Override
    public PredicateBuilder<Municipality> buildPredicateBuilder() {
        return new PredicateBuilder<>(Municipality.class, "municipality");
    }

    protected String createAlias(Municipality entity) {
        return entity.getName().replaceAll(" ", "").toLowerCase();
    };
}
