package com.pfv.admin.core.services.settings;

import com.pfv.admin.core.model.settings.SystemSettingDto;
import com.pfv.admin.common.services.Service;
import com.pfv.admin.data.model.settings.SystemSetting;
import com.pfv.admin.data.model.settings.SystemSettingType;

import java.util.Optional;

/**
 * System settings service interface.
 */
public interface SystemSettingService extends Service<SystemSettingDto, SystemSetting> {
    Optional<SystemSettingDto> findByProperty(SystemSettingType propertyName);

    Boolean getBooleanValue(SystemSettingType propertyName);
    String getValue(SystemSettingType propertyName);
    Integer getIntegerValue(SystemSettingType propertyName);
}
