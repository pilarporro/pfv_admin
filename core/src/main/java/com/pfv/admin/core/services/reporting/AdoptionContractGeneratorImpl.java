package com.pfv.admin.core.services.reporting;

import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.services.animals.AnimalService;
import com.pfv.admin.core.services.persons.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

import static com.pfv.admin.core.services.reporting.ReportUtils.LONG_DATE_FORMATTER;

@Component
@Slf4j
public class AdoptionContractGeneratorImpl implements AdoptionContractGenerator {
    public static final DateTimeFormatter BIRTHDATE_FORMATTER = LONG_DATE_FORMATTER;

    private static final int CONTRACT_DATE = 0;
    private static final int ADOPTER_NAME = 1;
    private static final int ADOPTER_IDNUMBER = 2;
    private static final int ADOPTER_PHONE = 3;
    private static final int ADOPTER_EMAIL = 4;
    private static final int ADOPTER_ADDRESS = 5;
    private static final int PERSON_NAME = 6;
    private static final int PERSON_IDNUMBER = 7;
    private static final int ANIMAL_NAME = 8;
    private static final int ANIMAL_GENDER = 9;
    private static final int ANIMAL_BIRTHDATE = 10;
    private static final int ANIMAL_COLOR = 11;
    private static final int SIGN_PERSON_NAME = 12;
    private static final int SIGN_ADOPTER_NAME = 13;

    @Autowired
    private PersonService personService;

    @Autowired
    private AnimalService animalService;

    @Override public byte[] generateAdoptionContract(AdoptionDto adoptionDto) {
        try {
            String args[] = new String[14];
            /* - DE UNA PARTE,  el ADOPTANTE: XX2XX, mayor de edad, con DNI: XX3XX,  teléfono móvil: XX4XX, correo electrónico: XXXXX5 y domicilio en  XXXXX6. */
            args[CONTRACT_DATE] = LONG_DATE_FORMATTER.format(adoptionDto.getDate());

            personService.findById(adoptionDto.getAdopter().getId()).ifPresent(adopter -> {
                args[ADOPTER_NAME] = adopter.getFullName();
                args[SIGN_ADOPTER_NAME] = adopter.getFullName();
                args[ADOPTER_IDNUMBER] = adopter.getIdNumber();
                args[ADOPTER_PHONE] = adopter.getPhone();
                args[ADOPTER_EMAIL] = adopter.getEmail();
                if (adopter.getLocation() != null) {
                    args[ADOPTER_ADDRESS] = adopter.getLocation().getFullAddress();
                }
            });

            /* - Y DE OTRA PARTE XXXXX7, con DNI XXXXX8, en representación de la Asociación Protección Felina Valdemorillo, con CIF: G-87640736 */
            personService.findById(adoptionDto.getPerson().getId()).ifPresent(person -> {
                args[PERSON_NAME] = person.getFullName();
                args[SIGN_PERSON_NAME] = person.getFullName();
                args[PERSON_IDNUMBER] = person.getIdNumber();
            });

            animalService.findById(adoptionDto.getAnimal().getId()).ifPresent(animal -> {
                args[ANIMAL_NAME] = animal.getName();
                if (animal.getGender() != null) {
                    args[ANIMAL_GENDER] = ResourceUtil.getMessage(animal.getGender().getMessageKey());
                }
                if (animal.getBirthDate() != null) {
                    args[ANIMAL_BIRTHDATE] = animal.getBirthDate().format(BIRTHDATE_FORMATTER);
                }
                if (animal.getColor() != null) {
                    args[ANIMAL_COLOR] = ResourceUtil.getMessage(animal.getColor().getMessageKey());
                }
            });

            for (int idx = 0; idx < args.length; idx++) {
                if (args[idx] == null) {
                    args[idx] = "_______________";
                }
            }

            return generate(args);
        } catch (Exception exception) {
            throw new ServiceException(exception);
        }
    }

    public byte[] generate(String... args) throws IOException, InvalidFormatException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("documents/adoption_contract.docx").getFile());

        XWPFDocument document = new XWPFDocument(OPCPackage.open(new FileInputStream(file)));
        ReportUtils.replacePlaceholders(document, args);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.write(out);
        return out.toByteArray();
    }

}
