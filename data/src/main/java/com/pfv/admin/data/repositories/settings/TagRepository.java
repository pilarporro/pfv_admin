/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.settings;

import com.pfv.admin.common.model.enums.TagType;
import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.settings.Tag;

import java.util.List;

public interface TagRepository extends Repository<Tag> {
    List<Tag> findByTypeAndNameStartsWithIgnoreCaseOrderByUsesDesc(TagType type, String name);
    Tag findFirstByTypeAndName(TagType type, String name);
    Long countByTypeAndNameIgnoreCase(TagType type, String name);
    List<Tag> findByTypeAndTermsRegexOrderByUsesDesc(TagType type, String like);
}
