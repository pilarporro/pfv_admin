package com.pfv.admin.data.model.activities;

import com.pfv.admin.common.model.enums.ActivityType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "activity")
public class OtherActivity extends Activity {

}
