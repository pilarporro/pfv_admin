package com.pfv.admin.data.model.places;

import com.pfv.admin.common.model.entity.AuditableEntity;
import com.pfv.admin.common.model.enums.PlaceType;
import com.pfv.admin.data.model.common.Location;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document
public abstract class Place extends AuditableEntity {
    @Indexed
    private PlaceType type;
    private String email;
    private String name;
    private String phone;
    private String notes;
    private Location location;
}
