/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.model.municipalities;

import com.pfv.admin.common.model.entity.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Municipality extends AuditableEntity {
    @Indexed
    private String name;
    private String province;
    @Indexed
    private String city;
    @Indexed
    private double[] location;
    private Integer population;
}
