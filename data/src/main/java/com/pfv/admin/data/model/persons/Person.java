package com.pfv.admin.data.model.persons;

import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import com.pfv.admin.common.model.enums.PaymentPeriodicity;
import com.pfv.admin.data.model.common.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Person extends AuditableEntityWithPhotos {
    private String email;
    private String name;
    private String lastName;
    private String phone;
    private String bankAccount;
    private String notes;
    private String bankMatcher;
    private Location location;


    private String idNumber;
    @Indexed
    private Boolean isMember;
    private LocalDate memberFrom;
    private LocalDate memberTo;
    private LocalDate nextPayment;
    private PaymentPeriodicity paymentPeriodicity;

    private List<AliasId> colonies;

    public String getFullName() {
        return String.format("%s%s", this.getName(), StringUtils.hasLength(this.getLastName()) ? " " + this.getLastName() : "");
    }
}
