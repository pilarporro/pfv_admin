package com.pfv.admin.data.model.activities;

import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.model.enums.PaymentType;
import com.pfv.admin.common.model.enums.VisitClinicType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;


@Getter @Setter
@Document
public abstract class Activity extends AuditableEntityWithPhotos {
    @Indexed
    private ActivityType type;

    private String notes;
    private List<String> tags;

    private VisitClinicType visitClinicType;

    @Indexed
    private LocalDate date;

    @Indexed
    private AliasId person;

    @Indexed
    private List<AliasId> animals;

    @Indexed
    private List<AliasId> persons;

    @Indexed
    private AliasId animal;

    @Indexed
    private AliasId vetClinic;

    @Indexed
    private AliasId caller;

    @Indexed
    private AliasId newMember;

    @Indexed
    private AliasId adopter;

    @Indexed
    private AliasId place;

    @Indexed
    private AliasId keeper;

    private PaymentType paymentType;
    private String invoiceRef;
    private String chip;

    private AliasId colony;


}
