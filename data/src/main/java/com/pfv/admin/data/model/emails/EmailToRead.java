package com.pfv.admin.data.model.emails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmailToRead {
    private String from;
    private String to;
    private String subject;
    private String message;
}
