package com.pfv.admin.data.model.emails;

import com.pfv.admin.common.model.entity.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Document
public class EmailToSend extends AuditableEntity {
    private EmailAddress from;
    private EmailAddress to;
    private String subject;
    private String message;
    private String smtpHost;
    private String smtpPort;
    private String smtpUsername;
    private String smtpPassword;
    private Integer retry;
}
