package com.pfv.admin.data.model.persons;

import com.pfv.admin.common.model.entity.AuditableEntity;
import com.pfv.admin.common.model.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class User extends AuditableEntity {
    private List<Role> roles = new ArrayList<>();
    @Indexed(unique = true, name = "username", direction = IndexDirection.ASCENDING, dropDups = true)
    private String username;
    private String password;
    private String email;
    private String phone;
    private String name;
    private String lastName;
    private Person person;
}
