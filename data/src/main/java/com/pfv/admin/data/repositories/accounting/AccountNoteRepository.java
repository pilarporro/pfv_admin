/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.accounting;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.accounting.AccountNote;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface AccountNoteRepository extends Repository<AccountNote> {

    List<AccountNote> findAllByActiveIsTrueOrderByDate();

    Long countByDateAndNotesAndTotal(LocalDate date, String notes, BigDecimal total);

    List<AccountNote> findByActiveIsTrueAndDateBetweenOrderByDate(LocalDate from, LocalDate to);

    AccountNote findFirstByActiveIsTrueAndDateIsBeforeOrderByDateDesc(LocalDate from);

}
