/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.municipalities;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.municipalities.Area;

import java.util.List;

public interface AreaRepository extends Repository<Area> {
    List<Area> findByMunicipalityAndNameStartsWithIgnoreCase(String municipalityId, String name);

}
