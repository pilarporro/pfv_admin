package com.pfv.admin.data.model.settings;

import com.pfv.admin.common.model.entity.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class SystemSetting extends AuditableEntity {
    private SystemSettingType property;
    private String value;
}
