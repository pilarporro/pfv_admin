package com.pfv.admin.data.model.animals;

import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import com.pfv.admin.data.model.common.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Colony extends AuditableEntityWithPhotos {
    @Indexed(unique = true, name = "name", direction = IndexDirection.ASCENDING, dropDups = true)
    private String name;
    private String notes;

    private Location location;

    private Integer totalAnimals;
    private Integer totalNeuteredAnimals;

    private Integer totalFemaleAnimals;
    private Integer totalNeuteredFemaleAnimals;

    private AliasId person;

    private LocalDate initDate;
}
