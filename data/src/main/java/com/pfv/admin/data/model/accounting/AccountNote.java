package com.pfv.admin.data.model.accounting;

import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import com.pfv.admin.common.model.enums.AccountSubtype;
import com.pfv.admin.common.model.enums.AccountType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class AccountNote extends AuditableEntityWithPhotos {
    private AccountType type;
    private AccountSubtype subtype;
    private String notes;
    private LocalDate date;
    private BigDecimal total;

    private BigDecimal bankBalance;
    private BigDecimal cashBalance;
    private BigDecimal grantBalance;

    @Indexed
    private AliasId place;

    @Indexed
    private AliasId person;

    @Indexed
    private List<AliasId> activities;
}
