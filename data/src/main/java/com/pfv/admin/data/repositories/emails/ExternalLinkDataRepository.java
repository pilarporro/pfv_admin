/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.emails;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.emails.ExternalLinkData;

import java.time.LocalDateTime;
import java.util.List;

public interface ExternalLinkDataRepository extends Repository<ExternalLinkData> {
    ExternalLinkData findFirstByKeyAndCreatedAfter(String key, LocalDateTime expireOn);
    List<ExternalLinkData> findAllByCreatedBefore(LocalDateTime expireOn);

}
