/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.animals;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.animals.Animal;

import java.util.List;

public interface AnimalRepository extends Repository<Animal> {
    List<Animal> findAllByActiveIsTrueAndNameStartsWithIgnoreCase(String name);

}
