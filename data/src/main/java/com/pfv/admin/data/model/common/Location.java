package com.pfv.admin.data.model.common;

import com.pfv.admin.common.model.enums.Country;
import com.pfv.admin.data.model.municipalities.Area;
import com.pfv.admin.data.model.municipalities.Municipality;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Location {
    private String street;
    private String number;
    private String postalCode;
    @DBRef
    private Area area;
    @DBRef
    private Municipality municipality;
    private Country country;
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private double[] geoLocation;

    private String placeId;
    private String url;

    public String getFullAddress() {
        return String.format("%s%s%s%s",
                this.getStreet() != null ? this.getStreet() : "",
                this.getNumber() != null ? " " + this.getNumber() : "",
                this.getPostalCode() != null ? ", " + this.getPostalCode() : "",
                this.getMunicipality() != null? ", " + this.getMunicipality().getCity() : ""
        );
    }
}
