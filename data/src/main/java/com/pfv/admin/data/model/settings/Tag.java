package com.pfv.admin.data.model.settings;

import com.pfv.admin.common.model.entity.AuditableEntity;
import com.pfv.admin.common.model.enums.TagType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document
@AllArgsConstructor
@NoArgsConstructor
public class Tag extends AuditableEntity {
    @Indexed
    private TagType type;
    private String name;
    private String terms;
    private Integer uses;
}
