/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.activities;

import com.pfv.admin.common.model.enums.ActivityType;
import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.activities.Activity;

import java.time.LocalDate;
import java.util.List;

public interface ActivityRepository extends Repository<Activity> {

    Activity findFirstByActiveIsTrueAndTypeAndDateAndPlaceAlias(ActivityType type, LocalDate date, String placeAlias);
    List<Activity> findByActiveIsTrueAndTypeAndDateBetweenOrderByDate(ActivityType type, LocalDate from, LocalDate to);
    List<Activity> findByActiveIsTrueAndTypeAndInvoiceRefIgnoreCase(ActivityType type, String invoiceRef);

}
