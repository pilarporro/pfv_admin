/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.activities;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.activities.Adoption;

public interface AdoptionRepository extends Repository<Adoption> {

}
