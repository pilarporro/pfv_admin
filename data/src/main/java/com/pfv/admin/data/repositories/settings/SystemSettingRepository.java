package com.pfv.admin.data.repositories.settings;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.settings.SystemSetting;
import com.pfv.admin.data.model.settings.SystemSettingType;

public interface SystemSettingRepository extends Repository<SystemSetting> {

    SystemSetting findByProperty(SystemSettingType propertyName);
}
