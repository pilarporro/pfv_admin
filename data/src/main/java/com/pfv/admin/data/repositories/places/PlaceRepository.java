/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.places;

import com.pfv.admin.common.model.enums.PlaceType;
import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.places.Place;

import java.util.List;

public interface PlaceRepository extends Repository<Place> {
    List<Place> findAllByActiveIsTrueAndAliasStartsWithIgnoreCase(String alias);
    List<Place> findAllByActiveIsTrueAndTypeAndAliasStartsWithIgnoreCase(PlaceType type, String alias);

}
