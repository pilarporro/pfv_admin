package com.pfv.admin.data.model.persons;

import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Trap extends AuditableEntityWithPhotos {
    private String name;
    private String notes;
    private AliasId owner;
    private AliasId person;
    private LocalDate usingFrom;
}
