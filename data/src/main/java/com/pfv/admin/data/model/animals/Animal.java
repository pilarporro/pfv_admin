package com.pfv.admin.data.model.animals;

import com.pfv.admin.common.model.entity.AliasId;
import com.pfv.admin.common.model.entity.AuditableEntityWithPhotos;
import com.pfv.admin.common.model.enums.AnimalStatus;
import com.pfv.admin.common.model.enums.AnimalType;
import com.pfv.admin.common.model.enums.Color;
import com.pfv.admin.common.model.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Document
public abstract class Animal extends AuditableEntityWithPhotos {
    @Indexed
    private AnimalType type;
    private String name;
    private LocalDate birthDate;
    private LocalDate deathDate;
    private Gender gender;
    private Color color;
    private AnimalStatus animalStatus;

    @Indexed
    private List<String> tags;
    private String notes;

    @DBRef
    private Colony colony;

    @Indexed
    private AliasId owner;

    @Indexed
    private AliasId adopter;
    private LocalDate adoptionDate;

    @Indexed
    private AliasId keeper;

    private LocalDate pickUpDate;
    private Boolean neutered;
}
