/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.data.repositories.municipalities;

import com.pfv.admin.common.repositories.Repository;
import com.pfv.admin.data.model.municipalities.Municipality;

import java.util.List;

public interface MunicipalityRepository extends Repository<Municipality> {
    List<Municipality> findByNameStartsWithIgnoreCase(String name);

    Municipality findFirstByNameIgnoreCase(String name);

    Municipality findByCityIgnoreCase(String name);

    Municipality findByCity(String city);
}
