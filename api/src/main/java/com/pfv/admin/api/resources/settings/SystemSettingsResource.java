package com.pfv.admin.api.resources.settings;

import com.pfv.admin.core.services.settings.SystemSettingService;
import com.pfv.admin.common.exceptions.resources.ResourceNotFoundException;
import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchFields;
import com.pfv.admin.common.resources.Resource;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.data.model.settings.SystemSettingType;
import com.pfv.admin.core.model.settings.SystemSettingDto;
import com.pfv.admin.data.model.settings.SystemSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * System settings controller.
 */
@RestController
@RequestMapping(value = ApiUris.API_SETTINGS_SYSTEM_PROPERTIES_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SystemSettingsResource extends Resource<SystemSettingDto, SystemSetting, SystemSettingService> {
    @Autowired
    private SystemSettingService systemSettingService;

    @GetMapping
    public Page<SystemSettingDto> getListWithPaging(final @RequestParam("page") Integer page, final @RequestParam("size") Integer size,
                                                    final @RequestParam(name = "sortField", required = false) String sortField,
                                                    final @RequestParam(name = "sortOrder", required = false) Integer sortOrder,
                                                    final @RequestParam(value = "filters", required = false) @SearchFields List<SearchCriteria> filters) {
        return getAllWithFiltersAndPaging(page, size, sortField, sortOrder, filters);
    }

    @GetMapping("{property}")
    public ResponseEntity<SystemSettingDto> getSystemSettingByProperty(final @PathVariable SystemSettingType property) {
        return new ResponseEntity<>(checkIfSystemSettingsExists(property), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<SystemSettingDto> updateSystemSetting(final @PathVariable String id,
                                                                final @RequestBody @Valid SystemSettingDto paramSystemSettingDto,
                                                                final BindingResult result) {
        return super.genericUpdate(id, paramSystemSettingDto, result);
    }

    private SystemSettingDto checkIfSystemSettingsExists(final SystemSettingType property) {
        Optional<SystemSettingDto> opSystemSetting = systemSettingService.findByProperty(property);
        if (!opSystemSetting.isPresent()) {
            throw new ResourceNotFoundException("systemSetting");
        }
        return opSystemSetting.get();
    }
}
