package com.pfv.admin.api.resources.settings;

import com.pfv.admin.core.services.settings.PropertyService;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.settings.PropertyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(value = ApiUris.API_SETTINGS_PROPERTIES_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PropertyResource  {
    @Autowired
    private PropertyService service;

    public ResponseEntity<PropertyDto> loadProperty(final String property) {
        Optional<PropertyDto> opProperty = service.getProperty(property);

        if (opProperty.isPresent()) {
            return ResponseEntity.ok(opProperty.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("{property}")
    public ResponseEntity<PropertyDto> getProperty(final @PathVariable("property") String property) {
        return loadProperty("config." + property);
    }


}
