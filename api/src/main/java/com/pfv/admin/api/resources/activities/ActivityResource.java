package com.pfv.admin.api.resources.activities;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchFields;
import com.pfv.admin.common.resources.Resource;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.utils.AppConstants;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.services.activities.ActivityService;
import com.pfv.admin.data.model.activities.Activity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(value = ApiUris.API_ACTIVITIES_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ActivityResource extends Resource<ActivityDto, Activity, ActivityService> {
    @GetMapping
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public Page<ActivityDto> getListWithPaging(final @RequestParam("page") Integer page, final @RequestParam("size") Integer size,
            final @RequestParam(name = "sortField", required = false) String sortField,
            final @RequestParam(name = "sortOrder", required = false) Integer sortOrder,
            final @RequestParam(value = "filters", required = false) @SearchFields List<SearchCriteria> filters) {
        return getAllWithFiltersAndPaging(page, size, sortField, sortOrder, filters);
    }

    @GetMapping("{id}")
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public ResponseEntity<ActivityDto> getById(final @PathVariable String id) {
        return genericGetById(id);
    }

    @PostMapping
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public ResponseEntity<ActivityDto> create(final @RequestBody @Valid ActivityDto dto,
            final BindingResult result) {
        return genericCreate(dto, result);
    }

    @PutMapping("{id}")
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public ResponseEntity<ActivityDto> update(final @PathVariable("id") String id,
            final @RequestBody @Valid ActivityDto dto,
            final BindingResult result) {
        return genericUpdate(id, dto, result);
    }

    @DeleteMapping("{id}")
    @PreAuthorize(value = AppConstants.AUTHORIZE_ADMIN)
    public ResponseEntity<ActivityDto> delete(final @PathVariable("id") String id) {
        return genericDelete(id);
    }

    @GetMapping(value = "{id}/contract", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_PDF_VALUE})
    public ResponseEntity<byte[]> generateAdoptionContrat(final @PathVariable String id) {
        log.debug("generateAdoptionContrat: {}", id);
        AdoptionDto adoptionDto = (AdoptionDto) getIfItemExists(id);

        byte[] wordFile = this.service.generateAdoptionContract(adoptionDto);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        String filename = String.format("contrato_%s_%s.docx", adoptionDto.getAdopter().getAlias(), adoptionDto.getAnimal().getAlias());
        // Content-Disposition "attachment" makes sure Swagger shows a download link
        headers.set("Content-Disposition", "attachment; filename=" + filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(wordFile, headers, HttpStatus.OK);
        return response;
    }
}
