package com.pfv.admin.api.resources.logs;

import com.pfv.admin.common.utils.AppUtil;
import com.pfv.admin.core.model.logs.LogDto;
import com.pfv.admin.common.utils.ApiUris;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j(topic = "clientlogger")
@RestController
@RequestMapping(value = ApiUris.API_LOGS_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class LogResource {

    @PostMapping()
    public void addLog(final @RequestBody @Valid LogDto dto) {
        if (dto.getLevel() == LogLevel.INFO) {
            log.info(getLogWithUser(dto));
        } else if (dto.getLevel() == LogLevel.DEBUG) {
            log.debug(getLogWithUser(dto));
        } else if (dto.getLevel() == LogLevel.ERROR) {
            log.error(getLogWithUser(dto));
        }
    }

    private String getLogWithUser(LogDto dto) {
        User user = AppUtil.getAuthorizedUser();

        if (user != null) {
            return user.getUsername() + ":: " + dto.getValue();
        } else {
            return  dto.getValue();
        }
    }
}
