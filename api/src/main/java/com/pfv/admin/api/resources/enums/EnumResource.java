package com.pfv.admin.api.resources.enums;

import com.pfv.admin.common.model.dto.EnumTranslationDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.resources.BaseEnumResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = ApiUris.API_BASE + "/enums", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EnumResource extends BaseEnumResource {
    @GetMapping(value = "{type}")
    public ResponseEntity<List<EnumTranslationDto>> getEnumTypeTranslations(@PathVariable(value = "type") final String type) {
        return super.getEnumTypeTranslations(type);
    }

    @GetMapping
    public Map<String, String> getAllTypes() {
        return super.getAllTypes();
    }
}