package com.pfv.admin.api.resources.settings;

import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.utils.ResourceUtil;
import com.pfv.admin.core.model.settings.LoadRequestDto;
import com.pfv.admin.core.model.settings.LoadResponseDto;
import com.pfv.admin.core.services.accounting.AccountNoteService;
import com.pfv.admin.core.services.settings.DataLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping(value = ApiUris.API_LOAD_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DataLoaderResource {
    @Autowired
    private DataLoaderService importService;

    @Autowired
    private AccountNoteService accountNoteService;

    @PostMapping
    public ResponseEntity<LoadResponseDto> load(final @RequestBody @Valid LoadRequestDto dto,
            final BindingResult result) {
        ResourceUtil.checkFieldErrors(result);
        return new ResponseEntity<>(importService.load(dto), HttpStatus.CREATED);
    }

    @PostMapping("account-notes")
    public ResponseEntity<LoadResponseDto> importAccountNotes(@RequestParam("file") final MultipartFile file) throws IOException {
        return ResponseEntity.ok(accountNoteService.importAccountNotes(file));
    }
}


