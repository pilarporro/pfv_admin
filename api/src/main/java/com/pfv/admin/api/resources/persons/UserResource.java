package com.pfv.admin.api.resources.persons;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchFields;
import com.pfv.admin.common.resources.Resource;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.utils.AppConstants;
import com.pfv.admin.common.utils.AppUtil;
import com.pfv.admin.core.model.persons.UserDto;
import com.pfv.admin.core.services.persons.UserService;
import com.pfv.admin.data.model.persons.User;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = ApiUris.API_USERS_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserResource extends Resource<UserDto, User, UserService> {
    @GetMapping
    @PreAuthorize(value = AppConstants.AUTHORIZE_ADMIN)
    public Page<UserDto> getListWithPaging(final @RequestParam("page") Integer page, final @RequestParam("size") Integer size,
            final @RequestParam(name = "sortField", required = false) String sortField,
            final @RequestParam(name = "sortOrder", required = false) Integer sortOrder,
            final @RequestParam(value = "filters", required = false) @SearchFields List<SearchCriteria> filters) {
        return getAllWithFiltersAndPaging(page, size, sortField, sortOrder, filters);
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDto> getById(final @PathVariable String id) {
        return service.findById(id).map(user -> {
            if (AppUtil.hasAuthorization(user.getUsername())) {
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else {
                return new ResponseEntity<UserDto>(HttpStatus.NOT_FOUND);
            }
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
