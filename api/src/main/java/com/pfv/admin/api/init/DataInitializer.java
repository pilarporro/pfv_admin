/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.api.init;

import com.pfv.admin.common.services.DataInit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class DataInitializer {
    @Autowired
    private List<DataInit> dataInits;

    @EventListener(value = ContextRefreshedEvent.class)
    public void init() {
        dataInits.forEach(dataInit -> dataInit.init());
    }
}
