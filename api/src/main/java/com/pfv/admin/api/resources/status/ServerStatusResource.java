package com.pfv.admin.api.resources.status;

import com.pfv.admin.core.model.status.ServerStatusDto;
import com.pfv.admin.core.services.status.ServerStatusService;
import com.pfv.admin.common.utils.ApiUris;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = ApiUris.API_SERVER_STATUS, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ServerStatusResource  {
    @Autowired
    private ServerStatusService service;

    @GetMapping("{userId}")
    public ServerStatusDto getStatus(final @PathVariable("userId") String userId) {
        return service.getServerStatus(userId);
    }
}
