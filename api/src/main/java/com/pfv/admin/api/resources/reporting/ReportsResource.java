package com.pfv.admin.api.resources.reporting;

import com.pfv.admin.common.exceptions.services.ServiceException;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.common.CatFile;
import com.pfv.admin.core.services.reporting.ActivityReportGenerator;
import com.pfv.admin.core.services.reporting.ColoniesExcelReportGenerator;
import com.pfv.admin.core.services.reporting.AccountsReportGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.pfv.admin.common.utils.AppConstants.REQUEST_DATE_FORMAT;

@RestController
@RequestMapping(value = ApiUris.API_REPORTS_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ReportsResource {

    @Autowired
    private ActivityReportGenerator activityReportGenerator;

    @Autowired
    private ColoniesExcelReportGenerator coloniesExcelReportGenerator;

    @Autowired
    private AccountsReportGenerator accountsReportGenerator;

    @GetMapping(value = "activities")
    public ResponseEntity<ByteArrayResource> getReport(
            final @RequestParam("from") @DateTimeFormat(pattern = REQUEST_DATE_FORMAT) LocalDate from,
            final @RequestParam("to") @DateTimeFormat(pattern = REQUEST_DATE_FORMAT) LocalDate to) {
        return ResponseEntity
                .ok()
                .contentType(new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8))
                .header("Content-Disposition", String.format("attachment; filename=\"%s\"", "report.docx"))
                .header("Content-Transfer-Encoding", "binary")
                .header("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                .header("Expires", "0")
                .body(new ByteArrayResource(activityReportGenerator.generateActivityReport(from, to)));
    }

    @GetMapping(value = "accounts")
    public ResponseEntity<ByteArrayResource> getAccountsReport(
            final @RequestParam("from") @DateTimeFormat(pattern = REQUEST_DATE_FORMAT) LocalDate from,
            final @RequestParam("to") @DateTimeFormat(pattern = REQUEST_DATE_FORMAT) LocalDate to) {
        return ResponseEntity
                .ok()
                .contentType(new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8))
                .header("Content-Disposition", String.format("attachment; filename=\"%s\"", "report.docx"))
                .header("Content-Transfer-Encoding", "binary")
                .header("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                .header("Expires", "0")
                .body(new ByteArrayResource(accountsReportGenerator.generateAccountsReport(from, to)));
    }


    @GetMapping(value = "emailcastrations")
    public ResponseEntity<ByteArrayResource> getReport(
            final @RequestParam("invoiceRef") String invoiceRef) {

        return ResponseEntity
                .ok()
                .contentType(new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8))
                .header("Content-Disposition", String.format("attachment; filename=\"%s\"", "resumen_y_fichas.zip"))
                .header("Content-Transfer-Encoding", "binary")
                .header("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                .header("Expires", "0")
                .body(new ByteArrayResource(zipBytes(
                        activityReportGenerator.generateCatFiles(invoiceRef),
                        activityReportGenerator.generateEmailCastrationsReport(invoiceRef))));
                //.body(new ByteArrayResource());
    }

    public static byte[] zipBytes(List<CatFile> catFiles, byte[] email) {

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ZipOutputStream zos = new ZipOutputStream(baos);


            for (CatFile catFile : catFiles) {
                ZipEntry entry = new ZipEntry(catFile.getName() + ".docx");
                entry.setSize(catFile.getContent().length);
                zos.putNextEntry(entry);
                zos.write(catFile.getContent());
                zos.closeEntry();
            }

            ZipEntry entry = new ZipEntry("resumen.docx");
            entry.setSize(email.length);
            zos.putNextEntry(entry);
            zos.write(email);
            zos.closeEntry();

            zos.close();
            return baos.toByteArray();

        } catch (IOException exception) {
            throw new ServiceException(exception);
        }
    }

    @GetMapping(value = "colonies")
    public ResponseEntity<ByteArrayResource> getColoniesReport() {
        return ResponseEntity
                .ok()
                .contentType(new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8))
                .header("Content-Disposition", String.format("attachment; filename=\"%s\"", "censo.docx"))
                .header("Content-Transfer-Encoding", "binary")
                .header("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                .header("Expires", "0")
                .body(new ByteArrayResource(activityReportGenerator.generateColonyReport()));
    }

    @GetMapping(value = "colonies-excel")
    public ResponseEntity<ByteArrayResource> getColoniesReportExcel() {
        return ResponseEntity
                .ok()
                .contentType(new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8))
                .header("Content-Disposition", String.format("attachment; filename=\"%s\"", "censo.xlsx"))
                .header("Content-Transfer-Encoding", "binary")
                .header("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
                .header("Expires", "0")
                .body(new ByteArrayResource(coloniesExcelReportGenerator.generateColonyReport()));
    }
}


