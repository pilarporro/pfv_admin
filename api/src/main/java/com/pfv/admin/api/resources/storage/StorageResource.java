package com.pfv.admin.api.resources.storage;

import com.pfv.admin.common.model.dto.PhotoDto;
import com.pfv.admin.common.services.StorageService;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.utils.ResourceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * StorageResource.
 */
@RestController
@RequestMapping(value = ApiUris.API_STORAGE_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
// @MultipartConfig(maxFileSize = 10737418240L, maxRequestSize = 10737418240L, fileSizeThreshold = 52428800)
public class StorageResource {
    @Autowired
    private StorageService storageService;

    @PostMapping
    public ResponseEntity<PhotoDto> handleFileUpload(@RequestParam("file") final MultipartFile file) {
        if (file.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        } else {
            final PhotoDto dto = storageService.saveTemporary(file);
            return ResponseEntity
                    .ok()
                    .body(dto);
        }
    }

    @PostMapping("clone")
    public ResponseEntity<PhotoDto> cloneStorage(final @RequestBody @Valid PhotoDto dto,
                                                   final BindingResult result) {
        ResourceUtil.checkFieldErrors(result);
        return ResponseEntity.ok().body(storageService.clonePhoto(dto));
    }
}
