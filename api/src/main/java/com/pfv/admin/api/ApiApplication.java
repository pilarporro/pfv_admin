package com.pfv.admin.api;

import com.pfv.admin.common.utils.AppUtil;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = { "com.pfv.admin" })
public class ApiApplication {
    public static void main(String[] args) {
        final SpringApplication app = new SpringApplication(ApiApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        final Environment env = app.run(args).getEnvironment();
        TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(env.getProperty("timezone"))));
        AppUtil.printBanner(
                env.getProperty("spring.application.name")
                        + " started on port "
                        + env.getProperty("server.port")
                        + " for profile(s): "
                        + Arrays.toString(env.getActiveProfiles())
                        + " with timezone "
                        + TimeZone.getDefault().getDisplayName()
        );
    }
}

