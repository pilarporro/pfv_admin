package com.pfv.admin.api.resources.animals;

import com.pfv.admin.common.model.querydsl.SearchCriteria;
import com.pfv.admin.common.model.querydsl.SearchFields;
import com.pfv.admin.common.resources.Resource;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.utils.AppConstants;
import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.services.persons.ColonyService;
import com.pfv.admin.data.model.animals.Colony;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiUris.API_COLONIES_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ColonyResource extends Resource<ColonyDto, Colony, ColonyService> {
    @GetMapping
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public Page<ColonyDto> getListWithPaging(final @RequestParam("page") Integer page, final @RequestParam("size") Integer size,
            final @RequestParam(name = "sortField", required = false) String sortField,
            final @RequestParam(name = "sortOrder", required = false) Integer sortOrder,
            final @RequestParam(value = "filters", required = false) @SearchFields List<SearchCriteria> filters) {
        return getAllWithFiltersAndPaging(page, size, sortField, sortOrder, filters);
    }

    @GetMapping("{id}")
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public ResponseEntity<ColonyDto> getById(final @PathVariable String id) {
        return genericGetById(id);
    }

    @PostMapping
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public ResponseEntity<ColonyDto> create(final @RequestBody @Valid ColonyDto dto,
            final BindingResult result) {
        return genericCreate(dto, result);
    }

    @PutMapping("{id}")
    @PreAuthorize(value = AppConstants.AUTHORIZE_USER)
    public ResponseEntity<ColonyDto> update(final @PathVariable("id") String id,
            final @RequestBody @Valid ColonyDto dto,
            final BindingResult result) {
        return genericUpdate(id, dto, result);
    }

    @DeleteMapping("{id}")
    @PreAuthorize(value = AppConstants.AUTHORIZE_ADMIN)
    public ResponseEntity<ColonyDto> delete(final @PathVariable("id") String id) {
        return genericDelete(id);
    }
}
