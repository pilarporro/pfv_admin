package com.pfv.admin.api.resources.municipalities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.resources.Resource;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.municipalities.MunicipalityDto;
import com.pfv.admin.core.services.municipalities.MunicipalityService;
import com.pfv.admin.data.model.municipalities.Municipality;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(value = ApiUris.API_MUNICIPALITIES_URI, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
// @CrossOrigin(origins =   {"https://www.pfvsecretaria.com", "https://www.pfvsecretaria.com/"})
public class MunicipalityResource extends Resource<MunicipalityDto, Municipality, MunicipalityService> {
    @GetMapping
    public List<AliasIdLabelDto> getMunicipalities(final @RequestParam("name") String name) {
        return service.getMunipalities(name);
    }

    @GetMapping("{id}/areas")
    public List<AliasIdLabelDto> getAreas(final @PathVariable(value = "id") String municipalityId, final @RequestParam("name") String name) {
        return service.getAreas(municipalityId, name);
    }

    @GetMapping("name-minimal/{name}")
    public AliasIdLabelDto getByNameMinimal(final @PathVariable String name) {
        return service.findMinimalByName(name);
    }

    @GetMapping("{id}/{areaId}/minimal")
    public AliasIdLabelDto getAreaByIdMinimal(final @PathVariable String id, final @PathVariable String areaId) {
        return service.findAreaMinimalById(areaId);
    }

    /*
    @GetMapping("default")
    public ResponseEntity<MunicipalityDto> getDefaultMunicipality() throws URISyntaxException {
        log.info("Requesting municipality default");
        MunicipalityDto dto = service.getDefaultMunicipality();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI("https://www.pfvapi.com/api/municipalities/default"));
        return new ResponseEntity<MunicipalityDto>(dto, headers, HttpStatus.OK);
    }
*/

    @GetMapping("default")
    public MunicipalityDto getDefaultMunicipality() throws URISyntaxException {
        log.info("Requesting municipality default");
        return service.getDefaultMunicipality();
    }
}
