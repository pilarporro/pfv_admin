/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pfv.admin.api.resources.auth;

import com.pfv.admin.api.configuration.jwt.JwtProvider;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.common.AuthResponseDto;
import com.pfv.admin.core.model.common.CredentialsDto;
import com.pfv.admin.core.model.persons.UserDto;
import com.pfv.admin.core.services.persons.UserService;
import com.pfv.admin.data.repositories.persons.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(ApiUris.API_BASE + "/auth")
public class AuthResource {
    @Autowired
    UserService userService;

    @Autowired(required = false) // We dont have it in test
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/signin")
    public ResponseEntity<AuthResponseDto> authenticateUser(@Valid @RequestBody CredentialsDto loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        return authenticateUser(authentication);
    }

    private ResponseEntity<AuthResponseDto> authenticateUser(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return ResponseEntity.ok(AuthResponseDto.builder()
                .token( jwtProvider.generateJwtToken(authentication))
                .user(userService.findByUsername(((UserDetails) authentication.getPrincipal()).getUsername())).build());
    }

    @GetMapping("/remember-me")
    public ResponseEntity<AuthResponseDto> rememberMe(final @RequestParam("key") String key) {
        Optional<UserDto> opClient = userService.findById(key);
        if (opClient.isPresent()) {
            return new ResponseEntity(
                    authenticateUser(AuthenticationImpl.of(userService.loadUserByUsername(opClient.get().getUsername()))), HttpStatus.OK);
        } else {
            return ResponseEntity.noContent().build();
        }
    }
}
