package com.pfv.admin.api.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

// @Configuration
@Slf4j
// @Profile({ AppConstants.APP_PROFILE_DEV })
public class CorsConfigDev extends WebMvcConfigurerAdapter {

    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        registry.addMapping("/**");
    }
}
