package com.pfv.admin.api.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

 @Configuration
@Slf4j
// @Profile({ AppConstants.APP_PROFILE_INTEGRATION, AppConstants.APP_PROFILE_PRODUCTION })
public class CorsConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        log.info("Add Cors Confi Production Environment");
        // registry.addMapping("/**");
        registry.addMapping("/**")
                // .allowedOrigins("https://www.pfvsecretaria.com", "https://www.pfvapi.com", "https://www.pfvapi.com:8440", "https://194.53.148.32", "http://localhost:4200")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                .allowedHeaders("*")
                .exposedHeaders()
                .allowedMethods("*");
    }
}
