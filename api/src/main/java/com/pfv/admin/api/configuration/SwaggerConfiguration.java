package com.pfv.admin.api.configuration;

import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.common.utils.AppConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@Profile({ AppConstants.APP_PROFILE_DEV })
@Slf4j
public class SwaggerConfiguration {

    @Bean
    public Docket productApi() {
        final ParameterBuilder languageParameterBuilder = new ParameterBuilder();
        languageParameterBuilder.name("Accept-Language").defaultValue("es").modelRef(new ModelRef("string")).parameterType("header").required(true).build();
        final List<Parameter> parameters = new ArrayList();
        parameters.add(languageParameterBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("admin")
                .apiInfo(apiInfo())
                .select()
                .paths(petstorePaths())
                .build().globalOperationParameters(parameters);

    }

    private Predicate<String> petstorePaths() {
        return regex(ApiUris.API_BASE + "/.*");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Protección Felina Valdemorillo Admin API")
                .build();
    }

}
