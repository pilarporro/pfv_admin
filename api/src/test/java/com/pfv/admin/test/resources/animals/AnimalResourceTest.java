package com.pfv.admin.test.resources.animals;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.test.RestIntegrationTest;
import com.pfv.admin.test.resources.persons.PersonTestData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AnimalResourceTest extends RestIntegrationTest {
    @Autowired
    private PersonService personService;

    @Override
    protected String getApiBase() {
        return ApiUris.API_ANIMALS_URI;
    }

    @Test
    @DisplayName("Test create a animal")
    public void testCreateAnimal() {
        createAnimal();
    }

    private CatDto createAnimal() {
        PersonDto adopter = personService.create(PersonTestData.personDto());

        CatDto toCreate = AnimalTestData.catDto("CatName", adopter.getId(), adopter.getColonies().get(0).getId());
        CatDto found = create(toCreate, CatDto.class);

        assertAll("Cat created",
                () -> assertEquals(toCreate.getName(), found.getName()),
                () -> assertEquals(toCreate.getAnimalStatus(), found.getAnimalStatus()),
                () -> assertEquals(toCreate.getAdopter().getId(), found.getAdopter().getId()),
                () -> assertEquals(toCreate.getColonyId(), found.getColonyId()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    @Test
    @DisplayName("Test update a animal")
    public void testUpdateAAnimal() {
        AnimalDto toUpdate = createAnimal();

        toUpdate.setName("NewName");

        AnimalDto found = update(toUpdate, AnimalDto.class);

        assertAll("Animal update",
                () -> assertEquals(found.getName(), toUpdate.getName())
        );
    }

    @Test
    @DisplayName("Test find by alias")
    public void testFindByAlias() {
        PersonDto owner = personService.create(PersonTestData.personDto());
        PersonDto adopter = personService.create(PersonTestData.personDto());

        create(AnimalTestData.catDto("ABC", adopter.getId(), owner.getColonies().get(0).getId()), AnimalDto.class);
        create(AnimalTestData.catDto("AC", adopter.getId(), owner.getColonies().get(0).getId()), AnimalDto.class);
        create(AnimalTestData.catDto("AB", adopter.getId(), owner.getColonies().get(0).getId()), AnimalDto.class);

        AliasIdLabelDto[] result =  givenBaseRequest()
                .queryParam("alias", "AB")
                .get(String.format("%s/by-alias-starts-with", getApiBase()))
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().as(AliasIdLabelDto[].class);

        assertEquals(2, result.length);
        assertEquals("ABC", result[0].getAlias());
        assertEquals("AB", result[1].getAlias());
    }

}
