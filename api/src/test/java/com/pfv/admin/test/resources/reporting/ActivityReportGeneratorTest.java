package com.pfv.admin.test.resources.reporting;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.dto.PhotoDto;
import com.pfv.admin.common.model.enums.Gender;
import com.pfv.admin.common.services.StorageServiceImpl;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.CastrationDto;
import com.pfv.admin.core.model.animals.AnimalDto;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.core.services.animals.AnimalServiceImpl;
import com.pfv.admin.core.services.reporting.ActivityReportGeneratorImpl;
import com.pfv.admin.core.services.reporting.ReportUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extensions;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;

@Extensions({@ExtendWith({ MockitoExtension.class})})
public class ActivityReportGeneratorTest {

    private ActivityReportGeneratorImpl generator;



    @Test
    public void test1() throws Exception {
        String storagePath = "C:/Apps/apache-tomcat-9.0.20/webapps/ROOT/pfv";

        generator = new ActivityReportGeneratorImpl();

        AnimalServiceImpl animalService = Mockito.mock(AnimalServiceImpl.class);

        ReflectionTestUtils.setField(generator, "storagePath", storagePath);
        ReflectionTestUtils.setField(generator, "animalService", animalService);

        List<ActivityDto> castrations = new LinkedList<>();

        doReturn(Optional.of(createCat(Gender.FEMALE, null))).when(animalService).findById("1");
        doReturn(Optional.empty()).when(animalService).findById("2");
        doReturn(Optional.of(createCat(Gender.MALE, null))).when(animalService).findById("3");
        doReturn(Optional.of(createCat(Gender.MALE, "Blacky"))).when(animalService).findById("4");

        castrations.add(createCastration("1", "A","bzEwz1616945540524.jpg"));
        castrations.add(createCastration("2","B","hPufo1616945855804.jpg"));
        castrations.add(createCastration("1","A","KY5Of1616946225467.jpg"));
        castrations.add(createCastration("3","A","bzEwz1616945540524.jpg"));
        castrations.add(createCastration("3","B","rV6uq1616945944635.jpg"));
        castrations.add(createCastration("4","C","56OXO1616945752674.jpg"));
        castrations.add(createCastration("4","C","wTO0X1616946197480.jpg"));

        List<ActivityDto> rescues = new LinkedList<>();

        Map<String, String> vetClinics = new HashMap<>();
        vetClinics.put("A", "Colmillos y Garras");
        vetClinics.put("B", "Vetsos");
        vetClinics.put("C", "Bluevet");

        byte[] result = generator.generate(
                vetClinics, castrations, rescues,new LinkedList<>(),
                ReportUtils.LONG_DATE_FORMATTER.format(LocalDate.now()), ReportUtils.LONG_DATE_FORMATTER.format(LocalDate.now().minusMonths(2)), "1", "2", "3", "4", "5",
                "6");
        Path path = Paths.get("C:/Dev/tmp/resultado.docx");
        Files.write(path, result);
    }

    private AnimalDto createCat(Gender gender, String name) {
        CatDto cat = new CatDto();
        cat.setGender(gender);
        cat.setName(name);
        return cat;
    }

    private CastrationDto createCastration(String animalId, String vetClinic, String diskName) {
        CastrationDto castrationDto1 = CastrationDto.builder()
                .animal(AliasIdLabelDto.ofId(animalId))
                .vetClinic(AliasIdLabelDto.ofId(vetClinic))
                .build();
        castrationDto1.setDate(LocalDate.of(2021, 1, 1));
        castrationDto1.setPhotos(Arrays.asList(
                PhotoDto.builder()
                        .diskName(diskName)
                        .folder("2021/03/28")
                        .build()
        ));

        return castrationDto1;
    }

    @Test
    public void testStorage() throws IOException {
        StorageServiceImpl storageService = new StorageServiceImpl();
        storageService.resize("jpg", "C:/Dev/cosas/kk/yy.jpg");

    }

}
