package com.pfv.admin.test.resources.places;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.places.ShopDto;
import com.pfv.admin.core.model.places.VetClinicDto;
import com.pfv.admin.test.RestIntegrationTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlaceResourceTest extends RestIntegrationTest {

    @Override
    protected String getApiBase() {
        return ApiUris.API_PLACES_URI;
    }

    @Test
    @DisplayName("Test create a place")
    public void testCreatePlace() {
        createPlace();

        ShopDto toCreate = PlaceTestData.shopDto("Shop");
        ShopDto found = create(toCreate, ShopDto.class);

        assertAll("Shop created",
                () -> assertEquals(toCreate.getName(), found.getName()),
                () -> assertEquals(toCreate.getLocation(), found.getLocation()),
                () -> assertEquals(toCreate.getAlias(), found.getAlias()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );
    }

    private VetClinicDto createPlace() {
        VetClinicDto toCreate = PlaceTestData.vetClinicDto("Clinic");
        VetClinicDto found = create(toCreate, VetClinicDto.class);

        assertAll("Vet clinic created",
                () -> assertEquals(toCreate.getName(), found.getName()),
                () -> assertEquals(toCreate.getLocation(), found.getLocation()),
                () -> assertEquals(toCreate.getAlias(), found.getAlias()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    @Test
    @DisplayName("Test update a place")
    public void testUpdateAPlace() {
        VetClinicDto toUpdate = createPlace();

        toUpdate.setName("NewName");

        VetClinicDto found = update(toUpdate, VetClinicDto.class);

        assertAll("Place update",
                () -> assertEquals(found.getName(), toUpdate.getName())
        );
    }

    @Test
    @DisplayName("Test find by alias")
    public void testFindByAlias() {
        create(PlaceTestData.vetClinicDto("ABC"), VetClinicDto.class);
        create(PlaceTestData.vetClinicDto("AC"), VetClinicDto.class);
        create(PlaceTestData.vetClinicDto("AB"), VetClinicDto.class);

        AliasIdLabelDto[] result =  givenBaseRequest()
                .queryParam("alias", "AB")
                .get(String.format("%s/by-alias-starts-with", getApiBase()))
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().as(AliasIdLabelDto[].class);

        assertEquals(2, result.length);
        assertEquals("ABC", result[0].getAlias());
        assertEquals("AB", result[1].getAlias());
    }

}
