package com.pfv.admin.test.resources.places;

import com.pfv.admin.core.model.common.LocationDto;
import com.pfv.admin.core.model.places.ShopDto;
import com.pfv.admin.core.model.places.VetClinicDto;
import com.pfv.admin.test.TestData;

public final class PlaceTestData extends TestData {
    public static VetClinicDto vetClinicDto(String alias) {
        VetClinicDto vetClinicDto = new VetClinicDto();
        vetClinicDto.setName(getRandomString(10));
        vetClinicDto.setLocation(LocationDto.builder()
                .street(getRandomString(10))
                .postalCode(getRandomString(5))
                .build());
        vetClinicDto.setAlias(alias);
        return vetClinicDto;
    }

    public static ShopDto shopDto(String alias) {
        ShopDto shopDto = new ShopDto();
        shopDto.setName(getRandomString(10));
        shopDto.setLocation(LocationDto.builder()
                .street(getRandomString(10))
                .postalCode(getRandomString(5))
                .build());
        shopDto.setAlias(alias);
        return shopDto;
    }
}
