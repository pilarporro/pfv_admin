package com.pfv.admin.test.resources.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.model.activities.BuySuppliesDto;
import com.pfv.admin.core.model.activities.CastrationDto;
import com.pfv.admin.core.model.activities.MeetingDto;
import com.pfv.admin.core.model.activities.NewMemberDto;
import com.pfv.admin.core.model.activities.OtherActivityDto;
import com.pfv.admin.core.model.activities.PickUpKeeperDto;
import com.pfv.admin.core.model.activities.RescueDto;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.model.places.ShopDto;
import com.pfv.admin.core.model.places.VetClinicDto;
import com.pfv.admin.core.services.animals.AnimalService;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.core.services.places.PlaceService;
import com.pfv.admin.test.RestIntegrationTest;
import com.pfv.admin.test.resources.animals.AnimalTestData;
import com.pfv.admin.test.resources.persons.PersonTestData;
import com.pfv.admin.test.resources.places.PlaceTestData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ActivityResourceTest extends RestIntegrationTest {
    @Autowired
    private PersonService personService;

    @Autowired
    private AnimalService animalService;

    @Autowired
    private PlaceService placeService;

    @Override
    protected String getApiBase() {
        return ApiUris.API_ACTIVITIES_URI;
    }

    @Test
    @DisplayName("Test create activities")
    public void testCreateActivity() {
        createRescue();
        createNewMember();
        createPickUpKeeper();
        createCastration();
        createOtherActivity();
        createAdoption();
        createMeeting();
        createBuySupplies();
    }

    private RescueDto createRescue() {
        PersonDto person = personService.create(PersonTestData.personDto());
        PersonDto caller = personService.create(PersonTestData.personDto());
        CatDto cat1 = (CatDto) animalService.create(AnimalTestData.catDto("Molly", person.getId(), person.getColonies().get(0).getId()));
        CatDto cat2 = (CatDto) animalService.create(AnimalTestData.catDto("Jenny", person.getId(), person.getColonies().get(0).getId()));
        CatDto cat3 = (CatDto) animalService.create(AnimalTestData.catDto("Fierecilla", person.getId(), person.getColonies().get(0).getId()));

        RescueDto toCreate = ActivityTestData.rescueDto(person.getId(), caller.getId(), cat1.getId(), cat2.getId(), cat3.getId());
        RescueDto found = create(toCreate, RescueDto.class);

        assertAll("PickUpKeeperDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getCaller(), found.getCaller()),
                () -> assertEquals(toCreate.getAnimals().get(0), toCreate.getAnimals().get(0)),
                () -> assertEquals(toCreate.getAnimals().get(1), toCreate.getAnimals().get(1)),
                () -> assertEquals(toCreate.getAnimals().get(2), toCreate.getAnimals().get(2)),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    private NewMemberDto createNewMember() {
        PersonDto person = personService.create(PersonTestData.personDto());
        PersonDto newMember = personService.create(PersonTestData.personDto());

        NewMemberDto toCreate = ActivityTestData.newMember(person.getId(), newMember.getId());
        NewMemberDto found = create(toCreate, NewMemberDto.class);

        assertAll("NewMemberDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getNewMember(), found.getNewMember()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    private PickUpKeeperDto createPickUpKeeper() {
        PersonDto person = personService.create(PersonTestData.personDto());
        PersonDto keeper = personService.create(PersonTestData.personDto());
        CatDto cat = (CatDto) animalService.create(AnimalTestData.catDto("Molly", person.getId(), person.getColonies().get(0).getId()));

        PickUpKeeperDto toCreate = ActivityTestData.pickUpKeeperDto(person.getId(), keeper.getId(), cat.getId());
        PickUpKeeperDto found = create(toCreate, PickUpKeeperDto.class);

        assertAll("PickUpKeeperDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getKeeper(), found.getKeeper()),
                () -> assertEquals(toCreate.getAnimals().get(0), toCreate.getAnimals().get(0)),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    private CastrationDto createCastration() {
        PersonDto person = personService.create(PersonTestData.personDto());
        VetClinicDto vetClinicDto = (VetClinicDto) placeService.create(PlaceTestData.vetClinicDto("COLMILLOS"));
        CatDto cat = (CatDto) animalService.create(AnimalTestData.catDto("Molly", person.getId(), person.getColonies().get(0).getId()));

        CastrationDto toCreate = ActivityTestData.castrationDto(person.getId(), vetClinicDto.getId(), cat.getId());
        CastrationDto found = create(toCreate, CastrationDto.class);

        assertAll("CastrationDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getAnimal(), found.getAnimal()),
                () -> assertEquals(toCreate.getVetClinic(), found.getVetClinic()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    private MeetingDto createMeeting() {
        PersonDto person = personService.create(PersonTestData.personDto());
        PersonDto person2 = personService.create(PersonTestData.personDto());


        MeetingDto toCreate = ActivityTestData.meetingDto(person.getId(), person2.getId());
        MeetingDto found = create(toCreate, MeetingDto.class);

        assertAll("MeetingDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getPersons().get(0), found.getPersons().get(0)),
                () -> assertEquals(toCreate.getPersons().get(1), found.getPersons().get(1)),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }


    private BuySuppliesDto createBuySupplies() {
        PersonDto person = personService.create(PersonTestData.personDto());
        ShopDto shopDto = (ShopDto) placeService.create(PlaceTestData.shopDto("TIENDA"));

        BuySuppliesDto toCreate = ActivityTestData.buySuppliesDto(person.getId(), shopDto.getId());
        BuySuppliesDto found = create(toCreate, BuySuppliesDto.class);

        assertAll("BuySuppliesDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getPlace().getId(), found.getPlace().getId()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    private AdoptionDto createAdoption() {
        PersonDto person = personService.create(PersonTestData.personDto());
        PersonDto owner = personService.create(PersonTestData.personDto());
        PersonDto adopter = personService.create(PersonTestData.personDto());
        CatDto cat = (CatDto) animalService.create(AnimalTestData.catDto("Molly", adopter.getId(), owner.getColonies().get(0).getId()));

        AdoptionDto toCreate = ActivityTestData.adoptionDto(person.getId(), adopter.getId(), cat.getId());
        AdoptionDto found = create(toCreate, AdoptionDto.class);

        assertAll("AdoptionDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertEquals(toCreate.getAdopter().getId(), found.getAdopter().getId()),
                () -> assertEquals(toCreate.getAnimal().getId(), found.getAnimal().getId()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    private OtherActivityDto createOtherActivity() {
        PersonDto person = personService.create(PersonTestData.personDto());

        OtherActivityDto toCreate = ActivityTestData.otherActivityDto(person.getId());
        OtherActivityDto found = create(toCreate, OtherActivityDto.class);

        assertAll("OtherActivityDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson().getId(), found.getPerson().getId()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    @Test
    @DisplayName("Test update a activity")
    public void testUpdateActivity() {
        PersonDto otherPerson = personService.create(PersonTestData.personDto());
        OtherActivityDto toUpdate = createOtherActivity();

        toUpdate.setNotes("NewNotes");
        toUpdate.setPerson(AliasIdLabelDto.ofId(otherPerson.getId()));

        OtherActivityDto found = update(toUpdate, OtherActivityDto.class);

        assertAll("Activity update",
                () -> assertEquals(found.getNotes(), toUpdate.getNotes()),
               () -> assertEquals(found.getPerson().getId(), toUpdate.getPerson().getId())
        );
    }


}
