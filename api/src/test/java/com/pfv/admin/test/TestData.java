package com.pfv.admin.test;

import net.bytebuddy.utility.RandomString;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public abstract class TestData {

    public static String getRandomString(final int length) {
        return RandomString.make(length);
    }

    public static LocalDate getRandomDate() {
        long minDay = LocalDate.of(1900, 1, 1).toEpochDay();
        long maxDay = LocalDate.of(2020, 12, 31).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    public static Boolean getRandomBoolean() {
        return ThreadLocalRandom.current().nextBoolean();
    }

    public static <ENUM extends Enum<ENUM>> ENUM getRandomEnum(Class<ENUM> enumClass) {
        return enumClass.getEnumConstants()[getRandomNumber(0, enumClass.getEnumConstants().length)];
    }

    public static Integer getRandomNumber() {
        return ThreadLocalRandom.current().nextInt(1, 15);
    }

    public static Integer getRandomNumber(final Integer from, final Integer to) {
        return ThreadLocalRandom.current().nextInt(from, to);
    }
}
