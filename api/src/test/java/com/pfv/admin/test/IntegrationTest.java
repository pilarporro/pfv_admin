package com.pfv.admin.test;

import com.pfv.admin.api.ApiApplication;
import com.pfv.admin.core.services.persons.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Locale;

// @ExtendWith(MockitoExtension.class)
// @DataMongoTest
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = { ApiApplication.class })
@ActiveProfiles({"test" })
@AutoConfigureDataMongo
public abstract class IntegrationTest  {
    public static final String AUTHENTICATED_USERNAME = "usertest";

    @Autowired
    protected MongoTemplate mongoTemplate;

    @Autowired
    protected UserServiceImpl userService;

    @BeforeAll
    public static void setup() {
        LocaleContextHolder.setLocale(new Locale("es", "es", ""));
    }
}
