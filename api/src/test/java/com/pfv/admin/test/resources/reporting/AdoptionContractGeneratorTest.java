package com.pfv.admin.test.resources.reporting;

import com.pfv.admin.common.services.StorageServiceImpl;
import com.pfv.admin.core.services.reporting.AdoptionContractGeneratorImpl;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import static com.pfv.admin.core.services.reporting.ReportUtils.LONG_DATE_FORMATTER;

public class AdoptionContractGeneratorTest {

    private AdoptionContractGeneratorImpl generator;


    @Test
    public void test1() throws IOException, InvalidFormatException {
        generator = new AdoptionContractGeneratorImpl();

        byte[] result = generator.generate(LONG_DATE_FORMATTER.format(LocalDate.now()), "Pepe", "María", "Juan");
        Path path = Paths.get("C:/Dev/cosas/tmp/resultado.docx");
        Files.write(path, result);
    }

    @Test
    public void testStorage() throws IOException {
        StorageServiceImpl storageService = new StorageServiceImpl();
        storageService.resize("jpg", "C:/Dev/cosas/kk/yy.jpg");

    }



}
