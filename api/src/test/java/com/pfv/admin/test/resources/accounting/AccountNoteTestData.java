package com.pfv.admin.test.resources.accounting;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.accounting.AccountNoteDto;
import com.pfv.admin.test.TestData;

import java.util.Arrays;
import java.util.LinkedList;

public final class AccountNoteTestData extends TestData {

    public static AccountNoteDto accountNoteDto(String personId, String vetClinicId, String...activityIds) {
        AccountNoteDto accountNoteDto = new AccountNoteDto();
        accountNoteDto.setPerson(AliasIdLabelDto.ofId(personId));
        accountNoteDto.setPlace(AliasIdLabelDto.ofId(vetClinicId));

        accountNoteDto.setActivities(new LinkedList<>());
        Arrays.stream(activityIds).forEach(activityId -> {
            accountNoteDto.getActivities().add(AliasIdLabelDto.ofId(activityId));
        });
        return accountNoteDto;
    }


}
