package com.pfv.admin.test.resources.persons;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.test.RestIntegrationTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PersonResourceTest extends RestIntegrationTest {

    @Override
    protected String getApiBase() {
        return ApiUris.API_PERSONS_URI;
    }

    @Test
    @DisplayName("Test create a person")
    public void testCreatePerson() {
        createPerson();
    }

    private PersonDto createPerson() {
        PersonDto toCreate = PersonTestData.personDto();
        PersonDto found = create(toCreate, PersonDto.class);

        assertAll("Person created",
                () -> assertEquals(toCreate.getName(), found.getName()),
                () -> assertEquals(toCreate.getLastName(), found.getLastName()),
                () -> assertEquals(toCreate.getAlias(), found.getAlias()),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId()),
                () -> assertNotNull(found.getColonies()),
                () -> assertEquals(1, found.getColonies().size()),
                // () -> assertEquals(AppConstants.DEFAULT_COLONY, found.getColonies().get(0).getName()),
                () -> assertNotNull(found.getColonies().get(0).getId())
        );

        return found;
    }

    @Test
    @DisplayName("Test update a person")
    public void testUpdateAPerson() {
        PersonDto toUpdate = createPerson();

        toUpdate.setName("NewName");
        // toUpdate.getColonies().get(0).setName("NewColony");
        // toUpdate.getColonies().add(PersonTestData.colonyDtoBuilder().build());
        // toUpdate.getColonies().add(PersonTestData.colonyDtoBuilder().build());

        PersonDto found = update(toUpdate, PersonDto.class);

        assertAll("Person update",
                () -> assertEquals(found.getName(), toUpdate.getName()),
                () -> assertEquals(found.getLastName(), toUpdate.getLastName()),
                () -> assertNotNull(found.getColonies()),
                () -> assertEquals(3, found.getColonies().size(), "The number of colonies is not equal"),
                // () -> assertEquals(toUpdate.getColonies().get(0).getName(), found.getColonies().get(0).getName()),
                () -> assertNotNull(found.getColonies().get(0).getId()),
                // () -> assertEquals(toUpdate.getColonies().get(1).getName(), found.getColonies().get(1).getName()),
                () -> assertNotNull(found.getColonies().get(1).getId())
        );
    }

    @Test
    @DisplayName("Test find by alias")
    public void testFindByAlias() {
        create(PersonTestData.personDto("ABC"), PersonDto.class);
        create(PersonTestData.personDto("AC"), PersonDto.class);
        create(PersonTestData.personDto("AB"), PersonDto.class);

        AliasIdLabelDto[] result =  givenBaseRequest()
                .queryParam("alias", "AB")
                .get(String.format("%s/by-alias-starts-with", getApiBase()))
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().as(AliasIdLabelDto[].class);

        assertEquals(2, result.length);
        assertEquals("ABC", result[0].getAlias());
        assertEquals("AB", result[1].getAlias());
    }

}
