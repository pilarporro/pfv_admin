package com.pfv.admin.test.resources.activities;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.core.model.activities.ActivityDto;
import com.pfv.admin.core.model.activities.AdoptionDto;
import com.pfv.admin.core.model.activities.BuySuppliesDto;
import com.pfv.admin.core.model.activities.CastrationDto;
import com.pfv.admin.core.model.activities.MeetingDto;
import com.pfv.admin.core.model.activities.NewMemberDto;
import com.pfv.admin.core.model.activities.OtherActivityDto;
import com.pfv.admin.core.model.activities.PickUpKeeperDto;
import com.pfv.admin.core.model.activities.RescueDto;
import com.pfv.admin.test.TestData;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

public final class ActivityTestData extends TestData {
    public static PickUpKeeperDto pickUpKeeperDto(String personId, String keeperId, String animalId) {
        PickUpKeeperDto pickUpKeeperDto = new PickUpKeeperDto();
        pickUpKeeperDto.setKeeper(AliasIdLabelDto.ofId(keeperId));
        pickUpKeeperDto.setAnimals(Arrays.asList(AliasIdLabelDto.ofId(animalId)));
        populateCommonData(pickUpKeeperDto, personId);
        return pickUpKeeperDto;
    }

    public static NewMemberDto newMember(String personId, String newMemberId) {
        NewMemberDto newMember = new NewMemberDto();
        newMember.setNewMember(AliasIdLabelDto.ofId(newMemberId));
        populateCommonData(newMember, personId);
        return newMember;
    }

    public static RescueDto rescueDto(String personId, String callerId, String...animalIds) {
        RescueDto rescueDto = new RescueDto();
        rescueDto.setCaller(AliasIdLabelDto.ofId(callerId));
        rescueDto.setAnimals(new LinkedList<>());
        Arrays.stream(animalIds).forEach(animalId -> {
            rescueDto.getAnimals().add(AliasIdLabelDto.ofId(animalId));
        });
        populateCommonData(rescueDto, personId);
        return rescueDto;
    }


    public static OtherActivityDto otherActivityDto(String personId) {
        OtherActivityDto otherActivityDto = new OtherActivityDto();
        populateCommonData(otherActivityDto, personId);
        return otherActivityDto;
    }

    private static void populateCommonData(ActivityDto activityDto, String personId) {
        AliasIdLabelDto aliasIdLabelDto = new AliasIdLabelDto();
        aliasIdLabelDto.setId(personId);
        activityDto.setPerson(aliasIdLabelDto);
        activityDto.setDate(LocalDate.now());
        activityDto.setNotes(getRandomString(10));
        activityDto.setTags(Arrays.asList("T1", "T2"));
    }

    public static AdoptionDto adoptionDto(String personId, String adopterId, String animalId) {
        AdoptionDto adoptionDto = new AdoptionDto();
        adoptionDto.setAdopter(AliasIdLabelDto.ofId(adopterId));
        adoptionDto.setAnimal(AliasIdLabelDto.ofId((animalId)));
        populateCommonData(adoptionDto, personId);
        return adoptionDto;
    }

    public static BuySuppliesDto buySuppliesDto(String personId, String shopId) {
        BuySuppliesDto buySuppliesDto = new BuySuppliesDto();
        buySuppliesDto.setPlace(AliasIdLabelDto.ofId(shopId));
        populateCommonData(buySuppliesDto, personId);
        return buySuppliesDto;
    }

    public static CastrationDto castrationDto(String personId, String vetCliniId, String animalId) {
        CastrationDto castrationDto = new CastrationDto();
        castrationDto.setVetClinic(AliasIdLabelDto.ofId(vetCliniId));
        castrationDto.setAnimal(AliasIdLabelDto.ofId(animalId));
        populateCommonData(castrationDto, personId);
        return castrationDto;
    }

    public static MeetingDto meetingDto(String personId, String personId2) {
        MeetingDto meetingDto = new MeetingDto();
        meetingDto.setPersons(Arrays.asList(AliasIdLabelDto.ofId(personId), AliasIdLabelDto.ofId(personId2)));
        populateCommonData(meetingDto, personId);
        return meetingDto;
    }
}
