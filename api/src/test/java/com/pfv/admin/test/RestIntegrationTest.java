package com.pfv.admin.test;

import com.pfv.admin.common.model.dto.EntityOperationsInfoDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.common.AuthResponseDto;
import com.pfv.admin.core.model.common.CredentialsDto;
import com.pfv.admin.core.model.persons.UserDto;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public abstract class RestIntegrationTest extends IntegrationTest {
    protected static final Header HEADER_LANGUAGE = new Header("Accept-Language", "es");
    protected static final String CONTENT_TYPE = "application/json";
    protected static final Header HEADER_CONTENT_TYPE = new Header("Content-Type", CONTENT_TYPE);
    private Header headerAuthentication;

    protected Map<String, Long> emptyPathParameters = this.getBaseUrlPathParameters();

    @LocalServerPort
    protected int serverPort;

    @Value("${server.servlet.context-path:}")
    private String contextPath;

    @BeforeEach
    public void setupResource() {
        RestAssured.port = serverPort;
        RestAssured.basePath = contextPath;
        RestAssured.defaultParser = Parser.JSON;
        this.doTestLogin();
    }

    protected abstract String getApiBase();

    protected Map<String, Long> getBaseUrlPathParameters() {
        return new HashMap<>();
    }

    protected RequestSpecification givenBaseRequest() {
        return given()
                .when()
                .header(HEADER_LANGUAGE)
                .header(HEADER_CONTENT_TYPE)
                .header(headerAuthentication)
                .contentType(CONTENT_TYPE);
    }

    protected void doTestLogin() {
        UserDto user = userService.findByUsername(AUTHENTICATED_USERNAME);
        if (user == null) {
            userService.addAdmin(AUTHENTICATED_USERNAME, "Pilar", "Porro", "kokoko", "pilar.porro@gmail.com", "nif");
        }

        requestLoging();
        /*
        UserDetails userDetails = userService.loadUserByUsername(AUTHENTICATED_USERNAME);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, (Object) null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
         */
    }

    private void requestLoging() {
        CredentialsDto loginRequest = new CredentialsDto();
        loginRequest.setPassword("kokoko");
        loginRequest.setUsername(AUTHENTICATED_USERNAME);

        AuthResponseDto response = given()
                .when()
                .header(HEADER_LANGUAGE)
                .header(HEADER_CONTENT_TYPE)
                .contentType(CONTENT_TYPE)
                .body(loginRequest)
                .post(ApiUris.API_BASE + "/auth/signin")
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(AuthResponseDto.class);

        headerAuthentication = new Header("Authorization", "Bearer " + response.getToken());
    }

    protected <DTO> DTO findById(String id, final Class<DTO> extractAs) {
        return givenBaseRequest()
                .get(String.format("%s/%s", getApiBase(), id))
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().as(extractAs);
    }

    protected <DTO extends EntityOperationsInfoDto> DTO create(final DTO toCreate, final Class<DTO> extractAs) {
        DTO created = givenBaseRequest()
                .body(toCreate)
                .post(getApiBase())
                .then()
                .statusCode(HttpStatus.CREATED.value())
                .extract().as(extractAs);

        return findById(created.getId(), extractAs);

    }

    protected <DTO extends EntityOperationsInfoDto> DTO update(final DTO toUpdate, final Class<DTO> extractAs) {
        givenBaseRequest()
                .body(toUpdate)
                .put(String.format("%s/%s", getApiBase(), toUpdate.getId()))
                .then()
                .statusCode(HttpStatus.OK.value())
                .extract().as(extractAs);

        return findById(toUpdate.getId(), extractAs);

    }

}
