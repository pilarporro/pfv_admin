package com.pfv.admin.test.resources.accounting;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.utils.ApiUris;
import com.pfv.admin.core.model.accounting.AccountNoteDto;
import com.pfv.admin.core.model.activities.OtherActivityDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.core.model.places.VetClinicDto;
import com.pfv.admin.core.services.activities.ActivityService;
import com.pfv.admin.core.services.persons.PersonService;
import com.pfv.admin.core.services.places.PlaceService;
import com.pfv.admin.test.RestIntegrationTest;
import com.pfv.admin.test.resources.activities.ActivityTestData;
import com.pfv.admin.test.resources.persons.PersonTestData;
import com.pfv.admin.test.resources.places.PlaceTestData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountNoteResourceTest extends RestIntegrationTest {
    @Autowired
    private PersonService personService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private PlaceService placeService;

    @Override
    protected String getApiBase() {
        return ApiUris.API_ACCOUNT_NOTES_URI;
    }

    @Test
    @DisplayName("Test create an account note")
    public void testCreateAccountNote() {
        createAccountNote();
    }

    private AccountNoteDto createAccountNote() {
        PersonDto person = personService.create(PersonTestData.personDto());
        VetClinicDto vetClinicDto = (VetClinicDto) placeService.create(PlaceTestData.vetClinicDto("Colmillos"));
        OtherActivityDto other1 = (OtherActivityDto) activityService.create(ActivityTestData.otherActivityDto(person.getId()));
        OtherActivityDto other2 = (OtherActivityDto) activityService.create(ActivityTestData.otherActivityDto(person.getId()));
        OtherActivityDto other3 = (OtherActivityDto) activityService.create(ActivityTestData.otherActivityDto(person.getId()));

        AccountNoteDto toCreate = AccountNoteTestData.accountNoteDto(person.getId(), vetClinicDto.getId(), other1.getId(), other2.getId(), other3.getId());
        AccountNoteDto found = create(toCreate, AccountNoteDto.class);

        assertAll("AccountNoteDto created",
                () -> assertEquals(toCreate.getDate(), found.getDate()),
                () -> assertEquals(toCreate.getNotes(), found.getNotes()),
                () -> assertEquals(toCreate.getPerson(), found.getPerson()),
                () -> assertEquals(toCreate.getPlace(), found.getPlace()),
                () -> assertEquals(toCreate.getActivities().get(0), toCreate.getActivities().get(0)),
                () -> assertEquals(toCreate.getActivities().get(1), toCreate.getActivities().get(1)),
                () -> assertEquals(toCreate.getActivities().get(2), toCreate.getActivities().get(2)),
                () -> assertTrue(found.isActive()),
                () -> assertNotNull(found.getId())
        );

        return found;
    }

    @Test
    @DisplayName("Test update an account note")
    public void testUpdateAccountNote() {
        PersonDto otherPerson = personService.create(PersonTestData.personDto());
        AccountNoteDto toUpdate = createAccountNote();

        toUpdate.setNotes("NewNotes");
        toUpdate.setPerson(AliasIdLabelDto.ofId(otherPerson.getId()));

        AccountNoteDto found = update(toUpdate, AccountNoteDto.class);

        assertAll("AccountNote update",
                () -> assertEquals(found.getNotes(), toUpdate.getNotes()),
                () -> assertEquals(found.getPerson(), toUpdate.getPerson())
        );
    }


}
