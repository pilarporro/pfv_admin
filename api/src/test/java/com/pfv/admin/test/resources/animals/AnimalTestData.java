package com.pfv.admin.test.resources.animals;

import com.pfv.admin.common.model.dto.AliasIdLabelDto;
import com.pfv.admin.common.model.enums.AnimalStatus;
import com.pfv.admin.core.model.animals.CatDto;
import com.pfv.admin.test.TestData;

public final class AnimalTestData extends TestData {

    public static CatDto catDto(String name, String adopterId, String colonyId) {
        CatDto catDto = new CatDto();
        catDto.setName(name);
        catDto.setAlias(name);
        catDto.setAnimalStatus(getRandomEnum(AnimalStatus.class));
        catDto.setAdopter(AliasIdLabelDto.ofId(adopterId));
        catDto.setPickUpDate(getRandomDate());
        catDto.setColonyId(colonyId);
        return catDto;
    }
}
