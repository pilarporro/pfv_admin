package com.pfv.admin.test;

import com.pfv.admin.core.model.common.AuthResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AuthResponseEntity extends ResponseEntity<AuthResponseDto> {
    public AuthResponseEntity(HttpStatus status) {
        super(status);
    }
}
