package com.pfv.admin.test.resources.persons;

import com.pfv.admin.core.model.animals.ColonyDto;
import com.pfv.admin.core.model.persons.PersonDto;
import com.pfv.admin.test.TestData;

public final class PersonTestData extends TestData {
    public static ColonyDto.ColonyDtoBuilder colonyDtoBuilder() {
        return ColonyDto.builder()
                .notes(getRandomString(8))
                .name(getRandomString(8))
                .totalAnimals(getRandomNumber())
                ;
    }

    public static PersonDto.PersonDtoBuilder personDtoBuilder() {
        return PersonDto.builder()
                .bankAccount(getRandomString(15))
                .name(getRandomString(8))
                .lastName(getRandomString(50))
                .isMember(true)
                ;
    }

    public static PersonDto personDto() {
        PersonDto dto = personDtoBuilder().build();
        dto.setAlias(getRandomString(8));
        return dto;
    }

    public static PersonDto personDto(String alias) {
        PersonDto dto = personDtoBuilder().build();
        dto.setAlias(alias);
        return dto;
    }
}
